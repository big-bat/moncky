`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.03.2021 12:48:48
// Design Name: 
// Module Name: testInverter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testInverter();
    reg A = 0;
    wire B;
    
    localparam period = 9;  
    
    inverter UUT (
      .A (A),
      .B (B)
    );
    
    initial
    begin
      A = 0;
      #period
      if (B != 1) $display("error: B should be 1 when A is 0");
      
      A = 1;
      #period
      if (B != 0) $display("error: B should be 0 when A is 1");
      
      $stop;
    end
endmodule
