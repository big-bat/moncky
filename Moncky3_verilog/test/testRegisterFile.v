`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.04.2021 16:30:13
// Design Name: 
// Module Name: testRegisterFile
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testRegisterFile();
    reg clk;
    reg [15:0] in;
    reg [3:0] inputSelect;
    reg writeEnable;
    reg [1:0] spControl;
    reg [3:0] select0, select1, select2;
    wire [15:0] out0, out1, out2;
    wire [15:0] PC;
    
    registerFile registers (
     clk, in, inputSelect, writeEnable, spControl, select0, select1, select2, out0, out1, out2, PC
    );
    
    localparam period = 1;
    
    integer counter;
    reg [15:0] expected;
    
    initial
    begin
        $display("************** TEST REGISTER FILE STARTED **************");
        
        clk = 1;
        in = 16'hABCD;
        inputSelect = 0;
        writeEnable = 0;
        spControl = 0;
        select0 = 0;
        select1 = 14;
        select2 = 15;
        
        // fill registers with data
        writeEnable = 1;
        for(counter=0; counter <16; counter = counter + 1)
        begin
          #period
          in = counter*4096 + (15-counter)*256 + ((counter * 2) % 16) * 16 + (((counter * 2) + 1) % 16);
          inputSelect = counter;
          #period
          clk = 1;
          #period
          clk = 0;
        end
        
        // test out0
        writeEnable = 0;
        for(counter=0; counter < 16; counter = counter + 1)
        begin
          expected = counter*4096 + (15-counter)*256 + ((counter * 2) % 16) * 16 + (((counter * 2) + 1) % 16);
          if (counter == 15) expected = expected + 1;
          select0 = counter;
          #period
          if (out0 != expected) $display("Error: select0 for counter=%h", counter); 
        end
        
        // test out1
        writeEnable = 0;
        for(counter=0; counter < 16; counter = counter + 1)
        begin
          expected = counter*4096 + (15-counter)*256 + ((counter * 2) % 16) * 16 + (((counter * 2) + 1) % 16);
          if (counter == 15) expected = expected + 1;
          select1 = counter;
          #period
          if (out1 != expected) $display("Error: select0 for counter=%h", counter); 
        end
        
        // test out2
        writeEnable = 0;
        for(counter=0; counter < 16; counter = counter + 1)
        begin
          expected = counter*4096 + (15-counter)*256 + ((counter * 2) % 16) * 16 + (((counter * 2) + 1) % 16);
          if (counter == 15) expected = expected + 1;
          select2 = counter;
          #period
          if (out2 != expected) $display("Error: select0 for counter=%h", counter); 
        end
        
        if (PC != 16'hF0EF) $display("Error: PC");
        select0 = 15;
        spControl = 3;
        #period
        if (PC != 16'hF0EF) $display("Error: PC");
        if (out0 != 16'hF0EF) $display("Error: PC");
        spControl = 0;
        
        #period
        select0 = 15;
        clk = 1;
        #period
        clk = 0;
        #period
        if (PC != 16'hF0F0) $display("Error: PC");
        if (out0 != 16'hF0F1) $display("Error: PC");
        
        select0 = 14;
        #period
        expected = out0 + 1;
        spControl = 1;
        clk = 1;
        #period
        clk = 0;
        #period
        if (out0 != expected) $display("Error inc sp");
        
        expected = out0 - 1;
        spControl = 2;
        clk = 1;
        #period
        clk = 0;
        #period
        if (out0 != expected) $display("Error inc sp");
        
        expected = out0 - 1;
        spControl = 2;
        clk = 1;
        #period
        clk = 0;
        #period
        if (out0 != expected) $display("Error inc sp");
    end
    
endmodule
