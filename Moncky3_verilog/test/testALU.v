`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.04.2021 10:45:50
// Design Name: 
// Module Name: testALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testALU();
  reg [15:0] A;
  reg [15:0] B;
  reg [3:0] op;
  reg Cin;
  wire [15:0] F;
  wire zero;
  wire sign;
  wire Cout;
  wire overflow_parity;
  
  localparam period = 1;  
 
  ALU UUT (
    .A (A),
    .B (B),
    .op (op),
    .Cin (Cin),
    .F (F),
    .zero (zero),
    .sign (sign),
    .Cout (Cout),
    .overflow_parity (overflow_parity)
  );
  
  reg [15:0] expected;
  
  initial
  begin
  
    $display("************** TEST STARTED **************");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 0;
    Cin = 0;
    expected = 16'h0AC3;
    #period 
    if (F != expected) $display("Error F: op=0, carry=0");
    if (Cout != 0) $display("Error Cout: op=0, carry=0");
    if (zero != 0) $display("Error zero: op=0, carry=0");
    if (overflow_parity != 0) $display("Error parity: op=0, carry=0");
    if (sign != 0) $display("Error sign: op=0, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 0;
    Cin = 1;
    expected = 16'h0AC3; 
    #period
    if (F != expected) $display("Error F: op=0, carry=1");
    if (Cout != 0) $display("Error Cout: op=0, carry=1");
    if (zero != 0) $display("Error zero: op=0, carry=1");
    if (overflow_parity != 0) $display("Error parity: op=0, carry=1");
    if (sign != 0) $display("Error sign: op=0, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 1;
    Cin = 0;
    expected = 16'h0EE7; 
    #period
    if (F != expected) $display("Error F: op=1, carry=0");
    if (Cout != 0) $display("Error Cout: op=1, carry=0");
    if (zero != 0) $display("Error zero: op=1, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=1, carry=0");
    if (sign != 0) $display("Error sign: op=1, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 1;
    Cin = 1;
    expected = 16'h0EE7;
    #period 
    if (F != expected) $display("Error F: op=1, carry=1");
    if (Cout != 0) $display("Error Cout: op=1, carry=1");
    if (zero != 0) $display("Error zero: op=1, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=1, carry=1");
    if (sign != 0) $display("Error sign: op=1, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 2;
    Cin = 0;
    expected = 16'h0881;
    #period 
    if (F != expected) $display("Error F: op=2, carry=0");
    if (Cout != 0) $display("Error Cout: op=2, carry=0");
    if (zero != 0) $display("Error zero: op=2, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=2, carry=0");
    if (sign != 0) $display("Error sign: op=2, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 2;
    Cin = 1;
    expected = 16'h0881;
    #period 
    if (F != expected) $display("Error F: op=2, carry=1");
    if (Cout != 0) $display("Error Cout: op=2, carry=1");
    if (zero != 0) $display("Error zero: op=2, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=2, carry=1");
    if (sign != 0) $display("Error sign: op=2, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 3;
    Cin = 0;
    expected = 16'h0666;
    #period 
    if (F != expected) $display("Error F: op=3, carry=0");
    if (Cout != 0) $display("Error Cout: op=3, carry=0");
    if (zero != 0) $display("Error zero: op=3, carry=0");
    if (overflow_parity != 0) $display("Error parity: op=3, carry=0");
    if (sign != 0) $display("Error sign: op=3, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 3;
    Cin = 1;
    expected = 16'h0666;
    #period 
    if (F != expected) $display("Error F: op=3, carry=1");
    if (Cout != 0) $display("Error Cout: op=3, carry=1");
    if (zero != 0) $display("Error zero: op=3, carry=1");
    if (overflow_parity != 0) $display("Error parity: op=3, carry=1");
    if (sign != 0) $display("Error sign: op=3, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 4;
    Cin = 0;
    expected = 16'h1768;
    #period 
    if (F != expected) $display("Error F: op=4, carry=0");
    if (Cout != 0) $display("Error Cout: op=4, carry=0");
    if (zero != 0) $display("Error zero: op=4, carry=0");
    if (overflow_parity != 0) $display("Error overflow: op=4, carry=0");
    if (sign != 0) $display("Error sign: op=4, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 4;
    Cin = 1;
    expected = 16'h1768;
    #period 
    if (F != expected) $display("Error F: op=4, carry=1");
    if (Cout != 0) $display("Error Cout: op=4, carry=1");
    if (zero != 0) $display("Error zero: op=4, carry=1");
    if (overflow_parity != 0) $display("Error overflow: op=4, carry=1");
    if (sign != 0) $display("Error sign: op=4, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 5;
    Cin = 0;
    expected = 16'h01E2;
    #period 
    if (F != expected) $display("Error F: op=5, carry=0");
    if (Cout != 1) $display("Error Cout: op=5, carry=0");
    if (zero != 0) $display("Error zero: op=5, carry=0");
    if (overflow_parity != 0) $display("Error overflow: op=5, carry=0");
    if (sign != 0) $display("Error sign: op=5, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 5;
    Cin = 1;
    expected = 16'h01E2;
    #period 
    if (F != expected) $display("Error F: op=5, carry=1");
    if (Cout != 1) $display("Error Cout: op=5, carry=1");
    if (zero != 0) $display("Error zero: op=5, carry=1");
    if (overflow_parity != 0) $display("Error overflow: op=5, carry=1");
    if (sign != 0) $display("Error sign: op=5, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0101;
    op = 6;
    Cin = 0;
    expected = 16'h194A;
    #period 
    if (F != expected) $display("Error F: op=6, carry=0");
    if (Cout != 0) $display("Error Cout: op=6, carry=0");
    if (zero != 0) $display("Error zero: op=6, carry=0");
    if (overflow_parity != 0) $display("Error parity: op=6, carry=0");
    if (sign != 0) $display("Error sign: op=6, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0101;
    op = 6;
    Cin = 1;
    expected = 16'h194A;
    #period 
    if (F != expected) $display("Error F: op=6, carry=1");
    if (Cout != 0) $display("Error Cout: op=6, carry=1");
    if (zero != 0) $display("Error zero: op=6, carry=1");
    if (overflow_parity != 0) $display("Error parity: op=6, carry=1");
    if (sign != 0) $display("Error sign: op=6, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0101;
    op = 7;
    Cin = 0;
    expected = 16'h0652;
    #period 
    if (F != expected) $display("Error F: op=7, carry=0");
    if (Cout != 1) $display("Error Cout: op=7, carry=0");
    if (zero != 0) $display("Error zero: op=7, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=7, carry=0");
    if (sign != 0) $display("Error sign: op=7, carry=0");

    A = 16'h0CA5;
    B = 16'h0101;
    op = 7;
    Cin = 1;
    expected = 16'h0652;
    #period 
    if (F != expected) $display("Error F: op=7, carry=1");
    if (Cout != 1) $display("Error Cout: op=7, carry=1");
    if (zero != 0) $display("Error zero: op=7, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=7, carry=1");
    if (sign != 0) $display("Error sign: op=7, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0101;
    op = 8;
    Cin = 0;
    expected = 16'h0652;
    #period 
    if (F != expected) $display("Error F: op=8, carry=0");
    if (Cout != 1) $display("Error Cout: op=8, carry=0");
    if (zero != 0) $display("Error zero: op=8, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=8, carry=0");
    if (sign != 0) $display("Error sign: op=8, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0101;
    op = 8;
    Cin = 1;
    expected = 16'h0652;
    #period 
    if (F != expected) $display("Error F: op=8, carry=1");
    if (Cout != 1) $display("Error Cout: op=8, carry=1");
    if (zero != 0) $display("Error zero: op=8, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=8, carry=1");
    if (sign != 0) $display("Error sign: op=8, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 9;
    Cin = 0;
    expected = 16'hF53C;
    #period 
    if (F != expected) $display("Error F: op=9, carry=0");
    if (Cout != 0) $display("Error Cout: op=9, carry=0");
    if (zero != 0) $display("Error zero: op=9, carry=0");
    if (overflow_parity != 0) $display("Error parity: op=9, carry=0");
    if (sign != 1) $display("Error sign: op=9, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 9;
    Cin = 1;
    expected = 16'hF53C;
    #period 
    if (F != expected) $display("Error F: op=9, carry=1");
    if (Cout != 0) $display("Error Cout: op=9, carry=1");
    if (zero != 0) $display("Error zero: op=9, carry=1");
    if (overflow_parity != 0) $display("Error parity: op=9, carry=1");
    if (sign != 1) $display("Error sign: op=9, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 10;
    Cin = 0;
    expected = 16'hF53D;
    #period 
    if (F != expected) $display("Error F: op=10, carry=0");
    if (Cout != 0) $display("Error Cout: op=10, carry=0");
    if (zero != 0) $display("Error zero: op=10, carry=0");
    if (overflow_parity != 0) $display("Error overflow: op=10, carry=0");
    if (sign != 1) $display("Error sign: op=10, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 10;
    Cin = 1;
    expected = 16'hF53D;
    #period 
    if (F != expected) $display("Error F: op=10, carry=1");
    if (Cout != 0) $display("Error Cout: op=10, carry=1");
    if (zero != 0) $display("Error zero: op=10, carry=1");
    if (overflow_parity != 0) $display("Error overflow: op=10, carry=1");
    if (sign != 1) $display("Error sign: op=10, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 11;
    Cin = 0;
    expected = 16'h1768;
    #period 
    if (F != expected) $display("Error F: op=11, carry=0");
    if (Cout != 0) $display("Error Cout: op=11, carry=0");
    if (zero != 0) $display("Error zero: op=11, carry=0");
    if (overflow_parity != 0) $display("Error overflow: op=11, carry=0");
    if (sign != 0) $display("Error sign: op=11, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 11;
    Cin = 1;
    expected = 16'h1769;
    #period 
    if (F != expected) $display("Error F: op=11, carry=1");
    if (Cout != 0) $display("Error Cout: op=11, carry=1");
    if (zero != 0) $display("Error zero: op=11, carry=1");
    if (overflow_parity != 0) $display("Error overflow: op=11, carry=1");
    if (sign != 0) $display("Error sign: op=11, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 12;
    Cin = 0;
    expected = 16'h01E1;
    #period 
    if (F != expected) $display("Error F: op=12, carry=0");
    if (Cout != 1) $display("Error Cout: op=12, carry=0");
    if (zero != 0) $display("Error zero: op=12, carry=0");
    if (overflow_parity != 0) $display("Error overflow: op=12, carry=0");
    if (sign != 0) $display("Error sign: op=12, carry=0");
    
    A = 16'h0CA5;
    B = 16'h0AC3;
    op = 12;
    Cin = 1;
    expected = 16'h01E2;
    #period 
    if (F != expected) $display("Error F: op=12, carry=1");
    if (Cout != 1) $display("Error Cout: op=12, carry=1");
    if (zero != 0) $display("Error zero: op=12, carry=1");
    if (overflow_parity != 0) $display("Error overflow: op=12, carry=1");
    if (sign != 0) $display("Error sign: op=12, carry=1");
    
    A = 16'h0CA5;
    B = 16'h0105;
    op = 13;
    Cin = 0;
    expected = 16'h94A0;
    #period 
    if (F != expected) $display("Error F: op=13, carry=0");
    if (Cout != 1) $display("Error Cout: op=13, carry=0");
    if (zero != 0) $display("Error zero: op=13, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=13, carry=0");
    if (sign != 1) $display("Error sign: op=13, carry=0");
    
    A = 16'h04A5;
    B = 16'h0105;
    op = 13;
    Cin = 1;
    expected = 16'h94BF;
    #period 
    if (F != expected) $display("Error F: op=13, carry=1");
    if (Cout != 0) $display("Error Cout: op=13, carry=1");
    if (zero != 0) $display("Error zero: op=13, carry=1");
    if (overflow_parity != 0) $display("Error parity: op=13, carry=1");
    if (sign != 1) $display("Error sign: op=13, carry=1");
    
    A = 16'h5CB0;
    B = 16'h0105;
    op = 14;
    Cin = 0;
    expected = 16'h02E5;
    #period 
    if (F != expected) $display("Error F: op=14, carry=0");
    if (Cout != 1) $display("Error Cout: op=14, carry=0");
    if (zero != 0) $display("Error zero: op=14, carry=0");
    if (overflow_parity != 0) $display("Error parity: op=14, carry=0");
    if (sign != 0) $display("Error sign: op=14, carry=0");
    
    A = 16'h5CA0;
    B = 16'h0105;
    op = 14;
    Cin = 1;
    expected = 16'hFAE5;
    #period 
    if (F != expected) $display("Error F: op=14, carry=1");
    if (Cout != 0) $display("Error Cout: op=14, carry=1");
    if (zero != 0) $display("Error zero: op=14, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=14, carry=1");
    if (sign != 1) $display("Error sign: op=14, carry=1");
    
    $display("************** PART 2 **************");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 0;
    Cin = 0;
    expected = 16'h7AC3;
    #period 
    if (F != expected) $display("Error F: op=0, carry=0");
    if (Cout != 0) $display("Error Cout: op=0, carry=0");
    if (zero != 0) $display("Error zero: op=0, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=0, carry=0");
    if (sign != 0) $display("Error sign: op=0, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 0;
    Cin = 1;
    expected = 16'h7AC3; 
    #period
    if (F != expected) $display("Error F: op=0, carry=1");
    if (Cout != 0) $display("Error Cout: op=0, carry=1");
    if (zero != 0) $display("Error zero: op=0, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=0, carry=1");
    if (sign != 0) $display("Error sign: op=0, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 1;
    Cin = 0;
    expected = 16'hFEE7; 
    #period
    if (F != expected) $display("Error F: op=1, carry=0");
    if (Cout != 0) $display("Error Cout: op=1, carry=0");
    if (zero != 0) $display("Error zero: op=1, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=1, carry=0");
    if (sign != 1) $display("Error sign: op=1, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 1;
    Cin = 1;
    expected = 16'hFEE7;
    #period 
    if (F != expected) $display("Error F: op=1, carry=1");
    if (Cout != 0) $display("Error Cout: op=1, carry=1");
    if (zero != 0) $display("Error zero: op=1, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=1, carry=1");
    if (sign != 1) $display("Error sign: op=1, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 2;
    Cin = 0;
    expected = 16'h2881;
    #period 
    if (F != expected) $display("Error F: op=2, carry=0");
    if (Cout != 0) $display("Error Cout: op=2, carry=0");
    if (zero != 0) $display("Error zero: op=2, carry=0");
    if (overflow_parity != 0) $display("Error parity: op=2, carry=0");
    if (sign != 0) $display("Error sign: op=2, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 2;
    Cin = 1;
    expected = 16'h2881;
    #period 
    if (F != expected) $display("Error F: op=2, carry=1");
    if (Cout != 0) $display("Error Cout: op=2, carry=1");
    if (zero != 0) $display("Error zero: op=2, carry=1");
    if (overflow_parity != 0) $display("Error parity: op=2, carry=1");
    if (sign != 0) $display("Error sign: op=2, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 3;
    Cin = 0;
    expected = 16'hD666;
    #period 
    if (F != expected) $display("Error F: op=3, carry=0");
    if (Cout != 0) $display("Error Cout: op=3, carry=0");
    if (zero != 0) $display("Error zero: op=3, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=3, carry=0");
    if (sign != 1) $display("Error sign: op=3, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 3;
    Cin = 1;
    expected = 16'hD666;
    #period 
    if (F != expected) $display("Error F: op=3, carry=1");
    if (Cout != 0) $display("Error Cout: op=3, carry=1");
    if (zero != 0) $display("Error zero: op=3, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=3, carry=1");
    if (sign != 1) $display("Error sign: op=3, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 4;
    Cin = 0;
    expected = 16'h2768;
    #period 
    if (F != expected) $display("Error F: op=4, carry=0");
    if (Cout != 1) $display("Error Cout: op=4, carry=0");
    if (zero != 0) $display("Error zero: op=4, carry=0");
    if (overflow_parity != 0) $display("Error overflow: op=4, carry=0");
    if (sign != 0) $display("Error sign: op=4, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 4;
    Cin = 1;
    expected = 16'h2768;
    #period 
    if (F != expected) $display("Error F: op=4, carry=1");
    if (Cout != 1) $display("Error Cout: op=4, carry=1");
    if (zero != 0) $display("Error zero: op=4, carry=1");
    if (overflow_parity != 0) $display("Error overflow: op=4, carry=1");
    if (sign != 0) $display("Error sign: op=4, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 5;
    Cin = 0;
    expected = 16'h31E2;
    #period 
    if (F != expected) $display("Error F: op=5, carry=0");
    if (Cout != 1) $display("Error Cout: op=5, carry=0");
    if (zero != 0) $display("Error zero: op=5, carry=0");
    if (overflow_parity != 1) $display("Error overflow: op=5, carry=0");
    if (sign != 0) $display("Error sign: op=5, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 5;
    Cin = 1;
    expected = 16'h31E2;
    #period 
    if (F != expected) $display("Error F: op=5, carry=1");
    if (Cout != 1) $display("Error Cout: op=5, carry=1");
    if (zero != 0) $display("Error zero: op=5, carry=1");
    if (overflow_parity != 1) $display("Error overflow: op=5, carry=1");
    if (sign != 0) $display("Error sign: op=5, carry=1");
    
    A = 16'hACA5;
    B = 16'h0102;
    op = 6;
    Cin = 0;
    expected = 16'hB294;
    #period 
    if (F != expected) $display("Error F: op=6, carry=0");
    if (Cout != 0) $display("Error Cout: op=6, carry=0");
    if (zero != 0) $display("Error zero: op=6, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=6, carry=0");
    if (sign != 1) $display("Error sign: op=6, carry=0");
    
    A = 16'hACA5;
    B = 16'h0102;
    op = 6;
    Cin = 1;
    expected = 16'hB294;
    #period 
    if (F != expected) $display("Error F: op=6, carry=1");
    if (Cout != 0) $display("Error Cout: op=6, carry=1");
    if (zero != 0) $display("Error zero: op=6, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=6, carry=1");
    if (sign != 1) $display("Error sign: op=6, carry=1");
    
    A = 16'hACA5;
    B = 16'h0102;
    op = 7;
    Cin = 0;
    expected = 16'h2B29;
    #period 
    if (F != expected) $display("Error F: op=7, carry=0");
    if (Cout != 0) $display("Error Cout: op=7, carry=0");
    if (zero != 0) $display("Error zero: op=7, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=7, carry=0");
    if (sign != 0) $display("Error sign: op=7, carry=0");

    A = 16'hACA5;
    B = 16'h0102;
    op = 7;
    Cin = 1;
    expected = 16'h2B29;
    #period 
    if (F != expected) $display("Error F: op=7, carry=1");
    if (Cout != 0) $display("Error Cout: op=7, carry=1");
    if (zero != 0) $display("Error zero: op=7, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=7, carry=1");
    if (sign != 0) $display("Error sign: op=7, carry=1");
    
    A = 16'hACA5;
    B = 16'h0102;
    op = 8;
    Cin = 0;
    expected = 16'hEB29;
    #period 
    if (F != expected) $display("Error F: op=8, carry=0");
    if (Cout != 0) $display("Error Cout: op=8, carry=0");
    if (zero != 0) $display("Error zero: op=8, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=8, carry=0");
    if (sign != 1) $display("Error sign: op=8, carry=0");
    
    A = 16'hACA5;
    B = 16'h0102;
    op = 8;
    Cin = 1;
    expected = 16'hEB29;
    #period 
    if (F != expected) $display("Error F: op=8, carry=1");
    if (Cout != 0) $display("Error Cout: op=8, carry=1");
    if (zero != 0) $display("Error zero: op=8, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=8, carry=1");
    if (sign != 1) $display("Error sign: op=8, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 9;
    Cin = 0;
    expected = 16'h853C;
    #period 
    if (F != expected) $display("Error F: op=9, carry=0");
    if (Cout != 0) $display("Error Cout: op=9, carry=0");
    if (zero != 0) $display("Error zero: op=9, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=9, carry=0");
    if (sign != 1) $display("Error sign: op=9, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 9;
    Cin = 1;
    expected = 16'h853C;
    #period 
    if (F != expected) $display("Error F: op=9, carry=1");
    if (Cout != 0) $display("Error Cout: op=9, carry=1");
    if (zero != 0) $display("Error zero: op=9, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=9, carry=1");
    if (sign != 1) $display("Error sign: op=9, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 10;
    Cin = 0;
    expected = 16'h853D;
    #period 
    if (F != expected) $display("Error F: op=10, carry=0");
    if (Cout != 0) $display("Error Cout: op=10, carry=0");
    if (zero != 0) $display("Error zero: op=10, carry=0");
    if (overflow_parity != 0) $display("Error overflow: op=10, carry=0");
    if (sign != 1) $display("Error sign: op=10, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 10;
    Cin = 1;
    expected = 16'h853D;
    #period 
    if (F != expected) $display("Error F: op=10, carry=1");
    if (Cout != 0) $display("Error Cout: op=10, carry=1");
    if (zero != 0) $display("Error zero: op=10, carry=1");
    if (overflow_parity != 0) $display("Error overflow: op=10, carry=1");
    if (sign != 1) $display("Error sign: op=10, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 11;
    Cin = 0;
    expected = 16'h2768;
    #period 
    if (F != expected) $display("Error F: op=11, carry=0");
    if (Cout != 1) $display("Error Cout: op=11, carry=0");
    if (zero != 0) $display("Error zero: op=11, carry=0");
    if (overflow_parity != 0) $display("Error overflow: op=11, carry=0");
    if (sign != 0) $display("Error sign: op=11, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 11;
    Cin = 1;
    expected = 16'h2769;
    #period 
    if (F != expected) $display("Error F: op=11, carry=1");
    if (Cout != 1) $display("Error Cout: op=11, carry=1");
    if (zero != 0) $display("Error zero: op=11, carry=1");
    if (overflow_parity != 0) $display("Error overflow: op=11, carry=1");
    if (sign != 0) $display("Error sign: op=11, carry=1");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 12;
    Cin = 0;
    expected = 16'h31E1;
    #period 
    if (F != expected) $display("Error F: op=12, carry=0");
    if (Cout != 1) $display("Error Cout: op=12, carry=0");
    if (zero != 0) $display("Error zero: op=12, carry=0");
    if (overflow_parity != 1) $display("Error overflow: op=12, carry=0");
    if (sign != 0) $display("Error sign: op=12, carry=0");
    
    A = 16'hACA5;
    B = 16'h7AC3;
    op = 12;
    Cin = 1;
    expected = 16'h31E2;
    #period 
    if (F != expected) $display("Error F: op=12, carry=1");
    if (Cout != 1) $display("Error Cout: op=12, carry=1");
    if (zero != 0) $display("Error zero: op=12, carry=1");
    if (overflow_parity != 1) $display("Error overflow: op=12, carry=1");
    if (sign != 0) $display("Error sign: op=12, carry=1");
    
    A = 16'hACA5;
    B = 16'h0105;
    op = 13;
    Cin = 0;
    expected = 16'h94A0;
    #period 
    if (F != expected) $display("Error F: op=13, carry=0");
    if (Cout != 1) $display("Error Cout: op=13, carry=0");
    if (zero != 0) $display("Error zero: op=13, carry=0");
    if (overflow_parity != 1) $display("Error parity: op=13, carry=0");
    if (sign != 1) $display("Error sign: op=13, carry=0");
    
    A = 16'hA4A5;
    B = 16'h0105;
    op = 13;
    Cin = 1;
    expected = 16'h94BF;
    #period 
    if (F != expected) $display("Error F: op=13, carry=1");
    if (Cout != 0) $display("Error Cout: op=13, carry=1");
    if (zero != 0) $display("Error zero: op=13, carry=1");
    if (overflow_parity != 0) $display("Error parity: op=13, carry=1");
    if (sign != 1) $display("Error sign: op=13, carry=1");
    
    A = 16'h5CBA;
    B = 16'h0105;
    op = 14;
    Cin = 0;
    expected = 16'h02E5;
    #period 
    if (F != expected) $display("Error F: op=14, carry=0");
    if (Cout != 1) $display("Error Cout: op=14, carry=0");
    if (zero != 0) $display("Error zero: op=14, carry=0");
    if (overflow_parity != 0) $display("Error parity: op=14, carry=0");
    if (sign != 0) $display("Error sign: op=14, carry=0");
    
    A = 16'h5CAA;
    B = 16'h0105;
    op = 14;
    Cin = 1;
    expected = 16'hFAE5;
    #period 
    if (F != expected) $display("Error F: op=14, carry=1");
    if (Cout != 0) $display("Error Cout: op=14, carry=1");
    if (zero != 0) $display("Error zero: op=14, carry=1");
    if (overflow_parity != 1) $display("Error parity: op=14, carry=1");
    if (sign != 1) $display("Error sign: op=14, carry=1");
  end
endmodule
