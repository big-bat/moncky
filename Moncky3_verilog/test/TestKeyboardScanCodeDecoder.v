`timescale 1us / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.05.2021 10:06:09
// Design Name: 
// Module Name: TestKeyboardScanCodeDecoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestKeyboardScanCodeDecoder();
    reg CLK100MHZ;
    reg [7:0] data;
    reg data_ready;
    reg error;
    wire [7:0] data_out;
    wire extended;
    wire data_out_ready;
    
    localparam timeToReceive = 2000;
    
    KeyboardScanCodeDecoder SUT (
      .CLK100MHZ (CLK100MHZ),
      .data (data),
      .data_ready (data_ready),
      .error (error),
      .data_out (data_out),
      .extended (extended),
      .data_out_ready (data_out_ready)
    );
    
    always #0.005 CLK100MHZ = ~CLK100MHZ;
    
    initial
    begin
      CLK100MHZ = 0;
      data = 0;
      data_ready = 0;
      error = 0;
      
      #timeToReceive
      data = 8'h13;
      data_ready = 1;
      #30
      data_ready = 0;
      
      #timeToReceive
      data = 8'hE0;
      data_ready = 1;
      #30
      data_ready = 0;
      #timeToReceive
      data = 8'h13;
      data_ready = 1;
      #30
      data_ready = 0;
    end
endmodule
