`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2021 17:39:24
// Design Name: 
// Module Name: testTop
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestTop();
    reg CLK100MHZ; // master clock
    reg ck_rst; // reset button
    reg btn0; // interrupt button
    // arduino connections
//    wire ck_io0, ck_io1, ck_io2, ck_io3, ck_io4, ck_io5, ck_io6, ck_io7, ck_io8, ck_io9, ck_io10_ss, ck_io11_mosi, ck_io12_miso, ck_io13_sck;
    wire [7:0] jc, jd; // vga connector
    reg [7:0] jb; // ps/2 connector
    
    top SUT (
        CLK100MHZ,
        ck_rst,
        btn0,
//        ck_io0, ck_io1, ck_io2, ck_io3, ck_io4, ck_io5, ck_io6, ck_io7, ck_io8, ck_io9, ck_io10_ss, ck_io11_mosi, ck_io12_miso, ck_io13_sck,
        jc, jd, jb
    );
    
    always #5 CLK100MHZ = ~CLK100MHZ;
 
    initial
    begin
      ck_rst = 0;
      CLK100MHZ = 0;
    end
    
    initial
    begin
      #12
      ck_rst = 1;
    end
    
    reg [1:0] data [0:599];
    reg [10:0] i;
   
    initial
    begin
        jb[6] = 1;
        jb[4] = 1;
        
      #10000
      $readmemb("escapeLog.txt", data);
      #5000
      
      // start at 5 us
      for(i = 0; i<66; i=i+1)
      begin
        jb[4] = data[i][0];
        jb[6] = data[i][1];
        #5000;
      end
      
      #100000 // start at 435 us
      $readmemb("escapeReleaseLog.txt", data);
      for(i = 0; i<211; i=i+1)
      begin
        jb[4] = data[i][0];
        jb[6] = data[i][1];
        #5;
      end
      
      #100000 // start at 1590 us
      $readmemb("arrowRightLog.txt", data);
      for(i = 0; i<212; i=i+1)
      begin
        jb[4] = data[i][0];
        jb[6] = data[i][1];
        #5000;
      end
      
      #100000 // start at 2750 us
      $readmemb("arrowRightReleaseLog.txt", data);
      for(i = 0; i<357; i=i+1)
      begin
        jb[4] = data[i][0];
        jb[6] = data[i][1];
        #5000;
      end
      
      #100000 // start at 4635 us
      $readmemb("prtscrLog.txt", data);
      for(i = 0; i<531; i=i+1)
      begin
        jb[4] = data[i][0];
        jb[6] = data[i][1];
        #5000;
      end
      
      #100000 // start at 7390 us
      $readmemb("prtscrReleaseLog.txt", data);
      for(i = 0; i<221; i=i+1)
      begin
        jb[4] = data[i][0];
        jb[6] = data[i][1];
        #5000;
      end
      
      #100000 // start at 8595 us
      $readmemb("prtscrReleaseErrorLog.txt", data);
      for(i = 0; i<331; i=i+1)
      begin
        jb[4] = data[i][0];
        jb[6] = data[i][1];
        #5000;
      end

    end
endmodule
