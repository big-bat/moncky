`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.04.2021 16:53:43
// Design Name: 
// Module Name: testControlLogic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testControlLogic();
    reg clk;
    reg [15:0] instruction;
    reg reset;
    reg interrupt;
    reg [3:0] flags;
    reg [15:0] register;
    wire [1:0] dataram;
    wire [1:0] registerMux;
    wire [3:0] inputSelect;
    wire writeEnable;
    wire [1:0] spControl;
    wire [3:0] select0, select1, select2;
    wire [15:0] immediate;
    wire aluA, aluB;
    wire [3:0] aluOperation;
    wire carry;
    wire io_control;
    
    wire [1:0] intControl;
    wire interruptGranted;
    wire [1:0] group;
    wire [15:0] realInstruction;
    wire [3:0] opcode4;
    wire [4:0] opcode5;
    wire [3:0] r, s, t;
    wire [2:0] flagSelect;
    wire selectedFlag;
    wire [28:0] signals;
    wire restoreFlags;
    wire saveFlags;
    wire [3:0] storedFlags;
    
    assign io_request = (group == 2);
    
    InterruptControl int_ctrl (
        .clk (clk),
        .control (intControl),
        .interrupt (interrupt),
        .granted (interruptGranted)
    );
    
    InstructionReader instr_reader (
        .clk (clk),
        .instruction (instruction),
        .reset (reset),
        .intGranted (interruptGranted),
        .group (group),
        .realInstruction (realInstruction),
        .opcode4 (opcode4),
        .opcode5 (opcode5),
        .r (r),
        .s (s),
        .t (t),
        .flagSelect (flagSelect)
    );
    
    InstructionDecoder instr_decoder (
        .instr0 (r),
        .instr1 (opcode5),
        .instr2 (t),
        .instr3 (opcode4),
        .group (group),
        .signals (signals)
    );

    FlagsRegister flags_reg (
        .clk (clk),
        .flags_input (flags),
        .fromRegister (register[3:0]),
        .restoreFlags (restoreFlags),
        .saveFlags (saveFlags),
        .selectFlag (flagSelect),
        .flags_output (storedFlags),
        .selectedFlag (selectedFlag)
    );

    SignalDecoder sgnl_decoder (
        .signals (signals),
        .flag (selectedFlag),
        .flags (storedFlags),
        .r (r),
        .s (s),
        .t (t),
        .dataram (dataram),
        .registerMux (registerMux),
        .inputSelect (inputSelect),
        .writeEnable (writeEnable),
        .spControl (spControl),
        .select0 (select0),
        .select1 (select1),
        .select2 (select2),
        .immediate (immediate),
        .aluA (aluA),
        .aluB (aluB),
        .aluOperation (aluOperation),
        .restoreFlags (restoreFlags),
        .saveFlags (saveFlags),
        .intControl (intControl)
    );
    
    assign carry = storedFlags[1];
    
    localparam period = 1;
        
  initial
  begin
  
  $display("************** TEST CONTROL LOGIC STARTED **************");
  
  clk = 0;
  reset = 1;
  interrupt = 0;
  flags = 4'b1010;
  register = 16'h0005;
  instruction = 0;
  
  instruction = 0003;
  #period
  clk = 1;
  #period
  clk = 0;
  
  instruction = 16'h0000;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 0) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 0) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 0) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0000) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 2) $display("Error: interruptControl for instruction %h", instruction);
 
  instruction = 16'h1000;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 15) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 0) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 0) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 5) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 1) $display("Error: interruptControl for instruction %h", instruction);
  
  instruction = 16'h2000;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 15) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 0) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 0) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 5) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 1) $display("Error: interruptControl for instruction %h", instruction);
  
  instruction = 16'h3000;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 15) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 0) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 0) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 5) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 2) $display("Error: interruptControl for instruction %h", instruction);
 
 instruction = 16'h4000;
  #period
  if (dataram != 1) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 3) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 1) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 14) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 0) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 0) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 4) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 1) $display("Error: interruptControl for instruction %h", instruction);
  
  instruction = 16'h5000;
  #period
  if (dataram != 2) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 2) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 14) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  if (select2 != 15) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0010) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 2) $display("Error: interruptControl for instruction %h", instruction);
  
  instruction = 16'h6000;
  #period
  if (dataram != 2) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 3) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 14) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  if (select2 != 15) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0010) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 2) $display("Error: interruptControl for instruction %h", instruction);

//reset instruction dataram registerMux inputSelect writeEnable spControl select0 select1 select2 immediate aluA aluB alu saveFlags restoreFlags interruptControl
//1     0xA000      0       x            x          0           0          x      x       x       x         x    x    x   0         0            0
  instruction = 16'hA000;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 14) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 15) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0010) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1     0xB080      2       x            x          0           2          x      0xE     0xB     x         x    0    0   0         0            0
  instruction = 16'hB080;
  #period
  if (dataram != 2) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 2) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 14) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  if (select2 != 11) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0010) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1     0xC100      1       3            0xC        1           1          0xE    x       x       0x0001    0    1    4   0         0            0
  instruction = 16'hC100;
  #period
  if (dataram != 1) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 3) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 12) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 1) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 14) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 11) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 4) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xD180      2       0            0xF        1           2          0xD    0xE     0xF     x         x    0    0   0         0            0
  instruction = 16'hD180;
  #period
  if (dataram != 2) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 2) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 13) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  if (select2 != 15) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1     0xE200      0       1            0xE        1           0          x      x       0xE     0x0000    x    x    x   0         0            0
  instruction = 16'hE200;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 14) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 13) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h000A) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1     0xF280      0       x            x          0           0          x      x       x       x         x    x    x   1         1            0
  instruction = 16'hF280;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 14) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 13) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0000) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 1) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 1) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
  clk = 1;
  #period
  clk = 0;
  #period
  if (storedFlags != 5) $display("Error to store flags from register");

//1     0x1300      0       0            0xF        1           0          0x1    x       x       x         x    x    x   0         0            0
  instruction = 16'h1300;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0000) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xC800      0       1            0xC        1           0          x      x       x       0x0000    x    1    0   0         0            0
  instruction = 16'hC800;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 12) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xD880      0       1            0xD        1           0          x      x       x       0x0001    x    1    0   0         0            0
  instruction = 16'hD880;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 13) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0000) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xE900      0       1            0xE        1           0          x      x       x       0x0000    x    1    0   0         0            0
  instruction = 16'hE900;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 14) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0000) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xF980      0       1            0xF        1           0          x      x       x       0x0001    x    1    0   0         0            0
  instruction = 16'hF980;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0x1A00      0       1            0x1        1           0          x      x       x       0x0000    x    1    0   0         0            0
  instruction = 16'h1A00;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 1) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0x2A80      0       1            0x2        1           0          x      x       x       0x0001    x    1    0   0         0            0
  instruction = 16'h2A80;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 2) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0000) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0x3B00      0       1            0x3        1           0          x      x       x       0x0000    x    1    0   0         0            0
  instruction = 16'h3B00;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 3) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0000) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0x4B80      0       1            0x4        1           0          x      x       x       0x0001    x    1    0   0         0            0
  instruction = 16'h4B80;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 4) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0x5C00      0       0            0xF        0           0          0x5    x       x       x         x    x    0   0         0            0
  instruction = 16'h5C00;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 5) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0x6C80      0       0            0xF        1           0          0x6    x       x       x         x    x    0   0         0            0
  instruction = 16'h6C80;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 6) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1     0x7D00      0       0            0xF        0           0          0x7    x       x       x         x    x    0   0         0            0
  instruction = 16'h7D00;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 7) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0x9D80      0       0            0xF        1           0          0x9    x       x       x         x    x    0   0         0            0
  instruction = 16'h9D80;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 9) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xAE00      0       0            0xF        0           0          0xA    x       x       x         x    x    0   0         0            0
  instruction = 16'hAE00;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 10) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xBE80      0       0            0xF        1           0          0xB    x       x       x         x    x    0   0         0            0
  instruction = 16'hBE80;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 11) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xCF00      0       0            0xF        0           0          0xC    x       x       x         x    x    0   0         0            0
  instruction = 16'hCF00;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 12) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1     0xDF80      0       0            0xF        1           0          0xD    x       x       x         x    x    0   0         0            0
  instruction = 16'hDF80;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 0) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 15) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 13) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h0001) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//reset instruction dataram registerMux inputSelect writeEnable spControl select0 select1 select2 immediate aluA aluB alu saveFlags restoreFlags interruptControl

//1 0xA2B1 0 1 0xA 1 0 x   x   x   0x002B x x x   0 0 0
  instruction = 16'hA2B1;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 1) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 10) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 13) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h002B) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  //if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  //if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1 0xC2B2 0 2 0xC 1 0 0xC x   x   0x2B00 0 1 0x1 0 0 0
  instruction = 16'hC2B2;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 12) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 12) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h2B00) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 1) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1 0x5E43 0 2 0x5 1 0 0x5 x   x   0xFFE4 0 1 0x4 1 0 0
  instruction = 16'h5E43;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 5) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 5) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'hFFE4) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 4) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1 0x9874 0 2 0x9 1 0 0x9 x   x   0x0087 0 1 0x2 1 0 0
  instruction = 16'h9874;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 9) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 9) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h0087) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 2) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1 0xABC5 0 2 0xA 1 0 0xA x   x   0x00BC 0 1 0x1 1 0 0
  instruction = 16'hABC5;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 10) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 10) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h00BC) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 1) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1 0xDEF6 0 x x   0 0 0xD x   x   0xFFEF 0 1 0x5 1 0 0
  instruction = 16'hDEF6;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 10) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 13) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 14) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'hFFEF) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 5) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1 0xDEF7 0 x x   0 0 x   0xD x   0xFFEF 1 0 0x5 1 0 0
  instruction = 16'hDEF7;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 10) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 13) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 13) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'hFFEF) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 1) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 5) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);

//1 0x1FF8 0 2 0x1 1 0 0x1 0xF x   x      0 0 0xF 1 0 0
  instruction = 16'h1FF8;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 1) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 15) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'hFFEF) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 15) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1 0x1809 0 x x   0 0 0x1 0x8 x   x      0 0 0x0 1 0 0
  instruction = 16'h1809;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 1) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 1) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 8) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'hFFEF) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1 0x5E2A 0 2 0x5 1 0 0X5 x   x   0x000E 0 1 0x2 1 0 0
  instruction = 16'h5E2A;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 5) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 5) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 8) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h000E) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 2) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1 0x9ADB 0 x x   0 0 0x9 x   x   0x000A 0 1 0xD 1 0 0
  instruction = 16'h9ADB;
  #period
  if (dataram != 0) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 2) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 5) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 9) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 8) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h000A) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 13) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 1) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1 0xA2BC 1 3 0xA 1 0 0xB 0x2 x   x      0 0 0x4 0 0 0
  instruction = 16'hA2BC;
  #period
  if (dataram != 1) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 3) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 10) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 11) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 2) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h000A) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 4) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1 0xCBAD 2 x x   0 0 0xA 0xB 0xC x      0 0 0x4 0 0 0
  instruction = 16'hCBAD;
  #period
  if (dataram != 2) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 3) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 10) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 10) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 11) $display("Error: select1 for instruction %h", instruction);
  if (select2 != 12) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h000A) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 4) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1 0xDCBE 1 3 0xD 1 0 0xB x   x   0x000C 0 1 0x4 0 0 0
  instruction = 16'hDCBE;
  #period
  if (dataram != 1) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 3) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 13) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 11) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 11) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 12) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h000C) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 4) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
//1 0xEDCF 2 x x   0 0 0xC x   0xE 0x000D 0 1 0x4 0 0 0
  instruction = 16'hEDCF;
  #period
  if (dataram != 2) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 3) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 13) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  if (select0 != 12) $display("Error: select0 for instruction %h", instruction);
  //if (select1 != 11) $display("Error: select1 for instruction %h", instruction);
  if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  if (immediate != 16'h000D) $display("Error: immediate for instruction %h", instruction);
  if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 1) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 4) $display("Error: aluOperation for instruction %h", instruction);
  if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
  instruction = 16'hA2B1; // li r10, 0x2B
  reset = 0;
  #period
  if (signals != 29'h02A008F0) $display("Error: signals of li");
  clk = 1;
  #period
  if (signals != 29'h02A008F0) $display("Error: signals of li");
  clk = 0;
  #period
  if (signals != 29'h03A60002) $display("Error: signals after reset");
  reset = 1;
  #period
  if (signals != 29'h03A60002) $display("Error: signals after reset");
  clk = 1;
  #period
  if (signals != 29'h03A60002) $display("Error: signals after reset");
  clk = 0;
  #period
  if (signals != 29'h02A008F0) $display("Error: signals of li");
  
  instruction = 16'h3000; // di
  #period
  if (signals != 29'h01860002) $display("Error: signals after di 1");
  clk = 1;
  #period
  if (signals != 29'h01860002) $display("Error: signals after di 2");
  clk = 0;
  #period
  if (signals != 29'h01860002) $display("Error: signals after di 3");
  
  interrupt = 1;
  #period
  if (signals != 29'h01860002) $display("Error: signals after di 4");
  clk = 1;
  #period
  if (signals != 29'h01860002) $display("Error: signals after di 5");
  clk = 0;
  interrupt = 0;
  #period
  if (signals != 29'h01860002) $display("Error: signals after di 6");
  
  instruction = 16'h2000; // ei
  #period
  if (signals != 29'h01860001) $display("Error: signals after ei 1");
  clk = 1;
  #period
  clk = 0;
  #period
  if (signals != 29'h01860001) $display("Error: signals after ei 2");
  
  instruction = 16'hA2B1; // li r10, 0x2B
  #period
  if (signals != 29'h02A008F0) $display("Error: signals of li 1");
  interrupt = 1;
  #period
  if (signals != 29'h02A008F0) $display("Error: signals of li 2");
  clk = 1;
  #period
  if (signals != 29'h02A008F0) $display("Error: signals of li 3");
  clk = 0;
  #period
  //clk = 1;
  //#period
  //clk = 0;
  //#period
  if (signals != 29'h13BF6002) $display("Error: signals for hi-1");
  
  clk = 1;
  #period
  if (signals != 29'h13BF6002) $display("Error: signals for hi-2");
  clk = 0;
  #period
  if (signals != 29'h02A008F0) $display("Error: signals of li after interrupt");
  
  if (io_request != 0) $display("Error: io_request should be 0");
  
  instruction = 16'h2310; // in r2, (r3)
  #period
  if (io_request != 1) $display("Error: io_request should be 1");
  if (dataram != 1) $display("Error: dataram for instruction %h", instruction);
  if (registerMux != 3) $display("Error: registerMux for instruction %h", instruction);
  if (inputSelect != 2) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 1) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 12) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 3) $display("Error: select1 for instruction %h", instruction);
  //if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h000D) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
  instruction = 16'hE720; // out r14, (r7)
  #period
  if (io_request != 1) $display("Error: io_request should be 1");
  if (dataram != 2) $display("Error: dataram for instruction %h", instruction);
  //if (registerMux != 3) $display("Error: registerMux for instruction %h", instruction);
  //if (inputSelect != 1) $display("Error: inputSelect for instruction %h", instruction);
  if (writeEnable != 0) $display("Error: writeEnable for instruction %h", instruction);
  if (spControl != 0) $display("Error: spControl for instruction %h", instruction);
  //if (select0 != 12) $display("Error: select0 for instruction %h", instruction);
  if (select1 != 7) $display("Error: select1 for instruction %h", instruction);
  if (select2 != 14) $display("Error: select2 for instruction %h", instruction);
  //if (immediate != 16'h000D) $display("Error: immediate for instruction %h", instruction);
  //if (aluA != 0) $display("Error: aluA for instruction %h", instruction);
  if (aluB != 0) $display("Error: aluB for instruction %h", instruction);
  if (aluOperation != 0) $display("Error: aluOperation for instruction %h", instruction);
  //if (carry != 0) $display("Error: carry for instruction %h", instruction);
  if (saveFlags != 0) $display("Error: saveFlags for instruction %h", instruction);
  if (restoreFlags != 0) $display("Error: restoreFlags for instruction %h", instruction);
  if (intControl != 0) $display("Error: interruptControl for instruction %h", instruction);
  
  end
endmodule
