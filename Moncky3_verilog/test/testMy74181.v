`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.03.2021 14:43:17
// Design Name: 
// Module Name: testMy74181
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testMy74181();
  reg [3:0] A;
  reg [3:0] B;
  reg [3:0] S;
  reg Cin;
  reg M;
  wire [3:0] F;
  wire zero;
  wire sign;
  wire Cout;
  wire overflow;
  
  localparam period = 1;  
 
  My74181 UUT (
    .A (A),
    .B (B),
    .S (S),
    .Cin (Cin),
    .M (M),
    .F (F),
    .zero (zero),
    .sign (sign),
    .Cout (Cout),
    .overflow_parity (overflow)
  );
  
    reg [3:0] expected;
    initial
    begin
      $display("************** TEST STARTED **************");
    
      S = 4'b1010; // B
      M = 0;
      Cin = 0;
      A = 4'b0000;
      B = 4'b0000;
      #period
      if (zero != 1) $display("Error with zero flag (1)");
      if (sign != 0) $display("Error with sign flag (1)");
      if (Cout != 0) $display("Error with carry flag (1)");
      if (overflow != 0) $display("Error with parity flag (1)");
    
      S = 4'b1010; // B
      M = 0;
      Cin = 0;
      A = 4'b0000;
      B = 4'b1000;
      #period
      if (zero != 0) $display("Error with zero flag (2)");
      if (sign != 1) $display("Error with sign flag (2)");
      if (Cout != 0) $display("Error with carry flag (2)");
      if (overflow != 1) $display("Error with parity flag (2)");
      
      S = 4'b1001; // A + B
      M = 1;
      Cin = 1;
      A = 4'b1111;
      B = 4'b0000;
      #period
      if (zero != 1) $display("Error with zero flag (3)");
      if (sign != 0) $display("Error with sign flag (3)");
      if (Cout != 1) $display("Error with carry flag (3)");
      if (overflow != 0) $display("Error with overflow flag (3)");
      
      S = 4'b1001; // A + B
      M = 1;
      Cin = 0;
      A = 4'b1111;
      B = 4'b0000;
      #period
      if (zero != 0) $display("Error with zero flag (4)");
      if (sign != 1) $display("Error with sign flag (4)");
      if (Cout != 0) $display("Error with carry flag (4)");
      if (overflow != 0) $display("Error with overflow flag (4)");
      
      S = 4'b1001; // A + B
      M = 1;
      Cin = 0;
      A = 4'b0111;
      B = 4'b0011;
      #period
      if (zero != 0) $display("Error with zero flag (5)");
      if (sign != 1) $display("Error with sign flag (5)");
      if (Cout != 0) $display("Error with carry flag (5)");
      if (overflow != 1) $display("Error with overflow flag (5)");
    
      S = 4'b1010;  // B
      M = 0;
      Cin = 0;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          if (F != B) $display("Error for B");
        end
      end
      
      S = 4'b1110; // A or B
      M = 0;
      Cin = 0;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          if (F != (A | B)) $display("Error for or");
        end
      end
      
      S = 4'b1011; // A and B
      M = 0;
      Cin = 0;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          if (F != (A & B)) $display("Error for and");
        end
      end
      
      S = 4'b0110; // A xor B
      M = 0;
      Cin = 0;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          if (F != (A ^ B)) $display("Error for xor");
        end
      end
      
      S = 4'b1001; // A + B
      M = 1;
      Cin = 0;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          if (F != (A + B)) $display("Error for plus");
        end
      end
      
      S = 4'b0110; // A - B
      M = 1;
      Cin = 1;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          if (F != (A - B)) $display("Error for min");
        end
      end
      
      S = 4'b0101; // ~B
      M = 0;
      Cin = 0;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          if (F != (~B)) $display("Error for not B");
        end
      end
      
      S = 4'b1001; //  addc
      M = 1;
      Cin = 1;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          expected = A+B+1;
          if (F != expected) $display("Error for addc");
        end
      end
      
      S = 4'b0110; //  subc
      M = 1;
      Cin = 0;
      
      for(A=0; A<15; A=A+1)
      begin
        for(B=0; B<15; B=B+1)
        begin
          #period
          expected = A-B-1;
          if (F != expected) $display("Error for subc");
        end
      end
    end
endmodule
