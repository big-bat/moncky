`timescale 1us / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.05.2021 17:38:18
// Design Name: 
// Module Name: testKeyboardInterface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testKeyboardInterface();
    reg CLK100MHZ;
    reg clk_in;
    reg data_in;
    wire [7:0] data_out;
    wire data_ready;
    wire error;
    
    localparam dataToClockSetupTime = 5; // 5-25us
    localparam clockTime = 30; // 30-50us
    localparam clockToDataHoldTime = 5; // 5-25us
    localparam timeout = 2000; // (>2*12*maxClockTime) to test the watchdog
    
    KeyboardInterface SUT (
      .CLK100MHZ (CLK100MHZ),
      .clk_in (clk_in),
      .data_in (data_in),
      .data_out (data_out),
      .data_ready (data_ready),
      .error (error)
    );
    
    always #0.005 CLK100MHZ = ~CLK100MHZ;
    
    initial
    begin
      CLK100MHZ = 0;
      clk_in = 1; // idle
      data_in = 1; // idle
      #1000
      
      // send FF
      data_in = 0; // start bit
      #dataToClockSetupTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // first data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // second data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // third data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // fourth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // fifth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // sixth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // seventh data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // eighth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // parity bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // stop bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // restore data to idle
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;  // restore clk to idle
      
      // send A5
      #clockTime
      data_in = 0; // start bit
      #dataToClockSetupTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // first data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // second data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // third data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // fourth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // fifth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // sixth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // seventh data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // eighth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // parity bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // stop bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // restore data to idle
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;  // restore clk to idle
      
      // send A5 with wrong parity
      #clockTime
      data_in = 0; // start bit
      #dataToClockSetupTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // first data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // second data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // third data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // fourth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // fifth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // sixth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // seventh data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // eighth data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // parity bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // stop bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // restore data to idle
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;  // restore clk to idle
      
      // send A5, but stop after 2 bits
      #clockTime
      data_in = 0; // start bit
      #dataToClockSetupTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 1; // first data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      #clockTime
      clk_in = 0;
      #clockToDataHoldTime
      data_in = 0; // second data bit
      #(clockTime-clockToDataHoldTime)
      clk_in = 1;
      data_in = 1;
      #timeout
      ;
    end
endmodule