`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.06.2021 12:21:28
// Design Name: 
// Module Name: TestSPIcontroller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestSPIcontroller();
    reg miso;
    wire sck;
    wire mosi;
    wire [3:0] ss; // up to 4 slaves
    wire ready;
    reg clk_mem;
    reg clk_io;
    reg enable;
    reg reset;
    reg [15:0] data_in;
    reg [15:0] address;
    reg [1:0] control;
    wire [15:0] data_out;
    
    SPIcontroller spi_ctrl (
    .miso (miso),
    .sck (sck),
    .mosi (mosi),
    .ss (ss),
    .ready (ready),
    .clk_mem (clk_mem),
    .clk_io (clk_io),
    .enable (enable),
    .reset (reset),
    .data_in (data_in),
    .address (address),
    .control (control),
    .data_out (data_out)
    );
    
    always #40 clk_io = ~clk_io;
    always #40 clk_mem = ~clk_mem;
    
    initial
    begin
        clk_io = 1;
        clk_mem = 1;
        miso = 0;
        enable = 0;
        reset = 0;
        data_in = 16'hA5C3;
        address = 16'h7E00;
        control = 0;
        
        #80 reset = 1;
        #80 control = 2; enable = 1; 
        #80 control = 0; enable = 0;
        #80 address = 16'h7E01; data_in = 16'h01E0; control = 2; enable = 1;
        #80 control = 0; enable = 0;
        
        #80 control = 1; enable = 1; address = 16'h7E01;
        #80 control = 1; enable = 1; address = 16'h7E00; 
        #80 control = 0; enable = 0;
        
        #80 address = 16'h7E01; data_in = 16'h01E1; control = 2; enable = 1;
        #80 control = 0; enable = 0;
        
        #4000 control = 1; enable = 1; address = 16'h7E01;
        #80 control = 1; enable = 1; address = 16'h7E00; 
        #80 control = 0; enable = 0;
        
        #80 address = 16'h7E01; data_in = 16'h01E0; control = 2; enable = 1;
        #80 address = 16'h7E01; data_in = 16'h01E1; control = 2; enable = 1;
        #80 control = 0; enable = 0;
    end
    
    initial
    begin
        #40 miso = 0;
        #560 miso = 1;
        #160 miso = 0;
        #160 miso = 1;
        #160 miso = 0;
        #160 miso = 1;
        #160 miso = 0;
        #160 miso = 1;
        #160 miso = 0;
        #160 miso = 1;
        #160 miso = 0;
        #160 miso = 1;
        #160 miso = 0;
        #160 miso = 1;
        #160 miso = 0;
        #160 miso = 1;
        #160 miso = 0;
    end
endmodule
