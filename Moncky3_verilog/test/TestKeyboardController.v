`timescale 1us / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.05.2021 10:58:11
// Design Name: 
// Module Name: TestKeyboardController
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestKeyboardController();
    reg kb_clk;
    reg kb_data;
    reg clk_12_5MHZ;
    reg clk_mem;
    reg enable;
    reg reset;
    reg [15:0] address;
    reg [1:0] control;
    wire [15:0] data_out;

    KeyboardController SUT (
        .kb_clk (kb_clk),
        .kb_data (kb_data),
        .clk_12_5MHZ (clk_12_5MHZ),
        .clk_mem (clk_mem),
        .enable (enable),
        .reset (reset),
        .address (address),
        .control (control),
        .data_out (data_out)
    );
    
    always #0.040 clk_12_5MHZ = ~clk_12_5MHZ;
    always #0.020 clk_mem = ~clk_mem;
    
    reg [1:0] data [0:599];
    reg [10:0] i;
    
    initial
    begin
      #10000
      enable = 1;
    end
    
    initial
    begin
        kb_clk = 1;
        kb_data = 1;
        clk_12_5MHZ = 1;
        clk_mem = 1;
        enable = 0;
        reset = 0;
        address = 16'h4000;
        control = 1;
        
        #8 reset = 1;
    
      $readmemb("escapeLog.txt", data);
      #5
      
      // start at 5 us
      for(i = 0; i<66; i=i+1)
      begin
        kb_data = data[i][0];
        kb_clk = data[i][1];
        #5;
      end
      
      #100 // start at 435 us
      $readmemb("escapeReleaseLog.txt", data);
      for(i = 0; i<211; i=i+1)
      begin
        kb_data = data[i][0];
        kb_clk = data[i][1];
        #5;
      end
      
      #100 // start at 1590 us
      $readmemb("arrowRightLog.txt", data);
      for(i = 0; i<212; i=i+1)
      begin
        kb_data = data[i][0];
        kb_clk = data[i][1];
        #5;
      end
      
      #100 // start at 2750 us
      $readmemb("arrowRightReleaseLog.txt", data);
      for(i = 0; i<357; i=i+1)
      begin
        kb_data = data[i][0];
        kb_clk = data[i][1];
        #5;
      end
      
      #100 // start at 4635 us
      $readmemb("prtscrLog.txt", data);
      for(i = 0; i<531; i=i+1)
      begin
        kb_data = data[i][0];
        kb_clk = data[i][1];
        #5;
      end
      
      #100 // start at 7390 us
      $readmemb("prtscrReleaseLog.txt", data);
      for(i = 0; i<221; i=i+1)
      begin
        kb_data = data[i][0];
        kb_clk = data[i][1];
        #5;
      end
      
      #100 // start at 8595 us
      $readmemb("prtscrReleaseErrorLog.txt", data);
      for(i = 0; i<331; i=i+1)
      begin
        kb_data = data[i][0];
        kb_clk = data[i][1];
        #5;
      end

    end
endmodule
