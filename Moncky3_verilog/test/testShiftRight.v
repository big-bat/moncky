`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.04.2021 10:38:26
// Design Name: 
// Module Name: testShiftRight
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testShiftRight();
  reg [15:0] A;
  reg [3:0] B;
  reg Cin;
  wire [15:0] F;
  wire zero;
  wire sign;
  wire Cout;
  wire parity;
  
  localparam period = 1;  
 
  shiftRight UUT (
    .A (A),
    .B (B),
    .Cin (Cin),
    .F (F),
    .zero (zero),
    .sign (sign),
    .Cout (Cout),
    .parity (parity)
  );
  
  reg [15:0] expected;
  
   initial
    begin
      $display("************** TEST STARTED **************");
      
      B = 0;
      Cin = 0;
      for(A=0; A<64000; A=A+4000)
      begin
        expected = A & 16'hFFFF;
        #period
        if (F != expected) $display("Error for B=0");
        if ((expected == 0) && (zero == 0)) $display("Error for B=0: zero flag");
        if (Cout == 1) $display("Error for B=0: carry flag");
      end
      
      B = 1;
      Cin = 0;
      for(A=0; A<64000; A=A+4000)
      begin
        expected = (A >> 1) & 16'hFFFF;
        #period
        if (F != expected) $display("Error for B=1");
        if ((expected == 0) && (zero == 0)) $display("Error for B=1: zero flag");
        if (Cout != A[0]) $display("Error for B=1: carry flag");
      end
      
      B = 1;
      Cin = 1;
      for(A=0; A<64000; A=A+4000)
      begin
        expected = ((A >> 1) + 16'h8000) & 16'hFFFF;
        #period
        if (F != expected) $display("Error for B=1 & C=1");
        if ((expected == 0) && (zero == 0)) $display("Error for B=1 & C=1: zero flag");
        if (Cout != A[0]) $display("Error for B=1 & C=1: carry flag");
      end
      
      B = 2;
      Cin = 0;
      for(A=0; A<64000; A=A+4000)
      begin
        expected = (A >> 2) & 16'hFFFF;
        #period
        if (F != expected) $display("Error for B=2");
        if ((expected == 0) && (zero == 0)) $display("Error for B=2: zero flag");
        if (Cout != A[1]) $display("Error for B=2: carry flag");
      end
      
      B = 2;
      Cin = 1;
      for(A=0; A<64000; A=A+4000)
      begin
        expected = ((A >> 2) + 16'hC000) & 16'hFFFF;
        #period
        if (F != expected) $display("Error for B=2 & C=1");
        if ((expected == 0) && (zero == 0)) $display("Error for B=2 & C=1: zero flag");
        if (Cout != A[1]) $display("Error for B=2 & C=1: carry flag");
      end
      
      B = 8;
      Cin = 0;
      for(A=0; A<64000; A=A+4000)
      begin
        expected = (A >> 8) & 16'hFFFF;
        #period
        if (F != expected) $display("Error for B=8");
        if ((expected == 0) && (zero == 0)) $display("Error for B=8: zero flag");
        if (Cout != A[7]) $display("Error for B=8: carry flag");
      end
      
      B = 8;
      Cin = 1;
      for(A=0; A<64000; A=A+4000)
      begin
        expected = ((A >> 8) + 16'hFF00) & 16'hFFFF;
        #period
        if (F != expected) $display("Error for B=8 & C=1");
        if ((expected == 0) && (zero == 0)) $display("Error for B=8 & C=1: zero flag");
        if (Cout != A[7]) $display("Error for B=8 & C=1: carry flag");
      end
      
    end
endmodule
