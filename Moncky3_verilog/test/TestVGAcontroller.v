`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2021 09:23:19
// Design Name: 
// Module Name: TestVGAcontroller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TestVGAcontroller();
    reg clk_25MHZ;
    reg reset;
    reg [15:0] data;
    wire [15:0] address;
    wire h_sync, v_sync;
    wire [5:0] red;
    wire [4:0] green;
    wire [4:0] blue;
 
    VGAcontroller SUT (
        clk_25MHZ, reset, data, address, h_sync, v_sync, red, green, blue
    );
    
    always #20 clk_25MHZ = ~clk_25MHZ;
 
    initial
    begin
      clk_25MHZ = 0;
      data = 16'hFC03;
      reset = 1;    
    end
endmodule
