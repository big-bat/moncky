`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: ALU
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module ALUdecoder (
    input [3:0] op,
    output reg [6:0] D
);
    reg [6:0] my_rom [0:15];

    always @ (op)
    begin
        D = my_rom[op];
    end

    initial begin
        my_rom[0] = 7'h28;
        my_rom[1] = 7'h38;
        my_rom[2] = 7'h2c;
        my_rom[3] = 7'h18;
        my_rom[4] = 7'h64;
        my_rom[5] = 7'h58;
        my_rom[6] = 7'h1;
        my_rom[7] = 7'h2;
        my_rom[8] = 7'h3;
        my_rom[9] = 7'h14;
        my_rom[10] = 7'h58;
        my_rom[11] = 7'h64;
        my_rom[12] = 7'h58;
        my_rom[13] = 7'h1;
        my_rom[14] = 7'h2;
        my_rom[15] = 7'h0;
    end
endmodule

module carryDecoder (
    input [3:0] op,
    output reg [1:0] D
);
    reg [1:0] my_rom [0:15];

    always @ (op) begin
      D = my_rom[op];
    end

    initial begin
        my_rom[0] = 2'h0;
        my_rom[1] = 2'h0;
        my_rom[2] = 2'h0;
        my_rom[3] = 2'h0;
        my_rom[4] = 2'h0;
        my_rom[5] = 2'h1;
        my_rom[6] = 2'h0;
        my_rom[7] = 2'h0;
        my_rom[8] = 2'h0;
        my_rom[9] = 2'h0;
        my_rom[10] = 2'h1;
        my_rom[11] = 2'h2;
        my_rom[12] = 2'h2;
        my_rom[13] = 2'h2;
        my_rom[14] = 2'h2;
        my_rom[15] = 2'h0;
    end
endmodule

module ALU (
  input [15:0] A,
  input [15:0] B,
  input [3:0] op,
  input Cin,
  output reg [15:0] F,
  output reg zero,
  output reg sign,
  output reg Cout,
  output reg overflow_parity
  );
  
  reg carry;
  wire [15:0] a = (op==4'b1010)?16'b0000_0000_0000_0000:A;
  
  wire [15:0] F_alu, F_shl, F_shr, F_ashr;
  wire zero_alu, zero_shl, zero_shr, zero_ashr;
  wire sign_alu, sign_shl, sign_shr, sign_ashr;
  wire cout_alu, cout_shl, cout_shr, cout_ashr;
  wire opar_alu, opar_shl, opar_shr, opar_ashr;
  
  wire [6:0] aluDecoderOutput;
  wire [1:0] carryDecoderOutput;

  ALUdecoder aludecoder(op,aluDecoderOutput);
  carryDecoder carryDec(op, carryDecoderOutput);
  
  wire [3:0] S = aluDecoderOutput[5:2];
  wire M = aluDecoderOutput[6];
  
  always @ (carryDecoderOutput, Cin)
    case (carryDecoderOutput)
      2'b00: carry = 1'b0;
      2'b01: carry = 1'b1;
      2'b10: carry = Cin;
      2'b11: carry = 1'b0;
    endcase

  My74181_16 alu16 (a,B,S,carry,M,F_alu,zero_alu,sign_alu,cout_alu,opar_alu);
  shiftLeft shl (a,B[3:0],carry,F_shl,zero_shl,sign_shl,cout_shl,opar_shl);
  shiftRight shr (a,B[3:0],carry,F_shr,zero_shr,sign_shr,cout_shr,opar_shr);
  shiftRight ashr (a,B[3:0],A[15],F_ashr,zero_ashr,sign_ashr,cout_ashr,opar_ashr);
  
  always @ (aluDecoderOutput, F_alu, F_shl, F_shr, F_ashr)
    case (aluDecoderOutput[1:0])
      2'b00: F = F_alu;
      2'b01: F = F_shl;
      2'b10: F = F_shr;
      2'b11: F = F_ashr;
    endcase
    
   always @ (aluDecoderOutput, zero_alu, zero_shl, zero_shr, zero_ashr)
    case (aluDecoderOutput[1:0])
      2'b00: zero = zero_alu;
      2'b01: zero = zero_shl;
      2'b10: zero = zero_shr;
      2'b11: zero = zero_ashr;
    endcase
    
    always @ (aluDecoderOutput, sign_alu, sign_shl, sign_shr, sign_ashr)
    case (aluDecoderOutput[1:0])
      2'b00: sign = sign_alu;
      2'b01: sign = sign_shl;
      2'b10: sign = sign_shr;
      2'b11: sign = sign_ashr;
    endcase
    
    always @ (aluDecoderOutput, cout_alu, cout_shl, cout_shr, cout_ashr)
    case (aluDecoderOutput[1:0])
      2'b00: Cout = cout_alu;
      2'b01: Cout = cout_shl;
      2'b10: Cout = cout_shr;
      2'b11: Cout = cout_ashr;
    endcase
    
    always @ (aluDecoderOutput, opar_alu, opar_shl, opar_shr, opar_ashr)
    case (aluDecoderOutput[1:0])
      2'b00: overflow_parity = opar_alu;
      2'b01: overflow_parity = opar_shl;
      2'b10: overflow_parity = opar_shr;
      2'b11: overflow_parity = opar_ashr;
    endcase
endmodule
