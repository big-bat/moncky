`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: KeyboardInterface
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


module KeyboardInterface(
    input clk_12_5MHZ,
    input reset,
    input clk_in,
    input data_in,
    output [7:0] data_out,
    output data_ready,
    output error
    );
    
    localparam timeout = 8191; // for watchdog (655.36 us)
    
    reg state;
    wire newState;
    always @(posedge clk_12_5MHZ)
    begin
      state <= reset?newState:0;  
    end
    
    reg [13:0] watchDog;
    wire kick;
    always @(posedge clk_12_5MHZ)
    begin
      watchDog <= (kick|(~reset)|(~state))?timeout:(watchDog - 1);  
    end
    
    wire watchDogReset = state & (watchDog == 0);
    
    reg lastDataIn;
    always @(posedge clk_12_5MHZ)
    begin
      lastDataIn <= data_in;
    end
    wire negEdgeData = (~data_in) & lastDataIn;
    
    reg lastClockIn;
    always @(posedge clk_12_5MHZ)
    begin
      lastClockIn <= clk_in;
    end
    wire posEdgeClock = (~lastClockIn) & clk_in;
    wire negEdgeClock = lastClockIn & (~clk_in);
    
    reg [3:0] counter;
    wire resetCounter;
    always @ (posedge clk_12_5MHZ)
    begin
      if (resetCounter | (~reset))
      begin
        counter <= 0;
      end else
      begin
        if (posEdgeClock) counter <= counter + 1;
      end
    end
    
    wire counterIsEleven = (counter == 11);
    
    wire A = state;
    wire B = negEdgeData;
    wire C = counterIsEleven;
    wire D = watchDogReset;
    assign newState = (~D) & ( (B & (~C)) | (A & (~C)) | (A & B) ); 
    assign kick =  negEdgeClock;
    assign resetCounter = ~state;
    
    reg [15:0] buffer;
    always @(posedge clk_12_5MHZ)
    begin
      if (negEdgeClock) buffer[counter] <= data_in;
    end
    
    assign data_out = buffer[8:1];
    
    wire parity = ~(^buffer[8:1]);
    assign data_ready = (~state) & (parity==buffer[9]) & (buffer[0] == 0) & (buffer[10] == 1);
    assign error = ((~state) & ((parity!=buffer[9]) | (buffer[0] != 0) | (buffer[10] != 1))) | watchDogReset;
    
    initial
    begin
      state <= 0;
      counter <= 0;
      watchDog <= timeout;
      lastDataIn <= 0;
      buffer <= 0;
    end
endmodule
