`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: KeyboardBuffer
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


// TODO: reset will not reset this keyboardbuffer because the statement is in an "if".  This needs to be changed.

module KeyboardBuffer(
    input clk_12_5MHZ,
    input [7:0] data_in,
    input extended,
    input released,
    input data_ready,
    
    input clk_mem,
    input select,
    input reset,
    output reg [15:0] data_out
    );
    
    reg [15:0] buffer [0:15];
    reg [3:0] startQueue, endQueue;
    
    reg lastDataReady;
    always @(posedge clk_12_5MHZ)
    begin
      lastDataReady <= data_ready;
    end
    wire posEdgeDataReady = data_ready & (~lastDataReady);
    
    wire [15:0] fullData;
    assign fullData[7:0] = data_in;
    assign fullData[8] = extended;
    assign fullData[9] = released;
    assign fullData[15:10] = 0;
    always @ (posedge clk_12_5MHZ)
    begin
        if (posEdgeDataReady)
        begin
            buffer[endQueue] <= fullData;
            endQueue <= reset?(endQueue + 1):4'h0;
        end
    end
    
    wire bufferEmpty = (startQueue == endQueue);
    always @ (posedge clk_mem)
    begin
        if (select)
        begin
            data_out <= bufferEmpty?16'h0000:buffer[startQueue];
            startQueue <= reset?(bufferEmpty?startQueue:(startQueue + 1)):4'h0;
        end
    end
    
    initial
    begin
        startQueue = 0;
        endQueue = 0;
    end
endmodule
