`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: VGAcontroller
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


module VGAcounters (
    input clk,
    output reg [9:0] x, y, // current x,y
    output [9:0] xx, yy // next x,y
    );
    
    wire newLine = (x>798);
    wire vertRetrace = (y>523);
    assign xx = (newLine)?0:(x+1);
    assign yy = (~newLine)?y:((vertRetrace)?0:(y+1));
    
    always @ (posedge clk)
    begin
      x <= xx;
      y <= yy;
    end
    
    initial
    begin
      x <= 0;
      y <= 0;
    end
endmodule

module VGAsyncGenerator (
    input [9:0] x, y,
    output h_sync, v_sync
    );
    assign h_sync = (x < 656 || x > 751)?1:0;
    // v_sync is moved 40 cycles so the screen is centered
    // normally: assign v_sync = (y < 490 || y > 491)?1:0;
    assign v_sync = (y < 450 || y > 451)?1:0;
endmodule

module address_generator (
    input [9:0] x, y,
    input buffer,
    output [15:0] address
    );
    wire [9:0] y1, y2, x1, s;
    assign y1[9:2] = y[8:1];
    assign y1[1:0] = 2'b00;
    assign y2[9] = 0;
    assign y2[8:0] = y[9:1];
    assign x1[9:3] = 7'b0000000;
    assign x1[2:0] = x[9:7];
    assign s = x1 + y1 + y2;
    assign address[15] = buffer;
    assign address[14:5] = s;
    assign address[4:0] = x[6:2];
endmodule

module color_generator (
    input [15:0] data,
    input [9:0] x,
    input in_screen,
    output [5:0] red,
    output [4:0] green,
    output [4:0] blue
    );
    assign red[2:0] = in_screen?red[5:3]:0;
    assign red[5:3] = in_screen?((x[1]==0)?data[7:5]:data[15:13]):0;
    assign green[1:0] = in_screen?green[4:3]:0;
    assign green[4:2] = in_screen?((x[1]==0)?data[4:2]:data[12:10]):0;
    assign blue[0] = in_screen?blue[4]:0;
    assign blue[2:1] = in_screen?blue[4:3]:0;
    assign blue[4:3] = in_screen?((x[1]==0)?data[1:0]:data[9:8]):0;
endmodule

module VGAcontroller(
    input clk_25MHZ,
    input reset,
    input [15:0] data,
    output [15:0] address,
    output h_sync, v_sync,
    output [5:0] red,
    output [4:0] green,
    output [4:0] blue,
    output vert_retrace
    );
    
    wire [9:0] x, y; // current x,y
    wire [9:0] xx, yy; // next x,y
    
    reg buffer; // which buffer to show on the screen
      
    VGAcounters counters (
        .clk (clk_25MHZ),
        .x (x),
        .y (y),
        .xx (xx),
        .yy (yy)
    );
    
    VGAsyncGenerator syncGen (
        .x (x),
        .y (y),
        .h_sync (h_sync),
        .v_sync (v_sync)
    );
    
    assign vert_retrace = (y > 399);
    
    wire inScreen = (x < 640) & (y < 400);
    wire nextInScreen = (xx < 640) & (yy < 400);
    
    wire [15:0] mem_addr;
    address_generator addrGen (
        .x (xx),
        .y (yy),
        .buffer (buffer),
        .address(mem_addr)
    );
    
    // if not in screen, load config
    assign address = nextInScreen?mem_addr:16'h7D00;

    always @ (posedge clk_25MHZ)
    begin
        if (inScreen == 0)
        begin
          buffer <= data[0];
        end
    end
    
    color_generator colGen (
        .data (data),
        .x (x),
        .in_screen (inScreen),
        .red (red),
        .green (green),
        .blue (blue)
    );
    
    initial
    begin
      buffer = 0;
    end
    
endmodule
