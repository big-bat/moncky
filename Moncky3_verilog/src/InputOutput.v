`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: InputOutput
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////



module InputOutput(
    input we,
    input clk_mem,
    input clk_video,
    input clk_io,
    input reset,
    input [15:0] address,
    input [15:0] data_in,
    input [1:0] control,
    output reg [15:0] data_out,
    output interrupt,
    output [15:0] vga_connector,
    input [7:0] ps2_connector,
    input spi_miso,
    output spi_sck,
    output spi_mosi,
    output [3:0] spi_ss
    );
    
    wire [15:0] video_address;
    wire [15:0] video_data;
    
    wire [15:0] vram_data;
    wire writeEnable = we & control[1];
    
    DualPortRam16 vram (
        .din (data_in),
        .addr1 (address),
        .addr2 (video_address),
        .we (writeEnable),
        .clk1 (clk_mem),
        .clk2 (clk_video),
        .dout1 (vram_data),
        .dout2 (video_data)
    );
    
    wire [5:0] red;
    wire [4:0] green;
    wire [4:0] blue;
    wire h_sync, v_sync;
    
    VGAcontroller vga (
        .clk_25MHZ (clk_video),
        .reset (reset),
        .data (video_data),
        .address (video_address),
        .h_sync (h_sync),
        .v_sync (v_sync),
        .red (red),
        .green (green),
        .blue (blue),
        .vert_retrace (interrupt)
    );
        
    assign vga_connector[0] = green[1];
    assign vga_connector[1] = green[2];
    assign vga_connector[2] = green[3];
    assign vga_connector[3] = green[4];
    assign vga_connector[4] = h_sync;
    assign vga_connector[5] = v_sync;
    assign vga_connector[6] = 0;
    assign vga_connector[7] = 0;
    assign vga_connector[8] = red[2];
    assign vga_connector[9] = red[3];
    assign vga_connector[10] = red[4];
    assign vga_connector[11] = red[5];
    assign vga_connector[12] = blue[1];
    assign vga_connector[13] = blue[2];
    assign vga_connector[14] = blue[3];
    assign vga_connector[15] = blue[4];
    
    wire kb_clk = ps2_connector[6];
    wire kb_data = ps2_connector[4];
    wire [15:0] keyboard_data;
   
    KeyboardController kb_ctrl (
        .kb_clk (kb_clk),
        .kb_data (kb_data),
        .clk_12_5MHZ (clk_io),
        .clk_mem (clk_mem),
        .enable (we),
        .reset (reset),
        .address (address),
        .control (control),
        .data_out (keyboard_data)
    );
    
    wire [15:0] spi_data;
    wire spi_ready;
    
    SPIcontroller spi_ctrl (
        .miso (spi_miso),
        .sck (spi_sck),
        .mosi (spi_mosi),
        .ss (spi_ss),
        .ready (spi_ready),
        .clk_mem (clk_mem),
        .clk_io (clk_io),
        .enable (we),
        .reset (reset),
        .data_in (data_in),
        .address (address),
        .control (control),
        .data_out (spi_data)
    );
    
    reg [4:0] clk_counter;
    reg [13:0] clk_counter2;
    reg [31:0] us_counter;
    reg [31:0] ms_counter;
    always @ (posedge clk_io)
    begin
        clk_counter = (~reset)?0:((clk_counter==24)?0:(clk_counter + 1));
        clk_counter2 = (~reset)?0:((clk_counter2==12499)?0:(clk_counter2 + 1));
        us_counter = (~reset)?0:((clk_counter==0)?(us_counter+2):us_counter);
        ms_counter = (~reset)?0:((clk_counter2==0)?(ms_counter+1):ms_counter);
    end
    
    always @(*)
    begin
        if (address == 16'h7E02) data_out <= us_counter[15:0];
        else if (address == 16'h7E03) data_out <= us_counter[31:16];
        else if (address == 16'h7E04) data_out <= ms_counter[15:0];
        else if (address == 16'h7E05) data_out <= ms_counter[31:16];
        else if (address == 16'h7D01) data_out <= keyboard_data;
        else if (address == 16'h7E00) data_out <= spi_data;
        else if (address == 16'h7E01) data_out <= spi_data;
        else data_out <= vram_data;
    end
    
    initial
    begin
      clk_counter = 0;
      clk_counter2 = 0;
      us_counter = 0;
      ms_counter = 0;
    end
endmodule
