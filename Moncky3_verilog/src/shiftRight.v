`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: ShiftRight
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


module shiftRight (
  input [15:0] A,
  input [3:0] B,
  input Cin,
  output reg [15:0] F,
  output zero,
  output sign,
  output reg Cout,
  output parity
);

always @ (A,B,Cin)
begin
    case(B)
        4'b0000: begin F=A; Cout=0; end
        4'b0001: begin F[14:0]=A[15:1]; F[15]=Cin; Cout=A[0]; end
        4'b0010: begin F[13:0]=A[15:2]; F[15:14]={2{Cin}}; Cout=A[1]; end
        4'b0011: begin F[12:0]=A[15:3]; F[15:13]={3{Cin}}; Cout=A[2]; end
        4'b0100: begin F[11:0]=A[15:4]; F[15:12]={4{Cin}}; Cout=A[3]; end
        4'b0101: begin F[10:0]=A[15:5]; F[15:11]={5{Cin}}; Cout=A[4]; end
        4'b0110: begin F[9:0]=A[15:6]; F[15:10]={6{Cin}}; Cout=A[5]; end
        4'b0111: begin F[8:0]=A[15:7]; F[15:9]={7{Cin}}; Cout=A[6]; end
        4'b1000: begin F[7:0]=A[15:8]; F[15:8]={8{Cin}}; Cout=A[7]; end
        4'b1001: begin F[6:0]=A[15:9]; F[15:7]={9{Cin}}; Cout=A[8]; end
        4'b1010: begin F[5:0]=A[15:10]; F[15:6]={10{Cin}}; Cout=A[9]; end
        4'b1011: begin F[4:0]=A[15:11]; F[15:5]={11{Cin}}; Cout=A[10]; end
        4'b1100: begin F[3:0]=A[15:12]; F[15:4]={12{Cin}}; Cout=A[11]; end
        4'b1101: begin F[2:0]=A[15:13]; F[15:3]={13{Cin}}; Cout=A[12]; end
        4'b1110: begin F[1:0]=A[15:14]; F[15:2]={14{Cin}}; Cout=A[13]; end
        4'b1111: begin F[0]=A[15]; F[15:1]={15{Cin}}; Cout=A[14]; end
    endcase
end
    assign zero = (F == 0);
    assign sign = F[15];
    assign parity = ^F;
endmodule
