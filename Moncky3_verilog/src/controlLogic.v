`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: ControlLogic
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module Group0decoder (
    input [3:0] instruction,
    output reg [28:0] signals
);
    reg [28:0] my_rom [0:15];

    always @ (instruction)
    begin
        signals = my_rom[instruction];
    end

    initial begin
        my_rom[0] = 29'h3a60002;
        my_rom[1] = 29'h5a60551;
        my_rom[2] = 29'h1860001;
        my_rom[3] = 29'h1860002;
        my_rom[4] = 29'hfac0541;
        my_rom[5] = 29'h13b76002;
        my_rom[6] = 29'h13bf6002;
        my_rom[7] = 29'd0;
        my_rom[8] = 29'd0;
        my_rom[9] = 29'd0;
        my_rom[10] = 29'd0;
        my_rom[11] = 29'd0;
        my_rom[12] = 29'd0;
        my_rom[13] = 29'd0;
        my_rom[14] = 29'd0;
        my_rom[15] = 29'd0;
    end
endmodule

module Group1decoder (
    input [4:0] instruction,
    output reg [28:0] signals
);
    reg [28:0] my_rom [0:31];

    always @ (instruction)
    begin
        signals = my_rom[instruction];
    end

    initial begin
        my_rom[0] = 29'h0;
        my_rom[1] = 29'h10910400;
        my_rom[2] = 29'head0540;
        my_rom[3] = 29'h11b14400;
        my_rom[4] = 29'h2a11c00;
        my_rom[5] = 29'h81040c;
        my_rom[6] = 29'h1a10400;
        my_rom[7] = 29'h0;
        my_rom[8] = 29'h0;
        my_rom[9] = 29'h0;
        my_rom[10] = 29'h0;
        my_rom[11] = 29'h0;
        my_rom[12] = 29'h0;
        my_rom[13] = 29'h0;
        my_rom[14] = 29'h0;
        my_rom[15] = 29'h0;
        my_rom[16] = 29'h2a11900;
        my_rom[17] = 29'h2a11900;
        my_rom[18] = 29'h2a11900;
        my_rom[19] = 29'h2a11900;
        my_rom[20] = 29'h2a11900;
        my_rom[21] = 29'h2a11900;
        my_rom[22] = 29'h2a11900;
        my_rom[23] = 29'h2a11900;
        my_rom[24] = 29'h1c10400;
        my_rom[25] = 29'h1c10400;
        my_rom[26] = 29'h1c10400;
        my_rom[27] = 29'h1c10400;
        my_rom[28] = 29'h1c10400;
        my_rom[29] = 29'h1c10400;
        my_rom[30] = 29'h1c10400;
        my_rom[31] = 29'h1c10400;
    end
endmodule

module Group2decoder (
    input [3:0] instruction,
    output reg [28:0] signals
    );
    
    reg [28:0] my_rom [0:15];

    always @ (instruction)
    begin
        signals = my_rom[instruction];
    end

    initial begin
        my_rom[0] = 29'h0;
        my_rom[1] = 29'hea08000;
        my_rom[2] = 29'h10008000;
        my_rom[3] = 29'h0;
        my_rom[4] = 29'h0;
        my_rom[5] = 29'h0;
        my_rom[6] = 29'h0;
        my_rom[7] = 29'h0;
        my_rom[8] = 29'h0;
        my_rom[9] = 29'h0;
        my_rom[10] = 29'h0;
        my_rom[11] = 29'h0;
        my_rom[12] = 29'h0;
        my_rom[13] = 29'h0;
        my_rom[14] = 29'h0;
        my_rom[15] = 29'h0;
    end
endmodule

module Group3decoder (
    input [3:0] instruction,
    output reg [28:0] signals
    );
    
    reg [28:0] my_rom [0:15];

    always @ (instruction)
    begin
        signals = my_rom[instruction];
    end

    initial begin
        my_rom[0] = 29'h0;
        my_rom[1] = 29'h2a008f0;
        my_rom[2] = 29'h4a00d10;
        my_rom[3] = 29'h4a01144;
        my_rom[4] = 29'h4a00924;
        my_rom[5] = 29'h4a00914;
        my_rom[6] = 29'h801154;
        my_rom[7] = 29'h801254;
        my_rom[8] = 29'h4a088f4;
        my_rom[9] = 29'h8088f4;
        my_rom[10] = 29'h4a015f4;
        my_rom[11] = 29'h8015f4;
        my_rom[12] = 29'hea28840;
        my_rom[13] = 29'h10828840;
        my_rom[14] = 29'hea21540;
        my_rom[15] = 29'h10821540;
    end
endmodule

module InterruptControl(
    input clk,
    input enable,
    input [1:0] control,
    input interrupt,
    output granted
    );
    
    reg intEnabled;
    reg lastInt;
    reg intGranted;
    
    assign granted = intGranted;
    
    wire controlNotZero = |control;
    wire posedgeInterrupt = (~lastInt) & interrupt;
    wire grant = posedgeInterrupt & intEnabled;
   
    always @ (negedge clk)
    begin
        if (enable)
        begin
            if (controlNotZero) intEnabled <= control[0];
            lastInt <= interrupt;
            intGranted <= grant;
        end
    end
    
    initial
    begin
      intEnabled <= 0;
      lastInt <= 0;
      intGranted <= 0;
    end
endmodule

module InstructionReader(
    input clk,
    input enable,
    input [15:0] instruction,
    input reset,
    input intGranted,
    output [1:0] group,
    output [15:0] realInstruction,
    output [3:0] opcode4,
    output [4:0] opcode5,
    output [3:0] r, s, t,
    output [2:0] flagSelect
    );
    
    reg rst;
    
    always @ (negedge clk)
    begin
      if (enable) rst <= reset;
    end
    
    wire [15:0] instrAfterInt = (intGranted==0)?instruction:16'h6000;
    wire [15:0] instrAfterReset = (rst==0)?16'h0000:instrAfterInt;
    
    assign realInstruction = instrAfterReset;
    
    wire group3detect = |instrAfterReset[3:0];
    wire group2detect = |instrAfterReset[6:4];
    wire group1detect = |instrAfterReset[11:7];
    
    assign group = (group3detect==1)?2'b11:((group2detect==1)?2'b10:((group1detect==1)?1:0));
    
    assign opcode4 = instrAfterReset[3:0];
    assign opcode5 = instrAfterReset[11:7];
    assign r = instrAfterReset[15:12];
    assign s = instrAfterReset[11:8];
    assign t = instrAfterReset[7:4];
    assign flagSelect = instrAfterReset[9:7];
    
    initial
    begin
      rst = 0;
    end
endmodule

module InstructionDecoder(
    input [3:0] instr0,
    input [4:0] instr1,
    input [3:0] instr2,
    input [3:0] instr3,
    input [1:0] group,
    output reg [28:0] signals
    );
    
    wire [28:0] group0signals, group1signals, group2signals, group3signals;
    
    Group0decoder g0d(instr0, group0signals);
    Group1decoder g1d(instr1, group1signals);
    Group2decoder g2d(instr2, group2signals);
    Group3decoder g3d(instr3, group3signals);
    
    always @ (group, group0signals, group1signals, group2signals, group3signals)
    begin
      case (group)
        2'b00 : signals = group0signals;
        2'b01 : signals = group1signals;
        2'b10 : signals = group2signals;
        2'b11 : signals = group3signals;
      endcase
    end
endmodule

module FlagsRegister (
    input clk,
    input enable,
    input [3:0] flags_input,
    input [3:0] fromRegister,
    input restoreFlags,
    input saveFlags,
    input [2:0] selectFlag,
    output reg [3:0] flags_output,
    output reg selectedFlag
    );
    
    wire [3:0] flagsToStore = (restoreFlags==0)?flags_input:fromRegister;
    
    always @ (negedge clk)
    begin
      if (saveFlags & enable) flags_output <= flagsToStore;
    end
    
    always @ (selectFlag, flags_output)
    begin
      case(selectFlag)
        3'b000 : selectedFlag = flags_output[0];
        3'b001 : selectedFlag = ~flags_output[0];
        3'b010 : selectedFlag = flags_output[1];
        3'b011 : selectedFlag = ~flags_output[1];
        3'b100 : selectedFlag = flags_output[2];
        3'b101 : selectedFlag = ~flags_output[2];
        3'b110 : selectedFlag = flags_output[3];
        3'b111 : selectedFlag = ~flags_output[3];
      endcase
    end
    
    initial
    begin
      flags_output = 0;
    end
endmodule

module SignalDecoder(
    input [28:0] signals,
    input flag,
    input [3:0] flags,
    input [3:0] r, s, t,
    output [1:0] dataram,
    output [1:0] registerMux,
    output reg [3:0] inputSelect,
    output reg writeEnable,
    output [1:0] spControl,
    output reg [3:0] select0, select1,
    output [3:0] select2,
    output reg [15:0] immediate,
    output aluA, aluB,
    output [3:0] aluOperation,
    output restoreFlags,
    output saveFlags,
    output [1:0] intControl
    );
    
    wire [15:0] uint8, uint16, int8, uint4, uint1f, uint4f;
    
    assign uint8[15:8] = 8'b0000_0000;
    assign uint8[3:0] = t;
    assign uint8[7:4] = s;
    assign uint16[15:12] = s;
    assign uint16[11:8] = t;
    assign uint16[7:0] = 8'b0000_0000;
    assign int8[15:8] = {8{s[3]}};
    assign int8[3:0] = t;
    assign int8[7:4] = s;
    assign uint4[15:4] = 12'b0000_0000_0000;
    assign uint4[3:0] = s;
    assign uint1f = {16{flag}};
    assign uint4f[15:4] = 12'b0000_0000_0000;
    assign uint4f[3:0] = flags;
    
    assign dataram = signals[28:27];
    assign registerMux = signals[26:25];
    
    always @ (signals[24:23], r)
    begin
      case (signals[24:23])
        2'b00 : inputSelect = 4'b0000;
        2'b01 : inputSelect = r; 
        2'b10 : inputSelect = 4'b1110;
        2'b11 : inputSelect = 4'b1111;
      endcase
    end
    
    always @ (signals[22:21], flag)
    begin
      case (signals[22:21])
        2'b00 : writeEnable = 0;
        2'b01 : writeEnable = 1; 
        2'b10 : writeEnable = flag;
        2'b11 : writeEnable = flag;
      endcase
    end
    
    assign spControl = signals[20:19];
    
    always @ (signals[18:17], r, t)
    begin
      case (signals[18:17])
        2'b00 : select0 = r;
        2'b01 : select0 = t; 
        2'b10 : select0 = 4'b1110;
        2'b11 : select0 = 4'b1111;
      endcase
    end
    
    always @ (signals[16:15], r, s)
    begin
      case (signals[16:15])
        2'b00 : select1 = r;
        2'b01 : select1 = s; 
        2'b10 : select1 = 4'b1110;
        2'b11 : select1 = 4'b1111;
      endcase
    end
    
    assign select2 = (signals[14]==0)?r:4'b1111;
    
    always @ (signals[13:10], uint8, uint16, int8, uint4, uint1f, uint4f)
    begin
      case (signals[13:10])
        4'b0000 : immediate = 16'b0000_0000_0000_0000;
        4'b0001 : immediate = 16'b0000_0000_0000_0001; 
        4'b0010 : immediate = uint8;
        4'b0011 : immediate = uint16;
        4'b0100 : immediate = int8;
        4'b0101 : immediate = uint4;
        4'b0110 : immediate = uint1f;
        4'b0111 : immediate = uint4f;
        4'b1000 : immediate = 16'b0000_0000_0001_0000;
        4'b1001 : immediate = 16'b0000_0000_0000_0000;
        4'b1010 : immediate = 16'b0000_0000_0000_0000;
        4'b1011 : immediate = 16'b0000_0000_0000_0000;
        4'b1100 : immediate = 16'b0000_0000_0000_0000;
        4'b1101 : immediate = 16'b0000_0000_0000_0000;
        4'b1110 : immediate = 16'b0000_0000_0000_0000;
        4'b1111 : immediate = 16'b0000_0000_0000_0000;
      endcase
    end
    
    assign aluA = signals[9];
    assign aluB = signals[8];
    
    assign aluOperation = (&signals[7:4]==1)?t:signals[7:4];
    
    assign restoreFlags = signals[3];
    assign saveFlags = signals[2];
    assign intControl = signals[1:0];
endmodule

module controlLogic(
    input clk,
    input enable,
    input [15:0] instruction,
    input reset,
    input interrupt,
    input [3:0] flags,
    input [15:0] register,
    output [1:0] dataram,
    output [1:0] registerMux,
    output [3:0] inputSelect,
    output writeEnable,
    output [1:0] spControl,
    output [3:0] select0, select1, select2,
    output [15:0] immediate,
    output aluA, aluB,
    output [3:0] aluOperation,
    output carry,
    output io_request
    );
    
    wire [1:0] intControl;
    wire interruptGranted;
    wire [1:0] group;
    wire [15:0] realInstruction;
    wire [3:0] opcode4;
    wire [4:0] opcode5;
    wire [3:0] r, s, t;
    wire [2:0] flagSelect;
    wire selectedFlag;
    wire [28:0] signals;
    wire restoreFlags;
    wire saveFlags;
    wire [3:0] storedFlags;
    
    assign io_request = (group == 2);
    
    InterruptControl int_ctrl (
        .clk (clk),
        .enable (enable),
        .control (intControl),
        .interrupt (interrupt),
        .granted (interruptGranted)
    );
    
    InstructionReader instr_reader (
        .clk (clk),
        .enable (enable),
        .instruction (instruction),
        .reset (reset),
        .intGranted (interruptGranted),
        .group (group),
        .realInstruction (realInstruction),
        .opcode4 (opcode4),
        .opcode5 (opcode5),
        .r (r),
        .s (s),
        .t (t),
        .flagSelect (flagSelect)
    );
    
    InstructionDecoder instr_decoder (
        .instr0 (r),
        .instr1 (opcode5),
        .instr2 (t),
        .instr3 (opcode4),
        .group (group),
        .signals (signals)
    );

    FlagsRegister flags_reg (
        .clk (clk),
        .enable (enable),
        .flags_input (flags),
        .fromRegister (register[3:0]),
        .restoreFlags (restoreFlags),
        .saveFlags (saveFlags),
        .selectFlag (flagSelect),
        .flags_output (storedFlags),
        .selectedFlag (selectedFlag)
    );

    SignalDecoder sgnl_decoder (
        .signals (signals),
        .flag (selectedFlag),
        .flags (storedFlags),
        .r (r),
        .s (s),
        .t (t),
        .dataram (dataram),
        .registerMux (registerMux),
        .inputSelect (inputSelect),
        .writeEnable (writeEnable),
        .spControl (spControl),
        .select0 (select0),
        .select1 (select1),
        .select2 (select2),
        .immediate (immediate),
        .aluA (aluA),
        .aluB (aluB),
        .aluOperation (aluOperation),
        .restoreFlags (restoreFlags),
        .saveFlags (saveFlags),
        .intControl (intControl)
    );
    
    assign carry = storedFlags[1];
endmodule
