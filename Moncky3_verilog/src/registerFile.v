`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: RegisterFile
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


// a simple register
module register(
    input clk,
    input [15:0] D,
    input enable,
    output reg [15:0] Q);
    
    always @ (negedge clk)
    begin
      if (enable == 1) Q <= D;
    end
    
    initial
    begin
      Q <= 0;
    end
endmodule

// contains all registers r0 to r15
// r15 acts as a program counter
// r14 acts as a stack pointer
module registerFile(
    input clk,
    input enable, // when set to 0, no state changes will happen
    input [15:0] in, // the data that needs to be stored in a register
    input [3:0] inputSelect, // this selects the register to which the data needs to be sent
    input writeEnable, // when set to 0, no register is written to, when set to 1 the input data is stored in the selected register
    input [1:0] spControl, // controls the behaviour of the stack pointer (r14) and the value read from thr program counter (r15)
    input [3:0] select0, select1, select2, // selects up to three registers
    output [15:0] out0, out1, out2, // the values of the selected registers
    output [15:0] PC, // the value of the program counter
    output [15:0] register13 // the value of r13.  This can be used to control external devices by altering the value of r13
    );

    wire [15:0] registers [0:15];
    wire [15:0] reg15;
    wire [15:0] we;
    reg [15:0] SPtemp;
    wire [15:0] PCtemp;
    wire [15:0] newSP;
    wire [15:0] newPC;
    
    assign we[0] = (inputSelect == 4'h0)? writeEnable : 1'd0;
    assign we[1] = (inputSelect == 4'h1)? writeEnable : 1'd0;
    assign we[2] = (inputSelect == 4'h2)? writeEnable : 1'd0;
    assign we[3] = (inputSelect == 4'h3)? writeEnable : 1'd0;
    assign we[4] = (inputSelect == 4'h4)? writeEnable : 1'd0;
    assign we[5] = (inputSelect == 4'h5)? writeEnable : 1'd0;
    assign we[6] = (inputSelect == 4'h6)? writeEnable : 1'd0;
    assign we[7] = (inputSelect == 4'h7)? writeEnable : 1'd0;
    assign we[8] = (inputSelect == 4'h8)? writeEnable : 1'd0;
    assign we[9] = (inputSelect == 4'h9)? writeEnable : 1'd0;
    assign we[10] = (inputSelect == 4'hA)? writeEnable : 1'd0;
    assign we[11] = (inputSelect == 4'hB)? writeEnable : 1'd0;
    assign we[12] = (inputSelect == 4'hC)? writeEnable : 1'd0;
    assign we[13] = (inputSelect == 4'hD)? writeEnable : 1'd0;
    assign we[14] = (inputSelect == 4'hE)? writeEnable : 1'd0;
    assign we[15] = (inputSelect == 4'hF)? writeEnable : 1'd0;
    
    always @ (spControl, registers[14])
    begin
      case(spControl)
        2'b00: SPtemp = registers[14];
        2'b01: SPtemp = registers[14] + 1;
        2'b10: SPtemp = registers[14] - 1;
        2'b11: SPtemp = registers[14] - 1;
      endcase
    end
    
    assign PCtemp = reg15 + 1;
 
    assign newSP = (we[14]==1'b0)?SPtemp:in;
    assign newPC = (we[15]==1'b0)?PCtemp:in;
    
    register r0(clk, in, we[0] & enable, registers[0]);
    register r1(clk, in, we[1] & enable, registers[1]);
    register r2(clk, in, we[2] & enable, registers[2]);
    register r3(clk, in, we[3] & enable, registers[3]);
    register r4(clk, in, we[4] & enable, registers[4]);
    register r5(clk, in, we[5] & enable, registers[5]);
    register r6(clk, in, we[6] & enable, registers[6]);
    register r7(clk, in, we[7] & enable, registers[7]);
    register r8(clk, in, we[8] & enable, registers[8]);
    register r9(clk, in, we[9] & enable, registers[9]);
    register r10(clk, in, we[10] & enable, registers[10]);
    register r11(clk, in, we[11] & enable, registers[11]);
    register r12(clk, in, we[12] & enable, registers[12]);
    register r13(clk, in, we[13] & enable, registers[13]);
    register r14(clk, newSP, enable, registers[14]);
    register r15(clk, newPC, enable, reg15);
 
    assign registers[15] = (spControl==2'b11)?reg15:PCtemp;
       
    assign out0 = registers[select0];
    assign out1 = registers[select1];
    assign out2 = registers[select2];
    assign PC = reg15;
    assign register13 = registers[13];
endmodule
