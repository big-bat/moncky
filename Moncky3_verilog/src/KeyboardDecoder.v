`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: KeyboardScanCodeDecoder
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module KeyboardScanCodeDecoder (
    input clk_12_5MHZ,
    input reset,
    input [7:0] data,
    input data_ready,
    input error,
    output [7:0] data_out,
    output reg extended,
    output reg released,
    output data_out_ready
    );
    
    // 0 = waiting
    // 1 = extended code
    // 2 = break code
    // 3 = extended break code
    reg [1:0] state;
    reg [1:0] newState;
    always @(posedge clk_12_5MHZ)
    begin
        state = reset?newState:0;
    end
     
    reg lastDataReady;
    always @(posedge clk_12_5MHZ)
    begin
      lastDataReady = data_ready;
    end
    wire negEdgeDataReady = (~data_ready) & lastDataReady;
    
    wire isExtended = (data[7:4] == 4'b1110);
    wire isBreakCode = (data[7:4] == 4'b1111);
    
    wire [1:0] info;
    assign info[1] = error;
    assign info[0] = data_ready;
    wire [1:0] info2;
    assign info2[1] = isExtended;
    assign info2[0] = isBreakCode;
    
    always @ (info, info2, state)
    begin
        case (info)
          2'b00 : newState = state;
          2'b01 : newState = info2;
          2'b10 : newState = 0;
          2'b11 : newState = 0;
        endcase
    end
    
    reg received;
    wire [1:0] extendedControl;
    wire [1:0] releasedControl;
    
    assign extendedControl[0] = (newState == 2);
    assign extendedControl[1] = (newState == 0) & (negEdgeDataReady | error);
    assign releasedControl[0] = (newState == 1);
    assign releasedControl[1] = (newState == 0) & (negEdgeDataReady | error);
     
    always @(posedge clk_12_5MHZ)
    begin
        received = (info == 1 & info2 == 0)?1:0;
        case(extendedControl)
            2'b00 : extended = reset?extended:0;
            2'b01 : extended = reset?1:0;
            2'b10 : extended = 0;
            2'b11 : extended = reset?extended:0;
        endcase
        case(releasedControl)
            2'b00 : released = reset?released:0;
            2'b01 : released = reset?1:0;
            2'b10 : released = 0;
            2'b11 : released = reset?released:0;
        endcase
    end
    
    assign data_out = data;
    assign data_out_ready = data_ready & received;
    
    initial
    begin
      state = 0;
      lastDataReady = 0;
      received = 0;
      extended = 0;
      released = 0;
    end
endmodule
