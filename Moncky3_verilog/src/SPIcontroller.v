`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: SPIcontroller
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module SPIcontroller (
    input miso,
    output sck,
    output mosi,
    output [3:0] ss, // up to 4 slaves
    output ready,
    input clk_mem,
    input clk_io,
    input enable,
    input reset,
    input [15:0] data_in,
    input [15:0] address,
    input [1:0] control,
    output reg [15:0] data_out
    );
    
    reg [15:0] ctrl, data_tx, data_rx;
    wire [7:0] divisor = ctrl[15:8];
    assign ss = ctrl[7:4];
    wire cpol = ctrl[2];
    wire cpha = ctrl[1];
    wire transfer = ctrl[0];
    
    reg state; // 0 = waiting, 1 = transmitting
    reg [7:0] clk_counter;  // to generate SPI SCK
    reg generated_clock; // SPI SCK
    reg last_transfer; // to detect rising edge on 'transfer' bit in ctrl
    wire transfer_posedge = (~last_transfer) & transfer; // indicates a rising edge on 'transfer' bit in ctrl
    wire new_generated_clock = ((~reset)|(~state))?0:((clk_counter==divisor)?(~generated_clock):generated_clock); // next value for SPI SCK
    wire clock_posedge = (~generated_clock) & new_generated_clock; // signals a positive edge on the next clk_io
    wire clock_negedge = generated_clock & (~new_generated_clock); // signals a negative edge on the next clk_io
    wire clock_edge_tx = cpha?clock_posedge:clock_negedge; // when to write the bit to be transmitted
    wire clock_edge_rx = cpha?clock_negedge:clock_posedge; // when to read a received bit
    reg [3:0] bit_counter; // used to know when to stop sending bits
    
    wire new_state = (~reset)?0:(transfer_posedge?1:(bit_counter==0 & clock_negedge)?0:state);
    assign ready = ~state;
    
    reg [15:0] shift_out; // used to send the bits from data_tx
    wire shift = (~cpha)?1:(bit_counter!=4'b0111);
    
    // access from cpu
    always @ (posedge clk_mem)
    begin
        data_out <= (address==16'h7E00)?data_rx:((address==16'h7E01)?{ctrl[15:4], ready, ctrl[2:1], 1'b0}:0);
        if (control[1] & enable & address==16'h7E00) data_tx <= reset?data_in:0;
        if (control[1] & enable & address==16'h7E01) ctrl <= reset?data_in:0;
    end
    
    // access from SPI interface
    always @ (posedge clk_io)
    begin
       state <= new_state;
       
       last_transfer <= transfer;
       
       clk_counter <= (~state)?0:((clk_counter==divisor)?0:(clk_counter+1));
       generated_clock <= new_generated_clock;
       bit_counter <= (~state)?4'b0111:(clock_negedge?(bit_counter-1):bit_counter);
       
       data_rx <= reset?(clock_edge_rx?{data_rx[14:0],miso}:data_rx):0;
       shift_out <= (state==0)?data_tx:((clock_edge_tx & shift)?{shift_out[14:0],1'b0}:shift_out);
    end
    
    assign sck = generated_clock ^ cpol;
    assign mosi = shift_out[7];
    
    initial
    begin
      clk_counter = 0;
      state = 0;
      generated_clock = 0;
      last_transfer = 0;
      ctrl = 0;
      data_tx = 0;
      data_out = 0;
    end
    
endmodule
