`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: KeyboardDebouncer
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module KeyboardDebouncer (
    input clk_12_5MHZ,
    input in,
    output reg debounced
    );
    reg sample0, sample1;
    reg [5:0] counter; // 2.56us
    
    wire resetCounter = sample0 ^ sample1;
    
    always @ (posedge clk_12_5MHZ)
    begin
      sample1 <= sample0;
      sample0 <= in;
      if (resetCounter == 1) counter <= 0; else counter <= counter + 1;
      if (counter == 6'b100000) debounced <= sample1;
    end
    
    initial
    begin
      sample0 <= 0;
      sample1 <= 0;
      counter <= 0;
      debounced <= 0;
    end
endmodule
