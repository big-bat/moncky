`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: KeyboardController
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module KeyboardController (
    input kb_clk,
    input kb_data,
    input clk_12_5MHZ,
    input clk_mem,
    input enable,
    input reset,
    input [15:0] address,
    input [1:0] control,
    output [15:0] data_out
    );
    
    wire k_clk, k_data; // debounced input signals
    KeyboardDebouncer clkDebounce (clk_12_5MHZ, kb_clk, k_clk);
    KeyboardDebouncer dataDebounce (clk_12_5MHZ, kb_data, k_data);
    
    wire [7:0] receivedByte;
    wire byteReceived;
    wire error;
    
    KeyboardInterface kbItf (
        .clk_12_5MHZ (clk_12_5MHZ),
        .reset(reset),
        .clk_in (k_clk),
        .data_in (k_data),
        .data_out (receivedByte),
        .data_ready (byteReceived),
        .error (error)
    );
      
    wire [7:0] scanCode;
    wire isExtended;
    wire isReleased;
    wire scanCodeReady;
    KeyboardScanCodeDecoder kbDcdr (
        .clk_12_5MHZ (clk_12_5MHZ),
        .reset (reset),
        .data (receivedByte),
        .data_ready (byteReceived),
        .error (error),
        .data_out (scanCode),
        .extended (isExtended),
        .released (isReleased),
        .data_out_ready (scanCodeReady)
    );
    
    wire selected = enable & address==16'h7D01 & control[0];
    wire [15:0] data;
    KeyboardBuffer kbBuf (
        .clk_12_5MHZ (clk_12_5MHZ),
        .data_in (scanCode),
        .extended (isExtended),
        .released (isReleased),
        .data_ready (scanCodeReady),
        .clk_mem (clk_mem),
        .select (selected),
        .reset (reset),
        .data_out (data)
        );
    
    assign data_out = data;
    
endmodule
