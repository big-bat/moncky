`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: DualPortRam16
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module DualPortRam16 (
    input [15:0] din,
    input [15:0] addr1,
    input [15:0] addr2,
    input we, clk1, clk2,
    output [15:0] dout1,
    output [15:0] dout2
    );

    reg [15:0] mem[65535:0];
    reg [15:0] data1, data2;

    assign dout1 = data1;
    assign dout2 = data2;

    always @(posedge clk1)
    begin
        data1 <= mem[addr1];
        if (we) mem[addr1] <= din;
    end
    
   always @(posedge clk2)
   begin
       data2 <= mem[addr2];
   end
endmodule
