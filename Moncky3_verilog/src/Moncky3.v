`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: Moncky3
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


module Moncky3 (
    input clk,
    input enable,
    input reset,
    input interrupt,
    input [15:0] codeData,
    input [15:0] dataRead,
    output [15:0] codeAddress,
    output [15:0] dataAddress,
    output [15:0] dataWrite,
    output [1:0] dataRamControl,
    output [15:0] outputs,
    output io_request
    );
    
    wire [3:0] flags;
    wire [15:0] reg0, reg1, reg2;
    wire [1:0] registerMux;
    wire [3:0] inputSelect;
    wire writeEnable;
    wire [1:0] spControl;
    wire [3:0] select0, select1, select2;
    wire [15:0] immediate;
    wire aluA, aluB;
    wire [3:0] aluOperation;
    wire carry;
    reg [15:0] registerInput;
    wire [15:0] aluAvalue, aluBvalue;
    wire [15:0] aluResult;
    wire [15:0] r13;
    
    always @ (registerMux, reg0, immediate, aluResult, dataRead)
    begin
      case(registerMux)
        2'b00 : registerInput = reg0;
        2'b01 : registerInput = immediate;
        2'b10 : registerInput = aluResult;
        2'b11 : registerInput = dataRead;
      endcase
    end
    
    registerFile registers (
        .clk (clk),
        .enable (enable),
        .in (registerInput),
        .inputSelect (inputSelect),
        .writeEnable (writeEnable),
        .spControl (spControl),
        .select0 (select0),
        .select1 (select1),
        .select2 (select2),
        .out0 (reg0),
        .out1 (reg1),
        .out2 (reg2),
        .PC (codeAddress),
        .register13 (r13)
    );
    
    assign aluAvalue = (aluA==0)?reg0:immediate;
    assign aluBvalue = (aluB==0)?reg1:immediate;
    
    ALU alu (
        .A (aluAvalue),
        .B (aluBvalue),
        .op (aluOperation),
        .Cin (carry),
        .F (aluResult),
        .zero (flags[0]),
        .sign (flags[2]),
        .Cout (flags[1]),
        .overflow_parity (flags[3])
    );
    
    controlLogic control (
        .clk (clk),
        .enable (enable),
        .instruction (codeData),
        .reset (reset),
        .interrupt (interrupt),
        .flags (flags),
        .register (reg2),
        .dataram (dataRamControl),
        .registerMux (registerMux),
        .inputSelect (inputSelect),
        .writeEnable (writeEnable),
        .spControl (spControl),
        .select0 (select0),
        .select1 (select1),
        .select2 (select2),
        .immediate (immediate),
        .aluA (aluA),
        .aluB (aluB),
        .aluOperation (aluOperation),
        .carry (carry),
        .io_request (io_request)
    );
    
    assign dataAddress = aluResult;
    assign dataWrite = reg2;
    assign outputs = r13;
endmodule
