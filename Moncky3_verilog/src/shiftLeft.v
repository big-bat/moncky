`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: ShiftLeft
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


module shiftLeft (
  input [15:0] A,
  input [3:0] B,
  input Cin,
  output reg [15:0] F,
  output zero,
  output sign,
  output reg Cout,
  output parity
);

always @ (A,B,Cin)
begin
    case(B)
        4'b0000: begin F=A; Cout=0; end
        4'b0001: begin F[15:1]=A[14:0]; F[0]=Cin; Cout=A[15]; end
        4'b0010: begin F[15:2]=A[13:0]; F[1:0]={2{Cin}}; Cout=A[14]; end
        4'b0011: begin F[15:3]=A[12:0]; F[2:0]={3{Cin}}; Cout=A[13]; end
        4'b0100: begin F[15:4]=A[11:0]; F[3:0]={4{Cin}}; Cout=A[12]; end
        4'b0101: begin F[15:5]=A[10:0]; F[4:0]={5{Cin}}; Cout=A[11]; end
        4'b0110: begin F[15:6]=A[9:0]; F[5:0]={6{Cin}}; Cout=A[10]; end
        4'b0111: begin F[15:7]=A[8:0]; F[6:0]={7{Cin}}; Cout=A[9]; end
        4'b1000: begin F[15:8]=A[7:0]; F[7:0]={8{Cin}}; Cout=A[8]; end
        4'b1001: begin F[15:9]=A[6:0]; F[8:0]={9{Cin}}; Cout=A[7]; end
        4'b1010: begin F[15:10]=A[5:0]; F[9:0]={10{Cin}}; Cout=A[6]; end
        4'b1011: begin F[15:11]=A[4:0]; F[10:0]={11{Cin}}; Cout=A[5]; end
        4'b1100: begin F[15:12]=A[3:0]; F[11:0]={12{Cin}}; Cout=A[4]; end
        4'b1101: begin F[15:13]=A[2:0]; F[12:0]={13{Cin}}; Cout=A[3]; end
        4'b1110: begin F[15:14]=A[1:0]; F[13:0]={14{Cin}}; Cout=A[2]; end
        4'b1111: begin F[15]=A[0]; F[14:0]={15{Cin}}; Cout=A[1]; end
    endcase
end
    assign zero = (F == 0);
    assign sign = F[15];
    assign parity = ^F;
endmodule
