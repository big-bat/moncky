`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: top
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module top(
    input CLK100MHZ, // master clock
    input ck_rst, // reset button
    input btn0, // interrupt button
    output [7:0] jc, jd, // vga connector
    input [7:0] jb, // ps/2 connector
    // spi connections
    input ck_io12_miso,
    output ck_io11_mosi,
    output ck_io13_sck,
    output ck_io10_ss
    );
    
    // generate clocks with Clocking Wizard:
    wire clocksLocked;
    wire clk_100MHZ;
    wire clk_50MHZ;
    wire clk_25MHZ;
    wire clk_12_5MHZ;
    wire clk_40MHZ;
    wire clk_20MHZ;
    wire clk_10MHZ;
    clk_wiz_0 clk_generator (
        .clk_50MHZ (clk_50MHZ),
        .clk_25MHZ (clk_25MHZ),
        .clk_12_5MHZ (clk_12_5MHZ),
        .clk_40MHZ (clk_40MHZ),
        .clk_20MHZ (clk_20MHZ),
        .clk_10MHZ (clk_10MHZ),
        .clk_100MHZ (clk_100MHZ),
        .locked (clocksLocked),
        .clk_in1 (CLK100MHZ)
    );
    wire clk_CPU = clk_12_5MHZ;
    wire clk_mem = clk_12_5MHZ;
    wire clk_video = clk_25MHZ;
    wire clk_io = clk_12_5MHZ;
    
    wire reset = ck_rst & clocksLocked; // keep processor in reset state as long as clocks are not locked or reset button was pressed
    
    // generate enable signals for cpu and memory
    reg divider;
    always @ (posedge(clk_CPU)) if (reset) divider <= ~divider;
    wire enableCPU = divider & clocksLocked; // cpu gets one out of 2 clock edges
    wire enableDataMem = ~(divider ^ clk_CPU) & clocksLocked; // data memory also gets 1 out of 2 edges, but in between 
    
    // arduino outputs: not used at the moment
    wire [15:0] outputs;
    
    wire [15:0] codeData;
    wire [15:0] codeAddress;
    wire [15:0] dataAddress;
    wire [15:0] dataWrite;
    wire [1:0] dataRamControl;
    wire interrupt;
    wire io_request;
    
    wire [1:0] memControl = io_request?0:dataRamControl;
    wire [1:0] ioControl = io_request?dataRamControl:0;
    wire [15:0] memData;
    wire [15:0] ioData;
    
    wire [15:0] dataRead = io_request?ioData:memData;
    
    Moncky3 moncky3 (
      .clk (clk_CPU),
      .enable (enableCPU),
      .reset (reset),
      .interrupt (interrupt),
      .codeData (codeData),
      .dataRead (dataRead),
      .codeAddress (codeAddress),
      .dataAddress (dataAddress),
      .dataWrite (dataWrite),
      .dataRamControl (dataRamControl),
      .outputs (outputs),
      .io_request (io_request)
    );
    
    Memory mem (
        .we (enableDataMem),
        .clk_mem (clk_mem),
        .reset (reset),
        .codeAddress (codeAddress),
        .dataAddress (dataAddress),
        .dataWrite (dataWrite),
        .dataControl (memControl),
        .codeRead (codeData),
        .dataRead (memData)
    );
    
    wire [3:0] spi_ss;
    assign spi_ss[0] = ck_io10_ss;
    
    InputOutput io (
        .we (enableDataMem),
        .clk_mem (clk_mem),
        .clk_video (clk_video),
        .clk_io (clk_io),
        .reset (reset),
        .address (dataAddress),
        .data_in (dataWrite),
        .control (ioControl),
        .data_out (ioData),
        .interrupt (interrupt),
        .vga_connector ({jc, jd}),
        .ps2_connector (jb),
        .spi_miso (ck_io12_miso),
        .spi_sck (ck_io13_sck),
        .spi_mosi (ck_io11_mosi),
        .spi_ss (spi_ss)
    );
    
    initial
    begin
        divider = 0;
    end
    
endmodule