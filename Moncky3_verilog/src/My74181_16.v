`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: My74181_16
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


module My74181_16 (
  input [15:0] A,
  input [15:0] B,
  input [3:0] S,
  input Cin,
  input M,
  output [15:0] F,
  output zero,
  output sign,
  output Cout,
  output overflow_parity
  );
  
  wire zero0, zero1, zero2, zero3;
  wire carry0, carry1, carry2, carry3;
  wire sign0, sign1, sign2, sign3;
  wire overflow0, overflow1, overflow2, overflow3;
  
  My74181 alu0 (
    .A (A[3:0]),
    .B (B[3:0]),
    .S (S),
    .Cin (Cin),
    .M (M),
    .F (F[3:0]),
    .zero (zero0),
    .sign (sign0),
    .Cout (carry0),
    .overflow_parity (overflow0)
  );
  
  My74181 alu1 (
    .A (A[7:4]),
    .B (B[7:4]),
    .S (S),
    .Cin (carry0),
    .M (M),
    .F (F[7:4]),
    .zero (zero1),
    .sign (sign1),
    .Cout (carry1),
    .overflow_parity (overflow1)
  );
  
  My74181 alu2 (
    .A (A[11:8]),
    .B (B[11:8]),
    .S (S),
    .Cin (carry1),
    .M (M),
    .F (F[11:8]),
    .zero (zero2),
    .sign (sign2),
    .Cout (carry2),
    .overflow_parity (overflow2)
  );
  
  My74181 alu3 (
    .A (A[15:12]),
    .B (B[15:12]),
    .S (S),
    .Cin (carry2),
    .M (M),
    .F (F[15:12]),
    .zero (zero3),
    .sign (sign3),
    .Cout (carry3),
    .overflow_parity (overflow3)
  );
  
  assign zero = (zero0 & zero1 & zero2 & zero3);
  assign sign = F[15];
  assign Cout = carry3;
  assign overflow_parity = (M & overflow3) | ((~M) & (overflow0 ^ overflow1 ^ overflow2 ^ overflow3));
endmodule
