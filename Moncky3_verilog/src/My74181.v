`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: My74181
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////


module My74181 (
  input [3:0] A,
  input [3:0] B,
  input [3:0] S,
  input Cin,
  input M,
  output [3:0] F,
  output zero,
  output sign,
  output Cout,
  output overflow_parity
  );
  
  wire P0 = ~(A[0] | (B[0] & S[0]) | (~B[0] & S[1]));
  wire G0 = ~((A[0] & ~B[0] & S[2]) | (A[0] & B[0] & S[3]));
  wire P1 = ~(A[1] | (B[1] & S[0]) | (~B[1] & S[1]));
  wire G1 = ~((A[1] & ~B[1] & S[2]) | (A[1] & B[1] & S[3]));
  wire P2 = ~(A[2] | (B[2] & S[0]) | (~B[2] & S[1]));
  wire G2 = ~((A[2] & ~B[2] & S[2]) | (A[2] & B[2] & S[3]));
  wire P3 = ~(A[3] | (B[3] & S[0]) | (~B[3] & S[1]));
  wire G3 = ~((A[3] & ~B[3] & S[2]) | (A[3] & B[3] & S[3]));
  
  wire Ci = ~Cin;
  wire C0 = ~(M & Ci);
  wire C1 = (~((M & P0) | (M & Ci & G0)));
  wire C2 = (~((M & P1) | (M & P0 & G1) | (M & Ci & G0 & G1)));
  wire C3 = (~((M & P2) | (M & P1 & G2) | (M & P0 & G1 & G2) | (M & Ci & G0 & G1 & G2)));
  
  assign F[0] = C0 ^ ((~P0) & G0);
  assign F[1] = C1 ^ ((~P1) & G1);
  assign F[2] = C2 ^ ((~P2) & G2);
  assign F[3] = C3 ^ ((~P3) & G3);
  assign zero = ~(|F);
  assign sign = F[3];
  assign Cout = M & (~(Ci & G0 & G1 & G2 & G3)) & (~((P0 & G1 & G2 & G3) | (P1 & G2 & G3) | (P2 & G3) | P3));
  assign overflow_parity =  (M & (C3 ^ Cout)) | ((~M) & (^F));
endmodule