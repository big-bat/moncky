`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BigBat
// Engineer: Kris Demuynck
// 
// Create Date: 28.03.2021 12:33:21
// Module Name: Memory
// Project Name: Moncky-3 computer
// Target Devices: Arty S7
//////////////////////////////////////////////////////////////////////////////////

module Memory (
    input we,
    input clk_mem,
    input reset,
    input [15:0] codeAddress,
    input [15:0] dataAddress,
    input [15:0] dataWrite,
    input [1:0] dataControl,
    output [15:0] codeRead,
    output [15:0] dataRead
    );
    
    reg ROM_shadow;
    always @ (posedge clk_mem)
    begin
      if (~reset) ROM_shadow <= 1;
      else if (dataAddress==16'hFFFF & dataWrite==16'hAAAA & dataControl[1] & we) ROM_shadow <= 0;
    end
    
    wire dataRamSelected = |dataAddress[15:12] | (~ROM_shadow);
    wire codeRamSelected = |codeAddress[15:12] | (~ROM_shadow);
    
    wire [15:0] romData, romCode, ramData, ramCode;
    
    ROM theRom (
        .clk_mem (clk_mem),
        .address1 (dataAddress),
        .address2 (codeAddress),
        .data1 (romData),
        .data2 (romCode)
    );
    
    wire writeEnable = dataControl[1] & we;
    
    DualPortRam16 ram (
        .din (dataWrite),
        .addr1 (dataAddress),
        .addr2 (codeAddress),
        .we (writeEnable),
        .clk1 (clk_mem),
        .clk2 (clk_mem),
        .dout1 (ramData),
        .dout2 (ramCode)
    );
    
    assign codeRead = codeRamSelected?ramCode:romCode;
    assign dataRead = dataRamSelected?ramData:romData;
endmodule