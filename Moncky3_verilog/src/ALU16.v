`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.04.2021 11:26:23
// Design Name: 
// Module Name: ALU16
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU16(
  input [15:0] A,
  input [15:0] B,
  input [3:0] op,
  input Cin,
  output reg [15:0] F,
  output reg zero,
  output reg sign,
  output reg Cout,
  output reg overflow_parity
  );
    
  wire [15:0] CinMSB;
  assign CinMSB[15] = Cin;
  assign CinMSB[14:0] = 0;
    
  always @(*)
  begin
    case(op)
      4'b0000 : begin F = B; Cout = 0; overflow_parity = 0; end
      4'b0001 : begin F = A | B; Cout = 0; overflow_parity = ^F; end
      4'b0010 : begin F = A & B; Cout = 0; overflow_parity = ^F; end
      4'b0011 : begin F = A ^ B; Cout = 0; overflow_parity = ^F; end
      4'b0100 : begin {Cout, F} = A + B; overflow_parity = (A[15] & B[15] & (~F[15])) | ((~A[15]) & (~B[15]) & F[15]); end
      4'b0101 : begin {Cout, F} = A + (~B) + 1'b1; overflow_parity = (A[15] & B[15] & (~F[15])) | ((~A[15]) & (~B[15]) & F[15]); end
      4'b0110 : begin F = A << B; Cout = A[15]; overflow_parity = ^F; end
      4'b0111 : begin F = A >> B; Cout = A[0]; overflow_parity = ^F; end
      4'b1000 : begin F = A >>> B; Cout = A[0]; overflow_parity = ^F; end
      4'b1001 : begin F = ~B; Cout = 0; overflow_parity = ^F; end
      4'b1010 : begin F = ~B + 1; Cout = 0; overflow_parity = ^F; end
      4'b1011 : begin {Cout, F} = A + B + Cin; overflow_parity = (A[15] & B[15] & (~F[15])) | ((~A[15]) & (~B[15]) & F[15]); end
      4'b1100 : begin {Cout, F} = A + (~B) + Cin; overflow_parity = (A[15] & B[15] & (~F[15])) | ((~A[15]) & (~B[15]) & F[15]); end
      4'b1101 : begin F = A << B | Cin; Cout = A[15]; overflow_parity = ^F; end
      4'b1110 : begin F = A >> B | CinMSB; Cout = A[0]; overflow_parity = ^F; end
      4'b1111 : begin F = B; Cout = 0; overflow_parity = 0; end
    endcase
    zero = ^(|F);
    sign = F[15];
  end
endmodule
