package moncky3.assembler;

import moncky3.assembler.backend.Util;

public class Library {
    private String filename;
    private String contents;
    private int numberOfLines;

    public Library(String filename) {
        this.filename = filename;
        this.contents = Util.readFile(filename);
        this.numberOfLines = Util.getNumberOfLines(contents);
    }

    public String getFilename() {
        return filename;
    }

    public String getContents() {
        return contents;
    }

    public int getNumberOfLines() {
        return numberOfLines;
    }
}
