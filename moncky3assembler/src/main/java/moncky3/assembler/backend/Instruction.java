package moncky3.assembler.backend;

/**
 * Holds all information of an assembly instruction.
 */
public class Instruction {
    /**
     * The assembly name of the instruction
     */
    private final String opcodeName;
    /**
     * The binary opcode associated with the instruction
     */
    private final int opcode;
    /**
     * The group to which this instruction belongs
     */
    private final int instructionGroup;
    /**
     * The type for each operand
     */
    private final OperandType[] operandTypes;

    public Instruction(String opcodeName, int opcode, int instructionGroup, OperandType[] operandTypes) {
        this.opcodeName = opcodeName;
        this.opcode = opcode;
        this.instructionGroup = instructionGroup;
        this.operandTypes = operandTypes;
    }

    public int getOpcode() {
        return opcode;
    }

    public int getInstructionGroup() {
        return instructionGroup;
    }

    public OperandType getOperandType(int i) {
        return operandTypes[i];
    }

    /**
     * Checks if the given values are valid operands for this instruction.
     *
     * @param values the values for each operand
     * @return an empty string if all operands are valid, an error message otherwise
     */
    public String check(int[] values) {
        if (values.length != operandTypes.length)
            return "" + operandTypes.length + " operands expected for instruction " + opcodeName;
        for (int i = 0; i < operandTypes.length; i++) {
            int value = values[i];
            switch (operandTypes[i]) {
                case EXTENDED_IMMEDIATE8:
                    if (value < -128 || value > 127)
                        return "expected signed 8-bit number for operand " + i + " of instruction " + opcodeName;
                    break;
                case IMMEDIATE8:
                    if (value < 0 || value > 255)
                        return "expected unsigned 8-bit number for operand " + i + " of instruction " + opcodeName;
                    break;
                case IMMEDIATE4:
                    if (value < 0 || value > 15)
                        return "expected unsigned 4-bit number for operand " + i + " of instruction " + opcodeName;
                    break;
                case REGISTERR:
                    if (value < 0 || value > 15)
                        return "expected register number for operand " + i + " of instruction " + opcodeName;
                    break;
                case REGISTERS:
                    if (value < 0 || value > 15)
                        return "expected register number for operand " + i + " of instruction " + opcodeName;
                    break;
                case REGISTERT:
                    if (value < 0 || value > 15)
                        return "expected register number for operand " + i + " of instruction " + opcodeName;
                    break;
            }
        }
        return "";
    }
}
