package moncky3.assembler.backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains all instructions that can be executed on the Moncky-3 processor.
 */
public class Moncky3InstructionSet implements InstructionSet {
    private Map<String, Instruction> instructions;

    public Moncky3InstructionSet() {
        instructions = new HashMap<>();
        instructions.put("reset", new Instruction("reset", 0, 0, new OperandType[]{}));
        instructions.put("halt", new Instruction("halt", 1, 0, new OperandType[]{}));
        instructions.put("ei", new Instruction("ei", 2, 0, new OperandType[]{}));
        instructions.put("di", new Instruction("di", 3, 0, new OperandType[]{}));
        instructions.put("reti", new Instruction("reti", 4, 0, new OperandType[]{}));
        instructions.put("int", new Instruction("int", 5, 0, new OperandType[]{}));

        instructions.put("push", new Instruction("push", 1, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("pop", new Instruction("pop", 2, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("call", new Instruction("call", 3, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("sflags", new Instruction("sflags", 4, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("rflags", new Instruction("rflags", 5, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jp", new Instruction("jp", 6, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("sz", new Instruction("sz", 16, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("snz", new Instruction("snz", 17, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("sc", new Instruction("sc", 18, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("snc", new Instruction("snc", 19, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("ss", new Instruction("ss", 20, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("sns", new Instruction("sns", 21, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("so", new Instruction("so", 22, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("sno", new Instruction("sno", 23, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jpz", new Instruction("jpz", 24, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jpnz", new Instruction("jpnz", 25, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jpc", new Instruction("jpc", 26, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jpnc", new Instruction("jpnc", 27, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jps", new Instruction("jps", 28, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jpns", new Instruction("jpns", 29, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jpo", new Instruction("jpo", 30, 1, new OperandType[]{OperandType.REGISTERR}));
        instructions.put("jpno", new Instruction("jpno", 31, 1, new OperandType[]{OperandType.REGISTERR}));

        instructions.put("in", new Instruction("in", 1, 2, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("out", new Instruction("out", 2, 2, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));

        instructions.put("li", new Instruction("li", 1, 3, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE8}));
        instructions.put("lih", new Instruction("lih", 2, 3, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE8}));
        instructions.put("addi", new Instruction("addi", 3, 3, new OperandType[]{OperandType.REGISTERR, OperandType.EXTENDED_IMMEDIATE8}));
        instructions.put("andi", new Instruction("andi", 4, 3, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE8}));
        instructions.put("ori", new Instruction("ori", 5, 3, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE8}));
        instructions.put("cmpi", new Instruction("cmpi", 6, 3, new OperandType[]{OperandType.REGISTERR, OperandType.EXTENDED_IMMEDIATE8}));
        instructions.put("cmpir", new Instruction("cmpir", 7, 3, new OperandType[]{OperandType.EXTENDED_IMMEDIATE8, OperandType.REGISTERR}));
        instructions.put("lda", new Instruction("lda", 12, 3, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS, OperandType.REGISTERT}));
        instructions.put("sta", new Instruction("sta", 13, 3, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS, OperandType.REGISTERT}));
        instructions.put("ldi", new Instruction("ldi", 14, 3, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERT, OperandType.IMMEDIATE4}));
        instructions.put("sti", new Instruction("sti", 15, 3, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERT, OperandType.IMMEDIATE4}));

        instructions.put("nop", new Instruction("nop", 0, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("or", new Instruction("or", 1, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("and", new Instruction("and", 2, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("xor", new Instruction("xor", 3, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("add", new Instruction("add", 4, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("sub", new Instruction("sub", 5, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shl", new Instruction("shl", 6, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shr", new Instruction("shr", 7, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("ashr", new Instruction("ashr", 8, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("not", new Instruction("not", 9, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("neg", new Instruction("neg", 10, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("addc", new Instruction("addc", 11, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("subc", new Instruction("subc", 12, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shlc", new Instruction("shlc", 13, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shrc", new Instruction("shrc", 14, 4, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));

        instructions.put("nopf", new Instruction("nopf", 0, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("orf", new Instruction("orf", 1, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("andf", new Instruction("andf", 2, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("xorf", new Instruction("xorf", 3, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("addf", new Instruction("addf", 4, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("subf", new Instruction("subf", 5, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shlf", new Instruction("shlf", 6, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shrf", new Instruction("shrf", 7, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("ashrf", new Instruction("ashrf", 8, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("notf", new Instruction("notf", 9, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("negf", new Instruction("negf", 10, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("addcf", new Instruction("addcf", 11, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("subcf", new Instruction("subcf", 12, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shlcf", new Instruction("shlcf", 13, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shrcf", new Instruction("shrcf", 14, 5, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));

        instructions.put("nopi", new Instruction("nopi", 0, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        // instructions.put("ori",   new Instruction("ori",   1,  6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        // instructions.put("andi",  new Instruction("andi",  2,  6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("xori", new Instruction("xori", 3, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        // instructions.put("addi",  new Instruction("addi",  4,  6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("subi", new Instruction("subi", 5, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("shli", new Instruction("shli", 6, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("shri", new Instruction("shri", 7, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("ashri", new Instruction("ashri", 8, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("noti", new Instruction("noti", 9, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("negi", new Instruction("negi", 10, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("addci", new Instruction("addci", 11, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("subci", new Instruction("subci", 12, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("shlci", new Instruction("shlci", 13, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("shrci", new Instruction("shrci", 14, 6, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));

        instructions.put("nopif", new Instruction("nopif", 0, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("orif", new Instruction("orif", 1, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("andif", new Instruction("andif", 2, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("xorif", new Instruction("xorif", 3, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("addif", new Instruction("addif", 4, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("subif", new Instruction("subif", 5, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("shlif", new Instruction("shlif", 6, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("shrif", new Instruction("shrif", 7, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("ashrif", new Instruction("ashrif", 8, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("notif", new Instruction("notif", 9, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("negif", new Instruction("negif", 10, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("addcif", new Instruction("addcif", 11, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("subcif", new Instruction("subcif", 12, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("shlcif", new Instruction("shlcif", 13, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
        instructions.put("shrcif", new Instruction("shrcif", 14, 7, new OperandType[]{OperandType.REGISTERR, OperandType.IMMEDIATE4}));
    }

    @Override
    public Instruction getInstruction(String opcodeName) {
        return instructions.get(opcodeName);
    }

    @Override
    public List<String> findAndReplacePseudoInstructions(List<String> parts) {
        if (parts.size() == 0) return parts;
        String opcode = parts.get(0);
        if (opcode.equals("nop") && parts.size() == 1) {
            List<String> result = new ArrayList<>(3);
            result.add("nop");
            result.add("r0");
            result.add("r0");
            return result;
        } else if (opcode.equals("jpi") && parts.size() == 2) {
            List<String> result = new ArrayList<>(3);
            result.add("li");
            result.add("pc");
            result.add(parts.get(1));
            return result;
        } else if (opcode.equals("jpr")) {
            List<String> result = new ArrayList<>(3);
            result.add("add");
            result.add("pc");
            result.add(parts.get(1));
            return result;
        } else if (opcode.equals("jpfi")) {
            List<String> result = new ArrayList<>(3);
            result.add("addi");
            result.add("pc");
            result.add(parts.get(1));
            return result;
        } else if (opcode.equals("jpbi")) {
            List<String> result = new ArrayList<>(3);
            result.add("subi");
            result.add("pc");
            result.add(parts.get(1));
            return result;
        } else if (opcode.equals("set")) {
            List<String> result = new ArrayList<>(3);
            result.add("nop");
            result.add(parts.get(1));
            result.add(parts.get(2));
            return result;
        } else if (opcode.equals("st")) {
            List<String> result = new ArrayList<>(4);
            result.add("sti");
            result.add(parts.get(1));
            result.add(parts.get(2));
            result.add("0");
            if (parts.size() == 4)
                result.add(parts.get(3)); // if there is an extra parameter, then just add it (probably the programmer write st in stead of lst)
            return result;
        } else if (opcode.equals("ld")) {
            List<String> result = new ArrayList<>(4);
            result.add("ldi");
            result.add(parts.get(1));
            result.add(parts.get(2));
            result.add("0");
            if (parts.size() == 4)
                result.add(parts.get(3)); // if there is an extra parameter, then just add it (probably the programmer write ld in stead of ldi)
            return result;
        } else if (opcode.equals("ret")) {
            List<String> result = new ArrayList<>(2);
            result.add("pop");
            result.add("pc");
            return result;
        } else if (opcode.equals("cmp") && parts.size() == 3) {
            List<String> result = new ArrayList<>(3);
            result.add("subf");
            result.add(parts.get(1));
            result.add(parts.get(2));
            return result;
        } else if (opcode.equals("inc") && parts.size() == 2) {
            List<String> result = new ArrayList<>(3);
            result.add("addi");
            result.add(parts.get(1));
            result.add("1");
            return result;
        } else if (opcode.equals("dec") && parts.size() == 2) {
            List<String> result = new ArrayList<>(3);
            result.add("subi");
            result.add(parts.get(1));
            result.add("1");
            return result;
        }
        return parts;
    }

    @Override
    public int toMachineCode(Instruction instruction, int[] values) {
        int bin = 0;
        for (int i = 0; i < values.length; i++) {
            OperandType operandType = instruction.getOperandType(i);
            if (operandType == OperandType.REGISTERR) bin = bin | ((values[i] & 0xF) << 12);
            else if (operandType == OperandType.REGISTERS) bin = bin | ((values[i] & 0xF) << 8);
            else if (operandType == OperandType.REGISTERT) bin = bin | ((values[i] & 0xF) << 4);
            else if (operandType == OperandType.IMMEDIATE4) bin = bin | ((values[i] & 0xF) << 8);
            else if (operandType == OperandType.IMMEDIATE8) bin = bin | ((values[i] & 0xFF) << 4);
            else if (operandType == OperandType.EXTENDED_IMMEDIATE8) bin = bin | ((values[i] & 0xFF) << 4);
            else throw new RuntimeException("Error: wrong type of operandType");
        }

        int instructionGroup = instruction.getInstructionGroup();
        int opcode = instruction.getOpcode();
        if (instructionGroup == 0) {
            bin = bin | (opcode << 12);
        } else if (instructionGroup == 1) {
            bin = bin | (opcode << 7);
        } else if (instructionGroup == 2) {
            bin = bin | (opcode << 4);
        } else if (instructionGroup == 3) {
            bin = bin | (opcode);
        } else if (instructionGroup == 4) {
            bin = bin | (opcode << 4) | 8;
        } else if (instructionGroup == 5) {
            bin = bin | (opcode << 4) | 9;
        } else if (instructionGroup == 6) {
            bin = bin | (opcode << 4) | 10;
        } else if (instructionGroup == 7) {
            bin = bin | (opcode << 4) | 11;
        } else {
            throw new RuntimeException("Error: wrong instruction group");
        }
        return bin;
    }

    /**
     * Creates a map of aliases and their values.
     * This initially consists of register names and their real name
     *
     * @return the created map
     */
    @Override
    public Map<String, String> createAliases() {
        Map<String, String> result = new HashMap<>();
        result.put("pc", "r15");
        result.put("sp", "r14");
        result.put("bp", "r12");
        return result;
    }
}
