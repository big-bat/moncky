package moncky3.assembler.backend;

/**
 * Contains all possible types for operands.
 * The type also indicates the position in the instruction.
 */
public enum OperandType {
    REGISTERR, REGISTERS, REGISTERT, IMMEDIATE8, IMMEDIATE4, EXTENDED_IMMEDIATE8
}
