package moncky3.assembler.backend;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains all instructions that can be executed on the Moncky-2 processor.
 * This instruction set contains all instructions of the Moncky-1 processor plus two instructions:
 * - reset: sets the program counter to 0
 * - pcto: copies the value of the program counter to a register
 */
public class Moncky2InstructionSet implements InstructionSet {
    private Map<String, Instruction> instructions;

    public Moncky2InstructionSet() {
        instructions = new HashMap<>();
        instructions.put("halt", new Instruction("halt", 0, 0, new OperandType[]{}));
        instructions.put("li", new Instruction("li", 1, 0, new OperandType[]{OperandType.REGISTERS, OperandType.IMMEDIATE8}));
        instructions.put("reset", new Instruction("reset", 2, 0, new OperandType[]{}));
        instructions.put("ld", new Instruction("ld", 8, 0, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("st", new Instruction("st", 10, 0, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("jp", new Instruction("jp", 12, 0, new OperandType[]{OperandType.REGISTERS}));
        instructions.put("pcto", new Instruction("pcto", 14, 0, new OperandType[]{OperandType.REGISTERS}));

        instructions.put("nop", new Instruction("nop", 0, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("or", new Instruction("or", 1, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("and", new Instruction("and", 2, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("xor", new Instruction("xor", 3, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("add", new Instruction("add", 4, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("sub", new Instruction("sub", 5, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shl", new Instruction("shl", 6, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("shr", new Instruction("shr", 7, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("ashr", new Instruction("ashr", 8, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("not", new Instruction("not", 9, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));
        instructions.put("neg", new Instruction("neg", 10, 1, new OperandType[]{OperandType.REGISTERR, OperandType.REGISTERS}));

        instructions.put("jpc", new Instruction("jpc", 0, 2, new OperandType[]{OperandType.REGISTERS}));
        instructions.put("jpnc", new Instruction("jpnc", 1, 2, new OperandType[]{OperandType.REGISTERS}));
        instructions.put("jpz", new Instruction("jpz", 2, 2, new OperandType[]{OperandType.REGISTERS}));
        instructions.put("jpnz", new Instruction("jpnz", 3, 2, new OperandType[]{OperandType.REGISTERS}));
        instructions.put("jps", new Instruction("jps", 4, 2, new OperandType[]{OperandType.REGISTERS}));
        instructions.put("jpns", new Instruction("jpns", 5, 2, new OperandType[]{OperandType.REGISTERS}));
        instructions.put("jpo", new Instruction("jpo", 6, 2, new OperandType[]{OperandType.REGISTERS}));
        instructions.put("jpno", new Instruction("jpno", 7, 2, new OperandType[]{OperandType.REGISTERS}));
    }

    public Instruction getInstruction(String opcodeName) {
        return instructions.get(opcodeName);
    }

    @Override
    public List<String> findAndReplacePseudoInstructions(List<String> parts) {
        return parts;
    }

    public int toMachineCode(Instruction instruction, int[] values) {
        int bin = 0;
        for (int i = 0; i < values.length; i++) {
            OperandType operandType = instruction.getOperandType(i);
            if (operandType == OperandType.REGISTERR) bin = bin | ((values[i] & 0xF) << 4);
            else if (operandType == OperandType.REGISTERS) bin = bin | ((values[i] & 0xF));
            else if (operandType == OperandType.IMMEDIATE8) bin = bin | ((values[i] & 0xFF) << 4);
            else {
                throw new RuntimeException("Error: wrong type of operandType");
            }
        }

        int instructionGroup = instruction.getInstructionGroup();
        int opcode = instruction.getOpcode();
        if (instructionGroup == 0) {
            bin = bin | (opcode << 12);
        } else if (instructionGroup == 1) {
            bin = bin | 0x4000 | (opcode << 8);
        } else if (instructionGroup == 2) {
            bin = bin | 0xF000 | (opcode << 4);
        } else {
            throw new RuntimeException("Error: wrong instruction group");
        }
        return bin;
    }

    /**
     * Creates a map of aliases and their values.
     * This initially consists of register names and their real name
     *
     * @return the created map
     */
    @Override
    public Map<String, String> createAliases() {
        Map<String, String> result = new HashMap<>();
        result.put("pc", "r15");
        result.put("sp", "r14");
        return result;
    }
}
