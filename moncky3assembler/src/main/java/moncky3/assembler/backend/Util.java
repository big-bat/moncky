package moncky3.assembler.backend;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * A class containing some static utility methods.
 */
public class Util {
    public static String toHex(int bin) {
        StringBuilder hex = new StringBuilder(Integer.toHexString(bin));
        while (hex.length() < 4) {
            hex.insert(0, "0");
        }
        return hex.toString().toUpperCase();
    }

    /**
     * Parses a string as an integer.
     * The string can also contain a number in octal, binary, or hexadecimal.
     * A label can also be used as a value
     *
     * @param s the string to be parsed
     * @return the integer value of the string
     */
    public static int getValue(String s) {
        int value;
        if (s.startsWith("0x")) {
            value = Integer.parseInt(s.substring(2), 16);
        } else if (s.startsWith("0b")) {
            value = Integer.parseInt(s.substring(2), 2);
        } else if (s.startsWith("0")) {
            value = Integer.parseInt(s, 8);
        } else if (s.startsWith("'")) {
            if (s.length() != 3) {
                value = 65536;
            } else {
                value = s.charAt(1);
            }
        } else {
            value = Integer.parseInt(s);
        }
        return value;
    }

    /**
     * Finds the smallest power of 2 that is equal or greater than the given integer
     *
     * @param number the given integer
     * @return the smallest power of 2 that is equal or greater than the given integer
     */
    public static int ceilPower2(int number) {
        for (int i = 1; i < 65536; i <<= 1) {
            if (i >= number) return i;
        }
        return 65536;
    }

    /**
     * Reads a file into a string.
     * If the file cannot be found an error is displayed and the systems stops.
     *
     * @param filename the file to be read
     * @return the contents of the file as a String
     */
    public static String readFile(String filename) {
        Scanner fileScanner = null;
        try {
            fileScanner = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
            System.out.println("Error: cannot find file " + filename);
            System.exit(1);
        }
        StringBuilder sourceBuffer = new StringBuilder();
        while (fileScanner.hasNextLine()) {
            String line = fileScanner.nextLine();
            sourceBuffer.append(line);
            sourceBuffer.append('\n');
        }
        return sourceBuffer.toString();
    }

    /**
     * Counts the number of lines in a String.
     *
     * @param contents a string containing a text
     * @return the number of lines in the given string
     */
    public static int getNumberOfLines(String contents) {
        int result = 1;
        for (int i = 0; i < contents.length(); i++) {
            char c = contents.charAt(i);
            if (c == '\n') result++;
        }
        return result;
    }
}
