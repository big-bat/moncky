package moncky3.assembler.backend;

import java.util.List;
import java.util.Map;

/**
 * An interface to be able to support different instruction sets.
 */
public interface InstructionSet {
    /**
     * Returns the instruction associated with the given opcodeName.
     *
     * @param opcodeName the assembly name of the instruction
     * @return the instruction associated with the opcode or null if not found.
     */
    Instruction getInstruction(String opcodeName);

    /**
     * If the given instruction contains a pseudo instruction, it is replaced by the real instruction.
     *
     * @param parts contains a list of all parts of an instruction (opcode + operands)
     * @return the real instruction or the original instruction if no pseudo instruction was present
     */
    List<String> findAndReplacePseudoInstructions(List<String> parts);

    /**
     * Translates the given instruction and operands to machine code.
     *
     * @param instruction the instruction that needs to be translated
     * @param values      the values of the operands
     * @return the generated machine code
     */
    int toMachineCode(Instruction instruction, int[] values);

    /**
     * Creates a map of aliases and their values.
     * This initially consists of register names and their real name
     *
     * @return the created map
     */
    Map<String, String> createAliases();
}
