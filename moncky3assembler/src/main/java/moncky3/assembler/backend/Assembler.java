package moncky3.assembler.backend;

import java.util.*;

import static moncky3.assembler.backend.Util.getValue;

/**
 * This class implements the actual assembler.
 * It will read a source string and translate it to machine code.
 */
public class Assembler {
    /**
     * Generated code lines
     */
    private CodeLines codeLines;

    /**
     * Maps label names to addresses
     */
    private Map<String, Integer> labels;

    /**
     * A set of all labels declared till now.  This is used to detect duplicate labels
     */
    private Set<String> declaredLabels;

    /**
     * Maps aliases to register numbers
     */
    private Map<String, String> aliases;

    /**
     * Current address for instruction
     */
    private int address;

    /**
     * The instructionSet
     */
    private InstructionSet instructionSet;

    /**
     * Constructor.
     *
     * @param instructionSet the instruction set that needs to be used by the assembler
     */
    public Assembler(InstructionSet instructionSet) {
        this.codeLines = new CodeLines();
        labels = new HashMap<>();
        address = 0;
        declaredLabels = new HashSet<>();
        this.instructionSet = instructionSet;
        aliases = instructionSet.createAliases();
    }

    /**
     * Parses one line of code and returns the result
     *
     * @param line       a string containing a line of source code
     * @param linenumber the line number
     * @return a CodeLine object containing the translated line of code including address, line number and error
     */
    private CodeLine parseLine(String line, int linenumber) {
        CodeLine codeLine = new CodeLine(linenumber, line, address);

        // split the line in its parts: opcode and operands
        // all tabs, spaces, and brackets are removed
        List<String> parts = splitLine(line);

        if (isLineEmpty(parts)) return codeLine;

        findAndDeleteComment(parts);
        if (isLineEmpty(parts)) return codeLine;

        boolean commandsFound = parseAssemblerSpecificCommands(parts, codeLine);
        if (commandsFound) return codeLine;

        findAndRegisterLabel(parts, codeLine);
        if (!codeLine.getError().isEmpty()) return codeLine;
        if (isLineEmpty(parts)) return codeLine;

        parts = instructionSet.findAndReplacePseudoInstructions(parts);

        findAndReplaceRegisterAliases(parts);

        // get instruction
        String opcode = parts.get(0);
        Instruction instruction = instructionSet.getInstruction(opcode);
        if (instruction == null) {
            codeLine.setError("instruction " + opcode + " not found");
            return codeLine;
        }

        // parse operands to integers (includes registers and labels)
        int[] operands = parseOperands(codeLine, parts, instruction);
        if (operands == null) return codeLine;

        // check if operands match the instruction
        String error = instruction.check(operands);
        if (!"".equals(error)) {
            codeLine.setError(error);
            return codeLine;
        }

        // generate machine code
        int machineCode = instructionSet.toMachineCode(instruction, operands);
        codeLine.setMachineCode(machineCode);
        address++;
        return codeLine;
    }

    private boolean isWhiteSpace(char c) {
        return (c == ' ' || c == '\t' || c == ',' || c == '(' || c == ')' || c == '[' || c == ']' || c == '+');
    }

    private char getCharFromString(String line, int charIndex) {
        char c;
        if (charIndex < line.length()) c = line.charAt(charIndex);
        else c = 0;
        return c;
    }

    /**
     * Splits a line into parts.
     * Parts are delimited by white space, comma's, brackets and the plus sign (these are all removed).
     * A string can be put between "" and a character can be put between ' '.  Whitespace between those signs is not removed.
     *
     * @param line the line that needs to be split
     * @return an array of parts
     */
    private List<String> splitLine(String line) {
        List<String> result = new ArrayList<>();
        int charIndex = 0;
        while (charIndex < line.length()) {
            char c;
            c = getCharFromString(line, charIndex);
            StringBuilder token = new StringBuilder();
            if (!isWhiteSpace(c)) {
                if (c == '"') {
                    token.append(c);
                    charIndex++;
                    c = getCharFromString(line, charIndex);
                    while (c != '"' && (charIndex < line.length())) {
                        token.append(c);
                        charIndex++;
                        c = getCharFromString(line, charIndex);
                    }
                    token.append(c);
                    result.add(token.toString());
                } else if (c == '\'') {
                    token.append(c);
                    charIndex++;
                    c = getCharFromString(line, charIndex);
                    while (c != '\'' && (charIndex < line.length())) {
                        token.append(c);
                        charIndex++;
                        c = getCharFromString(line, charIndex);
                    }
                    token.append('\'');
                    result.add(token.toString());
                } else {
                    token.append(c);
                    charIndex++;
                    c = getCharFromString(line, charIndex);
                    while (!isWhiteSpace(c) && (charIndex < line.length())) {
                        token.append(c);
                        charIndex++;
                        c = getCharFromString(line, charIndex);
                    }
                    result.add(token.toString());
                }
            }
            charIndex++;
        }
        return result;
    }

    /**
     * Examines the given parts of an instruction and replaces register aliases with their real name.
     * The result is put back in the given list.
     *
     * @param parts contains a list of all parts of an instruction (opcode + operands)
     */
    private void findAndReplaceRegisterAliases(List<String> parts) {
        for (int i = 0; i < parts.size(); i++) {
            String part = parts.get(i);
            if (aliases.containsKey(part)) {
                String value = aliases.get(part);
                parts.set(i, value);
            }
        }
    }

    /**
     * Looks for assembler-specific commands that begin with '.'.
     *
     * @param parts    the parts of the source code line
     * @param codeLine the current generated code line (used to put errors in)
     */
    private boolean parseAssemblerSpecificCommands(List<String> parts, CodeLine codeLine) {
        String firstPart = parts.get(0);
        if (!firstPart.startsWith(".")) return false;
        if (firstPart.equals(".org")) {
            if (parts.size() != 2) {
                codeLine.setError(".org should be followed by an address");
                return true;
            }
            int newAddress = getValue(parts.get(1));
            if (newAddress < 0 || newAddress > 65535) {
                codeLine.setError("value of .org should be 16-bit number");
                return true;
            }
            address = newAddress;
            return true;
        } else if (firstPart.equals(".def")) {
            if (parts.size() != 3) {
                codeLine.setError(".def must be followed by a name and a value");
                return true;
            }
            String name = parts.get(1);
            int value = getValue(parts.get(2));
            if (!name.startsWith(":")) {
                codeLine.setError("name of .def should start with ':'");
                return true;
            }
            if (value < 0 || value > 65535) {
                codeLine.setError("value of .def should be 16-bit number");
                return true;
            }
            labels.put(name, value);
            return true;
        } else if (firstPart.equals(".data")) {
            if (parts.size() < 2) {
                codeLine.setError("data expected after .data keyword");
                return true;
            }
            String part1 = parts.get(1);
            if (part1.startsWith("\"")) {
                part1 = part1.replace('"', (char) 0);
                part1 = part1 + (char) 0;
                char cLow = getCharFromString(part1, 1);
                char cHigh = getCharFromString(part1, 2);
                int value = (cLow & 0xFF) | ((cHigh & 0xFF) << 8);
                for (int i = 3; i < part1.length() - 1; i += 2) {
                    CodeLine newCodeLine = new CodeLine(codeLine);
                    newCodeLine.setMachineCode(value);
                    codeLines.add(newCodeLine);
                    address++;
                    cLow = getCharFromString(part1, i);
                    cHigh = getCharFromString(part1, i + 1);
                    value = (cLow & 0xFF) | ((cHigh & 0xFF) << 8);
                    codeLine.setMachineCode(value);
                    codeLine.setAddress(address);
                    codeLine.setSource("");
                }
                address++;
            } else {
                for (int i = 1; i < parts.size(); i++) {
                    int value = -1;
                    try {
                        String part = parts.get(i);
                        if (part.startsWith(":")) {
                            Integer v = labels.get(part);
                            if (v == null) value = 0;
                            else value = v;
                        } else {
                            value = getValue(part);
                        }
                    } catch (Exception e) {
                        codeLine.setError("values of .data should all be numbers, characters, labels, or a string");
                        return true;
                    }
                    if (value < 0 || value > 65535) {
                        codeLine.setError("values of .data should all be 16-bit numbers");
                        return true;
                    }
                    codeLine.setMachineCode(value);
                    if (i > 1) codeLine.setSource("");
                    address++;
                    if (i < (parts.size() - 1)) {
                        CodeLine newCodeLine = new CodeLine(codeLine);
                        newCodeLine.setMachineCode(value);
                        codeLines.add(newCodeLine);
                        codeLine.setAddress(address);
                    }
                }
            }
            return true;
        } else if (firstPart.equals(".alias")) {
            if (parts.size() != 3) {
                codeLine.setError(".alias should be followed by a name and a value");
                return true;
            }
            String name = parts.get(1);
            String value = parts.get(2);
            if (!name.startsWith("$")) {
                codeLine.setError("name of an alias should start with '$'");
                return true;
            }
            aliases.put(name, value);
            return true;
        } else if (firstPart.equals(".unalias")) {
            if (parts.size() != 2) {
                codeLine.setError(".unalias should be followed by a name");
                return true;
            }
            String name = parts.get(1);
            if (!aliases.containsKey(name)) {
                codeLine.setError("alias not found");
                return true;
            }
            aliases.remove(name);
            return true;
        } else if (firstPart.equals(".rmAliases")) {
            if (parts.size() != 1) {
                codeLine.setError(".rmAliases should not be followed by anything else");
                return true;
            }
            aliases = instructionSet.createAliases();
            return true;
        }
        return false;
    }

    /**
     * Parse operands to integers (includes registers and labels)
     *
     * @param codeLine    the current generated code line (used to put errors in)
     * @param parts       the parts of the source code line
     * @param instruction the instruction that defines the operands
     * @return the operands parsed as integers
     */
    private int[] parseOperands(CodeLine codeLine, List<String> parts, Instruction instruction) {
        int[] operands = new int[parts.size() - 1];
        for (int i = 0; i < operands.length; i++) {
            try {
                String part = parts.get(i + 1);
                if (instruction.getOperandType(i) == OperandType.REGISTERR
                        || instruction.getOperandType(i) == OperandType.REGISTERS
                        || instruction.getOperandType(i) == OperandType.REGISTERT) {
                    if (!part.startsWith("r")) {
                        codeLine.setError("register name should start with 'r'");
                        return null;
                    }
                    part = part.substring(1);
                } else if (part.startsWith(":")) {
                    boolean high = false;
                    if (part.startsWith("::")) {
                        part = part.substring(1);
                        high = true;
                    }
                    Integer addr = labels.get(part);
                    if (addr == null) {
                        codeLine.setError("label '" + part + "' not yet defined");
                        addr = 0;
                    }
                    int a = addr & 0xFF;
                    if (high) a = (addr >> 8) & 0xFF;
                    part = "" + a;
                }
                operands[i] = getValue(part);
            } catch (Exception e) {
                codeLine.setError("number expected for operand " + i);
                return null;
            }
        }
        return operands;
    }

    /**
     * Looks for a comment in the parts and removes it if present
     *
     * @param parts the parts of the source code line
     */
    private void findAndDeleteComment(List<String> parts) {
        boolean commentFound = false;
        int commentStart = 0;
        for (int i = 0; i < parts.size(); i++) {
            if (parts.get(i).startsWith(";")) {
                commentFound = true;
                commentStart = i;
                break;
            }
        }
        if (commentFound) {
            parts.subList(commentStart, parts.size()).clear();
        }
    }

    /**
     * Looks for the definition of a label in the instruction
     *
     * @param parts    the parts of the source code line
     * @param codeLine the current generated code line (used to put errors in)
     */
    private void findAndRegisterLabel(List<String> parts, CodeLine codeLine) {
        String opcode = parts.get(0);
        if (opcode.startsWith(":")) {
            if (declaredLabels.contains(opcode)) {
                codeLine.setError("label " + opcode + " already exists");
            } else {
                //System.out.println("new label " + opcode + " at 0x" + toHex(address));
                labels.put(opcode, address);
                declaredLabels.add(opcode);
            }
            parts.remove(0);
        }
    }

    /**
     * returns if the list of parts is empty
     *
     * @param parts the parts of the source code line
     * @return true if the list is empty
     */
    private boolean isLineEmpty(List<String> parts) {
        return parts.size() == 0;
    }

    /**
     * Translates the given source code from assembly to machine code
     * Two passes are done so all referenced labels can be used.
     *
     * @param source a string containing the whole source code (multiple lines)
     * @return a data structure containing the resulting machine code and errors (if any)
     */
    public CodeLines translate(String source) {
        labels.clear();
        onePass(source);
        onePass(source);
        return codeLines;
    }

    /**
     * Performs one pass of translating source code to machine code
     *
     * @param source a string containing the whole source code (multiple lines)
     */
    private void onePass(String source) {
        codeLines.clear();
        declaredLabels.clear();
        this.address = 0;
        String[] lines = source.split("[\n\r]");
        for (int linenumber = 0; linenumber < lines.length; linenumber++) {
            CodeLine codeLine = parseLine(lines[linenumber], linenumber + 1);
            codeLines.add(codeLine);
        }
    }

    /**
     * get all defined labels
     *
     * @return a map that maps all defined labels to their addresses
     */
    public Map<String, Integer> getLabels() {
        return labels;
    }
}
