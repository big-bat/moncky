package moncky3.assembler.backend;

/**
 * Contains one translated line of code.
 * It contains the line number, memory address, generated machine code, original source code, and an error if it occurred
 */
public class CodeLine {
    /**
     * Line number in the source code
     */
    private int linenumber;
    /**
     * The address in memory where the instruction is stored
     */
    private int address;
    /**
     * The generated machine code
     */
    private int machineCode;
    /**
     * The original source code for this line
     */
    private String source;
    /**
     * In case of an error, it can be retrieved through this field
     */
    private String error;

    public CodeLine(int linenumber, String source, int address) {
        this.linenumber = linenumber;
        this.source = source;
        this.address = address;
        this.machineCode = -1;
        this.error = "";
    }

    /**
     * copy constructor.
     */
    public CodeLine(CodeLine other) {
        if (other == null) throw new RuntimeException("cannot copy from a non-existing CodeLine");
        this.linenumber = other.linenumber;
        this.address = other.address;
        this.machineCode = other.machineCode;
        this.source = other.source;
        this.error = other.error;
    }

    public int getLinenumber() {
        return linenumber;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public int getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(int machineCode) {
        this.machineCode = machineCode;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "CodeLine{" +
                "linenumber=" + linenumber +
                ", address=" + address +
                ", machineCode=" + machineCode +
                ", source='" + source + '\'' +
                ", error='" + error + '\'' +
                '}';
    }
}
