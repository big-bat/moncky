package moncky3.assembler.backend;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static moncky3.assembler.backend.Util.ceilPower2;
import static moncky3.assembler.backend.Util.toHex;

/**
 * Contains all translated lines of code.
 * It is used by the assembler.  The result is put in an object of this class.
 */
public class CodeLines {
    private List<CodeLine> codeLines;

    public CodeLines() {
        this.codeLines = new ArrayList<>();
    }

    /**
     * Add a new code line.
     */
    public void add(CodeLine codeLine) {
        this.codeLines.add(codeLine);
    }

    /**
     * Remove all code lines.
     */
    public void clear() {
        codeLines.clear();
    }

    /**
     * Writes all code lines in a readable form to the given writer.
     *
     * @param writer The writer to which the output needs to be written (can be System.out for standard output).
     */
    public void writeResult(PrintStream writer) {
        for (CodeLine codeLine : codeLines) {
            String address = toHex(codeLine.getAddress());
            String machineCode = "    ";
            if (codeLine.getMachineCode() != -1) {
                machineCode = toHex(codeLine.getMachineCode());
            }
            String sourceLine = codeLine.getSource();
            String error = codeLine.getError();
            writer.println(address + " " + machineCode + " " + sourceLine + '\t' + error);
        }
    }

    /**
     * Gets the generated machine code as one string (newlines are put in between).
     * There is one line per line in the source code.
     * This is used for the GUI.
     */
    public String getMachineCodesAsString() {
        StringBuilder buffer = new StringBuilder(codeLines.size());
        int lastLineNumber = -1;
        for (CodeLine line : codeLines) {
            if (lastLineNumber != line.getLinenumber()) {
                if (line.getMachineCode() != -1) {
                    buffer.append(toHex(line.getMachineCode()));
                }
                buffer.append('\n');
            }
            lastLineNumber = line.getLinenumber();
        }
        return buffer.toString();
    }

    /**
     * Gets the generated errors as one string (newlines are put in between).
     * There is one line per line in the source code.
     * This is used for the GUI.
     */
    public String getErrorsAsString() {
        StringBuilder buffer = new StringBuilder(codeLines.size());
        int lastLineNumber = -1;
        for (CodeLine line : codeLines) {
            if (lastLineNumber != line.getLinenumber()) {
                buffer.append(line.getError());
                buffer.append('\n');
            }
            lastLineNumber = line.getLinenumber();
        }
        return buffer.toString();
    }

    /**
     * Gets the memory addresses as one string (newlines are put in between).
     * There is one line per line in the source code.
     * This is used for the GUI.
     */
    public String getAddressesAsString() {
        StringBuilder buffer = new StringBuilder(codeLines.size());
        int lastLineNumber = -1;
        for (CodeLine line : codeLines) {
            if (lastLineNumber != line.getLinenumber()) {
                buffer.append(toHex(line.getAddress()));
                buffer.append('\n');
            }
            lastLineNumber = line.getLinenumber();
        }
        return buffer.toString();
    }

    /**
     * Writes the generated machine code in a .hex file that can be read by the simulator.
     *
     * @param writer The writer to which the output needs to be written (can be System.out for standard output).
     */
    public void dumpHex(PrintStream writer) {
        int[] mem = new int[65536];
        int maxMem = createMemImage(mem);
        writer.println("v2.0 raw");
        for (int address = 0; address <= maxMem; address++) {
            int machineCode = mem[address];
            if (machineCode != -1) {
                writer.println(toHex(machineCode));
            } else {
                writer.println(toHex(0));
            }
        }
    }

    /**
     * Fills the given array with the generated machine code.
     *
     * @param mem an array of 65536 integers.  This array is expected to have already been created
     * @return the maximum address that was encountered
     */
    private int createMemImage(int[] mem) {
        for (int i = 0; i < 65536; i++) mem[i] = -1;
        int maxMem = 0;
        for (CodeLine line : codeLines) {
            int machineCode = line.getMachineCode();
            int address = line.getAddress();
            mem[address] = machineCode;
            if (address > maxMem) maxMem = address;
        }
        return maxMem;
    }

    public List<CodeLine> getCodeLines() {
        return codeLines;
    }

    /**
     * Writes verilog code to initialize the ROM in the Verilog version of the Moncky processor.
     *
     * @param writer The writer to which the output needs to be written (can be System.out for standard output).
     */
    public void writeVerilog(PrintStream writer) {
        int[] mem = new int[65536];
        int maxMem = createMemImage(mem);

        writer.println("module ROM (");
        writer.println("        input clk_mem,");
        writer.println("        input [15:0] address1,");
        writer.println("        output reg [15:0] data1,");
        writer.println("        input [15:0] address2,");
        writer.println("        output reg [15:0] data2");
        writer.println(");");
        writer.println("reg [15:0] my_rom [0:" + (ceilPower2(maxMem + 1) - 1) + "];");
        writer.println();
        writer.println("always @ (posedge clk_mem)");
        writer.println("begin");
        writer.println("    if (address1 < " + (maxMem + 1) + ")");
        writer.println("        data1 = my_rom[address1];");
        writer.println("    else");
        writer.println("        data1 = 16'h0;");
        writer.println("end");
        writer.println("always @ (posedge clk_mem)");
        writer.println("begin");
        writer.println("    if (address2 < " + (maxMem + 1) + ")");
        writer.println("        data2 = my_rom[address2];");
        writer.println("    else");
        writer.println("        data2 = 16'h0;");
        writer.println("end");
        writer.println();
        writer.println("\tinitial");
        writer.println("\tbegin");
        for (int address = 0; address <= maxMem; address++) {
            int machineCode = mem[address];
            if (machineCode != -1) {
                writer.println("\t\tmy_rom[" + address + "] = 16'h" + toHex(machineCode) + ";");
            } else {
                writer.println("\t\tmy_rom[" + address + "] = 16'h" + toHex(0) + ";");
            }
        }
        writer.println("\tend");
        writer.println("endmodule");
    }

    /**
     * Writes the generated machine code into a binary file.
     * The data is written in little endian (LSB first).
     *
     * @param writer The writer to which the output needs to be written (can be System.out for standard output).
     */
    public void writeBinary(PrintStream writer) {
        int[] mem = new int[65536];
        int maxMem = createMemImage(mem);
        for (int address = 0; address <= maxMem; address++) {
            int machineCode = mem[address];
            if (machineCode != -1) {
                writer.write((machineCode & 0xFF));
                writer.write((machineCode >> 8) & 0xFF);
            } else {
                writer.write(0);
                writer.write(0);
            }
        }
    }
}
