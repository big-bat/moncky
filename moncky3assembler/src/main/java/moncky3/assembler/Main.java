package moncky3.assembler;

import moncky3.assembler.backend.*;

import java.io.File;

/**
 * This class contains the main() method.
 * The program needs at least one command-line parameter indicating the instruction set to compile to.
 * If a second parameter is given (a filename ending in .asm is assumed), the program will translate that file and exit.
 * If three parameters are given, the second parameter is expected to be the location of the firmware libraries.
 * If only one parameter is given, the program starts up a GUI.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length == 0) printUsageAndExit();

        InstructionSet instructionSet = null;
        if ("Moncky1".equals(args[0])) instructionSet = new Moncky1InstructionSet();
        else if ("Moncky2".equals(args[0])) instructionSet = new Moncky2InstructionSet();
        else if ("Moncky3".equals(args[0])) instructionSet = new Moncky3InstructionSet();
        else printUsageAndExit();

        Assembler assembler = new Assembler(instructionSet);

        if (args.length == 1) {
            new AssemblerGUI(assembler);
        } else if (args.length == 2) {
            if (!args[1].endsWith(".asm")) {
                printUsageAndExit();
            }
            AssemblerCLI assemblerCLI = new AssemblerCLI(assembler, null);
            assemblerCLI.translate(args[1]);
        } else if (args.length == 3) {
            File file = new File(args[1]);
            if (!file.isDirectory() || !args[2].endsWith(".asm")) {
                printUsageAndExit();
            }
            AssemblerCLI assemblerCLI = new AssemblerCLI(assembler, args[1]);
            assemblerCLI.translate(args[2]);
        } else {
            printUsageAndExit();
        }
    }

    public static void printUsageAndExit() {
        System.err.println("Usage: " + Main.class.getName() + " Moncky1/2/3  [ [firmwareDir] source.asm ]");
        System.exit(1);
    }
}
