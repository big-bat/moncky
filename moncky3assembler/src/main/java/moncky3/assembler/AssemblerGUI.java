package moncky3.assembler;

import moncky3.assembler.backend.Assembler;
import moncky3.assembler.backend.CodeLines;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Map;

import static moncky3.assembler.backend.Util.toHex;

/**
 * This class starts up the assembler with a GUI.
 * The user can type or copy-paste text in a text field.  The source is continuously translated to machine code.
 */
public class AssemblerGUI extends JFrame {
    private final Assembler assembler;
    private JTextArea sourceTextArea;
    private JTextArea machineCodeTextArea;
    private JTextArea errorsTextArea;
    private JTextArea addressesTextArea;
    private JButton compileButton;
    private JButton dumpScreenButton;
    private JButton dumpLabelsButton;
    private CodeLines codeLines;

    public AssemblerGUI(Assembler assembler) throws HeadlessException {
        this.assembler = assembler;
        codeLines = new CodeLines();
        setTitle("Moncky3 assembler");
        createComponents();
        layoutComponents();
        addListeners();
        setSize(300, 300);
        setVisible(true);
    }

    private void addListeners() {
        compileButton.addActionListener(e -> codeLines.dumpHex(System.out));
        dumpScreenButton.addActionListener(e -> dumpScreen());
        sourceTextArea.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                translate();
            }
        });
        dumpLabelsButton.addActionListener(e -> dumpLabels());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void translate() {
        codeLines = assembler.translate(sourceTextArea.getText());
        machineCodeTextArea.setText(codeLines.getMachineCodesAsString());
        errorsTextArea.setText(codeLines.getErrorsAsString());
        addressesTextArea.setText(codeLines.getAddressesAsString());
    }

    private void dumpLabels() {
        Map<String, Integer> labels = assembler.getLabels();
        for (String label : labels.keySet()) {
            int address = labels.get(label);
            System.out.println(label + ": 0x" + toHex(address));
        }
    }

    private void dumpScreen() {
        codeLines.writeResult(System.out);
    }

    private void layoutComponents() {
        JPanel panel = new JPanel(new BorderLayout(5, 0));
        panel.add(sourceTextArea, BorderLayout.CENTER);
        JPanel panel2 = new JPanel(new BorderLayout(5, 0));
        JPanel panel3 = new JPanel(new BorderLayout(5, 0));
        panel3.add(machineCodeTextArea, BorderLayout.EAST);
        panel3.add(addressesTextArea, BorderLayout.WEST);
        panel2.add(panel3, BorderLayout.WEST);
        panel2.add(errorsTextArea, BorderLayout.CENTER);
        panel.add(panel2, BorderLayout.EAST);
        JScrollPane scrollPane = new JScrollPane(panel);
        Container contentPane = getContentPane();
        contentPane.add(scrollPane, BorderLayout.CENTER);
        JPanel panel4 = new JPanel(new GridLayout(1, 3, 5, 0));
        panel4.add(compileButton);
        panel4.add(dumpScreenButton);
        panel4.add(dumpLabelsButton);
        contentPane.add(panel4, BorderLayout.SOUTH);
    }

    private void createComponents() {
        this.sourceTextArea = new JTextArea();
        this.machineCodeTextArea = new JTextArea();
        this.machineCodeTextArea.setEditable(false);
        this.errorsTextArea = new JTextArea();
        this.errorsTextArea.setEditable(false);
        this.addressesTextArea = new JTextArea();
        this.addressesTextArea.setEditable(false);
        this.compileButton = new JButton("dump hex");
        this.dumpScreenButton = new JButton("dump screen");
        this.dumpLabelsButton = new JButton("dump labels");
    }
}
