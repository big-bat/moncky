import moncky3.simulator.Memory;
import moncky3.simulator.Moncky3Processor;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class TestMoncky3Processor {
    private Memory memory;
    private Moncky3Processor moncky3Processor;

    @BeforeTest
    public void setup() {
        memory = new Memory();
        moncky3Processor = new Moncky3Processor(memory);
    }

    @Test
    public void testReset() {
        moncky3Processor.registers[15] = 0xFF;
        moncky3Processor.performInstruction(0x0000);
        assertEquals(0, moncky3Processor.registers[15]);
    }

    @Test
    public void testPush() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[14] = 0xF000;
        moncky3Processor.registers[2] = 0xF0F0;
        moncky3Processor.performInstruction(0x2080);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0xF0F0, memory.getMem(0xF000));
        assertEquals(0xEFFF, moncky3Processor.registers[14]);
    }

    @Test
    public void testPop() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[14] = 0xF000;
        memory.setMem(0xF001, 0xABAB);
        moncky3Processor.performInstruction(0x5100);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0xABAB, moncky3Processor.registers[5]);
    }

    @Test
    public void testCall() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[14] = 0xF000;
        moncky3Processor.registers[6] = 0xABCD;
        moncky3Processor.performInstruction(0x6180);
        assertEquals(0xABCD, moncky3Processor.registers[15]);
        assertEquals(0x000B, memory.getMem(0xF000));
        assertEquals(0xEFFF, moncky3Processor.registers[14]);
    }

    @Test
    public void testAdd1() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[4] = 0x0035;
        moncky3Processor.registers[5] = 0x0037;
        moncky3Processor.performInstruction(0x4548);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0035+0x0037, moncky3Processor.registers[4]);
        assertEquals(false, moncky3Processor.carryFlag);
        assertEquals(false, moncky3Processor.zeroFlag);
        assertEquals(false, moncky3Processor.signFlag);
        assertEquals(false, moncky3Processor.overflowFlag);
    }

    @Test
    public void testAdd2() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[4] = 0xFFFE;
        moncky3Processor.registers[5] = 0x0037;
        moncky3Processor.performInstruction(0x4548);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0035, moncky3Processor.registers[4]);
        assertEquals(true, moncky3Processor.carryFlag);
        assertEquals(false, moncky3Processor.zeroFlag);
        assertEquals(false, moncky3Processor.signFlag);
        assertEquals(false, moncky3Processor.overflowFlag);
    }

    @Test
    public void testAdd3() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[4] = 0x7FFE;
        moncky3Processor.registers[5] = 0x0037;
        moncky3Processor.performInstruction(0x4548);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x8035, moncky3Processor.registers[4]);
        assertEquals(false, moncky3Processor.carryFlag);
        assertEquals(false, moncky3Processor.zeroFlag);
        assertEquals(true, moncky3Processor.signFlag);
        assertEquals(true, moncky3Processor.overflowFlag);
    }

    @Test
    public void testAdd4() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[4] = 0x0001;
        moncky3Processor.registers[5] = 0xFFFF;
        moncky3Processor.performInstruction(0x4548);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[4]);
        assertEquals(true, moncky3Processor.carryFlag);
        assertEquals(true, moncky3Processor.zeroFlag);
        assertEquals(false, moncky3Processor.signFlag);
        assertEquals(false, moncky3Processor.overflowFlag);
    }

    @Test
    public void testAdd5() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[4] = 0x0011;
        moncky3Processor.performInstruction(0x4EF3);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[4]);
        assertEquals(true, moncky3Processor.carryFlag);
        assertEquals(true, moncky3Processor.zeroFlag);
        assertEquals(false, moncky3Processor.signFlag);
        assertEquals(false, moncky3Processor.overflowFlag);
    }

    @Test
    public void testSub() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[4] = 0x0001;
        moncky3Processor.registers[5] = 0xFFFF;
        moncky3Processor.performInstruction(0x4558);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0002, moncky3Processor.registers[4]);
        assertEquals(false, moncky3Processor.carryFlag);
        assertEquals(false, moncky3Processor.zeroFlag);
        assertEquals(false, moncky3Processor.signFlag);
        assertEquals(false, moncky3Processor.overflowFlag);
    }

    @Test
    public void testSubi() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[4] = 0x0011;
        moncky3Processor.performInstruction(0x4F5A);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0002, moncky3Processor.registers[4]);
        assertEquals(true, moncky3Processor.carryFlag);
        assertEquals(false, moncky3Processor.zeroFlag);
        assertEquals(false, moncky3Processor.signFlag);
        assertEquals(false, moncky3Processor.overflowFlag);
    }

    @Test
    public void testJpz() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[7] = 0xCDEF;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7C00);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        moncky3Processor.zeroFlag = true;
        moncky3Processor.performInstruction(0x7C00);
        assertEquals(0xCDEF, moncky3Processor.registers[15]);
    }

    @Test
    public void testJpnz() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[7] = 0xCDEF;
        moncky3Processor.zeroFlag = true;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7C80);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        moncky3Processor.zeroFlag = false;
        moncky3Processor.performInstruction(0x7C80);
        assertEquals(0xCDEF, moncky3Processor.registers[15]);
    }

    @Test
    public void testJpc() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[7] = 0xCDEF;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7D00);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        moncky3Processor.carryFlag = true;
        moncky3Processor.performInstruction(0x7D00);
        assertEquals(0xCDEF, moncky3Processor.registers[15]);
    }

    @Test
    public void testJpnc() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[7] = 0xCDEF;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = true;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7D80);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        moncky3Processor.carryFlag = false;
        moncky3Processor.performInstruction(0x7D80);
        assertEquals(0xCDEF, moncky3Processor.registers[15]);
    }

    @Test
    public void testJps() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[7] = 0xCDEF;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7E00);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        moncky3Processor.signFlag = true;
        moncky3Processor.performInstruction(0x7E00);
        assertEquals(0xCDEF, moncky3Processor.registers[15]);
    }

    @Test
    public void testJpns() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[7] = 0xCDEF;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = true;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7E80);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        moncky3Processor.signFlag = false;
        moncky3Processor.performInstruction(0x7E80);
        assertEquals(0xCDEF, moncky3Processor.registers[15]);
    }

    @Test
    public void testJpo() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[7] = 0xCDEF;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7F00);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7F00);
        assertEquals(0xCDEF, moncky3Processor.registers[15]);
    }

    @Test
    public void testJpno() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.registers[7] = 0xCDEF;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7F80);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7F80);
        assertEquals(0xCDEF, moncky3Processor.registers[15]);
    }

    @Test
    public void testSz() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = true;
        moncky3Processor.signFlag = true;
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7800);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[7]);
        moncky3Processor.zeroFlag = true;
        moncky3Processor.performInstruction(0x7800);
        assertEquals(0x000C, moncky3Processor.registers[15]);
        assertEquals(0xFFFF, moncky3Processor.registers[7]);
    }

    @Test
    public void testSnz() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = false;
        moncky3Processor.carryFlag = true;
        moncky3Processor.signFlag = true;
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7880);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0xFFFF, moncky3Processor.registers[7]);
        moncky3Processor.zeroFlag = true;
        moncky3Processor.performInstruction(0x7880);
        assertEquals(0x000C, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[7]);
    }

    @Test
    public void testSc() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = true;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = true;
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7900);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[7]);
        moncky3Processor.carryFlag = true;
        moncky3Processor.performInstruction(0x7900);
        assertEquals(0x000C, moncky3Processor.registers[15]);
        assertEquals(0xFFFF, moncky3Processor.registers[7]);
    }

    @Test
    public void testSnc() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = true;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = true;
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7980);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0xFFFF, moncky3Processor.registers[7]);
        moncky3Processor.carryFlag = true;
        moncky3Processor.performInstruction(0x7980);
        assertEquals(0x000C, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[7]);
    }

    @Test
    public void testSs() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = true;
        moncky3Processor.carryFlag = true;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7A00);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[7]);
        moncky3Processor.signFlag = true;
        moncky3Processor.performInstruction(0x7A00);
        assertEquals(0x000C, moncky3Processor.registers[15]);
        assertEquals(0xFFFF, moncky3Processor.registers[7]);
    }

    @Test
    public void testSns() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = true;
        moncky3Processor.carryFlag = true;
        moncky3Processor.signFlag = false;
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7A80);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0xFFFF, moncky3Processor.registers[7]);
        moncky3Processor.signFlag = true;
        moncky3Processor.performInstruction(0x7A80);
        assertEquals(0x000C, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[7]);
    }

    @Test
    public void testSo() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = true;
        moncky3Processor.carryFlag = true;
        moncky3Processor.signFlag = true;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7B00);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[7]);
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7B00);
        assertEquals(0x000C, moncky3Processor.registers[15]);
        assertEquals(0xFFFF, moncky3Processor.registers[7]);
    }

    @Test
    public void testSno() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = true;
        moncky3Processor.carryFlag = true;
        moncky3Processor.signFlag = true;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7B80);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0xFFFF, moncky3Processor.registers[7]);
        moncky3Processor.overflowFlag = true;
        moncky3Processor.performInstruction(0x7B80);
        assertEquals(0x000C, moncky3Processor.registers[15]);
        assertEquals(0x0000, moncky3Processor.registers[7]);
    }

    @Test
    public void testSflagsRFlags() {
        moncky3Processor.registers[15] = 0x000A;
        moncky3Processor.zeroFlag = true;
        moncky3Processor.carryFlag = false;
        moncky3Processor.signFlag = true;
        moncky3Processor.overflowFlag = false;
        moncky3Processor.performInstruction(0x7200);
        assertEquals(0x000B, moncky3Processor.registers[15]);
        assertEquals(0x0005, moncky3Processor.registers[7]);
        moncky3Processor.registers[3] = 0x000A;
        moncky3Processor.performInstruction(0x3280);
        assertEquals(false, moncky3Processor.zeroFlag);
        assertEquals(true, moncky3Processor.carryFlag);
        assertEquals(false, moncky3Processor.signFlag);
        assertEquals(true, moncky3Processor.overflowFlag);
    }

    @Test
    public void testCalculations() {
        moncky3Processor.registers[3] = 0xD8;
        moncky3Processor.carryFlag = false;
        int result = moncky3Processor.performAluif(13, 3, 0xD8);
        assertEquals(0xD800, result);
    }

    @Test
    public void testShri() {
        moncky3Processor.registers[15] = 0x00FF;
        moncky3Processor.registers[4] = 0x8010;
        moncky3Processor.carryFlag = true;
        moncky3Processor.performInstruction(0x428A);
        assertEquals(0x0100, moncky3Processor.registers[15]);
        assertEquals(0xE004, moncky3Processor.registers[4]);
    }

}
