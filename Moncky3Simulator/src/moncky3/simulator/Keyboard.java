package moncky3.simulator;

import java.util.ArrayList;
import java.util.List;

public class Keyboard {
    private static final int KEYBOARD = 0x7D01;
    private Memory memory;
    private List<Integer> buffer;
    private int[] keyToScanCodes;

    public Keyboard(Memory memory) {
        this.memory = memory;
        this.buffer = new ArrayList<>();
        this.keyToScanCodes = new int[256];
        fillKeyToScanCodes();
    }

    private void fillKeyToScanCodes() {
        // @todo to be completed
        keyToScanCodes[32] = 0x29;
        keyToScanCodes[37] = 0x16B;
        keyToScanCodes[38] = 0x175;
        keyToScanCodes[39] = 0x174;
        keyToScanCodes[40] = 0x172;
    }

    public void emptyBuffer() {
        if (buffer.size()>0 && memory.getIo(KEYBOARD)==0) {
            memory.setIo(KEYBOARD, buffer.get(0));
            buffer.remove(0);
        }
    }

    private void writeCode(int keycode) {
        if (buffer.size()==0 && memory.getIo(KEYBOARD) ==0) {
            memory.setIo(KEYBOARD, keycode);
        } else {
            //System.out.println("adding to buffer");
            buffer.add(keycode);
        }
    }

    private int lookup(int keycode) {
        return keyToScanCodes[keycode];
    }

    public void press(int keycode) {
        keycode = lookup(keycode);
        writeCode(keycode);
    }

    public void release(int keycode) {
        keycode = lookup(keycode);
        writeCode(512 + keycode);
    }
}
