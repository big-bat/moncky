package moncky3.simulator;

import javax.swing.*;
import java.awt.*;

public class Moncky3ProcessorFrame extends JFrame {
    private JTextArea textArea;
    private JButton reloadButton;
    private JToggleButton automaticButton;
    private Moncky3Processor moncky3Processor;
    private Memory memory;
    private MyThread thread;

    class MyThread implements Runnable {
        private boolean running = true;

        public void setRunning(boolean running) {
            this.running = running;
        }

        @Override
        public void run() {
            while (running) {
                reload();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public Moncky3ProcessorFrame(Moncky3Processor moncky3Processor, Memory memory) throws HeadlessException {
        super("Moncky3 processor");
        this.moncky3Processor = moncky3Processor;
        this.memory = memory;
        this.textArea = new JTextArea();
        this.reloadButton = new JButton("Reload");
        this.automaticButton = new JToggleButton("Auto");
        JPanel panel = new JPanel(new GridLayout(1, 2));
        panel.add(reloadButton);
        panel.add(automaticButton);
        this.add(panel, BorderLayout.SOUTH);
        this.add(textArea, BorderLayout.CENTER);
        reloadButton.addActionListener(actionEvent -> reload());
        automaticButton.addActionListener(actionEvent -> automatic());
        this.setSize(200, 400);
        this.setLocation(720, 0);
        setVisible(true);
    }

    private void automatic() {
        if (automaticButton.isSelected()) {
            thread = new MyThread();
            new Thread(thread).start();
        } else {
            thread.setRunning(false);
        }
    }

    private void reload() {
        StringBuilder builder = new StringBuilder();
        if (moncky3Processor.halted) {
            builder.append("*** HALTED ***\n");
        }
        builder.append("Speed: ");
        builder.append(moncky3Processor.getAverageDelay());
        builder.append('\n');
        builder.append("Registers:\n");
        for(int i=0; i<16; i++) {
            builder.append("r" + i + " = " + Integer.toHexString(moncky3Processor.registers[i]) + '\n');
        }
        builder.append("Top of stack:\n");
        for(int i=10; i>0; i--) {
            if ((moncky3Processor.registers[14] + i) < 0xFFFF) {
                String value = Integer.toHexString(memory.getMem(moncky3Processor.registers[14] + i));
                builder.append("stack(" + Integer.toHexString(moncky3Processor.registers[14]+i) + ") = " + value + '\n');
            }
        }
        this.textArea.setText(builder.toString());
    }
}