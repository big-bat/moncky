package moncky3.simulator;

import java.awt.*;
import java.awt.image.BufferedImage;

public class VGAController {
    private final static int NUMBER_OF_LINES = 525;
    private final static int NUMBER_OF_COLUMNS = 800;
    private BufferedImage image;
    private Memory memory;
    private Moncky3Processor moncky3Processor;
    private int y;

    public VGAController(Memory memory, Moncky3Processor moncky3Processor) {
        this.memory = memory;
        this.moncky3Processor = moncky3Processor;
        this.image = new BufferedImage(320, 200, BufferedImage.TYPE_3BYTE_BGR);
        this.y = 0;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void updateOneLine() {
        if ((y < 400) && ((y & 1) == 0)) {
            for(int x = 0; x < 320; x+=2) {
                int config = memory.getMem(0x7D00);
                int address = (x / 2) + (y/2) * 160 + ((config & 1) << 15);
                int value = memory.getIo(address);
                int msb = value >> 8;
                int lsb = value & 0xFF;
                int r1 = lsb >> 5;
                r1 = (r1 << 5) | (r1 << 2) | (r1 >> 1);
                int g1 = (lsb >> 2) & 7;
                g1 = (g1 << 5) | (g1 << 2) | (g1 >> 1);
                int b1 = lsb & 3;
                b1 = (b1 << 6) | (b1 << 4) | (b1 << 2) | b1;
                int r2 = msb >> 5;
                r2 = (r2 << 5) | (r2 << 2) | (r2 >> 1);
                int g2 = (msb >> 2) & 7;
                g2 = (g2 << 5) | (g2 << 2) | (g2 >> 1);
                int b2 = msb & 3;
                b2 = (b2 << 6) | (b2 << 4) | (b2 << 2) | b2;
                Graphics graphics = image.getGraphics();
                Color color = new Color(r1, g1, b1);
                graphics.setColor(color);
                graphics.fillRect(x, (y/2), 1, 1);
                color = new Color(r2, g2, b2);
                graphics.setColor(color);
                graphics.fillRect((x + 1), (y/2), 1, 1);
            }
        }
        y++;
        if (y == 400) {
            moncky3Processor.interrupt();
        }
        if (y > NUMBER_OF_LINES) {
            y = 0;
        }
    }
}
