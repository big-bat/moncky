package moncky3.simulator;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        Memory memory = new Memory();
        memory.load("../Moncky3Compiler/files/MONCKYOS");
        Moncky3Processor moncky3Processor = new Moncky3Processor(memory);
        Keyboard keyboard = new Keyboard(memory);
        Timer timer = new Timer(memory);
        VGAController vgaController = new VGAController(memory, moncky3Processor);
        new Moncky3ProcessorFrame(moncky3Processor, memory);
        ScreenFrame screenFrame = new ScreenFrame(keyboard, vgaController);
        long millis = System.currentTimeMillis();
        Set<Integer> breakpoints = new HashSet<>();
        //breakpoints.add(0x2F32);
        boolean stop = false;
        long lastTime  = 0; // to measure times between breakpoints
        long[] profilingTable = new long[65536];
        for(int i = 0; i< 65535; i++) profilingTable[i] = 0;
        long timeToStop = -1; //600000000; // in clockcycles; put at negative value to never stop
        long line = 0; // the number of lines generated on screen so far
        while(!stop) {
            line++;
            while (moncky3Processor.getTime()<(10000000.0/60.0/525.0*line)) {
                profilingTable[moncky3Processor.registers[15]] ++;
                moncky3Processor.clock();
                if (breakpoints.contains(moncky3Processor.registers[15])) {
                    System.out.println("time(@0x" + Integer.toHexString(moncky3Processor.registers[15]) + ") = " + (moncky3Processor.getTime()-lastTime));
                    lastTime = moncky3Processor.getTime();
                    //stop = true;
                }
                keyboard.emptyBuffer();
                timer.tick();
            }
            vgaController.updateOneLine();
            screenFrame.repaint();
            if ((timeToStop > 0) && (moncky3Processor.getTime() >= timeToStop)) stop = true;
            long timePast = System.currentTimeMillis() - millis;
            moncky3Processor.recordSpeed(timePast);
            //if (timePast < 16) {
            //    Thread.sleep(16 - timePast);
            //}
            millis = System.currentTimeMillis();
        }
        printProfilingTable(profilingTable);
    }

    private static void printProfilingTable(long[] profilingTable) {
        for(int i=0; i<100; i++) {
            int maxIndex = findMaxIndex(profilingTable);
            System.out.println("0x" + Integer.toHexString(maxIndex) + ": " + profilingTable[maxIndex]);
            profilingTable[maxIndex] = 0;
        }
    }

    private static int findMaxIndex(long[] profilingTable) {
        long max = 0;
        int maxindex = 0;
        for(int i=0; i<65536; i++) {
            if (profilingTable[i] > max) {
                max = profilingTable[i];
                maxindex = i;
            }
        }
        return maxindex;
    }
}
