package moncky3.simulator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ScreenFrame extends JFrame {
    private final ScreenPanel panel;

    public ScreenFrame(Keyboard keyboard, VGAController vgaController) {
        this.setTitle("Moncky screen");
        panel = new ScreenPanel(vgaController);
        this.add(panel, BorderLayout.CENTER);
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                int keycode = keyEvent.getKeyCode();
                keyboard.press(keycode);
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                int keycode = keyEvent.getKeyCode();
                keyboard.release(keycode);
            }
        });
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosed(e);
                System.exit(0);
            }
        });
        this.pack();
        setVisible(true);
    }
}
