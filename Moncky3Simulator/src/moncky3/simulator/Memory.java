package moncky3.simulator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Memory {
    private static final int KEYBOARD = 0x7D01;
    private int[] memory;
    private int[] ioMemory;

    public Memory() {
        this.memory = new int[65536];
        this.ioMemory = new int[65536];
        for(int i=0; i<65536; i++) {
            this.memory[i] = 0;
            this.ioMemory[i] = 0;
        }
    }

    public int getMem(int address) {
        return memory[address];
    }

    synchronized public void setMem(int address, int value) {
        memory[address] = value;
    }

    public int getIo(int address) {
        int result = ioMemory[address];
        if (address == KEYBOARD) {
            ioMemory[address] = 0;
        }
        return result;
    }

    synchronized public void setIo(int address, int value) {
        ioMemory[address] = value;
    }

    public void load(String filename) throws IOException {
        InputStream inputStream = new FileInputStream(filename);
        int byteRead;
        int address = 0;
        while ((byteRead = inputStream.read()) != -1) {
            int wordRead = byteRead;
            byteRead = inputStream.read();
            if (byteRead == -1) throw new IOException("number of bytes should be even");
            wordRead = wordRead | (byteRead << 8);
            memory[address] = wordRead;
            address++;
        }
        inputStream.close();
    }
}
