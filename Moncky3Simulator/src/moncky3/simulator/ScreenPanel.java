package moncky3.simulator;

import javax.swing.*;
import java.awt.*;

public class ScreenPanel extends JPanel {
    private final static int SIZE = 4;
    private final VGAController vgaController;

    public ScreenPanel(VGAController vgaController) {
        this.vgaController = vgaController;
        setPreferredSize(new Dimension(320 * SIZE, 200 * SIZE));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(vgaController.getImage(), 0, 0, (320 * SIZE) - 1, (200 * SIZE) -1, null);
    }
}
