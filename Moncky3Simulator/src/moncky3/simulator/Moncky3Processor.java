package moncky3.simulator;

import java.util.*;

public class Moncky3Processor {
    private static final int PC = 15;
    private static final int SP = 14;
    public final int[] registers;
    public boolean zeroFlag;
    public boolean carryFlag;
    public boolean signFlag;
    public boolean overflowFlag;
    private final Memory memory;
    public boolean halted;
    private boolean interruptsEnabled;
    private boolean pcUpdated;
    private int instruction;
    private long clockCycles;

    private final LinkedList<Long> delays;

    private int lastInstruction;
    private int lastPC;

    public Moncky3Processor(Memory memory) {
        this.registers = new int[16];
        this.zeroFlag = false;
        this.carryFlag = false;
        this.signFlag = false;
        this.overflowFlag = false;
        registers[PC] = 0;
        this.memory = memory;
        this.halted = false;
        this.interruptsEnabled = false;
        this.delays = new LinkedList<>();
        this.clockCycles = 0;
    }

    /**
     *
     * @return true if a breakpoint was reached
     */
    public void clock() {
        clockCycles++;
        instruction = memory.getMem(registers[PC]);
        if (instruction == 0x0000) {
            System.out.println("***** reset *****");
            printStatus();
            //System.exit(1);
        }
        lastPC = registers[15];
        performInstruction(instruction);
        lastInstruction = instruction;
    }

    public void performInstruction(int instruction) {
        this.pcUpdated = false;
        if ((instruction & 0xFFF) == 0) {
            decodeGroup0(instruction);
        } else if ((instruction & 0x7F) == 0) {
            decodeGroup1(instruction);
        } else if ((instruction & 0xF) == 0) {
            decodeGroup2(instruction);
        } else {
            decodeGroup3(instruction);
        }
        increasePC();
    }

    private void increasePC() {
        if (!pcUpdated && !halted) registers[PC]++;
    }

    private int getRegister(int reg) {
        if (reg == PC) {
            return registers[PC] + 1;
        } else {
            return registers[reg];
        }
    }

    private void setRegister(int reg, int value) {
        if (value < 0 || value > 0xFFFF) {
            printStatus();
            throw new IllegalArgumentException("value in register r" + reg + " out of range (" + value + ")");
        }
        registers[reg] = value;
        if (reg == PC) {
            pcUpdated = true;
        }
    }

    private void performReset() {
        setRegister(PC, 0);
        performDi();
    }

    private void performHalt() {
        this.halted = true;
        performEi();
    }

    private void performEi() {
        this.interruptsEnabled = true;
    }

    private void performDi() {
        this.interruptsEnabled = false;
    }

    private void performReti() {
        performPop(PC);
        performEi();
    }

    private void performInt() {
        performPush(PC);
        setRegister(PC, 0x10);
        performDi();
    }

    public void interrupt() {
        if (interruptsEnabled) {
            memory.setMem(getRegister(SP), registers[PC]);
            halted = false;
            registers[SP]--;
            performDi();
            setRegister(PC, 0x10);
        }
    }

    private void performPush(int register) {
        memory.setMem(getRegister(SP), getRegister(register));
        registers[SP]--;
    }

    private void performPop(int register) {
        registers[SP]++;
        setRegister(register, memory.getMem(getRegister(SP)));
    }

    private void performCall(int register) {
        performPush(PC);
        setRegister(PC, getRegister(register));
    }

    private void performSFlags(int register) {
        int flags = 0;
        if (zeroFlag) flags |= 1;
        if (carryFlag) flags |= 2;
        if (signFlag) flags |= 4;
        if (overflowFlag) flags |= 8;
        setRegister(register, flags);
    }

    private void performRFlags(int register) {
        int flags = getRegister(register);
        zeroFlag = ((flags & 1) != 0);
        carryFlag = ((flags & 2) != 0);
        signFlag = ((flags & 4) != 0);
        overflowFlag = ((flags & 8) != 0);
    }

    private void performJp(int register) {
        setRegister(PC, getRegister(register));
    }

    private void performSaveFlag(int flagSelect, int register) {
        boolean b;
        switch (flagSelect) {
            case 0: b = zeroFlag; break;
            case 1: b = !zeroFlag; break;
            case 2: b = carryFlag; break;
            case 3: b = !carryFlag; break;
            case 4: b = signFlag; break;
            case 5: b = !signFlag; break;
            case 6: b = overflowFlag; break;
            case 7: b = !overflowFlag; break;
            default: throw new IllegalArgumentException("flagSelect out of range: " + flagSelect);
        }
        if (b) {
            setRegister(register, 0xFFFF);
        } else {
            setRegister(register, 0);
        }
    }

    private void performJpc(int flagSelect, int register) {
        switch (flagSelect) {
            case 0: if (zeroFlag) performJp(register); break;
            case 1: if (!zeroFlag) performJp(register); break;
            case 2: if (carryFlag) performJp(register); break;
            case 3: if (!carryFlag) performJp(register); break;
            case 4: if (signFlag) performJp(register); break;
            case 5: if (!signFlag) performJp(register); break;
            case 6: if (overflowFlag) performJp(register); break;
            case 7: if (!overflowFlag) performJp(register); break;
            default: throw new IllegalArgumentException("flagSelect out of range: " + flagSelect);
        }
    }

    private void performIn(int regR, int regS) {
        int address = getRegister(regS);
        int value = memory.getIo(address);
        setRegister(regR, value);
    }

    private void performOut(int regR, int regS) {
        int address = getRegister(regS);
        memory.setIo(address, getRegister(regR));
    }

    private void decodeGroup0(int instruction) {
        int opcode = instruction >> 12;
        switch (opcode) {
            case 0: performReset(); break;
            case 1: performHalt(); break;
            case 2: performEi(); break;
            case 3: performDi(); break;
            case 4: performReti(); break;
            case 5: performInt(); break;
            default: throw new IllegalArgumentException("unknown instruction: " + Integer.toHexString(instruction));
        }
    }

    private void decodeGroup1(int instruction) {
        int opcode = (instruction >> 7) & 0x1F;
        int param = instruction >> 12;
        switch (opcode) {
            case 1: performPush(param); break;
            case 2: performPop(param); break;
            case 3: performCall(param); break;
            case 4: performSFlags(param); break;
            case 5: performRFlags(param); break;
            case 6: performJp(param); break;
            default: {
                if ((opcode & 0x18) == 0x10) {
                    int flagSelect = opcode & 7;
                    performSaveFlag(flagSelect, param);
                } else if ((opcode & 0x18) == 0x18) {
                    int flagSelect = opcode & 7;
                    performJpc(flagSelect, param);
                } else {
                    throw new IllegalArgumentException("unknown instruction: " + Integer.toHexString(instruction));
                }
            }
        }
    }

    private void performLi(int register, int value) {
        setRegister(register, value);
    }

    private void performLih(int register, int value) {
        int val = getRegister(register);
        val = val | (value << 8);
        setRegister(register, val);
    }

    private int setFlags(int value) {
        // carry
        if ((value & 0x10000) !=0) {
            value = value & 0xFFFF;
            carryFlag = true;
        } else {
            carryFlag = false;
        }
        // sign
        signFlag = ((value & 0x8000) != 0);
        // zero
        zeroFlag = (value == 0);
        return value;
    }

    private void performAddi(int register, int immediate) {
        int val = getRegister(register);
        val = val + immediate;
        val = setFlags(val);
        setRegister(register, val);
    }

    private void performAndi(int register, int immediate) {
        int val = getRegister(register);
        val = val & immediate;
        val = setFlags(val);
        setRegister(register, val);
    }

    private void performOri(int register, int immediate) {
        int val = getRegister(register);
        val = val | immediate;
        val = setFlags(val);
        setRegister(register, val);
    }

    private void performCmpi(int register, int immediate) {
        int val = getRegister(register);
        val = val + inverse(immediate) + 1;
        setFlags(val);
    }

    private void performCmpir(int register, int immediate) {
        int val = getRegister(register);
        val = immediate + inverse(val) + 1;
        setFlags(val);
    }

    private int inverse(int value) {
        return (~value) & 0xFFFF;
    }

    private void performAlu(int operation, int regR, int regS) {
        int result = performAluf(operation, regR, regS);
        if (result <0) {
            System.err.println("operation = " + operation);
            System.err.println("regR = " + regR);
            System.err.println("regS = " + regS);
        }
        setRegister(regR, result);
    }

    private int performAluf(int operation, int regR, int regS) {
        int valS = getRegister(regS);
        return performAluif(operation, regR, valS);
    }

    private void performAlui(int operation, int regR, int immediate) {
        int result = performAluif(operation, regR, immediate);
        setRegister(regR, result);
    }

    public int performAluif(int operation, int regR, int immediate) {
        int valR = getRegister(regR);
        int result;
        overflowFlag = false;
        switch (operation) {
            case 0: result  = immediate; break;
            case 1: result = valR | immediate; break;
            case 2: result = valR & immediate; break;
            case 3: result = valR ^ immediate; break;
            case 4: result = sum(valR, immediate, 0); break;
            case 5: result = sum(valR, inverse(immediate), 1); break;
            case 6: result = shiftLefCarry(valR, immediate, 0); break;
            case 7: result = shiftRightCarry(valR, immediate, 0); break;
            case 8: result = shiftRightCarry(valR, immediate, (valR & 0x8000)>>>15); break;
            case 9: result = inverse(immediate); break;
            case 10: result = sum(0, inverse(immediate), 1); break;
            case 11: result = sum(valR, immediate, getCarryFlag()); break;
            case 12: result = sum(valR, inverse(immediate), getCarryFlag()); break;
            case 13: result = shiftLefCarry(valR, immediate, getCarryFlag()); break;
            case 14: result = shiftRightCarry(valR, immediate, getCarryFlag()); break;
            default: throw new IllegalArgumentException("unsupported ALU operation: " + operation);
        }
        return setFlags(result);
    }

    private int sum(int val1, int val2, int carryFlag) {
        int result = val1 + val2 + carryFlag;
        if (((val1 & 0x8000)==(val2 & 0x8000)) && ((result & 0x8000)!=(val1 & 0x8000))) overflowFlag = true;
        return result;
    }

    private int shiftRightCarry(int number, int places, int carryFlag) {
        places = places & 15;
        int carry = 0;
        for(int i=0; i<places; i++) {
            carry = number & 1;
            number = (number >>> 1) | (carryFlag << 15);
        }
        number = number | (0x10000 * carry);
        return number;
    }

    private int shiftLefCarry(int number, int places, int carryFlag) {
        places = places & 15;
        for(int i=0; i<places; i++) {
            number = (number << 1) | carryFlag;
        }
        return (number & 0x1FFFF);
    }

    private int getCarryFlag() {
        return carryFlag?1:0;
    }

    private void performLda(int regR, int regS, int regT) {
        int valS = getRegister(regS);
        performLdi(regR, regT, valS);
    }

    private void performSta(int regR, int regS, int regT) {
        int valS = getRegister(regS);
        performSti(regR, regT, valS);
    }

    private void performLdi(int regR, int regT, int immediate) {
        int valT = getRegister(regT);
        int address = immediate + valT;
        int value = memory.getMem(address);
        setRegister(regR, value);
    }

    private void performSti(int regR, int regT, int immediate) {
        int valT = getRegister(regT);
        int address = immediate + valT;
        memory.setMem(address, getRegister(regR));
    }

    private void decodeGroup2(int instruction) {
        int opcode = (instruction >> 4) & 0xF;
        int param1 = instruction >> 12;
        int param2 = (instruction >> 8) & 0xF;
        switch (opcode) {
            case 1: performIn(param1, param2); break;
            case 2: performOut(param1, param2); break;
            default: throw new IllegalArgumentException("unknown instruction: " + Integer.toHexString(instruction));
        }
    }

    private void decodeGroup3(int instruction) {
        int opcode = instruction & 0xF;
        int param1 = instruction >> 12;
        int param2 = (instruction >> 8) & 0xF;
        int param3 = (instruction >> 4) & 0xF;
        int immediate = (instruction >> 4) & 0xFF;
        int signExtended = ((immediate & 128)==128)?(immediate|0xFF00):immediate;
        switch (opcode) {
            case 1: performLi(param1, immediate); break;
            case 2: performLih(param1, immediate); break;
            case 3: performAddi(param1, signExtended); break;
            case 4: performAndi(param1, immediate); break;
            case 5: performOri(param1, immediate); break;
            case 6: performCmpi(param1, signExtended); break;
            case 7: performCmpir(param1, signExtended); break;
            case 8: performAlu(param3, param1, param2); break;
            case 9: performAluf(param3, param1, param2); break;
            case 10: performAlui(param3, param1, param2); break;
            case 11: performAluif(param3, param1, param2); break;
            case 12: performLda(param1, param2, param3); break;
            case 13: performSta(param1, param2, param3); break;
            case 14: performLdi(param1, param3, param2); break;
            case 15: performSti(param1, param3, param2); break;
            default: throw new IllegalArgumentException("unknown instruction: " + Integer.toHexString(instruction));
        }
    }

    public void printStatus() {
        if (halted) {
            System.out.println("*** HALTED ***");
        }
        System.out.println("***** PC = 0x" + Integer.toHexString(registers[PC]));
        System.out.println("instruction = 0x" + Integer.toHexString(instruction));
        System.out.println("lastInstruction = 0x" + Integer.toHexString(lastInstruction));
        System.out.println("lastPC = 0x" + Integer.toHexString(lastPC));
        System.out.println("clockCycles = " + clockCycles);;
        System.out.println("Registers:");
        for(int i=0; i<16; i++) {
            System.out.println("r" + i + " = 0x" + Integer.toHexString(registers[i]));
        }
        System.out.println("Top of stack:");
        for(int i=10; i>0; i--) {
            if ((registers[SP] + i) < 0xFFFF) {
                String value = Integer.toHexString(memory.getMem(registers[SP] + i));
                System.out.println("stack(0x" + Integer.toHexString(registers[SP]+i) + ") = 0x" + value);
            }
        }
    }

    public synchronized void recordSpeed(long timePast) {
        delays.addFirst(timePast);
        if (delays.size()>25) {
            delays.removeLast();
        }
    }

    public synchronized long getAverageDelay() {
        long result = 0;
        for(long delay : delays) {
            result += delay;
        }
        return result/delays.size();
    }

    public long getTime() {
        return clockCycles;
    }
}
