package moncky3.simulator;

public class Timer {
    private final Memory memory;
    private int ticks;
    private long usec;
    private long msec;

    public Timer(Memory memory) {
        this.memory = memory;
        this.ticks = 0;
        this.usec = 0;
        this.msec = 0;
    }

    public void tick() {
        ticks++;
        if (ticks % 10 == 0) {
            usec++;
        }
        if (ticks % 10000 == 0) {
            msec++;
            ticks = 0;
        }
        int usec_lsb = (int)(usec & 0xFFFF);
        int usec_msb = (int)(usec >>> 16);
        int msec_lsb = (int)(msec & 0xFFFF);
        int msec_msb = (int)(msec >>> 16);
        memory.setIo(0x7E02, usec_lsb);
        memory.setIo(0x7E03, usec_msb);
        memory.setIo(0x7E04, msec_lsb);
        memory.setIo(0x7E05, msec_msb);
    }
}
