; *****
; * This file contains the code of the Moncky-3 SPI library.
; * The code is based on http://www.rjhcoding.com/avrc-sd-interface-1.php
; * depends on: graphics.asm, text.asm, utilities.asm, spi.asm
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * firmware variables (I/O)
; *****
.def :sd_type 0x7E06 ; LSB = type, MSB = error
.def :sd_result 0x7E07 ; 5 words

; *****
; * constants
; *****
.def :SD_SUCCESS 0
.def :SD_ERROR_CANNOT_GO_TO_IDLE_STATE 1
.def :SD_ERROR_CANNOT_DETECT_TYPE 2
.def :SD_ERROR_TIMEOUT 3
.def :SD_ERROR_CANNOT_GET_OCR 4
.def :SD_ERROR_CANNOT_SET_BLOCK_SIZE 5
.def :SD_ERROR_CANNOT_READ 6

:sd_result_strings
.data :sd_s_success, :sd_s_cannot_idle, :sd_s_cannot_type, :sd_s_timeout, :sd_s_cannot_ocr, :sd_s_cannot_block_size, :sd_s_cannot_read
:sd_s_success
.data "SD card initialised"
:sd_s_cannot_idle
.data "Cannot put SD card into idle state"
:sd_s_cannot_type
.data "Cannot determine SD card type"
:sd_s_timeout
.data "Timout during SD card initialisation"
:sd_s_cannot_ocr
.data "Cannot read OCR from SD card"
:sd_s_cannot_block_size
.data "Cannot set block size to 512 bytes"
:sd_s_cannot_read
.data "Cannot read from SD card"

:sd_type_strings
.data :sd_unknown, :sd_sd1, :sd_sd2, :sd_sdhc
:sd_unknown
.data "unknown"
:sd_sd1
.data "SD1"
:sd_sd2
.data "SD2"
:sd_sdhc
.data "SDHC"

; ***** uint8 sd_disable()
; * disable slave select
; *****
:sd_disable_log
; .data " sd_disable "
:sd_disable
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_disable_log
	; lih r1, ::sd_disable_log
	; li r2, 0
	; call [r0]

	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_disable
	lih r0, ::spi_disable
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	ret
	
; ***** uint8 sd_enable()
; * enable slave select of SD card interface
; *****
:sd_enable_log
; .data " sd_enable "
:sd_enable
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_enable_log
	; lih r1, ::sd_enable_log
	; li r2, 0
	; call [r0]
	
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, 0
	push r0
	li r0, :spi_enable
	lih r0, ::spi_enable
	call [r0]
	addi sp, 1
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	ret

; ***** uint8 sd_powerUpSeq()
; * allow the SD card to power up
; *****
:sd_powerUpSeq_log
; .data " sd_powerUpSeq "
:sd_powerUpSeq
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_powerUpSeq_log
	; lih r1, ::sd_powerUpSeq_log
	; li r2, 0
	; call [r0]

	; sd_disable
	li r0, :sd_disable
	lih r0, ::sd_disable
	call [r0]
	; util_sleepMillis(1)
	li r0, 1
	push r0
	li r0, :util_sleepMillis
	lih r0, ::util_sleepMillis
	call [r0]
	addi sp, 1
	; spi_setSpeed(24) // 250 KHz
	li r0, 24
	push r0
	li r0, :spi_setSpeed
	lih r0, ::spi_setSpeed
	call [r0]
	addi sp, 1
	; send 80 clock cycles
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	ret

; ***** void sd_command(uint8 cmd, uint32 arg, uint8 crc)
; * will send a command to the SD card with the given arguments and CRC
; *****
:sd_command_log
; .data " sd_command "
:sd_command
	push bp
	set bp, sp
	
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_command_log
	; lih r1, ::sd_command_log
	; li r2, 4
	; call [r0]
	
	; spi_transfer(cmd|0x40);
	ldi r0, (bp+6) ; cmd
	ori r0, 0x40
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; SPI_transfer((uint8_t)(arg >> 24));
	ldi r0, (bp+5)
	shri r0, 8
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; SPI_transfer((uint8_t)(arg >> 16));
	ldi r0, (bp+5)
	andi r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; SPI_transfer((uint8_t)(arg >> 8));
	ldi r0, (bp+4)
	shri r0, 8
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; SPI_transfer((uint8_t)(arg));
	ldi r0, (bp+4)
	andi r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; SPI_transfer(crc|0x01);
	ldi r0, (bp+3)
	andi r0, 0xFF ; clip
	ori r0, 1
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	pop bp
	ret

; ***** uint8 sd_readRes1()
; * read 1 byte result from SD card
; *****
:sd_readRes1_log
; .data " sd_readRes1 "
:sd_readRes1
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_readRes1_log
	; lih r1, ::sd_readRes1_log
	; li r2, 0
	; call [r0]
	
	; resgister uint8 i (r1) = 0, res1 (r0);
	li r1, 0
	; while((res1 = SPI_transfer(0xFF)) == 0xFF) {
:sd_readRes1_loop
	push r1
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	pop r1
	li r2, 0xFF
	cmp r0, r2
	li r2, :sd_readRes1_return
	lih r2, ::sd_readRes1_return
	jpnz [r2]
        ; i++;
        inc r1
        ; if (i > 8) break;
        cmpir 8, r1
        jpnc [r2]
        ; }
        li r2, :sd_readRes1_loop
        lih r2, ::sd_readRes1_loop
        jp [r2]
:sd_readRes1_return
	; return res1;
	ret

; ***** uint8 sd_readUntil(uint8 expected, uint16 timeout)
; * reads SPI until it encounters the expected byte
; * if received, the expected byte is returned
; * if a timeout is encountered 0xFF is returned
; *****
:sd_readUntil_log
; .data " sd_readUntil "
:sd_readUntil
	push bp
	set bp, sp
	
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_readUntil_log
	; lih r1, ::sd_readUntil_log
	; li r2, 2
	; call [r0]
	
	; uint16 t1 = util_msec_LSB;
	li r0, :util_msecLSB
	lih r0, ::util_msecLSB
	in r0, (r0)
	push r0 ; keep t1 on stack
	; while((res1 = spi_transfer(0xFF)) != expected) {
:sd_readUntil_while
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	ldi r1, (bp+4) ; expected
	cmp r0, r1
	li r1, :sd_readUntil_end_while
	lih r1, ::sd_readUntil_end_while
	jpz [r1]
	; if ((util_msec_LSB-t1)>timeout) return 0xFF;
	li r1, :util_msecLSB
	lih r1, ::util_msecLSB
	in r1, (r1)
	ldi r2, (sp+1) ; t1
	sub r1, r2
	ldi r2, (bp+3) ; timeout
	cmp r2, r1
	li r1, :sd_readUntil_while
	lih r1, ::sd_readUntil_while
	jpc [r1]
	li r0, 0xFF
:sd_readUntil_end_while
	addi sp, 1 ; remove t1
	pop bp
	ret

; ***** uint8 sd_goIdleState()
; * puts the SD card in SPI mode (idle state)
; *****
:sd_goIdleState_log
; .data " sd_goIdleState "
:sd_goIdleState
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_goIdleState_log
	; lih r1, ::sd_goIdleState_log
	; li r2, 0
	; call [r0]
	
	; sd_enable();
	li r0, :sd_enable
	lih r0, ::sd_enable
	call [r0]
	; sd_command(CMD0, CMD0_ARG, CMD0_CRC);
	li r0, 0
	push r0
	push r0
	push r0
	li r0, 0x94
	push r0
	li r0, :sd_command
	lih r0, ::sd_command
	call [r0]
	addi sp, 4
	; uint8_t res1 = sd_readRes1();
	li r0, :sd_readRes1
	lih r0, ::sd_readRes1
	call [r0]
	push r0 ; save result on stack
	; spi_disable();
	li r0, :sd_disable
	lih r0, ::sd_disable
	call [r0]
	; return res1;
	pop r0 ; get result from stack
	ret

; ***** void sd_readRes3_7(uint8 *res)
; * read result R3 or R7 for commands 8 and 58
; * res is supposed to point in I/O space!
; *****
:sd_readRes3_7_log
; .data " sd_readRes3_7 "
:sd_readRes3_7
	; log
	; push bp
	; set bp, sp
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_readRes3_7_log
	; lih r1, ::sd_readRes3_7_log
	; li r2, 1
	; call [r0]
	; pop bp

	; res[0] = sd_readRes1();
	li r0, :sd_readRes1
	lih r0, ::sd_readRes1
	call [r0]
	ldi r1, (sp+2) ; res
	out r0, (r1)
	; if (res[0] > 1) return;
	cmpir 1, r0
	li r0, :sd_readRes3_7_return
	lih r0, ::sd_readRes3_7_return
	jpnc [r0]
	; res[1] = spi_transfer(0xFF);
	push r1
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	pop r1
	inc r1
	out r0, (r1)
	; res[2] = spi_transfer(0xFF);
	push r1
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	pop r1
	inc r1
	out r0, (r1)
	; res[3] = spi_transfer(0xFF);
	push r1
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	pop r1
	inc r1
	out r0, (r1)
	; res[4] = spi_transfer(0xFF);
	push r1
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	pop r1
	inc r1
	out r0, (r1)
:sd_readRes3_7_return
	ret

; ***** void sd_sendIfCond(uint8 *res)
; * sends CMD8 to detect SD1 or higher
; *****
:sd_sendIfCond_log
; .data " sd_sendIfCond "
:sd_sendIfCond
	push bp
	set bp, sp

	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_sendIfCond_log
	; lih r1, ::sd_sendIfCond_log
	; li r2, 1
	; call [r0]

	; sd_enable();
	li r0, :sd_enable
	lih r0, ::sd_enable
	call [r0]
	; sd_command(8, 0x0000001AA, 0x86);
	li r0, 8
	push r0
	li r0, 0
	push r0
	li r0, 0xAA
	lih r0, 1
	push r0
	li r0, 0x86
	push r0
	li r0, :sd_command
	lih r0, ::sd_command
	call [r0]
	addi sp, 4
	; sd_readRes3_7(res);
	ldi r0, (bp+3) ; res
	push r0
	li r0, :sd_readRes3_7
	lih r0, ::sd_readRes3_7
	call [r0]
	addi sp, 1
	; sd_disable();
	li r0, :sd_disable
	lih r0, ::sd_disable
	call [r0]
	pop bp
	ret

; ***** void sd_readOCR(uint8 *res)
; * sends CMD58 to receive the OCR
; *****
:sd_readOCR_log
; .data " sd_readOCR "
:sd_readOCR
	push bp
	set bp, sp

	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_readOCR_log
	; lih r1, ::sd_readOCR_log
	; li r2, 1
	; call [r0]

	; sd_enable();
	li r0, :sd_enable
	lih r0, ::sd_enable
	call [r0]
	; sd_command(58, 0x00000000, 0x00);
	li r0, 58
	push r0
	li r0, 0
	push r0
	push r0
	push r0
	li r0, :sd_command
	lih r0, ::sd_command
	call [r0]
	addi sp, 4
	; sd_readRes3_7(res);
	ldi r0, (bp+3) ; res
	push r0
	li r0, :sd_readRes3_7
	lih r0, ::sd_readRes3_7
	call [r0]
	addi sp, 1
	; sd_disable();
	li r0, :sd_disable
	lih r0, ::sd_disable
	call [r0]
	pop bp
	ret

; ***** uint8 sd_sendApp()
; * sends CMD55 to initiate an App command
; *****
:sd_sendApp_log
; .data " sd_sendApp "
:sd_sendApp
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_sendApp_log
	; lih r1, ::sd_sendApp_log
	; li r2, 0
	; call [r0]

	; sd_enable();
	li r0, :sd_enable
	lih r0, ::sd_enable
	call [r0]
	; sd_command(55, 0x00000000, 0x00);
	li r0, 55
	push r0
	li r0, 0
	push r0
	push r0
	push r0
	li r0, :sd_command
	lih r0, ::sd_command
	call [r0]
	addi sp, 4
	; register uint8 res1 (r0) = sd_readRes1();
	li r0, :sd_readRes1
	lih r0, ::sd_readRes1
	call [r0]
	; sd_disable();
	push r0
	li r0, :sd_disable
	lih r0, ::sd_disable
	call [r0]
	pop r0
	; return res1
	ret

; ***** uint8 sd_sendOpCond()
; * sends CMD41 to get the operational conditions
; *****
:sd_sendOpCond_log
; .data " sd_sendOpCond "
:sd_sendOpCond
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_sendOpCond_log
	; lih r1, ::sd_sendOpCond_log
	; li r2, 0
	; call [r0]

	; sd_enable();
	li r0, :sd_enable
	lih r0, ::sd_enable
	call [r0]
	; sd_command(41, (sd_type==2?0x40000000:0), 0);
	li r0, 41
	push r0
	li r0, :sd_type
	lih r0, ::sd_type
	in r0, (r0)
	cmpi r0, 2
	sz r0
	li r1, 0
	lih r1, 0x40
	and r0, r1
	push r0
	li r0, 0
	push r0
	push r0
	li r0, :sd_command
	lih r0, ::sd_command
	call [r0]
	addi sp, 4
	; register uint8 res1 (r0) = sd_readRes1();
	li r0, :sd_readRes1
	lih r0, ::sd_readRes1
	call [r0]
	; sd_disable();
	push r0
	li r0, :sd_disable
	lih r0, ::sd_disable
	call [r0]
	pop r0
	; return res1
	ret

; ***** uint8 sd_sendBlockSize()
; * sets the block size to 512 bytes
; *****
:sd_sendBlockSize_log
; .data " sd_sendBlockSize "
:sd_sendBlockSize
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_sendBlockSize_log
	; lih r1, ::sd_sendBlockSize_log
	; li r2, 0
	; call [r0]

	; sd_enable();
	li r0, :sd_enable
	lih r0, ::sd_enable
	call [r0]
	; sd_command(16, 512, 0);
	li r0, 16
	push r0
	li r0, 0
	push r0
	lih r0, 2
	push r0
	li r0, 0
	push r0
	li r0, :sd_command
	lih r0, ::sd_command
	call [r0]
	addi sp, 4
	; register uint8 res1 (r0) = sd_readRes1();
	li r0, :sd_readRes1
	lih r0, ::sd_readRes1
	call [r0]
	; sd_disable();
	push r0
	li r0, :sd_disable
	lih r0, ::sd_disable
	call [r0]
	pop r0
	; return res1
	ret

; ***** void sd_init()
; * initialises the SD card so it is ready to be read
; * this should be called every time a new card is inserted
; *****
:sd_init_log
; .data " sd_init "
:sd_init
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_init_log
	; lih r1, ::sd_init_log
	; li r2, 0
	; call [r0]
	
	; sd_type = 0;
	li r0, 0
	li r1, :sd_type
	lih r1, ::sd_type
	out r0, (r1)
	; uint8 cmdAttempts = 0;
	li r1, 0
	push r1
	; sd_powerUpSeq()
	li r0, :sd_powerUpSeq
	lih r0, ::sd_powerUpSeq
	call [r0]
	; while((sd_result[0] = sd_goIdleState()) != 0x01) {
:sd_init_while
	li r0, :sd_goIdleState
	lih r0, ::sd_goIdleState
	call [r0]
	li r1, :sd_result
	lih r1, ::sd_result
	out r0, (r1)
	cmpi r0, 1
	li r0, :sd_init_end_while
	lih r0, ::sd_init_end_while
	jpz [r0]
	; cmdAttempts++;
	ldi r1, (sp+1)
	inc r1
	sti r1, (sp+1)
	; if (cmdAttempts > 10) {
	cmpir 10, r1
	li r0, :sd_init_else
	lih r0, ::sd_init_else
	jpc [r0]
	; sd_error = sd_result[0];
	li r0, :sd_type
	lih r0, ::sd_type
	in r2, (r0)
	andi r2, 0xFF
	li r3, :sd_result
	lih r3, ::sd_result
	in r4, (r3)
	shli r4, 8
	or r2, r4
	out r2, (r0)
	; return SD_ERROR_CANNOT_GO_TO_IDLE_STATE;
	li r0, :SD_ERROR_CANNOT_GO_TO_IDLE_STATE
	li r1, :sd_init_return
	lih r1, ::sd_init_return
	jp [r1]
:sd_init_else
	li r0, :sd_init_while
	lih r0, ::sd_init_while
	jp [r0]
:sd_init_end_while
	; sd_sendIfCond(sd_result);
	li r0, :sd_result
	lih r0, ::sd_result
	push r0
	li r0, :sd_sendIfCond
	lih r0, ::sd_sendIfCond
	call [r0]
	addi sp, 1
; 	; sd_type = (result[0] & 4 == 0x04)?1:2;
	li r0, :sd_result
	lih r0, ::sd_result
	in r0, (r0)
	andi r0, 4
	cmpi r0, 4
	sz r1
	snz r2
	andi r1, 1
	andi r2, 2
	or r1, r2
	li r0, :sd_type
	lih r0, ::sd_type
	out r1, (r0)
	; if (sd_type == 2 && sd_result[4] != 0xAA) {
	cmpi r1, 2
	snz r2
	li r0, :sd_result
	lih r0, ::sd_result
	addi r0, 4
	in r0, (r0)
	li r1, 0xAA
	cmp r0, r1
	sz r0
	or r0, r2
	li r0, :sd_init_else2
	lih r0, ::sd_init_else2
	jpnz [r0]
	; sd_error = sd_result[0];
	li r0, :sd_result
	lih r0, ::sd_result
	in r0, (r0)
	shli r0, 8
	li r1, :sd_type
	lih r1, ::sd_type
	in r2, (r1)
	or r0, r2
	out r0, (r1)
	; return SD_ERROR_CANNOT_DETECT_TYPE;
	li r0, :SD_ERROR_CANNOT_DETECT_TYPE
	li r1, :sd_init_return
	lih r1, ::sd_init_return
	jp [r1]
:sd_init_else2
	; cmdAttempts = 0;
	li r0, 0
	sti r0, (sp+1)
	; do {
:sd_init_do
	; if (cmdAttempts > 100) return SD_ERROR_TIMEOUT;
	ldi r0, (sp+1); cmdAttempts
	li r1, 100
	cmp r1, r0
	li r0, :sd_init_else3
	lih r0, ::sd_init_else3
	jpc [r0]
	li r0, :SD_ERROR_TIMEOUT
	li r1, :sd_init_return
	lih r1, ::sd_init_return
	jp [r1]
:sd_init_else3
	; sd_result[0] = sd_sendApp();
	li r0, :sd_sendApp
	lih r0, ::sd_sendApp
	call [r0]
	li r1, :sd_result
	lih r1, ::sd_result
	out r0, (r1)
	; if (sd_result[0] < 2) {
	cmpi r0, 2
	li r1, :sd_init_else4
	lih r1, ::sd_init_else4
	jpc [r1]
	; sd_result[0] = sd_sendOpCond();
	li r0, :sd_sendOpCond
	lih r0, ::sd_sendOpCond
	call [r0]
	li r1, :sd_result
	lih r1, ::sd_result
	out r0, (r1)
:sd_init_else4
	; util_sleepMillis(10);
	li r0, 10
	push r0
	li r0, :util_sleepMillis
	lih r0, ::util_sleepMillis
	call [r0]
	addi sp, 1
	; cmdAttempts++;
	ldi r1, (sp+1)
	inc r1
	sti r1, (sp+1)
	; } while(sd_result[0] != 0);
	li r0, :sd_result
	lih r0, ::sd_result
	in r0, (r0)
	or r0, r0
	li r0, :sd_init_do
	lih r0, ::sd_init_do
	jpnz [r0]
	; sd_readOCR(sd_result);
	li r0, :sd_result
	lih r0, ::sd_result
	push r0
	li r0, :sd_readOCR
	lih r0, ::sd_readOCR
	call [r0]
	addi sp, 1
	; if (!(sd_result[1] & 0x80)) return SD_ERROR_CANNOT_GET_OCR;
	li r0, :sd_result
	lih r0, ::sd_result
	addi r0, 1
	in r0, (r0)
	andi r0, 0x80
	li r0, :sd_init_else5
	lih r0, ::sd_init_else5
	jpnz [r0]
	li r0, :SD_ERROR_CANNOT_GET_OCR
	li r1, :sd_init_return
	lih r1, ::sd_init_return
	jp [r1]
:sd_init_else5
	; sd_type = (sd_result[1] & 0xC0 == 0xC0)?3:sd_type;
	li r0, :sd_result
	lih r0, ::sd_result
	addi r0, 1
	in r0, (r0)
	li r1, 0xC0
	and r0, r1
	cmp r0, r1
	sz r2
	snz r3
	andi r2, 3
	li r4, :sd_type
	lih r4, ::sd_type
	in r5, (r4)
	and r3, r5
	or r2, r3
	out r2, (r4)
	; uint8 resp = sd_sendBlockSize();
	li r0, :sd_sendBlockSize
	lih r0, ::sd_sendBlockSize
	call [r0]
	; if (resp != 0) return SD_ERROR_CANNOT_SET_BLOCK_SIZE; else return SD_SUCCESS;
	nop r0, r0
	li r0, :SD_SUCCESS
	li r1, :sd_init_return
	lih r1, ::sd_init_return
	jpz [r1]
	li r0, :SD_ERROR_CANNOT_SET_BLOCK_SIZE
:sd_init_return
	addi sp, 1 ; remove cmdAttempts
	; spi_setSpeed(0) // 6.25 MHz
	push r0 ; save return value
	li r0, 0
	push r0
	li r0, :spi_setSpeed
	lih r0, ::spi_setSpeed
	call [r0]
	addi sp, 1
	pop r0 ; restore return value
	ret

; ***** uint8 sd_read(uint32 blockNumber, uint16 *destination, bool io)
; * reads a block (512 bytes) of data from disk and stores the data into the destination (LSB first)
; * the boolean io indicates if the destination is in io memory space
; * the destination must have space for 256 words
; *****
:sd_read_log
; .data " sd_read "
:sd_read
	push bp
	set bp, sp

	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :sd_read_log
	; lih r1, ::sd_read_log
	; li r2, 4
	; call [r0]

	; sd_enable();
	li r0, :sd_enable
	lih r0, ::sd_enable
	call [r0]
	; if (sd_type != 3) blockNumber <<= 9;
	li r0, :sd_type
	lih r0, ::sd_type
	in r0, (r0)
	cmpi r0, 3
	ldi r0, (bp+5) ; blockNumber_LSB
	ldi r1, (bp+6) ; blockNumber_MSB
	li r2, :sd_read_else
	lih r2, ::sd_read_else
	jpz [r2]
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
:sd_read_else
	; sd_command(17, blockNumber, 0);
	li r2, 17
	push r2
	push r1
	push r0
	li r0, 0
	push r0
	li r0, :sd_command
	lih r0, ::sd_command
	call [r0]
	addi sp, 4
	; uint8 res1 = sd_readUntil(0xFE, 100);
	li r0, 0xFE
	push r0
	li r0, 100
	push r0
	li r0, :sd_readUntil
	lih r0, ::sd_readUntil
	call [r0]
	addi sp, 2
	; if (res1 != 0xFE) return SD_ERROR_CANNOT_READ;
	li r1, 0xFE
	cmp r0, r1
	li r0, :sd_read_else2
	lih r0, ::sd_read_else2
	jpz [r0]
	li r0, :SD_ERROR_CANNOT_READ
	li r1, :sd_read_return
	lih r1, ::sd_read_return
	jp [r1]
:sd_read_else2
	; for(uint16 i = 0; i < 256; i++) {
	li r0, 0
	push r0 ; save i to stack
:sd_read_for
	ldi r0, (sp+1) ; i
	li r1, 0
	lih r1, 1
	cmp r0, r1
	li r0, :sd_read_end_for
	lih r0, ::sd_read_end_for
	jpc [r0]
	;   *destination = spi_transfer(0xFF) | (spi_transfer(0xFF) << 8);
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	push r0
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	pop r1
	shli r0, 8
	or r0, r1
	ldi r1, (bp+4) ; destination
	ldi r2, (bp+3) ; io
	nop r2, r2
	li r2, :sd_read_mem
	lih r2, ::sd_read_mem
	jpz [r2]
	out r0, (r1)
	li r2, :sd_read_io
	lih r2, ::sd_read_io
	jp [r2]
:sd_read_mem
	st r0, (r1)
:sd_read_io 
	;   destination++;
	ldi r0, (bp+4) ; destination
	inc r0
	sti r0, (bp+4)
	; }
	ldi r0, (sp+1) ; i
	inc r0
	sti r0, (sp+1)
	li r0, :sd_read_for
	lih r0, ::sd_read_for
	jp [r0]
:sd_read_end_for
	addi sp, 1 ; clean up "i"
	; spi_transfer(0xFF);
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	; spi_transfer(0xFF);
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; sd_disable();
	li r0, :sd_disable
	lih r0, ::sd_disable
	call [r0]
	; return SD_SUCCESS;
	li r0, :SD_SUCCESS
:sd_read_return
	pop bp
	ret

