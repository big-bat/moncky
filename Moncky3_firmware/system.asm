; *****
; * This file contains the code of the Moncky-3 system library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * constants
; *****
.def :startOfStack 0xFFFE

; *****
; * hardware variables (I/O)
; *****
.def :system_screenConfig 0x7D00

; *****
; * start of code
; *****
.org 0x0000
	di
	li sp, :startOfStack
	lih sp, ::startOfStack
	li r0, :start_system
	lih r0, ::start_system
	jp [r0]

; *****
; * reset after firmware is loaded
; *****
.org 0x0008
	reset
	reset
	reset
	reset
	reset
	reset
	reset
	reset

; *****
; * interrupt handler
; * The second bit of screenConfig is toggled.  This can be used to detect the vertical retrace.
; * IMPORTANT: the interrupt handler must always save all registers used and all flags
; *****
.org 0x0010
	push r0 ; save all registers and flags
	push r1
	push r2
	sflags r2
	li r0, :system_screenConfig
	lih r0, ::system_screenConfig
	in r1, (r0)
	xori r1, 2 ; toggle bit
	out r1, (r0)
	rflags r2 ; restore flags and registers
	pop r2
	pop r1
	pop r0
	reti

