; *****
; * This file contains the final code for the ROM of the Moncky-3 computer.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * constants
; *****
.def	:startOfStack 0xFFFE

; *****
; * variables (I/O)
; *****
.def	:screenConfig	0x7D00
.def	:keyboardLastKey 0x7D01
.def	:cursorPosition 0x7D02 ; LSB = X, MSB = Y
.def	:shiftPressed	0x7D03
.def	:bgCol 	0x7D04
.def	:savedCursor	0x7D05 ; until 0x7D25
.def	:spiData 	0x7E00
.def	:spiControl	0x7E01
.def	:usecLSB 0x7E02
.def	:usecMSB 0x7E03
.def	:msecLSB 0x7E04
.def	:msecMSB 0x7E05
.def	:SD_cardType 			0x7E06
.def	:disk_sector 			0xFD00
.def	:disk_startPartition_LSB	0xFE00
.def	:disk_startPartition_MSB	0xFE01
.def	:disk_lengthPartition_LSB	0xFE02
.def	:disk_lengthPartition_MSB	0xFE03
.def	:disk_sectorsPerFat		0xFE04
.def	:disk_fatPosition		0xFE05
.def	:disk_rootFolderPosition_LSB 	0xFE06
.def	:disk_rootFolderPosition_MSB	0xFE07
.def	:disk_numberOfSectorsForRootFolder 0xFE08
.def	:disk_fileStart		0xFE09
.def	:disk_fileLength_LSB		0xFE0A
.def	:disk_fileLength_MSB		0xFE0B
.def	:disk_currentClusterNumber	0xFE0C
.def	:disk_sectorsPerCluster	0xFE0D
.def	:disk_currentSectorNumber	0xFE0E

; *****
; * start of code
; *****
.org 0x0000
	di
	; jump to start
	li r0, :start_system
	lih r0, ::start_system
	jp [r0]

; *****
; * turn rom shadow off and reboot
; *****
.org 0x0008
:bootMonckyOS
	li r0, 0xFF
	lih r0, 0xFF
	li r1, 0xAA
	lih r1, 0xAA
	st r1, (r0)
	reset

; *****
; * interrupt handler
; *****
.org 0x0010
	reti


; *****
; * 8x8 font for all ASCII characters
; * based on Amstrad CPC 464 font
; *****
:font
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0000 (null)
        .data 0x6CFE, 0xFEFE, 0x7C38, 0x1000 ; U+0001 (heart)
        .data 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF ; U+0002 (full, cursor)
        .data 0x0000, 0x0001, 0x071E, 0x3E7E ; U+0003 (bigbat00)
        .data 0x0000, 0x0000, 0x0024, 0x3C24 ; U+0004 (bigbat01)
        .data 0x0000, 0x0080, 0xE078, 0x7C7E ; U+0005 (bigbat02)
        .data 0x7FFF, 0xFF1F, 0x0F07, 0x070F ; U+0006 (bigbat10)
        .data 0x3CFF, 0xFFFF, 0xFFFF, 0xFFFF ; U+0007 (bigbat11)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0008 (backspace)        
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0009 (tab)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000A (newline)
        .data 0xFEFF, 0xFFF8, 0xF0E0, 0xE0F0 ; U+000B (bigbat12)
        .data 0x1810, 0x0000, 0x0000, 0x0000 ; U+000C (bigbat20)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000D (carriage return)
        .data 0x3C18, 0x1818, 0x0000, 0x0000 ; U+000E (bigbat21)
        .data 0x1808, 0x0000, 0x0000, 0x0000 ; U+000F (bigbat22)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0010
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0011
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0012
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0013
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0014
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0015
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0016
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0017
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0018
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0019
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001A
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001B
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001C
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001D
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001E
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001F
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0020 (space)
        .data 0x1818, 0x1818, 0x1800, 0x1800 ; U+0021 (!)
        .data 0x6C6C, 0x6C00, 0x0000, 0x0000 ; U+0022 (")
        .data 0x6C6C, 0xFE6C, 0xFE6C, 0x6C00 ; U+0023 (#)
        .data 0x183E, 0x583C, 0x1A7C, 0x1800 ; U+0024 ($)
        .data 0x00C6, 0xCC18, 0x3066, 0xC600 ; U+0025 (%)
        .data 0x386C, 0x3876, 0xDCCC, 0x7600 ; U+0026 (&)
        .data 0x1818, 0x3000, 0x0000, 0x0000 ; U+0027 (')
        .data 0x0C18, 0x3030, 0x3018, 0x0C00 ; U+0028 (()
        .data 0x3018, 0x0C0C, 0x0C18, 0x3000 ; U+0029 ())
        .data 0x0066, 0x3CFF, 0x3C66, 0x0000 ; U+002A (*)
        .data 0x0018, 0x187E, 0x1818, 0x0000 ; U+002B (+)
        .data 0x0000, 0x0000, 0x0018, 0x1830 ; U+002C (,)
        .data 0x0000, 0x007E, 0x0000, 0x0000 ; U+002D (-)
        .data 0x0000, 0x0000, 0x0018, 0x1800 ; U+002E (.)
        .data 0x060C, 0x1830, 0x60C0, 0x8000 ; U+002F (/)
        .data 0x7CC6, 0xCED6, 0xE6C6, 0x7C00 ; U+0030 (0)
        .data 0x1838, 0x1818, 0x1818, 0x7E00 ; U+0031 (1)
        .data 0x3C66, 0x063C, 0x6066, 0x7E00 ; U+0032 (2)
        .data 0x3C66, 0x061C, 0x0666, 0x3C00 ; U+0033 (3)
        .data 0x1C3C, 0x6CCC, 0xFE0C, 0x1E00 ; U+0034 (4)
        .data 0x7E62, 0x607C, 0x0666, 0x3C00 ; U+0035 (5)
        .data 0x3C66, 0x607C, 0x6666, 0x3C00 ; U+0036 (6)
        .data 0x7E66, 0x060C, 0x1818, 0x1800 ; U+0037 (7)
        .data 0x3C66, 0x663C, 0x6666, 0x3C00 ; U+0038 (8)
        .data 0x3C66, 0x663E, 0x0666, 0x3C00 ; U+0039 (9)
        .data 0x0000, 0x1818, 0x0018, 0x1800 ; U+003A (:)
        .data 0x0000, 0x1818, 0x0018, 0x1830 ; U+003B (;)
        .data 0x0C18, 0x3060, 0x3018, 0x0C00 ; U+003C (<)
        .data 0x0000, 0x7E00, 0x007E, 0x0000 ; U+003D (=)
        .data 0x6030, 0x180C, 0x1830, 0x6000 ; U+003E (>)
        .data 0x3C66, 0x660C, 0x1800, 0x1800 ; U+003F (?)
        .data 0x7CC6, 0xDEDE, 0xDEC0, 0x7C00 ; U+0040 (@)
        .data 0x183C, 0x6666, 0x7E66, 0x6600 ; U+0041 (A)
        .data 0xFC66, 0x667C, 0x6666, 0xFC00 ; U+0042 (B)
        .data 0x3C66, 0xC0C0, 0xC066, 0x3C00 ; U+0043 (C)
        .data 0xF86C, 0x6666, 0x666C, 0xF800 ; U+0044 (D)
        .data 0xFE62, 0x6878, 0x6862, 0xFE00 ; U+0045 (E)
        .data 0xFE62, 0x6878, 0x6860, 0xF000 ; U+0046 (F)
        .data 0x3C66, 0xC0C0, 0xCE66, 0x3E00 ; U+0047 (G)
        .data 0x6666, 0x667E, 0x6666, 0x6600 ; U+0048 (H)
        .data 0x7E18, 0x1818, 0x1818, 0x7E00 ; U+0049 (I)
        .data 0x1E0C, 0x0C0C, 0xCCCC, 0x7800 ; U+004A (J)
        .data 0xE666, 0x6C78, 0x6C66, 0xE600 ; U+004B (K)
        .data 0xF060, 0x6060, 0x6266, 0xFE00 ; U+004C (L)
        .data 0xC6EE, 0xFEFE, 0xD6C6, 0xC600 ; U+004D (M)
        .data 0xC6E6, 0xF6DE, 0xCEC6, 0xC600 ; U+004E (N)
        .data 0x386C, 0xC6C6, 0xC66C, 0x3800 ; U+004F (O)
        .data 0xFC66, 0x667C, 0x6060, 0xF000 ; U+0050 (P)
        .data 0x386C, 0xC6C6, 0xDACC, 0x7600 ; U+0051 (Q)
        .data 0xFC66, 0x667C, 0x6C66, 0xE600 ; U+0052 (R)
        .data 0x3C66, 0x603C, 0x0666, 0x3C00 ; U+0053 (S)
        .data 0x7E5A, 0x1818, 0x1818, 0x3C00 ; U+0054 (T)
        .data 0x6666, 0x6666, 0x6666, 0x3C00 ; U+0055 (U)
        .data 0x6666, 0x6666, 0x663C, 0x1800 ; U+0056 (V)
        .data 0xC6C6, 0xC6D6, 0xFEEE, 0xC600 ; U+0057 (W)
        .data 0xC66C, 0x3838, 0x6CC6, 0xC600 ; U+0058 (X)
        .data 0x6666, 0x663C, 0x1818, 0x3C00 ; U+0059 (Y)
        .data 0xFEC6, 0x8C18, 0x3266, 0xFE00 ; U+005A (Z)
        .data 0x3C30, 0x3030, 0x3030, 0x3C00 ; U+005B ([)
        .data 0xC060, 0x3018, 0x0C06, 0x0200 ; U+005C (\)
        .data 0x3C0C, 0x0C0C, 0x0C0C, 0x3C00 ; U+005D (])
        .data 0x183C, 0x7E18, 0x1818, 0x1800 ; U+005E (^)
        .data 0x0000, 0x0000, 0x0000, 0x00FF ; U+005F (_)
        .data 0x3018, 0x0C00, 0x0000, 0x0000 ; U+0060 (`)
        .data 0x0000, 0x780C, 0x7CCC, 0x7600 ; U+0061 (a)
        .data 0xE060, 0x7C66, 0x6666, 0xDC00 ; U+0062 (b)
        .data 0x0000, 0x3C66, 0x6066, 0x3C00 ; U+0063 (c)
        .data 0x1C0C, 0x7CCC, 0xCCCC, 0x7600 ; U+0064 (d)
        .data 0x0000, 0x3C66, 0x7E60, 0x3C00 ; U+0065 (e)
        .data 0x1C36, 0x3078, 0x3030, 0x7800 ; U+0066 (f)
        .data 0x0000, 0x3E66, 0x663E, 0x067C ; U+0067 (g)
        .data 0xE060, 0x6C76, 0x6666, 0xE600 ; U+0068 (h)
        .data 0x1800, 0x3818, 0x1818, 0x3C00 ; U+0069 (i)
        .data 0x0600, 0x0E06, 0x0666, 0x663C ; U+006A (j)
        .data 0xE060, 0x666C, 0x786C, 0xE600 ; U+006B (k)
        .data 0x3818, 0x1818, 0x1818, 0x3C00 ; U+006C (l)
        .data 0x0000, 0x6CFE, 0xD6D6, 0xC600 ; U+006D (m)
        .data 0x0000, 0xDC66, 0x6666, 0x6600 ; U+006E (n)
        .data 0x0000, 0x3C66, 0x6666, 0x3C00 ; U+006F (o)
        .data 0x0000, 0xDC66, 0x667C, 0x60F0 ; U+0070 (p)
        .data 0x0000, 0x76CC, 0xCC7C, 0x0C1E ; U+0071 (q)
        .data 0x0000, 0xDC60, 0x6060, 0xF000 ; U+0072 (r)
        .data 0x0000, 0x3C60, 0x3C06, 0x7C00 ; U+0073 (s)
        .data 0x3030, 0x7C30, 0x3036, 0x1C00 ; U+0074 (t)
        .data 0x0000, 0x6666, 0x6666, 0x3E00 ; U+0075 (u)
        .data 0x0000, 0x6666, 0x663C, 0x1800 ; U+0076 (v)
        .data 0x0000, 0xC6D6, 0xD6FE, 0x6C00 ; U+0077 (w)
        .data 0x0000, 0xC66C, 0x386C, 0xC600 ; U+0078 (x)
        .data 0x0000, 0x6666, 0x663E, 0x067C ; U+0079 (y)
        .data 0x0000, 0x7E4C, 0x1832, 0x7E00 ; U+007A (z)
        .data 0x0E18, 0x1870, 0x1818, 0x0E00 ; U+007B ({)
        .data 0x1818, 0x1818, 0x1818, 0x1800 ; U+007C (|)
        .data 0x7018, 0x180E, 0x1818, 0x7000 ; U+007D (})
        .data 0x76DC, 0x0000, 0x0000, 0x0000 ; U+007E (~)
        .data 0xCC33, 0xCC33, 0xCC33, 0xCC33 ; U+007F
        
; ***** void clrscr(uint8 buffer)
; * clears the specified buffer with the background colour (:bgCol)
; * uses r0, r1, r2, r3, registers are saved
; * 27+4*32000 = 128027 clock cycles
; *****
:clrscr
	push bp
	set bp, sp
	push r0 ; save registers
	push r1
	push r2
	push r3
	; determine start of video (r0) and length (r1) to be cleared
	ldi r0, (bp+3) ; buffer
	andi r0, 1
	shli r0, 15
	li r1, 0xFF
	lih r1, 0x7C
	; prepare for loop
	li r3, :bgCol
	lih r3, ::bgCol
	in r2, (r3)
	andi r2, 0xFF
	set r3, r2
	shli r3, 8
	or r2, r3
	li r3, :clrscr_loop
	lih r3, ::clrscr_loop
	add r0, r1	; r0 now points to the end of the ram that needs to be cleared
:clrscr_loop
	out r2, (r0)
	dec r0
	dec r1
	jpc [r3]
	pop r3 ; restore registers
	pop r2
	pop r1
	pop r0
	pop bp
	ret

; ***** void plot(uint16 x, uint16 y, uint16 col, uint8 buffer)
; * plots a pixel at location (x,y) with the given colour in the given screen buffer
; * uses r0, r1, r2, r3, registers are saved
; * 48 clock cycles
; *****
:plot
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3

	ldi r0, (bp+6) ; x
	ldi r1, (bp+5) ; y
	
	; clipping
	li r2, :plot_return
	lih r2, ::plot_return
	li r3, 0x40 ; li r3, 320
	lih r3, 0x01
	cmp r0, r3
	jpc [r2]
	li r3, 200
	cmp r1, r3
	jpc [r2]
	
	; find memory location (r1 = (320*y+x)/2+buffer<<15)
	set r2, r1
	shli r1, 6
	shli r2, 8
	add r1, r2
	add r1, r0
	shri r1, 1
	ldi r2, (bp+3) ; screen buffer
	shli r2, 15	; r2 points to start of screen buffer
	add r1, r2

	; set right byte to colour
	andi r0, 1
	shli r0, 3
	li r3, 0xFF ; mask: 8 bits per pixel
	ldi r2, (bp+4) ; col
	and r2, r3
	shl r2, r0
	shl r3, r0
	not r3, r3
	in r0, (r1)
	and r0, r3
	or r0, r2
	out r0, (r1)

:plot_return
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

; ***** void scroll_up(uint8 amount, uint8 buffer)
; * Scrolls up the screen buffer with the specified number of lines.
; * The new lines on the bottom are filled with the background colour.
; * all registers are saved
; *****
:scroll_up
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	; r1 = source, r2 = dest, r3 = count
	ldi r0, (bp+4) ; amount
	set r1, r0
	shli r0, 7
	shli r1, 5
	add r0, r1
	ldi r1, (bp+3) ; buffer
	andi r1, 1
	shli r1, 15
	set r2, r1
	add r1, r0
	li r3, 0x00
	lih r3, 0x7D
	sub r3, r0
	li r0, :scroll_up_loop
	lih r0, ::scroll_up_loop	
:scroll_up_loop
	in r4, (r1)
	out r4, (r2)
	inc r1
	inc r2
	dec r3
	jpnz [r0]
	li r3, :bgCol
	lih r3, ::bgCol
	in r3, (r3)
	set r4, r3
	shli r4, 8
	or r3, r4
	li r4, 0x00
	lih r4, 0x7D
	li r0, :scroll_up_loop2
	lih r0, ::scroll_up_loop2
:scroll_up_loop2
	out r3, (r2)
	inc r2
	cmp r2, r4
	jpnz [r0]
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

; ***** void display_char(uint16 x, uint16 y, uint16 char, uint16 col, uint8 buffer)
; * draws a character (0-127) at the given (x,y) coordinates with the given colour on the given screen buffer
; *****
; * all registers are saved
; * r0 points to bit pattern
; * r1 counter (4 words)
; * r2 bit pattern
; * r3 bitMask
; * r4 temp
; * r5 points to plot function
:display_char
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	push r5
	; prepare stack for plot(x,y,col,buffer)
	ldi r0, (bp+7) ; x
	push r0
	ldi r0, (bp+6) ; y
	push r0
	ldi r0, (bp+4) ; col
	push r0
	ldi r0, (bp+3) ; buffer
	push r0
	li r5, :plot
	lih r5, ::plot
	
	ldi r0, (bp+5)	; char
	andi r0, 0x7F	; clip
	shli r0, 2	; times 4
	li r1, :font
	lih r1, ::font
	add r0, r1	; r0 points now to the start of the bit patterns
	li r1, 4	; r1 = counter: 4 words
:display_char_nextPattern
	ld r2, (r0)	; r2 contains the bit pattern
	li r3, 0	; r3 = bitMask
	lih r3, 0x80
:display_char_nextPixel
	li r4, :display_char_noPixel
	lih r4, ::display_char_noPixel
	andf r2, r3
	jpz r4
	call [r5]
:display_char_noPixel
	ldi r4, (sp+4) ; inc(x)
	inc r4
	sti r4, (sp+4)
	shri r3, 1
	li r4, 128
	cmp r3, r4
	li r4, :display_char_sameRow
	lih r4, ::display_char_sameRow
	jpnz [r4]
	ldi r4, (sp+4) ; x = x - 8
	subi r4, 8
	sti r4, (sp+4)
	ldi r4, (sp+3) ; y++
	inc r4
	sti r4, (sp+3)
:display_char_sameRow
	cmpi r3, 0
	li r4, :display_char_nextPixel
	lih r4, ::display_char_nextPixel
	jpnz [r4]
	ldi r4, (sp+4) ; x = x - 8
	subi r4, 8
	sti r4, (sp+4)
	ldi r4, (sp+3) ; y++
	inc r4
	sti r4, (sp+3)
	inc r0
	li r4, :display_char_nextPattern
	lih r4, ::display_char_nextPattern
	dec r1
	jpnz [r4]
	addi sp, 4
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

	
; ***** void print_char(uint16 char, uint16 col, uint8 buffer)
; * Prints a character (0-127) at the current cursor position with the given colour on the given screen buffer.
; * The cursor is advanced one position.  If the cursor goes off the screen, the screen is scrolled up one line.
; * This routine also 'prints' cursor movements: arrows and del
; * all registers are saved
; *****
:print_char
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	in r0, (r0)
	set r1, r0
	andi r0, 0xFF ; x
	shri r1, 8 ; y	
	ldi r2, (bp+5) ; char
	cmpi r2, 10
	li r3, :print_char_newline
	lih r3, ::print_char_newline
	jpz [r3]
:print_char_normal
	shli r0, 3
	shli r1, 3
	; display_char(x,y,char,col)
	push r0
	push r1
	ldi r2, (bp+5) ; char
	push r2
	ldi r2, (bp+4) ; col
	push r2
	ldi r2, (bp+3) ; buffer
	push r2
	li r2, :display_char
	lih r2, ::display_char
	call [r2]
	addi sp, 3
	pop r1 ; restore x and y and increase position or move cursor
	pop r0
	shri r0, 3
	shri r1, 3
	li r2, :print_char_no_newline
	lih r2, ::print_char_no_newline
	jp [r2]
:print_char_newline
	li r0, 39
:print_char_no_newline
	inc r0
	cmpi r0, 40
	li r2, :print_char_store_cursor
	lih r2, ::print_char_store_cursor
	jpnz [r2]
	li r0, 0
	inc r1
	cmpi r1, 25
	jpnz [r2]
	li r1, 24
	; scroll_up(8, buffer)
	li r2, 8
	push r2
	ldi r2, (bp+3) ; buffer
	push r2
	li r2, :scroll_up
	lih r2, ::scroll_up
	call [r2]
	addi sp, 2
:print_char_store_cursor
	shli r1, 8
	or r1, r0
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	out r1, (r0)
:print_char_end
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret


; ***** void print_string(uint16* string, uint16 col, uint8 buffer)
; * prints a null-terminated string at the current cursor position with the given colour on the given screen buffer
; * all registers are saved
; *****
:print_string
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	; prepare stack for print_char
	push r0 ; dummy for char
	ldi r0, (bp+4) ; col
	push r0
	ldi r0, (bp+3) ; buffer
	push r0	
	ldi r0, (bp+5) ; string
:print_string_loop
	ld r1, (r0)
	nop r1, r1
	li r2, :print_string_end
	lih r2, ::print_string_end
	jpz [r2]
	sti r1, (sp+3)
	li r2, :print_char
	lih r2, ::print_char
	call [r2]
	inc r0
	li r2, :print_string_loop
	lih r2, ::print_string_loop
	jp [r2]
:print_string_end
	addi sp, 3
	pop r2
	pop r1
	pop r0
	pop bp
	ret
	
; ***** void print_hex(uint16 i, uint16 col, uint8 buffer)
; * prints an int (i) at the current cursor position with the given colour on the given screen buffer
; * all registers are saved
; *****
:print_hex
	push bp
	set bp, sp
	push r0
	push r1
	; prepare stack for print_char(char,col,buffer)
	push r0	; char
	ldi r0, (bp+4) ; col
	push r0
	ldi r0, (bp+3) ; buffer
	push r0
	
	ldi r0, (bp+5) ; i
	shri r0, 12
	cmpi r0, 10
	li r1, :display_hex_nodigit1
	lih r1, ::display_hex_nodigit1
	jpnc [r1]
	addi r0, 7
:display_hex_nodigit1
	addi r0, 48
	sti r0, (sp+3) ; set char in stack
	li r1, :print_char
	lih r1, ::print_char
	call [r1]
	ldi r0, (bp+5) ; i
	shri r0, 8
	andi r0, 15
	cmpi r0, 10
	li r1, :display_hex_nodigit2
	lih r1, ::display_hex_nodigit2
	jpnc [r1]
	addi r0, 7
:display_hex_nodigit2
	addi r0, 48
	sti r0, (sp+3) ; set char in stack
	li r1, :print_char
	lih r1, ::print_char
	call [r1]
	ldi r0, (bp+5) ; i
	shri r0, 4
	andi r0, 15
	cmpi r0, 10
	li r1, :display_hex_nodigit3
	lih r1, ::display_hex_nodigit3
	jpnc [r1]
	addi r0, 7
:display_hex_nodigit3
	addi r0, 48
	sti r0, (sp+3) ; set char in stack
	li r1, :print_char
	lih r1, ::print_char
	call [r1]
	ldi r0, (bp+5) ; i
	andi r0, 15
	cmpi r0, 10
	li r1, :display_hex_nodigit4
	lih r1, ::display_hex_nodigit4
	jpnc [r1]
	addi r0, 7
:display_hex_nodigit4
	addi r0, 48
	sti r0, (sp+3) ; set char in stack
	li r1, :print_char
	lih r1, ::print_char
	call [r1]
	addi sp, 3
	pop r1
	pop r0
	pop bp
	ret

; ***** void log()
; * logs a message (in r1) and parameters on stack (# in r2).
; * this subroutine must be called right after "push bp" and "set bp, sp"
; * all register values are preserved
; *****
:log
	push r2
	push r1
	li r1, :msecLSB
	lih r1, ::msecLSB
	in r1, (r1)
	push r1
	li r1, 0xFC
	push r1
	li r1, 0
	push r1
	li r1, :print_hex
	lih r1, ::print_hex
	call [r1]
	addi sp, 3
	pop r1
	push r1
	li r0, 0xFC
	push r0
	li r0, 0
	push r0
	li r0, :print_string
	lih r0, ::print_string
	call [r0]
	addi sp, 3
	pop r2
:log_loop
	cmpi r2, 0
	li r0, :log_end
	lih r0, ::log_end
	jpz [r0]
	push r2
	addi r2, 2
	lda r1, (bp+r2)
	push r1
	li r0, 0xFC
	push r0
	li r0, 0
	push r0
	li r0, :print_hex
	lih r0, ::print_hex
	call [r0]
	li r0, 0x20
	sti r0, (sp+3)
	li r0, :print_char
	lih r0, ::print_char
	call [r0]
	addi sp, 3
	pop r2
	dec r2
	li r0, :log_loop
	lih r0, ::log_loop
	jp [r0]
:log_end
	li r0, 10
	push r0
	li r0, 0xFC
	push r0
	li r0, 0
	push r0
	li r0, :print_char
	lih r0, ::print_char
	call [r0]
	addi sp, 3
	ret

; ***** uint16 wait_key(uint16 timeout)
; * waits until a key is pressed or timeout (in milliseconds) occurs and returns the key code
; * if timeout occurs, 0 is returned
; * a timeout value of 0 means: wait indefinitelly
; * release of keys is skipped
; * return value in r0
; *****
:wait_key
	push bp
	set bp, sp
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r1, (r0)
	li r2, :wait_key_return
	lih r2, ::wait_key_return
	li r3, :wait_key_loop
	lih r3, ::wait_key_loop
	li r6, :keyboardLastKey
	lih r6, ::keyboardLastKey
:wait_key_loop
	in r0, (r6)
	shri r0, 8
	andi r0, 2
	jpnz [r3] ; skip release codes
	in r0, (r6)
	or r0, r0
	jpnz [r2] ; valid key was detected
	ldi r5, (bp+3) ; timeout
	nop r5, r5
	jpz [r3]
	li r4, :msecLSB
	lih r4, ::msecLSB
	in r4, (r4)
	sub r4, r1
	subf r4, r5
	jpnc [r3]
:wait_key_return
	pop bp
	ret

; ***** sleepMillis(uint16 millis)
; * waits for the given amount of milliseconds
; * all register values are saved
; *****
:sleepMillis
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r1, (r0)
	ldi r3, (bp+3)
:sleepMillis_loop
	in r2, (r0)
	sub r2, r1
	cmp r2, r3
	li r2, :sleepMillis_loop
	lih r2, ::sleepMillis_loop
	jpnc [r2]
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

; *****
; * variables (data memory)
; *****

; *****
; * constants
; *****
.def :SD_CARD_ERROR_CMD0	1
.def :SD_CARD_ERROR_CMD8	2
.def :SD_CARD_ERROR_CMD17	3
.def :SD_CARD_ERROR_CMD58	4
.def :SD_CARD_ERROR_ACMD41	5
.def :SD_CARD_ERROR_READ_TIMEOUT 6
.def :SD_CARD_ERROR_READ	7
.def :FAT_ERROR_NOT_FORMATTED	8
.def :FAT_ERROR_PARTITION_TOO_BIG	9
.def :FAT_ERROR_INVALID_SECTOR_SIZE 10
.def :FAT_ERROR_INVALID_NUNMBER_OF_FATS 11
.def :FAT_ERROR_OS_NOT_FOUND	12

:s_error
.data	'E', 'r', 'r', 'o', 'r', 32, 'e', 'n', 'c', 'o', 'u', 'n', 't', 'e', 'r', 'e', 'd', ':', 32, 0
:s_press_key
.data	10, 'P', 'r', 'e', 's', 's', 32, 'a', 'n', 'y', 32, 'k', 'e', 'y', 32, 't', 'o', 32, 'r', 'e', 's', 'e', 't', 0


; ***** void error(uint8 code)
; * displays an errorcode and stops
; *****
:sd_error
	push bp
	set bp, sp
	; set all slave selects to 1
	li r0, :spiControl
	lih r0, ::spiControl
	in r1, (r0)
	ori r1, 0xF0
	out r1, (r0)
	; print error
	li r0, :s_error
	lih r0, ::s_error
	push r0
	li r0, 0xE0
	push r0
	li r0, 0
	push r0
	li r0, :print_string
	lih r0, ::print_string
	call [r0]
	ldi r0, (bp+3) ; code
	sti r0, (sp+3)
	li r0, :print_hex
	lih r0, ::print_hex
	call [r0]
	li r0, :s_press_key
	lih r0, ::s_press_key
	sti r0, (sp+3)
	li r0, :print_string
	lih r0, ::print_string
	call [r0]
	addi sp, 3
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :wait_key
	lih r0, ::wait_key
	call [r0]
	addi sp, 1
	reset
	ret

; ------------ SD control
	
; ***** uint8 spi_transfer(uint8 d)
; * send and receive 8 bits of data
; * return value in r0
; *****
:spi_transfer_log
; .data	32, 't', 'r', 'a', 'n', 's' ,'f', 'e', 'r', 32, 0
:spi_transfer
	push bp
	set bp, sp
	
	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :spi_transfer_log
	; lih r1, ::spi_transfer_log
	; li r2, 1
	; call [r0]
	
	li r0, :spiData
	lih r0, ::spiData
	ldi r1, (bp+3)
	out r1, (r0)	; set data to d
	li r0, :spiControl
	lih r0, ::spiControl
	in r1, (r0)	; read spiControl
	ori r1, 1	; initiate transfer setting transfer-bit
	out r1, (r0)	; write spiControl
:spi_transfer_loop 
	in r1, (r0)	; read spiControl
	andif r1, 8	; check ready-bit
	li r2, :spi_transfer_loop
	lih r2, ::spi_transfer_loop
	jpz [r2]
	li r2, 1
	not r2, r2
	and r1, r2	; clear transfer bit again
	out r1, (r0)	; write spiControl
	li r0, :spiData
	lih r0, ::spiData
	in r0, (r0)	; read received data
	andi r0, 0xFF	; only select low byte
	
	; debug:
	; push r0
	; push r0
	; li r1, 0xFC
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_hex
	; lih r1, ::print_hex
	; call [r1]
	; addi sp, 3
	; li r1, 10
	; push r1
	; li r1, 0xFC
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_char
	; lih r1, ::print_char
	; call [r1]
	; addi sp, 3
	; pop r0
	
	pop bp
	ret
	
; ***** void sd_waitNotBusy()
; * wait for card to go not busy, timeout = 300 msec
; *****
:sd_waitNotBusy_log
; .data	32, 'w', 'a', 'i', 't', 'N', 'o', 't', 'B', 'u', 's', 'y', 32, 0
:sd_waitNotBusy
	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :sd_waitNotBusy_log
	; lih r1, ::sd_waitNotBusy_log
	; li r2, 0
	; call [r0]
	
	subi sp, 1 ; make space for local var t0 = sp+1
	; t0 = millis()
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r0, (r0)
	sti r0, (sp+1);
:sd_waitNotBusy_loop
	; if (spi_transfer(0xFF)==0xFF) return true;
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	li r2, 0xFF
	cmp r0, r2
	li r1, :sd_waitNotBusy_return
	lih r1, ::sd_waitNotBusy_return
	jpz [r1]
	; d = millis() - t0
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r1, (r0)
	ldi r2, (sp+1)
	sub r1, r2
	; if (d < 300) goto loop
	li r2, 0x2C
	lih r2, 0x01
	cmp r1, r2
	li r0, :sd_waitNotBusy_loop
	lih r0, ::sd_waitNotBusy_loop
	jpnc [r0]
:sd_waitNotBusy_return
	addi sp, 1	; remove local vars
	ret

; ***** void sd_waitStartBlock()
; * wait for start block token
; *****
:sd_waitStartBlock_log
; .data	32, 'w', 'a', 'i', 't', 'S', 't', 'a', 'r', 't', 'B', 'l', 'o', 'c', 'k', 32, 0
:sd_waitStartBlock
	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :sd_waitStartBlock_log
	; lih r1, ::sd_waitStartBlock_log
	; li r2, 0
	; call [r0]

	subi sp, 1 ; local var t0 = sp+1	
	; t0 = millis()
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r0, (r0)
	sti r0, (sp+1);
	; while ((status = spi_transfer(0xFF)) == 0xFF)
:sd_waitStartBlock_loop
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; li r0, 0xFE ; DEBUG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	li r1, 0xFF
	cmp r0, r1
	li r1, :sd_waitStartBlock_endloop
	lih r1, ::sd_waitStartBlock_endloop
	jpnz [r1]
	; d = millis() - t0
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r0, (r0)
	ldi r1, (sp+1)
	sub r0, r1
	; if (d < 300) goto :sd_waitStartBlock_loop
	li r1, 0x2C
	lih r1, 0x01
	cmp r0, r1
	li r0, :sd_waitStartBlock_loop
	lih r0, ::sd_waitStartBlock_loop
	jpnc [r0]
	; sd_error(SD_CARD_ERROR_READ_TIMEOUT)
	li r0, :SD_CARD_ERROR_READ_TIMEOUT
	lih r0, ::SD_CARD_ERROR_READ_TIMEOUT
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:sd_waitStartBlock_endloop
	li r1, 0xFE
	cmp r0, r1
	li r0, :sd_waitStartBlock_return
	lih r0, ::sd_waitStartBlock_return
	jpz [r0]
	; sd_error(SD_CARD_ERROR_READ)
	li r0, :SD_CARD_ERROR_READ
	lih r0, ::SD_CARD_ERROR_READ
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:sd_waitStartBlock_return
	addi sp, 1
	ret

; ***** uint8 sd_cardCommand(uint8 cmd, uint32 arg)
; * send command and return error code.  Return zero for OK
; * return value in r0
; *****
:sd_cardCommand_log
; .data	32, 'c','a','r','d','C','o','m','m','a','n','d', 32, 0
:sd_cardCommand
	push bp
	set bp, sp
	
	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :sd_cardCommand_log
	; lih r1, ::sd_cardCommand_log
	; li r2, 3
	; call [r0]
	
	; set slave select to 0
	li r0, :spiControl
	lih r0, ::spiControl
	in r1, (r0)
	li r2, 17	; create mask to erase the ss bit (and the transfer bit)
	not r2, r2
	and r1, r2
	out r1, (r0)
	; sd_waitNotBusy()
	li r0, :sd_waitNotBusy
	lih r0, ::sd_waitNotBusy
	call [r0]
	; spi_transfer(cmd | 0x40)
	ldi r0, (bp+5)
	ori r0, 0x40
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; for (int8 s = 24; s >= 0; s -= 8) spi_transfer(arg >> s);
  	ldi r1, (bp+4)
  	shri r1, 8
  	push r1
  	li r0, :spi_transfer
  	lih r0, ::spi_transfer
  	call [r0]
  	ldi r1, (bp+4)
  	andi r1, 0xFF
  	sti r1, (sp+1)
  	li r0, :spi_transfer
  	lih r0, ::spi_transfer
  	call [r0]
  	ldi r1, (bp+3)
  	shri r1, 8
  	sti r1, (sp+1)
  	li r0, :spi_transfer
  	lih r0, ::spi_transfer
  	call [r0]
  	ldi r1, (bp+3)
  	andi r1, 0xFF
  	sti r1, (sp+1)
  	li r0, :spi_transfer
  	lih r0, ::spi_transfer
  	call [r0]
	addi sp, 1
	; uint8_t crc = 0XFF;
  	; if (cmd == CMD0) crc = 0X95;  // correct crc for CMD0 with arg 0
	; if (cmd == CMD8) crc = 0X87;  // correct crc for CMD8 with arg 0x1AA
	; spi_transfer(crc);
  	ldi r2, (bp+5)
  	cmpi r2, 0x00
  	sz r1
  	andi r1, 0x95
  	cmpi r2, 0x08
  	sz r3
  	andi r3, 0x87
  	or r1, r3
  	sz r3
  	or r1, r3
  	andi r1, 0xFF
  	push r1
  	li r0, :spi_transfer
  	lih r0, ::spi_transfer
  	call [r0]
  	addi sp, 1
  	; for (uint8 i = 0; ((spi_transfer(0xFF)) & 0x80) && i != 0xFF; i++)
  	li r1, 0xFF
  	lih r1, 0xFF
:sd_cardCommand_loop
	inc r1
  	push r1
  	li r0, 0xFF
  	push r0
  	li r0, :spi_transfer
  	lih r0, ::spi_transfer
  	call [r0]
  	; li r0, 0x01 ; DEBUG!!!!!!!!!!!!!!!!!!!!!!!!!
  	addi sp, 1
  	pop r1
  	li r2, 0x80
  	andf r0, r2
  	snz r2
  	li r3, 0xFF
  	cmp r1, r3
  	snz r3
  	and r2, r3
  	li r2, :sd_cardCommand_loop
  	lih r2, ::sd_cardCommand_loop
  	jpnz [r2]
	pop bp
	ret

; ***** uint8 sd_cardAcmd(uint8 cmd, uint32 arg)
; * send A-command and return error code.  Return zero for OK
; * return value in r0
; *****
:sd_cardAcmd_log
; .data	32, 'c', 'a', 'r', 'd', 'A', 'c', 'm', 'd', 32, 0
:sd_cardAcmd
	push bp
	set bp, sp
	
	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :sd_cardAcmd_log
	; lih r1, ::sd_cardAcmd_log
	; li r2, 3
	; call [r0]
	
	; sd_cardCommand(0x37, 0)	
	li r0, 0x37
	push r0
	li r0, 0
	push r0
	push r0
	li r0, :sd_cardCommand
	lih r0, ::sd_cardCommand
	call [r0]
	addi sp, 3
	; sd_cardCommand(cmd, arg)
	ldi r0, (bp+5)
	push r0
	ldi r0, (bp+4)
	push r0
	ldi r0, (bp+3)
	push r0
	li r0, :sd_cardCommand
	lih r0, ::sd_cardCommand
	call [r0]
	addi sp, 3
	pop bp
	ret

; ***** void sd_init()
; * initialises the SD card.  If no SD card can be found the system stops.
; *****
:sd_init
	subi sp, 2 ; local vars: t0_L=sp+1, arg_H=sp+2
	; SD_cardType = 0;
	li r0, 0
	li r1, :SD_cardType
	lih r1, ::SD_cardType
	out r0, (r1)
	; t0 = millis();
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r0, (r0)
	sti r0, (sp+1);
	; SPISettings(250000, MSBFIRST, SPI_MODE0);
	li r0, :spiControl
	lih r0, ::spiControl
	li r1, 0xF0
	lih r1, 24
	out r1, (r0)
	; for (uint8 i = 0; i < 10; i++) spi_transfer(0xFF);
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; set slave select to 0
	li r0, :spiControl
	lih r0, ::spiControl
	in r1, (r0)
	li r2, 17	; create mask to erase the ss bit (and the transfer bit)
	not r2, r2
	and r1, r2
	out r1, (r0)
	; while ((status = sd_cardCommand(CMD0, 0)) != R1_IDLE_STATE) {
	;   unsigned int d = millis() - t0;
	;   if (d > 2000) {
	;     error(SD_CARD_ERROR_CMD0);
	;   }
	; }
:sd_init_loop
	li r0, 0
	push r0
	push r0
	push r0
	li r0, :sd_cardCommand
	lih r0, ::sd_cardCommand
	call [r0]
	addi sp, 3
	; li r0, 1 ; DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!
	cmpi r0, 0x01
	li r0, :sd_init_idle
	lih r0, ::sd_init_idle
	jpz [r0]
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r1, (r0)
	ldi r2, (sp+1)
	sub r1, r2
	li r0, 0xD0
	lih r0, 0x07
	cmp r1, r0
	li r0, :sd_init_loop
	lih r0, ::sd_init_loop
	jpnc [r0]
	li r0, :SD_CARD_ERROR_CMD0
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:sd_init_idle
	
	; if ((cardCommand(CMD8, 0x1AA) & R1_ILLEGAL_COMMAND)) {
	;   SD_cardType = SD_CARD_TYPE_SD1;
	; } else {
	;   // only need last byte of r7 response
	;   for (uint8_t i = 0; i < 4; i++) {
	;     status = spi_transfer(0xFF);
	;   }
	;   if (status != 0XAA) {
	;     error(SD_CARD_ERROR_CMD8);
	;   }
	;   SD_cardType = SD_CARD_TYPE_SD2;
	; }
	li r0, 0x08
	push r0
	li r0, 0
	push r0
	li r0, 0xAA
	lih r0, 0x01
	push r0
	li r0, :sd_cardCommand
	lih r0, ::sd_cardCommand
	call [r0]
	addi sp, 3
	; li r0, 5 ; DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!
	andi r0, 4
	li r0, :sd_init_else2
	lih r0, ::sd_init_else2
	jpz [r0]
	li r0, 1
	li r1, :SD_cardType
	lih r1, ::SD_cardType
	out r0, (r1)
	li r0, :sd_init_endif
	lih r0, ::sd_init_endif
	jp [r0]
:sd_init_else2
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; li r0, 0xAA ; DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	li r1, 0xAA
	cmp r0, r1
	li r0, :sd_init_else3
	lih r0, ::sd_init_else3
	jpz [r0]
	li r0, :SD_CARD_ERROR_CMD8
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:sd_init_else3
	li r0, 2
	li r1, :SD_cardType
	lih r1, ::SD_cardType
	out r0, (r1)
:sd_init_endif
	; arg = SD_cardType == SD_CARD_TYPE_SD2 ? 0x40000000 : 0
	li r1, 0x00
	lih r1, 0x40
	li r0, :SD_cardType
	lih r0, ::SD_cardType
	in r0, (r0)
	cmpi r0, 0x02
	sz r2
	and r1, r2
	sti r1, (sp+2)
	; while ((status = cardAcmd(ACMD41, arg)) != R1_READY_STATE) {
	;   unsigned int d = millis() - t0;
	;   if (d > SD_INIT_TIMEOUT) {
	;     error(SD_CARD_ERROR_ACMD41);
	;   }
	; }
:sd_init_loop2	
	li r0, 0x29
	push r0
	ldi r0, (sp+3)
	push r0
	li r0, 0
	push r0
	li r0, :sd_cardAcmd
	lih r0, ::sd_cardAcmd
	call [r0]
	addi sp, 3
	; li r0, 0x00 ; DEBUG !!!!!!!!!!!!!!!!!!!!!!!!!
	cmpi r0, 0x00
	li r0, :sd_init_idle2
	lih r0, ::sd_init_idle2
	jpz [r0]
	li r0, :msecLSB
	lih r0, ::msecLSB
	in r1, (r0)
	ldi r2, (sp+1)
	sub r1, r2
	li r0, 0xD0
	lih r0, 0x10
	cmp r1, r0
	li r0, :sd_init_loop2
	lih r0, ::sd_init_loop2
	jpnc [r0]
	li r0, :SD_CARD_ERROR_ACMD41
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:sd_init_idle2
	; if (SD_cardType != SD_CARD_TYPE_SD2) goto :sd_init_return
	li r0, :SD_cardType
	lih r0, ::SD_cardType
	in r0, (r0)
	cmpi r0, 2
	li r0, :sd_init_return
	lih r0, ::sd_init_return
	jpnz r0
	; if (cardCommand(CMD58, 0)) error(SD_CARD_ERROR_CMD58);
	li r0, 0x3A
	push r0
	li r0, 0
	push r0
	push r0
	li r0, :sd_cardCommand
	lih r0, ::sd_cardCommand
	call [r0]
	addi sp, 3
	; li r0, 0 ; DEBUG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	or r0, r0
	li r0, :sd_init_else4
	lih r0, ::sd_init_else4
	jpz [r0]
	li r0, :SD_CARD_ERROR_CMD58
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:sd_init_else4
	; if ((spi_transfer(0xFF) & 0xC0) == 0xC0) SD_cardType = SD_CARD_TYPE_SDHC;
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; li r0, 0x21 ; DEBUG !!!!!!!!!!!!!!!!!!!
	li r1, 0xC0
	and r0, r1
	cmp r0, r1
	li r0, :sd_init_else5
	lih r0, ::sd_init_else5
	jpnz [r0]
	li r0, 3
	li r1, :SD_cardType
	lih r1, ::SD_cardType
	out r0, (r1)
:sd_init_else5
	; for (uint8 i = 0; i < 3; i++) spi_transfer(0xFF)
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
:sd_init_return
	; digitalWrite(SS, 1) ; setSckRate(0)
	li r0, :spiControl
	lih r0, ::spiControl
	in r1, (r0)
	ori r1, 16
	andi r1, 0xFE
	out r1, (r0)
	addi sp, 2 ; clean up local vars
	ret

; ***** void sd_readData(uint32 block, uint16* dst, bool io)
; * reads one block of data (512 bytes) into the destination
; * if io is true (non zero) the data is written to io memory
; * the bytes are read into 16 bits, LSB first
; *****
:sd_readData_log
; .data	32, 'r', 'e', 'a', 'd', 'D', 'a', 't', 'a', 32, 0
:sd_readData
	push bp
	set bp, sp
	
	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :sd_readData_log
	; lih r1, ::sd_readData_log
	; li r2, 4
	; call [r0]

	; if (SD_cardType != SD_CARD_TYPE_SDHC) blockNumber <<= 9;
	li r0, :SD_cardType
	lih r0, ::SD_cardType
	in r0, (r0)
	cmpi r0, 3
	li r0, :sd_readData_sdhc
	lih r0, ::sd_readData_sdhc
	jpz [r0]
	ldi r0, (bp+5)
	ldi r1, (bp+6)
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	shli r0, 1
	shlci r1, 1
	sti r0, (bp+5)
	sti r1, (bp+6)
:sd_readData_sdhc
	; if (sd_cardCommand(CMD17, block)) sd_error(SD_CARD_ERROR_CMD17)
	li r0, 0x11
	push r0
	ldi r0, (bp+6)
	push r0
	ldi r0, (bp+5)
	push r0
	li r0, :sd_cardCommand
	lih r0, ::sd_cardCommand
	call [r0]
	addi sp, 3
	; li r0, 0 ; DEBUG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cmpi r0, 0
	li r0, :sd_readData_else
	lih r0, ::sd_readData_else
	jpz [r0]
	li r0, :SD_CARD_ERROR_CMD17
	lih r0, ::SD_CARD_ERROR_CMD17
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:sd_readData_else
	; sd_waitStartBlock()
	li r0, :sd_waitStartBlock
	lih r0, ::sd_waitStartBlock
	call [r0]
	; for (uint16 i = 0; i < 256; i++) dst[i] = (spi_transfer(0xFF) << 8 | spi_transfer(0xFF))
	li r1, 0
:sd_readData_loop
	li r0, 0
	lih r0, 1
	cmp r1, r0
	li r0, :sd_readData_return
	lih r0, ::sd_readData_return
	jpz [r0]
	push r1
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; li r0, 0x55 ; DEBUG !!!!!!!!!!!!!!!!!!
	push r0
	li r0, 0xFF
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	; li r0, 0xAA ; DEBUG !!!!!!!!!!!!!!!!!!
	pop r1
	shli r0, 8
	or r0, r1
	pop r1
	ldi r2, (bp+4) ; dst
	add r2, r1 ; dst + i
	ldi r3, (bp+3) ; io
	nop r3, r3
	li r3, :sd_readData_no_io
	lih r3, ::sd_readData_no_io
	jpz [r3]
	out r0, (r2)
	li r3, :sd_readData_next
	lih r3, ::sd_readData_next
	jp [r3]
:sd_readData_no_io
	st r0, (r2)
:sd_readData_next
	inc r1
	li r0, :sd_readData_loop
	lih r0, ::sd_readData_loop
	jp [r0]
:sd_readData_return
	pop bp
	ret
	
; --------- FAT

; ***** void fat_findAndCheckPartition()
; * finds the first logical partition on the disk.
; * if the disk was not formatted or the partition>=32MB an error is shown
; *****
:fat_findAndCheckPartition_log
; .data	32, 'f', 'i', 'n', 'd', 'A', 'n', 'd', 'C', 'h', 'e', 'c', 'k', 'P', 'a', 'r', 't', 'i', 't', 'i', 'o', 'n', 32, 0
:fat_findAndCheckPartition

	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :fat_findAndCheckPartition_log
	; lih r1, ::fat_findAndCheckPartition_log
	; li r2, 0
	; call [r0]
	
	; startPartition = lengthPartition = 0
	li r1, 0
	li r0, :disk_startPartition_LSB
	lih r0, ::disk_startPartition_LSB
	out r1, (r0)
	li r0, :disk_startPartition_MSB
	lih r0, ::disk_startPartition_MSB
	out r1, (r0)
	li r0, :disk_lengthPartition_LSB
	lih r0, ::disk_lengthPartition_LSB
	out r1, (r0)
	li r0, :disk_lengthPartition_MSB
	lih r0, ::disk_lengthPartition_MSB
	out r1, (r0)
	; read MBR
	; readData(0, disk_sector, true)
	li r0, 0
	push r0
	push r0
	li r0, :disk_sector
	lih r0, ::disk_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_readData
	lih r0, ::sd_readData
	call [r0]
	addi sp, 4
	; check signature 0xAA55
	li r0, :disk_sector
	lih r0, ::disk_sector
	li r1, 0xFF
	add r0, r1
	in r0, (r0)
	
	; debug
	; push r0
	; li r0, 0xF0
	; push r0
	; li r0, 0
	; push r0
	; li r0, :print_hex
	; lih r0, ::print_hex
	; call [r0]
	; addi sp, 2
	; pop r0
	
	li r1, 0x55
	lih r1, 0xAA
	cmp r0, r1
	li r0, :fat_findAndCheckPartition_sig_ok
	lih r0, ::fat_findAndCheckPartition_sig_ok
	jpz [r0]
	; error: disk not formatted!
	li r0, :FAT_ERROR_NOT_FORMATTED
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:fat_findAndCheckPartition_sig_ok
	; read start and length of 1st partition
	li r0, :disk_sector
	lih r0, ::disk_sector
	li r1, 0xE3
	add r1, r0
	in r2, (r1)
	li r3, :disk_startPartition_LSB
	lih r3, ::disk_startPartition_LSB
	out r2, (r3)

	; debug
	; push r2
	; li r2, 0xF0
	; push r2
	; li r2, 0
	; push r2
	; li r2, :print_hex
	; lih r2, ::print_hex
	; call [r2]
	; addi sp, 2
	; pop r2
	
	inc r1
	in r2, (r1)
	li r3, :disk_startPartition_MSB
	lih r3, ::disk_startPartition_MSB
	out r2, (r3)
	
	; debug
	; push r2
	; li r2, 0xF0
	; push r2
	; li r2, 0
	; push r2
	; li r2, :print_hex
	; lih r2, ::print_hex
	; call [r2]
	; addi sp, 2
	; pop r2
	
	inc r1
	in r2, (r1)
	li r3, :disk_lengthPartition_LSB
	lih r3, ::disk_lengthPartition_LSB
	out r2, (r3)
	
	; debug
	; push r2
	; li r2, 0xF0
	; push r2
	; li r2, 0
	; push r2
	; li r2, :print_hex
	; lih r2, ::print_hex
	; call [r2]
	; addi sp, 2
	; pop r2
	
	inc r1
	in r2, (r1)
	li r3, :disk_lengthPartition_MSB
	lih r3, ::disk_lengthPartition_MSB
	out r2, (r3)
	
	; debug
	; push r2
	; li r2, 0xF0
	; push r2
	; li r2, 0
	; push r2
	; li r2, :print_hex
	; lih r2, ::print_hex
	; call [r2]
	; addi sp, 2
	; pop r2
	
	; check lengthPartition < 65536
	nop r2, r2
	li r0, :fat_findAndCheckPartition_return
	lih r0, ::fat_findAndCheckPartition_return
	jpz [r0]
	; error: partition too big
	li r0, :FAT_ERROR_PARTITION_TOO_BIG
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:fat_findAndCheckPartition_return
	ret
	
; ***** void fat_findAndCheckFAT()
; * finds the FAT on the first logical partition
; * the FAT is expected to have 512 bytes per sector and 2 FAT tables
; *****
:fat_findAndCheckFAT_log
; .data	32, 'f', 'i', 'n', 'd', 'A', 'n', 'd', 'C', 'h', 'e', 'c', 'k', 'F', 'A', 'T', 32, 0
:fat_findAndCheckFAT

	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :fat_findAndCheckFAT_log
	; lih r1, ::fat_findAndCheckFAT_log
	; li r2, 0
	; call [r0]
	
	; fatPosition = rootFolderPosition = numberOfSectorsForRootFolder = 0
	li r1, 0
	li r0, :disk_fatPosition
	lih r0, ::disk_fatPosition
	out r1, (r0)
	li r0, :disk_rootFolderPosition_LSB
	lih r0, ::disk_rootFolderPosition_LSB
	out r1, (r0)
	li r0, :disk_rootFolderPosition_MSB
	lih r0, ::disk_rootFolderPosition_MSB
	out r1, (r0)
	li r0, :disk_numberOfSectorsForRootFolder
	lih r0, ::disk_numberOfSectorsForRootFolder
	out r1, (r0)
	; read VBR
	; readData(startPartition, disk_sector, true)
	li r0, :disk_startPartition_MSB
	lih r0, ::disk_startPartition_MSB
	in r0, (r0)
	push r0
	li r0, :disk_startPartition_LSB
	lih r0, ::disk_startPartition_LSB
	in r0, (r0)
	push r0
	li r0, :disk_sector
	lih r0, ::disk_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_readData
	lih r0, ::sd_readData
	call [r0]
	addi sp, 4
	; bytesPerSector = read16(11)
	li r0, :disk_sector
	lih r0, ::disk_sector
	addi r0, 5
	in r1, (r0)
	shri r1, 8
	inc r0
	in r2, (r0)
	andi r2, 0xFF
	shli r2, 8
	or r1, r2 ; bytesPerSector now in r1
	
	; debug
	; push r1
	; li r1, 0xF0
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_hex
	; lih r1, ::print_hex
	; call [r1]
	; addi sp, 2
	; pop r1
	
	; if (bytesPerSector != 512) error
	li r2, 0x00
	lih r2, 0x02
	cmp r1, r2
	li r2, :fat_findAndCheckFAT_bytesPerSector_OK
	lih r2, ::fat_findAndCheckFAT_bytesPerSector_OK
	jpz [r2]
	li r0, :FAT_ERROR_INVALID_SECTOR_SIZE
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:fat_findAndCheckFAT_bytesPerSector_OK
	; numberOfFATs = read8(16)
	li r0, :disk_sector
	lih r0, ::disk_sector
	addi r0, 8
	in r0, (r0)
	andi r0, 0xFF
	
	; debug
	; push r0
	; li r0, 0xF0
	; push r0
	; li r0, 0
	; push r0
	; li r0, :print_hex
	; lih r0, ::print_hex
	; call [r0]
	; addi sp, 2
	; pop r0
	
	; if (numberOfFATs != 2) error
	cmpi r0, 2
	li r2, :disk_findAndCheckFAT_numberOfFats_OK
	lih r2, ::disk_findAndCheckFAT_numberOfFats_OK
	jpz [r2]
	li r0, :FAT_ERROR_INVALID_NUNMBER_OF_FATS
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
:disk_findAndCheckFAT_numberOfFats_OK
	; sectorsPerFat = read16(22);
	li r0, :disk_sector
	lih r0, ::disk_sector
	addi r0, 11
	in r0, (r0)
	li r1, :disk_sectorsPerFat
	lih r1, ::disk_sectorsPerFat
	out r0, (r1)
	
	; debug
	; push r0
	; li r0, 0xF0
	; push r0
	; li r0, 0
	; push r0
	; li r0, :print_hex
	; lih r0, ::print_hex
	; call [r0]
	; addi sp, 2
	; pop r0
	
	; fatPosition = read16(14);
	li r0, :disk_sector
	lih r0, ::disk_sector
	addi r0, 7
	in r0, (r0)
	li r1, :disk_fatPosition
	lih r1, ::disk_fatPosition
	out r0, (r1)
	
	; debug
	; push r0
	; li r0, 0xF0
	; push r0
	; li r0, 0
	; push r0
	; li r0, :print_hex
	; lih r0, ::print_hex
	; call [r0]
	; addi sp, 2
	; pop r0
	
	; rootFolderPosition = startPartition + fatPosition + sectorsPerFat * 2;
	li r0, :disk_startPartition_LSB
	lih r0, ::disk_startPartition_LSB
	in r1, (r0)
	li r0, :disk_startPartition_MSB
	lih r0, ::disk_startPartition_MSB
	in r2, (r0)
	li r0, :disk_fatPosition
	lih r0, ::disk_fatPosition
	in r3, (r0)
	add r1, r3
	addci r2, 0
	li r0, :disk_sectorsPerFat
	lih r0, ::disk_sectorsPerFat
	in r3, (r0)
	shli r3, 1
	add r1, r3
	addci r2, 0
	li r0, :disk_rootFolderPosition_LSB
	lih r0, ::disk_rootFolderPosition_LSB
	out r1, (r0)
	
	; debug
	; push r1
	; li r1, 0xF0
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_hex
	; lih r1, ::print_hex
	; call [r1]
	; addi sp, 2
	; pop r1
	
	li r0, :disk_rootFolderPosition_MSB
	lih r0, ::disk_rootFolderPosition_MSB
	out r2, (r0)
	
	; debug
	; push r2
	; li r2, 0xF0
	; push r2
	; li r2, 0
	; push r2
	; li r2, :print_hex
	; lih r2, ::print_hex
	; call [r2]
	; addi sp, 2
	; pop r2
	
        ; numberOfSectorsForRootFolder = read16(17) / 16;
        li r0, :disk_sector
	lih r0, ::disk_sector
	addi r0, 8
	in r1, (r0)
	shri r1, 8
	inc r0
	in r2, (r0)
	andi r2, 0xFF
	shli r2, 8
	or r1, r2 ; read16(17) now in r1
	shri r1, 4
	li r0, :disk_numberOfSectorsForRootFolder
	lih r0, ::disk_numberOfSectorsForRootFolder
	out r1, (r0)
	
	; debug
	; push r1
	; li r1, 0xF0
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_hex
	; lih r1, ::print_hex
	; call [r1]
	; addi sp, 2
	; pop r1
	
	; sectorsPerCluster = read8(13)
	li r0, :disk_sector
	lih r0, ::disk_sector
	addi r0, 6
	in r0, (r0)
	shri r0, 8
	li r1, :disk_sectorsPerCluster
	lih r1, ::disk_sectorsPerCluster
	out r0, (r1)
	
	; debug
	; push r0
	; li r0, 0xF0
	; push r0
	; li r0, 0
	; push r0
	; li r0, :print_hex
	; lih r0, ::print_hex
	; call [r0]
	; addi sp, 2
	; pop r0
	
	ret

; ***** void openFile(char* filename)
; * opens a file with a certain filename (11 chars: 8+3)
; * the string is expected to be an array of 11 chars (not null terminated)
; * in FAT filenames are all capitals and spaces are used to pad
; * when the file was not found, fileStart is put to zero
; *****
:disk_openFile_log
; .data	32, 'o', 'p', 'e', 'n', 'F', 'i', 'l', 'e', 32, 0
:disk_openFile
	push bp
	set bp, sp
	
	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :disk_openFile_log
	; lih r1, ::disk_openFile_log
	; li r2, 1
	; call [r0]
	
	; fileStart = fileLength = currentSectorNumber = 0;
	li r1, 0
	li r0, :disk_fileStart
	lih r0, ::disk_fileStart
	out r1, (r0)
	li r0, :disk_fileLength_LSB
	lih r0, ::disk_fileLength_LSB
	out r1, (r0)
	li r0, :disk_fileLength_MSB
	lih r0, ::disk_fileLength_MSB
	out r1, (r0)
	li r0, :disk_currentSectorNumber
	lih r0, ::disk_currentSectorNumber
	out r1, (r0)
        ; currentClusterNumber = 0xFFFF; // end of file
        li r1, 0xFF
        lih r1, 0xFF
        li r0, :disk_currentClusterNumber
        lih r0, ::disk_currentClusterNumber
        out r1, (r0)
        ; for(register int i = 0; i < numberOfSectorsForRootFolder; i++) {
        ; i -> r1
        li r1, 0
:disk_openFile_loop

	; debug
	; push r1
	; li r1, 0xF0
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_hex
	; lih r1, ::print_hex
	; call [r1]
	; addi sp, 2
	; pop r1
	
	li r0, :disk_numberOfSectorsForRootFolder
	lih r0, ::disk_numberOfSectorsForRootFolder
	in r0, (r0)
	cmp r1, r0
	li r0, :disk_openFile_endLoop
	lih r0, ::disk_openFile_endLoop
	jpc [r0]
	push r1	; save r1 ("i")
	; disk.readSector(rootFolderPosition + i);
	li r0, :disk_rootFolderPosition_LSB
	lih r0, ::disk_rootFolderPosition_LSB
	in r2, (r0)
	li r0, :disk_rootFolderPosition_MSB
	lih r0, ::disk_rootFolderPosition_MSB
	in r3, (r0)
	add r2, r1
	addci r3, 0
	push r3
	push r2
	li r0, :disk_sector
	lih r0, ::disk_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_readData
	lih r0, ::sd_readData
	call [r0]
	addi sp, 4
	; for (register int entry = 0; entry < 16; entry++) {
	; entry -> r1
	li r1, 0
:disk_openFile_loop2

	; debug
	; push r1
	; li r1, 0xF0
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_hex
	; lih r1, ::print_hex
	; call [r1]
	; addi sp, 2
	; pop r1
	
	cmpi r1, 16
	li r0, :disk_openFile_endLoop2
	lih r0, ::disk_openFile_endLoop2
	jpc [r0]
	push r1
	; position (r1) = entry * 32;
	shli r1, 4
	; if (filename.equals(name))
	; compare string at sector[position] with filename
	li r0, :disk_sector
	lih r0, ::disk_sector
	add r0, r1	; r0 = found filename
	ldi r2, (bp+3)	; r2 = filename
	li r5, :disk_openFile_loop2_continue
	lih r5, ::disk_openFile_loop2_continue
	in r3, (r0)
	andi r3, 0xFF
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	ld r4, (r2)
	in r3, (r0)
	shri r3, 8
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	andi r3, 0xFF
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	ld r4, (r2)
	in r3, (r0)
	shri r3, 8
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	andi r3, 0xFF
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	ld r4, (r2)
	in r3, (r0)
	shri r3, 8
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	andi r3, 0xFF
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	ld r4, (r2)
	in r3, (r0)
	shri r3, 8
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	andi r3, 0xFF
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	ld r4, (r2)
	in r3, (r0)
	shri r3, 8
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	andi r3, 0xFF
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	; currentClusterNumber = fileStart = read16(position + 26)
	pop r1	; restore r1 = entry
	shli r1, 4 ; position (r1) = entry * 32;
	addi r1, 13
	li r2, :disk_sector
	lih r2, ::disk_sector
	add r2, r1
	in r0, (r2)
	li r3, :disk_fileStart
	lih r3, ::disk_fileStart
	out r0, (r3)
	li r3, :disk_currentClusterNumber
	lih r3, ::disk_currentClusterNumber
	out r0, (r3)
	
	; debug
	; push r0
	; li r0, 0xF0
	; push r0
	; li r0, 0
	; push r0
	; li r0, :print_hex
	; lih r0, ::print_hex
	; call [r0]
	; addi sp, 2
	; pop r0
	
	; fileLength = read32(position + 28)
	inc r2
	in r3, (r2)
	li r0, :disk_fileLength_LSB
	lih r0, ::disk_fileLength_LSB
	out r3, (r0)
	
	; debug
	; push r3
	; li r3, 0xF0
	; push r3
	; li r3, 0
	; push r3
	; li r3, :print_hex
	; lih r3, ::print_hex
	; call [r3]
	; addi sp, 2
	; pop r3
	
	inc r2
	in r3, (r2)
	li r0, :disk_fileLength_MSB
	lih r0, ::disk_fileLength_MSB
	out r3, (r0)
	
	; debug
	; push r3
	; li r3, 0xF0
	; push r3
	; li r3, 0
	; push r3
	; li r3, :print_hex
	; lih r3, ::print_hex
	; call [r3]
	; addi sp, 2
	; pop r3
	
	; return
	addi sp, 1 ; remove "i" from stack
	li r0, :disk_openFile_endLoop
	lih r0, ::disk_openFile_endLoop
	jp [r0]
:disk_openFile_loop2_continue
	; }
	pop r1
	inc r1
	li r0, :disk_openFile_loop2
	lih r0, ::disk_openFile_loop2
	jp [r0]
:disk_openFile_endLoop2
	; }
	pop r1
	inc r1
	li r0, :disk_openFile_loop
	lih r0, ::disk_openFile_loop
	jp [r0]
:disk_openFile_endLoop
	pop bp
	ret

; ***** boolean disk_readNextFileSector(uint16 location[256])
; * reads the next sector into the given location
; * returns false if end of file is reached
; *****
:disk_readNextFileSector_log
; .data	32, 'r', 'e', 'a', 'd', 'N', 'e', 'x', 't', 'F', 'i', 'l', 'e', 'S', 'e', 'c', 't', 'o', 'r', 32, 0
:disk_readNextFileSector
	push bp
	set bp, sp
	
	; log
	; li r0, :log
	; lih r0, ::log
	; li r1, :disk_readNextFileSector_log
	; lih r1, ::disk_readNextFileSector_log
	; li r2, 1
	; call [r0]

	; if (currentClusterNumber >= 0xFFF8) return false; // end of file
	li r0, :disk_currentClusterNumber
	lih r0, ::disk_currentClusterNumber
	in r0, (r0)
	
	; debug
	; push r0
	; li r0, 0xF0
	; push r0
	; li r0, 0
	; push r0
	; li r0, :print_hex
	; lih r0, ::print_hex
	; call [r0]
	; addi sp, 2
	; pop r0
	
	li r1, 0xF8
	lih r1, 0xFF
	cmp r0, r1
	li r0, 0
	li r1, :disk_readNextFileSector_return
	lih r1, ::disk_readNextFileSector_return
	jpc [r1]
	; sd_readData(rootFolderPosition + numberOfSectorsForRootFolder + (currentClusterNumber-2)*sectorsPerCluster + currentSectorNumber)
	li r0, :disk_rootFolderPosition_LSB
	lih r0, ::disk_rootFolderPosition_LSB
	in r1, (r0)
	li r0, :disk_rootFolderPosition_MSB
	lih r0, ::disk_rootFolderPosition_MSB
	in r2, (r0)
	li r0, :disk_numberOfSectorsForRootFolder
	lih r0, ::disk_numberOfSectorsForRootFolder
	in r3, (r0)
	add r1, r3
	addci r2, 0
	li r0, :disk_currentClusterNumber
	lih r0, ::disk_currentClusterNumber
	in r3, (r0)
	subi r3, 2
	li r4, 0
	li r0, :disk_sectorsPerCluster
	lih r0, ::disk_sectorsPerCluster
	in r5, (r0)
	nop r5, r5
	li r0, :disk_readNextFileSector_multloop_end
	lih r0, ::disk_readNextFileSector_multloop_end
	jpz [r0]
:disk_readNextFileSector_multloop
	andif r5, 1
	li r0, :disk_readNextFileSector_multloop_end
	lih r0, ::disk_readNextFileSector_multloop_end
	jpnz [r0]
	shri r5, 1
	shli r3, 1
	shlci r4, 1
	li r0, :disk_readNextFileSector_multloop
	lih r0, ::disk_readNextFileSector_multloop
	jp [r0]
:disk_readNextFileSector_multloop_end
	add r1, r3
	addc r2, r4
	li r0, :disk_currentSectorNumber
	lih r0, ::disk_currentSectorNumber
	in r3, (r0)
	add r1, r3
	addci r2, 0
	
	; debug
	; push r2
	; li r2, 0xF0
	; push r2
	; li r2, 0
	; push r2
	; li r2, :print_hex
	; lih r2, ::print_hex
	; call [r2]
	; addi sp, 2
	; pop r2
	; debug
	; push r1
	; li r1, 0xF0
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_hex
	; lih r1, ::print_hex
	; call [r1]
	; addi sp, 2
	; pop r1
	
	push r2
	push r1
	ldi r1, (bp+3)
	push r1
	li r1, 0
	push r1
	li r0, :sd_readData
	lih r0, ::sd_readData
	call [r0]
	addi sp, 4
	; currentSectorNumber++;
	li r1, :disk_currentSectorNumber
	lih r1, ::disk_currentSectorNumber
	in r2, (r1)
	inc r2
	out r2, (r1)
	
	; debug
	; push r2
	; li r2, 0xF0
	; push r2
	; li r2, 0
	; push r2
	; li r2, :print_hex
	; lih r2, ::print_hex
	; call [r2]
	; addi sp, 2
	; pop r2
	
	; if (currentSectorNumber < sectorsPerCluster) return true;
	li r3, :disk_sectorsPerCluster
	lih r3, ::disk_sectorsPerCluster
	in r3, (r3)
	cmp r2, r3
	li r0, 0xFF
	lih r0, 0xFF
	li r3, :disk_readNextFileSector_return
	lih r3, ::disk_readNextFileSector_return
	jpnc [r3]
	; currentSectorNumber = 0;
	li r2, 0
	out r2, (r1)
	; register int fatSectorNumber = currentClusterNumber / 256 --> r1
	li r0, :disk_currentClusterNumber
	lih r0, ::disk_currentClusterNumber
	in r1, (r0)
	shri r1, 8
	; register int fatOffset = 2*currentClusterNumber % 512 --> r2
	in r2, (r0)
	andi r2, 0xFF
	; readData(startPartition + fatPosition + fatSectorNumber);
	push r2	; save fatOffset
	li r0, :disk_startPartition_LSB
	lih r0, ::disk_startPartition_LSB
	in r3, (r0)
	li r0, :disk_startPartition_MSB
	lih r0, ::disk_startPartition_MSB
	in r4, (r0)
	li r0, :disk_fatPosition
	lih r0, ::disk_fatPosition
	in r5, (r0)
	add r3, r5
	addci r4, 0
	add r3, r1
	addci r4, 0
	
	; debug
	; push r4
	; li r4, 0xF0
	; push r4
	; li r4, 0
	; push r4
	; li r4, :print_hex
	; lih r4, ::print_hex
	; call [r4]
	; addi sp, 2
	; pop r4
	; debug
	; push r3
	; li r3, 0xF0
	; push r3
	; li r3, 0
	; push r3
	; li r3, :print_hex
	; lih r3, ::print_hex
	; call [r3]
	; addi sp, 2
	; pop r3
	
        push r4
        push r3
        li r0, :disk_sector
        lih r0, ::disk_sector
        push r0
        li r0, 1
        push r0
        li r0, :sd_readData
        lih r0, ::sd_readData
        call [r0]
        addi sp, 4
	pop r2	; restore fatOffset
        ; currentClusterNumber = read16(fatOffset);
        li r0, :disk_sector
        lih r0, ::disk_sector
        add r0, r2
        in r1, (r0)
        li r0, :disk_currentClusterNumber
        lih r0, ::disk_currentClusterNumber
        out r1, (r0)
        
        ; debug
	; push r1
	; li r1, 0xF0
	; push r1
	; li r1, 0
	; push r1
	; li r1, :print_hex
	; lih r1, ::print_hex
	; call [r1]
	; addi sp, 2
	; pop r1
        
        ; return true;
	li r0, 0xFF
	lih r0, 0xFF
:disk_readNextFileSector_return
	pop bp
	ret
	
:s_splash_screen
.data	3, 4, 5, 32, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 32, 3, 4, 5,10
.data	6, 7, 11, 32, '*', '*', '*', 32, 32, 32, 32, 'M', 'o', 'n', 'c', 'k', 'y', '-', '3', 32, 'C', 'o', 'm', 'p', 'u', 't', 'e', 'r', 32, 32, 32, 32, '*', '*', '*', 32, 6, 7, 11,10
.data	12, 14, 15, 32, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 32, 12, 14, 15,10
.data	32, 32, 32, 32, 32, 32, 32, 32, 40, 'c', 41, '2', '0', '2', '1', 32, 'K', 'r', 'i', 's', 32, 'D', 'e', 'm', 'u', 'y', 'n', 'c', 'k', 10, 10
:s_ready
.data	'R', 'e', 'a', 'd', 'y', '.'
:s_newline
.data	10, 0
:s_searching
.data	'S', 'e', 'a', 'r', 'c', 'h', 'i', 'n', 'g', 32, 'f', 'o', 'r', 32, 'S', 'D', 32, 'c', 'a', 'r', 'd', '.', '.', '.', 10, 0
:s_sd_sd1
.data	'S', 'D', '1', 0
:s_sd_sd2
.data	'S', 'D', '2', 0
:s_sd_sdhc
.data	'S', 'D', 'H', 'C', 0
:s_sd_ready
.data	32, 'c', 'a', 'r', 'd', 32, 'f', 'o', 'u', 'n', 'd', 32, 'a', 'n', 'd', 32, 'i', 'n', 'i', 't', 'i', 'a', 'l', 'i', 's', 'e', 'd', 10, 0
:s_reading_data
.data	'R', 'e', 'a', 'd', 'i', 'n', 'g', 32, 'f', 'r', 'o', 'm', 32, 'c', 'a', 'r', 'd', '.', '.', '.', 10, 0
:s_monckyos
.data 'M', 'O', 'N', 'C', 'K', 'Y', 'O', 'S', 32, 32, 32, 0

; ***** start_system()
; * initializes the system and jumps to main()
; *****
:start_system
	; initialize stack
	li sp, :startOfStack
	lih sp, ::startOfStack
	; set graphics to buffer 0
	li r0, :screenConfig
	lih r0, ::screenConfig
	li r1, 0
	out r1, (r0)
	; set cursor to (0,0)
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	li r1, 0
	out r1, (r0)
	; initialize keyboard
	li r0, :shiftPressed
	lih r0, ::shiftPressed
	li r1, 0
	out r1, (r0)
	; erase keyboard buffer
:erase_keyboard_buffer_loop
	li r0, :keyboardLastKey
	lih r0, ::keyboardLastKey
	in r0, (r0)
	nop r0, r0
	li r0, :erase_keyboard_buffer_loop
	lih r0, ::erase_keyboard_buffer_loop
	jpnz [r0]
	; initialize SPI
	li r0, :spiControl
	lih r0, ::spiControl
	li r1, 0xF0
	out r1, (r0)
	di
	
:main
	; set background colour
	li r0, :bgCol
	lih r0, ::bgCol
	li r1, 0x05
	out r1, (r0)
	; clrscr(0)
	li r1, 0
	push r1
	li r1, :clrscr
	lih r1, ::clrscr
	call [r1]
	addi sp, 1
	
	; setCursor(0,0)
	li r0, 0
	lih r0, 0
	li r1, :cursorPosition
	lih r1, ::cursorPosition
	out r0, (r1)
	
	; print(s_splash_screen)
	li r0, :s_splash_screen
	lih r0, ::s_splash_screen
	push r0
	li r0, 0xFF
	push r0
	li r0, 0
	push r0
	li r0, :print_string
	lih r0, ::print_string
	call [r0]
	addi sp, 3
	
	; sleepMillis(1000)
	li r0, 0xE8
	lih r0, 0x03
	push r0
	li r0, :sleepMillis
	lih r0, ::sleepMillis
	call [r0]
	addi sp, 1
	
	; print(s_searching)
	li r0, :s_searching
	lih r0, ::s_searching
	push r0
	li r0, 0xFF
	push r0
	li r0, 0
	push r0
	li r0, :print_string
	lih r0, ::print_string
	call [r0]
	addi sp, 3
	
	; sd_init()
	li r0, :sd_init
	lih r0, ::sd_init
	call [r0]
	
	; print(SD_cardType)
	li r0, :SD_cardType
	lih r0, ::SD_cardType
	in r0, (r0)
	li r1, :s_sd_sd1
	lih r1, ::s_sd_sd1
	cmpi r0, 1
	sz r2
	and r1, r2
	li r3, :s_sd_sd2
	lih r3, ::s_sd_sd2
	cmpi r0, 2
	sz r2
	and r3, r2
	li r4, :s_sd_sdhc
	lih r4, ::s_sd_sdhc
	cmpi r0, 3
	sz r2
	and r4, r2
	or r1, r3
	or r1, r4
	push r1
	li r0, 0xFF
	push r0
	li r0, 0
	push r0
	li r0, :print_string
	lih r0, ::print_string
	call [r0]
	addi sp, 3
	
	; print(s_sd_ready)
	li r0, :s_sd_ready
	lih r0, ::s_sd_ready
	push r0
	li r0, 0xFF
	push r0
	li r0, 0
	push r0
	li r0, :print_string
	lih r0, ::print_string
	call [r0]
	addi sp, 3
	
	; print(s_reading_data)
	li r0, :s_reading_data
	lih r0, ::s_reading_data
	push r0
	li r0, 0xFF
	push r0
	li r0, 0
	push r0
	li r0, :print_string
	lih r0, ::print_string
	call [r0]
	addi sp, 3
	
	; fat_findAndCheckPartition()
	li r0, :fat_findAndCheckPartition
	lih r0, ::fat_findAndCheckPartition
	call [r0]
	
	; fat_findAndCheckFAT()
	li r0, :fat_findAndCheckFAT
	lih r0, ::fat_findAndCheckFAT
	call [r0]
	
	; openFile("MONCKYOS   ")
	li r0, :s_monckyos
	lih r0, ::s_monckyos
	push r0
	li r0, :disk_openFile
	lih r0, ::disk_openFile
	call [r0]
	addi sp, 1
	
	; if (fileStart == 0) error(FAT_ERROR_OS_NOT_FOUND)
	li r0, :disk_fileStart
	lih r0, ::disk_fileStart
	in r0, (r0)
	nop r0, r0
	li r0, :read_file
	lih r0, ::read_file
	jpnz [r0]
	li r0, :FAT_ERROR_OS_NOT_FOUND
	push r0
	li r0, :sd_error
	lih r0, ::sd_error
	call [r0]
	addi sp, 1
	
	; read file in memory from address 0x0000
:read_file
	; do { success = readNextFileSector();} while (success)
	li r1, 0x00
:while_loop
	push r1 ; save r1
	push r1
	li r0, :disk_readNextFileSector
	lih r0, ::disk_readNextFileSector
	call [r0]
	addi sp, 1
	pop r1 ; restore r1
	li r2, 0x00
	lih r2, 0x01
	add r1, r2
	nop r0, r0
	li r0, :while_loop
	lih r0, ::while_loop
	jpnz [r0]
	
	; boot the MonckyOS
	li r0, :bootMonckyOS
	lih r0, ::bootMonckyOS
	jp [r0]
	
	halt
