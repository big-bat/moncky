; *****
; * This file contains code to demo the Moncky-3 keyboard library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * global variables
; *****
:s_splash_screen
.data	3, 4, 5, 32, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 32, 3, 4, 5,10
.data	6, 7, 11, 32, '*', '*', '*', 32, 32, 32, 32, 'M', 'o', 'n', 'c', 'k', 'y', '-', '3', 32, 'C', 'o', 'm', 'p', 'u', 't', 'e', 'r', 32, 32, 32, 32, '*', '*', '*', 32, 6, 7, 11,10
.data	12, 14, 15, 32, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 32, 12, 14, 15,10
.data	32, 32, 32, 32, 32, 32, 32, 32, 40, 'c', 41, '2', '0', '2', '1', 32, 'K', 'r', 'i', 's', 32, 'D', 'e', 'm', 'u', 'y', 'n', 'c', 'k', 10, 10
:s_ready
.data	'R', 'e', 'a', 'd', 'y', '.'
:s_newline
.data	10, 0

:start_system
	li r0, :graphics_init
	lih r0, ::graphics_init
	call [r0]

	li r0, :text_init
	lih r0, ::text_init
	call [r0]
	
	li r0, :keyboard_init
	lih r0, ::keyboard_init
	call [r0]

	li r0, 0x05
	push r0
	li r0, :graphics_setBgCol
	lih r0, ::graphics_setBgCol
	call [r0]
	addi sp, 1

	li r0, :graphics_clrscr
	lih r0, ::graphics_clrscr
	call [r0]

	li r0, 0xFF
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1

	li r0, :s_splash_screen
	lih r0, ::s_splash_screen
	push r0
	li r0, :text_printString16
	lih r0, ::text_printString16
	call [r0]
	addi sp, 1

:loop
	; draw_cursor()
	li r0, :text_drawCursor
	lih r0, ::text_drawCursor
	call [r0]

	; keyboard_waitChar(0)
	li r0, 0
	push r0
	li r0, :keyboard_waitChar
	lih r0, ::keyboard_waitChar
	call [r0]
	addi sp, 1

	push r0
	; erase_cursor()
	li r1, :text_eraseCursor
	lih r1, ::text_eraseCursor
	call [r1]
	pop r0

	; if (r0 != 0) print_char(r0)
	nop r0, r0
	li r1, :loop
	lih r1, ::loop
	jpz [r1]
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1

	li r0, :loop
	lih r0, ::loop
	jp [r0]

