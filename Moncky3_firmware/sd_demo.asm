; *****
; * This file contains code to demo the Moncky-3 sd library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * firmware variables (I/O)
; *****
.def :fat_sector 0xFD00

; *****
; * constants
; *****
:s_press_key
.data "Insert SD card and press any key to initialise it."
:s_found
.data "Found a "
:s_card
.data " card."
:s_reading
.data "Reading sector 0..."
:s_reading2
.data "Reading sector 2180..."

:start_system
	li r0, :graphics_init
	lih r0, ::graphics_init
	call [r0]
	
	li r0, :graphics_clrscr
	lih r0, ::graphics_clrscr
	call [r0]

	li r0, :text_init
	lih r0, ::text_init
	call [r0]

	li r0, :keyboard_init
	lih r0, ::keyboard_init
	call [r0]
	
	li r0, :spi_init
	lih r0, ::spi_init
	call [r0]
	
	li r0, :s_press_key
	lih r0, ::s_press_key
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	
	li r0, 0
	push r0
	li r0, :keyboard_waitKey
	lih r0, ::keyboard_waitKey
	call [r0]
	addi sp, 1
	
	li r0, :sd_init
	lih r0, ::sd_init
	call [r0]
	
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	pop r0
	
	li r1, :sd_result_strings
	lih r1, ::sd_result_strings
	add r1, r0
	ld r0, (r1)
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	
	li r0, :s_found
	lih r0, ::s_found
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, :sd_type
	lih r0, ::sd_type
	in r0, (r0)
	andi r0, 3
	li r1, :sd_type_strings
	lih r1, ::sd_type_strings
	add r1, r0
	ld r0, (r1)
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, :s_card
	lih r0, ::s_card
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1

	li r0, :s_reading
	lih r0, ::s_reading
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1

	li r0, 0
	push r0
	push r0
	li r0, :fat_sector
	lih r0, ::fat_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_read
	lih r0, ::sd_read
	call [r0]
	addi sp, 4
	li r1, :sd_result_strings
	lih r1, ::sd_result_strings
	add r1, r0
	ld r0, (r1)
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1

	li r0, :fat_sector
	lih r0, ::fat_sector
	li r1, 0xFF
	add r0, r1
	in r0, (r0)
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	
	li r0, :s_reading2
	lih r0, ::s_reading2
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1

	li r0, 0
	push r0
	li r0, 0x84
	lih r0, 0x08
	push r0
	li r0, :fat_sector
	lih r0, ::fat_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_read
	lih r0, ::sd_read
	call [r0]
	addi sp, 4
	li r1, :sd_result_strings
	lih r1, ::sd_result_strings
	add r1, r0
	ld r0, (r1)
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1

	li r1, 0 ; counter
:loop
	li r2, 0xFF
	cmp r1, r2
	li r2, :stop
	lih r2, ::stop
	jpns [r2]
	
	li r0, :fat_sector
	lih r0, ::fat_sector
	add r0, r1
	in r0, (r0)
	
	push r1 ; save counter
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1
	pop r1 ; restore counter
	
	inc r1
	li r0, :loop
	lih r0, ::loop
	jp [r0]
:stop
	halt
