; *****
; * This file contains the code of the Moncky-3 utilities library.
; * depends on: graphics.asm, text.asm
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * hardware variables (I/O)
; *****
.def	:util_usecLSB 0x7E02
.def	:util_usecMSB 0x7E03
.def	:util_msecLSB 0x7E04
.def	:util_msecMSB 0x7E05

; *****
; * global variables
; *****

:util_log_col ; to save the draw colour
.data 0xFF

; ***** void util_log(message, numberOfParameters)
; * this is a special function. It does not follow the normal calling convention.
; * The parameters are passed as r1 and r2 respectively.
; * The function is called right after the ‘push bp, set bp, sp’ sequence.
; * It will display the current time (milliseconds), a message (8 bits per char) and all parameters on screen.
; * A programmer can use this to debug software
; *****
:util_log
	push r2
	push r1
	; save draw colour
	li r1, :graphics_drawCol
	lih r1, ::graphics_drawCol
	in r2, (r1)
	li r1, :util_log_col
	lih r1, ::util_log_col
	st r2, (r1)
	li r1, :graphics_drawCol
	lih r1, ::graphics_drawCol
	li r2, 0xFC
	out r2, (r1)
	; print msec
	li r1, :util_msecLSB
	lih r1, ::util_msecLSB
	in r1, (r1)
	push r1
	li r1, :text_printHex
	lih r1, ::text_printHex
	call [r1]
	addi sp, 1
	; restore r1 (message)
	pop r1
	; print message
	push r1
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	; restore r2 (numberOfParameters)
	pop r2
	; print parameters
:util_log_loop
	cmpi r2, 0
	li r0, :util_log_end
	lih r0, ::util_log_end
	jpz [r0]
	push r2
	addi r2, 2
	lda r1, (bp+r2)
	push r1
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	li r0, 0x20 ; space char
	sti r0, (sp+1)
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	pop r2
	dec r2
	li r0, :util_log_loop
	lih r0, ::util_log_loop
	jp [r0]
:util_log_end
	; print new line
	li r0, 10
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	; restore draw colour
	li r1, :util_log_col
	lih r1, ::util_log_col
	ld r2, (r1)
	li r1, :graphics_drawCol
	lih r1, ::graphics_drawCol
	out r2, (r1)
	ret

; ***** void util_memcopy(uint16* source, uint16* destination, uint16 length)
; * this function copies a given amount of 16-bit numbers from source to destination
; * the data is copied from lowest to highest address
; *****
:util_memcopy
	ldi r0, (sp+4) ; source
	ldi r1, (sp+3) ; destination
	ldi r2, (sp+2) ; length
	li r3, :util_memcopy_loop
	lih r3, ::util_memcopy_loop
:util_memcopy_loop
	ld r4, (r0)
	st r4, (r1)
	inc r0
	inc r1
	dec r2
	jpnz r3
	ret

; ***** void util_memclear(uint16* destination, uint16 length, uint16 value)
; * this function fills a given amount of 16-bit numbers with a given value 
; * the data is copied from highest to lowest address
; *****
:util_memclear
	ldi r0, (sp+4) ; destination
	ldi r1, (sp+3) ; length
	ldi r2, (sp+2) ; value
	li r3, :util_memclear_loop
	lih r3, ::util_memclear_loop
:util_memclear_loop
	st r2, (r0)
	inc r0
	dec r1
	jpnz r3
	ret

; ***** void util_sleepMillis(uint16 time)
; * this function will wait for the specified amount of milliseconds.
; * This is a blocking wait.
; *****
:util_sleepMillis
	li r0, :util_msecLSB
	lih r0, ::util_msecLSB
	in r1, (r0)
	ldi r3, (sp+2) ; time
	li r2, :util_sleepMillis_loop
	lih r2, ::util_sleepMillis_loop
:util_sleepMillis_loop
	in r4, (r0)
	sub r4, r1
	cmp r4, r3
	jpnc [r2]
	ret

; ***** void util_getMillis(): uint32
; * this function returns the number of milli seconds since startup
; *****
:util_getMillis
	li r2, :util_msecLSB
	lih r2, ::util_msecLSB
	in r0, (r2)
	li r2, :util_msecMSB
	lih r2, ::util_msecMSB
	in r1, (r2)
	ret

; ***** void util_getMicros(): uint32
; * this function returns the number of micro seconds since startup
; *****
:util_getMicros
	li r2, :util_usecLSB
	lih r2, ::util_usecLSB
	in r0, (r2)
	li r2, :util_usecMSB
	lih r2, ::util_usecMSB
	in r1, (r2)
	ret

; ***** util_error(uint16 code)
; * this function will display an error on the screen, with the given code in hexadecimal
; *****
:util_s_error
.data "Error encountered: "
:util_error
	; save draw colour
	li r1, :graphics_drawCol
	lih r1, ::graphics_drawCol
	in r2, (r1)
	li r1, :util_log_col
	lih r1, ::util_log_col
	st r2, (r1)
	li r1, :graphics_drawCol
	lih r1, ::graphics_drawCol
	li r2, 0xE0
	out r2, (r1)
	; print error
	li r0, :util_s_error
	lih r0, ::util_s_error
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	ldi r0, (sp+3) ; code
	sti r0, (sp+1)
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1
	; restore draw colour
	li r1, :util_log_col
	lih r1, ::util_log_col
	ld r2, (r1)
	li r1, :graphics_drawCol
	lih r1, ::graphics_drawCol
	out r2, (r1)
	ret

; ***** util_enableInterrupts()
; * this function will enable the interrupts
; *****
:util_enableInterrupts
	ei
	ret

; ***** util_disableInterrupts()
; * this function will disable the interrupts
; *****
:util_disableInterrupts
	di
	ret

