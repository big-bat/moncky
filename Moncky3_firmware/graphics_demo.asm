; *****
; * This file contains code to demo the Moncky-3 graphics library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * global variables
; *****
.def :x0	0x4000
.def :y0	0x4001
.def :x1	0x4002
.def :y1	0x4003
.def :dx0	0x4004
.def :dy0	0x4005
.def :dx1	0x4006
.def :dy1	0x4007
.def :col	0x4008

.def :sprite_x 0x4009
.def :sprite_y 0x400A
.def :sprite_buffer 0x400B

:sprite
.data 0x0000, 0x0000, 0x0000, 0xE000, 0x00E0, 0x0000, 0x0000, 0x0000
.data 0x0000, 0x0000, 0x0000, 0x00E0, 0xE000, 0x0000, 0x0000, 0x0000
.data 0x0000, 0x0000, 0xE000, 0x0000, 0x0000, 0x00E0, 0x0000, 0x0000
.data 0x0000, 0x0000, 0x00E0, 0x0000, 0x0000, 0x0E00, 0x0000, 0x0000
.data 0x0000, 0xE000, 0x0000, 0x0000, 0x0000, 0x0000, 0x00E0, 0x0000
.data 0x0000, 0x00E0, 0x0000, 0x0000, 0x0000, 0x0000, 0xE000, 0x0000
.data 0xE000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x00E0
.data 0x00E0, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xE000
.data 0x001C, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x1C00
.data 0x1C00, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x001C
.data 0x0000, 0x001C, 0x0000, 0x0000, 0x0000, 0x0000, 0x1C00, 0x0000
.data 0x0000, 0x1C00, 0x0000, 0x0000, 0x0000, 0x0000, 0x001C, 0x0000
.data 0x0000, 0x0000, 0x001C, 0x0000, 0x0000, 0x1C00, 0x0000, 0x0000
.data 0x0000, 0x0000, 0x1C00, 0x0000, 0x0000, 0x001C, 0x0000, 0x0000
.data 0x0000, 0x0000, 0x0000, 0x001C, 0x1C00, 0x0000, 0x0000, 0x0000
.data 0x0000, 0x0000, 0x0000, 0x1C00, 0x001C, 0x0000, 0x0000, 0x0000

:start_system
	li r0, :graphics_init
	lih r0, ::graphics_init
	call [r0]

	li r0, 0x00
	push r0
	li r0, :graphics_setBgCol
	lih r0, ::graphics_setBgCol
	call [r0]
	addi sp, 1

	li r0, 10
	li r1, :x0
	lih r1, ::x0
	st r0, (r1)
	li r0, 10
	li r1, :y0
	lih r1, ::y0
	st r0, (r1)
	li r0, 100
	li r1, :x1
	lih r1, ::x1
	st r0, (r1)
	li r0, 50
	li r1, :y1
	lih r1, ::y1
	st r0, (r1)
	li r0, 2
	li r1, :dx0
	lih r1, ::dx0
	st r0, (r1)
	li r0, 3
	neg r0, r0
	li r1, :dy0
	lih r1, ::dy0
	st r0, (r1)
	li r0, 4
	li r1, :dx1
	lih r1, ::dx1
	st r0, (r1)
	li r0, 1
	li r1, :dy1
	lih r1, ::dy1
	st r0, (r1)
	li r0, 0
	li r1, :col
	lih r1, ::col
	st r0, (r1)
	
	li r0, 0
	li r1, :sprite_x
	lih r1, ::sprite_x
	st r0, (r1)
	li r0, 0
	li r1, :sprite_y
	lih r1, ::sprite_y
	st r0, (r1)
:loop
	ei
	li r0, :graphics_waitVertRetrace
	lih r0, ::graphics_waitVertRetrace
	call [r0]
	di

	; switch screen buffers
	li r0, :graphics_getDrawBuffer
	lih r0, ::graphics_getDrawBuffer
	call [r0]
	push r0
	li r0, :graphics_setScreenBuffer
	lih r0, ::graphics_setScreenBuffer
	call [r0]
	pop r0
	li r1, 1
	sub r1, r0
	push r1
	li r0, :graphics_setDrawBuffer
	lih r0, ::graphics_setDrawBuffer
	call [r0]
	addi sp, 1

	li r0, :graphics_clrscr
	lih r0, ::graphics_clrscr
	call [r0]

	li r0, :col
	lih r0, ::col
	ld r0, (r0)
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1

	li r0, :x0
	lih r0, ::x0
	ld r0, (r0)
	push r0
	li r0, :y0
	lih r0, ::y0
	ld r0, (r0)
	push r0
	li r0, :x1
	lih r0, ::x1
	ld r0, (r0)
	push r0
	li r0, :y1
	lih r0, ::y1
	ld r0, (r0)
	push r0
	li r0, :graphics_line
	lih r0, ::graphics_line
	call [r0]
	addi sp, 4

	; update col
	li r0, :col
	lih r0, ::col
	ld r1, (r0)
	inc r1
	andi r1, 0xFF
	st r1, (r0)

	; update x0 and dx0
	li r0, :x0
	lih r0, ::x0
	ld r1, (r0)
	li r2, :dx0
	lih r2, ::dx0
	ld r3, (r2)
	add r1, r3
	sns r4
	and r1, r4
	set r5, r3
	and r5, r4
	not r4, r4
	neg r6, r3
	and r4, r6
	or r4, r5
	set r3, r4
	li r4, 0x3F ; 319
	lih r4, 0x01
	cmp r4, r1
	ss r5
	and r4, r5
	set r6, r3
	neg r6, r6
	and r6, r5
	not r5, r5
	and r1, r5
	or r1, r4
	and r3, r5
	or r3, r6
	st r1, (r0)
	st r3, (r2)

	; update y0 and dy0
	li r0, :y0
	lih r0, ::y0
	ld r1, (r0)
	li r2, :dy0
	lih r2, ::dy0
	ld r3, (r2)
	add r1, r3
	sns r4
	and r1, r4
	set r5, r3
	and r5, r4
	not r4, r4
	neg r6, r3
	and r4, r6
	or r4, r5
	set r3, r4
	li r4, 199 ; 199
	cmp r4, r1
	ss r5
	and r4, r5
	set r6, r3
	neg r6, r6
	and r6, r5
	not r5, r5
	and r1, r5
	or r1, r4
	and r3, r5
	or r3, r6
	st r1, (r0)
	st r3, (r2)

	; update x1 and dx1
	li r0, :x1
	lih r0, ::x1
	ld r1, (r0)
	li r2, :dx1
	lih r2, ::dx1
	ld r3, (r2)
	add r1, r3
	sns r4
	and r1, r4
	set r5, r3
	and r5, r4
	not r4, r4
	neg r6, r3
	and r4, r6
	or r4, r5
	set r3, r4
	li r4, 0x3F ; 319
	lih r4, 0x01
	cmp r4, r1
	ss r5
	and r4, r5
	set r6, r3
	neg r6, r6
	and r6, r5
	not r5, r5
	and r1, r5
	or r1, r4
	and r3, r5
	or r3, r6
	st r1, (r0)
	st r3, (r2)

	; update y1 and dy1
	li r0, :y1
	lih r0, ::y1
	ld r1, (r0)
	li r2, :dy1
	lih r2, ::dy1
	ld r3, (r2)
	add r1, r3
	sns r4
	and r1, r4
	set r5, r3
	and r5, r4
	not r4, r4
	neg r6, r3
	and r4, r6
	or r4, r5
	set r3, r4
	li r4, 199 ; 199
	cmp r4, r1
	ss r5
	and r4, r5
	set r6, r3
	neg r6, r6
	and r6, r5
	not r5, r5
	and r1, r5
	or r1, r4
	and r3, r5
	or r3, r6
	st r1, (r0)
	st r3, (r2)
	
	; draw sprite
	li r0, :sprite
	lih r0, ::sprite
	push r0
	li r0, :sprite_buffer
	lih r0, ::sprite_buffer
	push r0
	li r0, :sprite_x
	lih r0, ::sprite_x
	ld r0, (r0)
	push r0
	li r0, :sprite_y
	lih r0, ::sprite_y
	ld r0, (r0)
	push r0
	li r0, :graphics_drawSprite
	lih r0, ::graphics_drawSprite
	call [r0]
	addi sp, 4
	
	; update sprite_x
	li r0, :sprite_x
	lih r0, ::sprite_x
	ld r1, (r0)
	inc r1
	li r2, 0x2F ; 303
	lih r2, 0x01
	cmp r2, r1
	sns r3
	and r1, r3
	st r1, (r0)	

	; update sprite_y
	li r0, :sprite_y
	lih r0, ::sprite_y
	ld r1, (r0)
	inc r1
	li r2, 0xB7 ; 183
	cmp r2, r1
	sns r3
	and r1, r3
	st r1, (r0)

	li r0, :loop
	lih r0, ::loop
	jp [r0]

	halt

