; *****
; * This file contains code to demo the Moncky-3 utilities library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; ***** uint16 add(uint16 a, uint16 b)
; * will add the 2 numbers and return the result
; *****
:test_log
.data " add "
:add
	push bp
	set bp, sp
	
	; display log message
	li r1, :test_log
	lih r1, ::test_log
	li r2, 2
	li r0, :util_log
	lih r0, ::util_log
	call [r0]
	
	ldi r0, (bp+3)
	ldi r1, (bp+4)
	add r0, r1
	pop bp
	ret

:s_sum
.data "The Answer is... "
:start_system
	li r0, :graphics_init
	lih r0, ::graphics_init
	call [r0]

	li r0, :text_init
	lih r0, ::text_init
	call [r0]

	li r0, 0x05
	push r0
	li r0, :graphics_setBgCol
	lih r0, ::graphics_setBgCol
	call [r0]
	addi sp, 1
	
	li r0, :graphics_clrscr
	lih r0, ::graphics_clrscr
	call [r0]

	; add 0x3F and 3
	li r0, 0x3F
	push r0
	li r0, 3
	push r0
	li r0, :add
	lih r0, ::add
	call [r0]
	addi sp, 2
	
	; print sum
	push r0
	li r0, :s_sum
	lih r0, ::s_sum
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	
	; wait 5 seconds
	li r0, 0x88
	lih r0, 0x13
	push r0
	li r0, :util_sleepMillis
	lih r0, ::util_sleepMillis
	call [r0]
	addi sp, 1
	
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1
	
	li r0, 0x0A
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	
	; wait 5 seconds
	li r0, 0x88
	lih r0, 0x13
	push r0
	li r0, :util_sleepMillis
	lih r0, ::util_sleepMillis
	call [r0]
	addi sp, 1

	li r0, 0x42
	lih r0, 0x42
	push r0
	li r0, :util_error
	lih r0, ::util_error
	call [r0]
	addi sp, 1
	
	halt
