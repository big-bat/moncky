; *****
; * This file contains the code of the Moncky-3 keyboard library.
; * depends on: utilities.asm, grapics.asm, text, asm
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * hardware variables (I/O)
; *****
.def	:keyboard_LastKey 0x7D01

; *****
; * firmware variables (I/O)
; *****
.def	:keyboard_shiftPressed 0x7D03

:keyboard_scancode_to_ascii
 	.data 0x00,0x0C,0x00,0x05,0x03,0x01,0x02,0x10
 	.data 0x00,0x00,0x0B,0x06,0x04,0x09,0x60,0x00
 	.data 0x00,0x00,0x00,0x00,0x00,0x71,0x31,0x00
 	.data 0x00,0x00,0x7A,0x73,0x61,0x77,0x32,0x00
 	.data 0x00,0x63,0x78,0x64,0x65,0x34,0x33,0x00
 	.data 0x00,0x20,0x76,0x66,0x74,0x72,0x35,0x00
 	.data 0x00,0x6E,0x62,0x68,0x67,0x79,0x36,0x00
 	.data 0x00,0x00,0x6D,0x6A,0x75,0x37,0x38,0x00
 	.data 0x00,0x2C,0x6B,0x69,0x6F,0x30,0x39,0x00
 	.data 0x00,0x2E,0x2F,0x6C,0x3B,0x70,0x2D,0x00
 	.data 0x00,0x00,0x27,0x00,0x5B,0x3D,0x00,0x00
 	.data 0x00,0x00,0x0A,0x5D,0x00,0x5C,0x00,0x00
 	.data 0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00
	.data 0x00,0x31,0x00,0x34,0x37,0x00,0x00,0x00
	.data 0x30,0x2E,0x32,0x35,0x36,0x38,0x1B,0x00
	.data 0x0F,0x2B,0x33,0x2D,0x2A,0x39,0x00,0x00
:keyboard_scancode_to_ascii_shifted
	.data 0x00,0x0C,0x00,0x05,0x03,0x01,0x02,0x10
	.data 0x00,0x00,0x0B,0x06,0x04,0x09,0x7E,0x00
	.data 0x00,0x00,0x00,0x00,0x00,0x51,0x21,0x00
	.data 0x00,0x00,0x5A,0x53,0x41,0x57,0x40,0x00
	.data 0x00,0x43,0x58,0x44,0x45,0x24,0x23,0x00
	.data 0x00,0x20,0x56,0x46,0x54,0x52,0x25,0x00
	.data 0x00,0x4E,0x42,0x48,0x47,0x59,0x5E,0x00
	.data 0x00,0x00,0x4D,0x4A,0x55,0x26,0x2A,0x00
	.data 0x00,0x3C,0x4B,0x49,0x4F,0x29,0x28,0x00
	.data 0x00,0x3E,0x3F,0x4C,0x3A,0x50,0x5F,0x00
	.data 0x00,0x00,0x22,0x00,0x7B,0x2B,0x00,0x00
	.data 0x00,0x00,0x0A,0x7D,0x00,0x7C,0x00,0x00
	.data 0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00
	.data 0x00,0x31,0x00,0x34,0x37,0x00,0x00,0x00
	.data 0x30,0x2E,0x32,0x35,0x36,0x38,0x1B,0x00
	.data 0x0F,0x2B,0x33,0x2D,0x2A,0x39,0x00,0x00
:keyboard_ext_scancode_to_ascii_keys
	.data 0x012F,0x014A,0x015A,0x169,0x016B,0x16C
	.data 0x0170,0x0171,0x172,0x0174,0x0175,0x17A
	.data 0x017D, 0x0083
:keyboard_ext_scancode_to_ascii_values
	.data 0x1C,0x2F,0x0A,0x1A,0x11,0x19,0x16,0x15
	.data 0x14,0x12,0x13,0x18,0x17,0x07

; ***** void keyboard_init()
; * sets shift key to 0 and clears the hardware buffer
; *****
:keyboard_init
	li r0, :keyboard_shiftPressed
	lih r0, ::keyboard_shiftPressed
	li r1, 0
	out r1, (r0)
:keyboard_init_loop
	li r0, :keyboard_LastKey
	lih r0, ::keyboard_LastKey
	in r0, (r0)
	nop r0, r0
	li r0, :keyboard_init_loop
	lih r0, ::keyboard_init_loop
	jpnz [r0]
	ret

; ***** uint16 keyboard_readKeyCode()
; * will return the key code of the next key in the buffer. If no key was pressed, zero is returned
; *****
:keyboard_readKeyCode
	li r0, :keyboard_LastKey
	lih r0, ::keyboard_LastKey
	in r0, (r0)
	ret

; ***** uint16 keyboard_waitKey(uint16 timeout)
; * will wait until a key is pressed or released and return the key code.
; * It will return 0 if a timeout occurs (time is in milliseconds).
; * If the timeout is 0 the computer will wait indefinitely.
; *****
:keyboard_waitKey
	li r0, :util_msecLSB
	lih r0, ::util_msecLSB
	in r1, (r0)
	li r2, :keyboard_waitKey_return
	lih r2, ::keyboard_waitKey_return
	li r3, :keyboard_waitKey_loop
	lih r3, ::keyboard_waitKey_loop
	li r6, :keyboard_LastKey
	lih r6, ::keyboard_LastKey
:keyboard_waitKey_loop
	in r0, (r6)
	or r0, r0
	jpnz [r2] ; valid key was detected
	ldi r5, (sp+2) ; timeout
	nop r5, r5
	jpz [r3] ; timeout was zero
	li r4, :util_msecLSB
	lih r4, ::util_msecLSB
	in r4, (r4)
	sub r4, r1
	subf r4, r5
	jpnc [r3]
	li r0, 0
:keyboard_waitKey_return
	ret

; ***** uint16 keyboard_toChar(uint16 keycode)
; * will return the ASCII code that corresponds with the given key code
; * It translates key codes to ASCII. If the shift key was pressed the corresponding value will be altered accordingly.
; * char c = 0
; * if (code == 0x12 || code == 0x59) shiftPressed = 1; return c
; * if (code == 0x212 || code == 0x259) shiftPressed = 0; return c
; * if (code < 0x80)
; *     c = scancode_to_ascii[code+shiftPressed<<7]; return c
; * for(int i in 13:0)
; *   if (ext_scancode_to_ascii_keys[i] == code)
; *     c = :ext_scancode_to_ascii_values[i]; return c
; *****
.alias $code r0
.alias $c r1
.alias $i r2
:keyboard_toChar
	ldi $code, (sp+2) ; keycode
	; check for shift press or release
	li r3 :keyboard_readChar_shiftPressedOrReleased
	lih r3, ::keyboard_readChar_shiftPressedOrReleased
	li $c, 1
	cmpi $code, 0x12
	jpz [r3]
	cmpi $code, 0x59
	jpz [r3]
	li $c, 0
	li r4, 0x12
	lih r4, 2
	cmp $code, r4
	jpz [r3]
	li r4, 0x59
	lih r4, 2
	cmp $code, r4
	jpz [r3]
	; check for extended key
	li r4, 0x80
	cmp $code, r4
	li r3, :keyboard_readChar_ext
	lih r3, ::keyboard_readChar_ext
	jpns [r3]
	; no extended key
	li r3, :keyboard_shiftPressed
	lih r3, ::keyboard_shiftPressed
	in r3, (r3)
	shli r3, 7
	add $code, r3
	li r3, :keyboard_scancode_to_ascii
	lih r3, ::keyboard_scancode_to_ascii
	lda $c, ($code+r3)
	li r3, :keyboard_readChar_return
	lih r3, ::keyboard_readChar_return
	jp [r3]
:keyboard_readChar_ext
	li $i, 13
:keyboard_readChar_loop
	nop $i, $i
	li r3, :keyboard_readChar_return
	lih r3, ::keyboard_readChar_return
	jps [r3]
	li r3, :keyboard_ext_scancode_to_ascii_keys
	lih r3, ::keyboard_ext_scancode_to_ascii_keys
	lda r3, (r3+$i)
	cmp $code, r3
	li r3, :keyboard_readChar_next
	lih r3, ::keyboard_readChar_next
	jpnz [r3]
	li r3, :keyboard_ext_scancode_to_ascii_values
	lih r3, ::keyboard_ext_scancode_to_ascii_values
	lda $c, (r3+$i)
	li r3, :keyboard_readChar_return
	lih r3, ::keyboard_readChar_return
	jp [r3]
:keyboard_readChar_next
	dec $i
	li r4, :keyboard_readChar_loop
	lih r4, ::keyboard_readChar_loop
	jp [r4]
:keyboard_readChar_shiftPressedOrReleased
	li r2, :keyboard_shiftPressed
	lih r2, ::keyboard_shiftPressed
	out $c, (r2)
	li $c, 0
:keyboard_readChar_return
	set r0, $c
	ret
.rmAliases


; ***** uint16 keyboard_readChar()
; * will return the ASCII code that corresponds with the key pressed on the keyboard.
; * this is readKeyCode() followed by toChar()
; *****
:keyboard_readChar
	li r0, :keyboard_readKeyCode
	lih r0, ::keyboard_readKeyCode
	call [r0]
	push r0
	li r0, :keyboard_toChar
	lih r0, ::keyboard_toChar
	call [r0]
	addi sp, 1
	ret

; ***** uint16 keyboard_waitChar(uint16 timeout)
; * wait until a key is pressed and return the corresponding ASCII code
; * returns 0 if timeout occurred
; *****
:keyboard_waitChar
	li r0, :util_msecLSB
	lih r0, ::util_msecLSB
	in r1, (r0)
	li r2, :keyboard_waitChar_return
	lih r2, ::keyboard_waitChar_return
	li r3, :keyboard_waitChar_loop
	lih r3, ::keyboard_waitChar_loop
	li r6, :keyboard_LastKey
	lih r6, ::keyboard_LastKey
:keyboard_waitChar_loop
	in r0, (r6)
	or r0, r0
	jpnz [r2] ; valid key was detected
	ldi r5, (sp+2) ; timeout
	nop r5, r5
	jpz [r3] ; timeout was zero
	li r4, :util_msecLSB
	lih r4, ::util_msecLSB
	in r4, (r4)
	sub r4, r1
	subf r4, r5
	jpnc [r3]
	li r0, 0
	ret
:keyboard_waitChar_return
	push r1
	push r2
	push r3
	push r6
	push r0
	li r0, :keyboard_toChar
	lih r0, ::keyboard_toChar
	call [r0]
	addi sp, 1
	pop r6
	pop r3
	pop r2
	pop r1
	nop r0, r0
	jpz [r3]
	ret
