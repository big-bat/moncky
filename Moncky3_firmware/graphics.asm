; *****
; * This file contains the code of the Moncky-3 graphics library.
; * depends on: system.asm
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * hardware variables (I/O)
; *****
.def :graphics_screenConfig 0x7D00 ; from system.asm

; *****
; * firmware variables (I/O)
; *****
.def :graphics_bgCol 0x7D04
.def :graphics_drawCol    0x7D26
.def :graphics_drawBuffer 0x7D27

; ***** void graphics_init()
; * initialises the graphics screen
; * screenBuffer = 0
; * bgCol = 0
; * drawCol = 0xFF
; * drawBuffer = 0
; *****
:graphics_init
	li r0, 0
	li r1, :graphics_screenConfig
	lih r1, ::graphics_screenConfig
	out r0, (r1)
	li r1, :graphics_bgCol
	lih r1, ::graphics_bgCol
	out r0, (r1)
	li r1, :graphics_drawBuffer
	lih r1, ::graphics_drawBuffer
	out r0, (r1)
	li r0, 0xFF
	li r1, :graphics_drawCol
	lih r1, ::graphics_drawCol
	out r0, (r1)
	ret

; ***** void graphics_setBgCol(uint8 col)
; * sets the bacground colour to the specified value
; *****
:graphics_setBgCol
	ldi r0, (sp+2) ; col
	andi r0, 0xFF ; clip col to 8 bits
	li r1, :graphics_bgCol
	lih r1, ::graphics_bgCol
	out r0, (r1)
	ret

; ***** void graphics_clrscr()
; * clears the current drawBuffer with the current background colour
; * 16+4*32000 = 128016 clock cycles (0.0128 = 1/78 sec) 
; *****
:graphics_clrscr
	li r0, :graphics_drawBuffer
	lih r0, ::graphics_drawBuffer
	in r0, (r0)
	andi r0, 1 ; r0 = drawBuffer
	shli r0, 15 ; r0 = start of buffer in VRAM
	li r1, 0xFF
	lih r1, 0x7C ; r1 = buffer length
	; prepare for loop
	li r2, :graphics_bgCol
	lih r2, ::graphics_bgCol
	in r2, (r2)
	andi r2, 0xFF ; r2 = background colour
	set r3, r2
	shli r3, 8
	or r2, r3 ; r2 = colour in both MSB and LSB
	li r3, :graphics_clrscr_loop
	lih r3, ::graphics_clrscr_loop
:graphics_clrscr_loop
	out r2, (r0)
	inc r0
	dec r1 ; decrease counter
	jpc [r3]
	ret

; ***** void graphics_setCol(uint8 col)
; * sets the current colour (used by plot and line)
; *****
:graphics_setCol
	ldi r0, (sp+2) ; col
	andi r0, 0xFF ; clip to 8 bits
	li r1, :graphics_drawCol
	lih r1, ::graphics_drawCol
	out r0, (r1)
	ret

; ***** void graphics_setScreenBuffer(uint1 buffer)
; * sets the current buffer to be displayed on the screen
; *****
:graphics_setScreenBuffer
	ldi r0, (sp+2) ; buffer
	andi r0, 1 ; clip buffer to 1 bit
	li r1, :graphics_screenConfig
	lih r1, ::graphics_screenConfig
	out r0, (r1)
	ret

; ***** void graphics_waitVertRetrace()
; * waits until the next vertical retrace happened
; *****
:graphics_waitVertRetrace
	li r0, :graphics_screenConfig
	lih r0, ::graphics_screenConfig
	in r1, (r0)
	andi r1, 2 ; select toggle bit
	li r3, :graphics_waitVertRetrace_loop
	lih r3, ::graphics_waitVertRetrace_loop
:graphics_waitVertRetrace_loop
	in r2, (r0)
	andi r2, 2 ; select toggle bit
	xor r2, r1 ; compare the bits
	jpz [r3]
	ret

; ***** void graphics_setDrawBuffer(uint1 buffer)
; * sets the current buffer to draw on (used by plot and line)
; *****
:graphics_setDrawBuffer
	ldi r0, (sp+2) ; buffer
	andi r0, 1 ; clip to 1 bit
	li r1, :graphics_drawBuffer
	lih r1, ::graphics_drawBuffer
	out r0, (r1)
	ret

; ***** uint1 graphics_getDrawBuffer()
; * gets the current buffer to draw on
; *****
:graphics_getDrawBuffer
	li r0, :graphics_drawBuffer
	lih r0, ::graphics_drawBuffer
	in r0, (r0)
	andi r0, 1 ; clip to 1 bit
	ret

; ***** void graphics_plot(uint16 x, uint16 y)
; * plots a point on the given coordinates with the current colour on the current buffer
; * all registers are saved
; *****
:graphics_plot
	push r0 ; save registers
	push r1
	push r2
	push r3
	ldi r0, (sp+7) ; x
	ldi r1, (sp+6) ; y
	; clipping
	li r2, :plot_return
	lih r2, ::plot_return
	li r3, 0x40 ; li r3, 320
	lih r3, 0x01
	cmp r0, r3
	jpc [r2]
	li r3, 200
	cmp r1, r3
	jpc [r2]
	; find memory location (r1 = (320*y+x)/2+buffer<<15)
	set r2, r1
	shli r1, 6
	shli r2, 8
	add r1, r2
	add r1, r0
	shri r1, 1
	li r3, :graphics_drawBuffer
	lih r3, ::graphics_drawBuffer
	in r2, (r3)
	shli r2, 15
	add r1, r2
	; set right byte to colour
	andi r0, 1
	shli r0, 3
	li r3, :graphics_drawCol
	lih r3, ::graphics_drawCol
	in r2, (r3)
	li r3, 0xFF ; mask: 8 bits per pixel
	and r2, r3
	shl r2, r0
	shl r3, r0
	not r3, r3
	in r0, (r1)
	and r0, r3
	or r0, r2
	out r0, (r1)
:plot_return
	pop r3 ; restore registers
	pop r2
	pop r1
	pop r0
	ret

; ***** void graphics_line(uint16 x0, uint16 y0, uint16 x1, uint16 y1)
; * draws a line from (x0, y0) to (x1, y1) in the current colour on the current screen buffer
; * uses Bresenham's algorithm:
; * dx = abs(x1-x0)
; * sx = sign(x1-x0)
; * dy = -abs(y1-y0)
; * sy = sign(y1-y0)
; * err = dx + dy
; * while (x0 != x1 || y0 != y1) {
; *   plot(x0, y0)
; *   e2 = 2 * err
; *   if (e2 > dy) {
; *     err += dy;
; *     x0 += sx;
; *   }
; *   if (e2 <= dx) {
; *     err += dx;
; *     y0 += sy;
; *   }
; * }
; * plot(x0, y0)
; *
; * uses r0, r1 (x0), r2 (y0), r3 (x1), r4 (y1), r5 (:plot), r6 (dx), r7 (sx), r8 (dy), r9 (sy), r10 (err), r11 (e2)
; *****
.alias $x0 r1
.alias $y0 r2
.alias $x1 r3
.alias $y1 r4
.alias $dx r6
.alias $sx r7
.alias $dy r8
.alias $sy r9
.alias $err r10
.alias $e2 r11
:graphics_line
	ldi $x0, (sp+5)
	ldi $y0, (sp+4)
	ldi $x1, (sp+3)
	ldi $y1, (sp+2)
	; dx and sx
	set r0, $x1
	sub r0, $x0
	set $dx, r0
	set $sx, r0
	ashri $sx, 15
	xor $dx, $sx
	sub $dx, $sx
	nop r0, r0
	sns r0
	snz r5
	and r0, r5
	andi r0, 1
	add $sx, r0
	; dy and sy
	set r0, $y1
	sub r0, $y0
	set $dy, r0
	set $sy, r0
	ashri $sy, 15
	xor $dy, $sy
	sub $dy, $sy
	neg $dy, $dy
	nop r0, r0
	sns r0
	snz r5
	and r0, r5
	andi r0, 1
	add $sy, r0
	; err = dx + dy
	set $err, $dx
	add $err, $dy
	; prepare stack for graphics_plot(x,y)
	push $x0 ; x
	push $y0 ; y
	li r5, :graphics_plot
	lih r5, ::graphics_plot
:graphics_line_loop
	cmp $x0, $x1
	sz r0
	cmp $y0, $y1
	sz $e2
	and r0, $e2
	li r0, :graphics_line_end
	lih r0, ::graphics_line_end
	jpnz r0
	sti $x0, (sp+2)
	sti $y0, (sp+1)
	call [r5]
	set $e2, $err
	shli $e2, 1
	cmp $e2, $dy
	li r0, :graphics_line_else1
	lih r0, ::graphics_line_else1
	jps r0
	add $err, $dy
	add $x0, $sx
:graphics_line_else1
	cmp $dx, $e2
	li r0, :graphics_line_else2
	lih r0, ::graphics_line_else2
	jps r0
	add $err, $dx
	add $y0, $sy
:graphics_line_else2
	li r0, :graphics_line_loop
	lih r0, ::graphics_line_loop
	jp r0
:graphics_line_end
	sti $x0, (sp+2)
	sti $y0, (sp+1)
	call [r5]
	addi sp, 2
	ret
.rmAliases

; ***** void graphics_drawHorizontalEven(uin16 x, uint16 y, uint16 width)
; * draws a horizontal line from the given point to the right
; * both x and width are supposed to be even
; *****
:graphics_drawHorizontalEven
	ldi r0, (sp+4)	; x
	ldi r1, (sp+3)	; y
	ldi r2, (sp+2)	; width
	shri r0, 1
	set r3, r1	; y = y * 160
	shli r3, 2
	add r1, r3
	shli r1, 5
	add r1, r0	; y = y + x
	li r0, :graphics_drawBuffer
	lih r0, ::graphics_drawBuffer
	in r0, (r0)
	andi r0, 1	; r0 = drawBuffer
	shli r0, 15	; r0 = start of buffer in VRAM
	add r0, r1	; r0 = start address in VRAM
	shri r2, 1	; width /= 2
	li r3, :graphics_drawCol
	lih r3, ::graphics_drawCol
	in r1, (r3)
	andi r1, 0xFF	; mask: 8 bits per pixel
	set r3, r1
	shli r3, 8
	or r1, r3	; r1 now contains two pixel values
	li r3, :graphics_drawHorizontalEven_loop
	lih r3, ::graphics_drawHorizontalEven_loop
:graphics_drawHorizontalEven_loop
	out r1, (r0)
	inc r0
	dec r2
	jpnz [r3]
	ret
	

; ***** void graphics_drawHorizontal(uin16 x, uint16 y, uint16 width)
; * draws a horizontal line from the given point to the right
; * width is assumed to be at least 2
; *****
:graphics_drawHorizontal
	ldi r0, (sp+4) ; x
	ldi r1, (sp+3) ; y
	ldi r2, (sp+2) ; width
	andif r0, 1
	li r3, :graphics_drawHorizontal_xEven
	lih r3, ::graphics_drawHorizontal_xEven
	jpz [r3]
	push r0
	push r1
	li r3, :graphics_plot
	lih r3, ::graphics_plot
	call [r3]
	addi sp, 2
	inc r0
	dec r2
:graphics_drawHorizontal_xEven
	andif r2, 1
	li r3, :graphics_drawHorizontal_widthEven
	lih r3, ::graphics_drawHorizontal_widthEven
	jpz [r3]
	dec r2
	add r0, r2
	push r0
	push r1
	li r3, :graphics_plot
	lih r3, ::graphics_plot
	call [r3]
	addi sp, 2
	sub r0, r2
:graphics_drawHorizontal_widthEven
	push r0
	push r1
	push r2
	li r3, :graphics_drawHorizontalEven
	lih r3, ::graphics_drawHorizontalEven
	call [r3]
	addi sp, 3
	ret

; ***** void graphics_drawVertical(uin16 x, uint16 y, uint16 height)
; * draws a vertical line from the given point towards the bottom
; *****
:graphics_drawVertical
	ldi r0, (sp+4) ; x
	ldi r1, (sp+3) ; y
	ldi r2, (sp+2) ; height
	li r3, :graphics_drawVertical_loop
	lih r3, ::graphics_drawVertical_loop
	li r4, :graphics_plot
	lih r4, ::graphics_plot
	push r0
	push r1
:graphics_drawVertical_loop
	sti r1, (sp+1)
	call [r4]
	inc r1
	dec r2
	jpnz [r3]
	addi sp, 2
	ret

; ***** void graphics_drawRectangle(uin16 x1, uint16 y1, uint16 width, uint16 height)
; * draws a rectangle from the top-left corner with the given width and height
; *****
:graphics_drawRectangle
	push bp
	set bp, sp
	ldi r0, (bp+6) ; x1
	push r0
	ldi r0, (bp+5) ; y1
	push r0
	ldi r0, (bp+4) ; width
	push r0
	li r2, :graphics_drawHorizontal
	lih r2, ::graphics_drawHorizontal
	call [r2]
	addi sp, 3
	ldi r0, (bp+6)
	push r0
	ldi r0, (bp+5)
	push r0
	ldi r0, (bp+3)
	push r0
	li r2, :graphics_drawVertical
	lih r2, ::graphics_drawVertical
	call [r2]
	addi sp, 3
	ldi r0, (bp+6)
	push r0
	ldi r1, (bp+5)
	ldi r0, (bp+3)
	add r0, r1
	addi r0, -1
	push r0
	ldi r0, (bp+4)
	push r0
	li r2, :graphics_drawHorizontal
	lih r2, ::graphics_drawHorizontal
	call [r2]
	addi sp, 3
	ldi r1, (bp+6)
	ldi r0, (bp+4)
	add r0, r1
	addi r0, -1
	push r0
	ldi r0, (bp+5)
	push r0
	ldi r0, (bp+3)
	push r0
	li r2, :graphics_drawVertical
	lih r2, ::graphics_drawVertical
	call [r2]
	addi sp, 3
	pop bp
	ret
	
; ***** void graphics_fillRectangle(uin16 x1, uint16 y1, uint16 width, uint16 height)
; * fills a rectangle from the top-left corner with the given width and height
; *****
:graphics_fillRectangle
	push bp
	set bp, sp
	ldi r4, (bp+5) ; counter y = y1
	ldi r0, (bp+3) ; height
	add r0, r4
	sti r0, (bp+3) ; height = y1 + height
:graphics_fillRectangle_whileStart
	ldi r3, (bp+3)
	subf r4, r3
	li r0, :graphics_fillRectangle_whileEnd
	lih r0, ::graphics_fillRectangle_whileEnd
	jpc [r0]
	push r4	; save r4
	ldi r0, (bp+6) ; x1
	push r0
	push r4
	ldi r0, (bp+4) ; width
	push r0
	li r0, :graphics_drawHorizontal
	lih r0, ::graphics_drawHorizontal
	call [r0]
	addi sp, 3
	pop r4	; restore r4
	inc r4
	li r0, :graphics_fillRectangle_whileStart
	lih r0, ::graphics_fillRectangle_whileStart
	jp [r0]
:graphics_fillRectangle_whileEnd
	pop bp
	ret

; ***** void graphics_scrollUp(uint16 amount)
; * Scrolls the screen up by the given amount of lines.
; * The bottom lines are filled with the background colour
; *****
:graphics_scrollUp
	; r1 = source, r2 = dest, r3 = count
	ldi r0, (sp+2) ; amount
	set r1, r0
	shli r0, 7
	shli r1, 5
	add r0, r1
	li r2, :graphics_drawBuffer
	lih r2, ::graphics_drawBuffer
	in r1, (r2)
	andi r1, 1
	shli r1, 15
	set r2, r1
	add r1, r0
	li r3, 0x00
	lih r3, 0x7D
	sub r3, r0
	li r0, :graphics_scroll_up_loop
	lih r0, ::graphics_scroll_up_loop	
:graphics_scroll_up_loop
	in r4, (r1)
	out r4, (r2)
	inc r1
	inc r2
	dec r3
	jpnz [r0]
	li r3, :graphics_bgCol
	lih r3, ::graphics_bgCol
	in r3, (r3)
	set r4, r3
	shli r4, 8
	or r3, r4
	li r4, 0x00
	lih r4, 0x7D
	li r0, :graphics_scroll_up_loop2
	lih r0, ::graphics_scroll_up_loop2
:graphics_scroll_up_loop2
	out r3, (r2)
	inc r2
	cmp r2, r4
	jpnz [r0]
	ret

; ***** helper function for drawSprite
; * draws two pixels on the screen
; * r0 points to the right location in the screen
; * r1 points to the next two pixels to draw
; * r2 points to the next two pixels in the buffer
; * r5 contains the mask 0xFF00
; * r11 contains the mask 0x00FF
; * r3, r4, r6, and r7 are used
; *****
:graphics_drawSprite_helper
	ld r3, (r1) ; next two pixels to draw
	in r4, (r0) ; old values of next two pixels
	st r4, (r2) ; save old values
	andf r3, r11 ; check low byte
	sz r6
	andi r6, 0xFF ; r6 is the mask for the old pixels
	andf r3, r5 ; check high byte
	sz r7
	and r7, r5
	or r6, r7 ; r6 is the mask for the old pixels
	and r4, r6 ; put mask on old pixels
	or r3, r4 ; retain old pixels where colour was black
	out r3, (r0) ; draw pixels
	inc r0
	inc r1
	inc r2
	ret

; ***** void graphics_drawSprite(uint8 sprite[16][16], uint8 buffer[16][16], uint16 x, uint16 y)
; * Draws a square 16x16 sprite at the given location and saves the erased points
; * The x position is rounded down to an even number (sprites cannot be drawn at odd x positions).  This keeps the speed of the rendering high
; * Black pixels are regarded transparant
; *****
:graphics_drawSprite
	ldi r0, (sp+3) ; x
	ldi r1, (sp+2) ; y
	shri r0, 1 ; ignore odd x positions
	shli r1, 5
	add r0, r1
	shli r1, 2
	add r0, r1
	li r1, :graphics_drawBuffer
	lih r1, ::graphics_drawBuffer
	in r1, (r1)
	shli r1, 15
	add r0, r1 ; r0 now points to location in screen buffer
	ldi r1, (sp+5) ; sprite
	ldi r2, (sp+4) ; buffer
	li r5, 0
	lih r5, 0xFF ; r5 is the mask 0xFF00
	li r11, 0xFF ; r11 is the mask 0x00FF
	li r8, :graphics_drawSprite_helper
	lih r8, ::graphics_drawSprite_helper
	li r9, :graphics_drawSprite_loop
	lih r9, ::graphics_drawSprite_loop
	li r10, 16
:graphics_drawSprite_loop
	call [r8]
	call [r8]
	call [r8]
	call [r8]
	call [r8]
	call [r8]
	call [r8]
	call [r8]
	addi r0, 25
	addi r0, 127
	dec r10
	jpnz [r9]
	ret

; ***** helper function for restoreSprite
; * restores two pixels on the screen
; * r0 points to the right location in the screen
; * r1 points to the next two pixels in the buffer
; *****
:graphics_restoreSprite_helper
	ld r2, (r1)
	out r2, (r0)
	inc r1
	inc r0
	ret

; ***** void graphics_restoreSprite(uint8 buffer[16][16], uint16 x, uint16 y)
; * Restores the saved pixels that were erased by drawing a sprite at the given location
; * This function can also be used to draw a sprite without saving the background
; * The x position is rounded to an even number (sprites cannot be drawn at odd x positions).  This keeps the speed of the rendering high
; *****
:graphics_restoreSprite
	ldi r0, (sp+3) ; x
	ldi r1, (sp+2) ; y
	shri r0, 1 ; ignore odd x positions
	shli r1, 5
	add r0, r1
	shli r1, 2
	add r0, r1
	li r1, :graphics_drawBuffer
	lih r1, ::graphics_drawBuffer
	in r1, (r1)
	shli r1, 15
	add r0, r1 ; r0 now points to location in screen buffer
	ldi r1, (sp+4) ; buffer
	li r3, :graphics_restoreSprite_helper
	lih r3, ::graphics_restoreSprite_helper
	li r4, :graphics_restoreSprite_loop
	lih r4, ::graphics_restoreSprite_loop
	li r5, 16
:graphics_restoreSprite_loop
	call [r3]
	call [r3]
	call [r3]
	call [r3]
	call [r3]
	call [r3]
	call [r3]
	call [r3]
	addi r0, 25
	addi r0, 127
	dec r5
	jpnz [r4]
	ret

