; *****
; * This file contains code to demo the Moncky-3 text library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * global variables
; *****
:s_splash_screen
.data	3, 4, 5, 32, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 32, 3, 4, 5,10
.data	6, 7, 11, 32, '*', '*', '*', 32, 32, 32, 32, 'M', 'o', 'n', 'c', 'k', 'y', '-', '3', 32, 'C', 'o', 'm', 'p', 'u', 't', 'e', 'r', 32, 32, 32, 32, '*', '*', '*', 32, 6, 7, 11,10
.data	12, 14, 15, 32, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 32, 12, 14, 15,10
.data	32, 32, 32, 32, 32, 32, 32, 32, 40, 'c', 41, '2', '0', '2', '1', 32, 'K', 'r', 'i', 's', 32, 'D', 'e', 'm', 'u', 'y', 'n', 'c', 'k', 10, 10
:s_ready
.data	'R', 'e', 'a', 'd', 'y', '.'
:s_newline
.data	10, 0

:s_bigbat8
.data	0x0403, 0x0A05, 0x0706, 0x0A0B, 0x0E0C, 0x000F


:start_system
	li r0, :graphics_init
	lih r0, ::graphics_init
	call [r0]

	li r0, :text_init
	lih r0, ::text_init
	call [r0]

	li r0, 0x05
	push r0
	li r0, :graphics_setBgCol
	lih r0, ::graphics_setBgCol
	call [r0]
	addi sp, 1

	li r0, :graphics_clrscr
	lih r0, ::graphics_clrscr
	call [r0]
	
	li r0, 0xE0
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1

	li r0, 200
	push r0
	li r0, 100
	push r0
	li r0, 1
	push r0
	li r0, :text_drawChar
	lih r0, ::text_drawChar
	call [r0]
	addi sp, 3

	li r0, 0xFF
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1

	li r0, :s_splash_screen
	lih r0, ::s_splash_screen
	push r0
	li r0, :text_printString16
	lih r0, ::text_printString16
	call [r0]
	addi sp, 1

	li r0, 1
	push r0
	li r0, 2
	push r0
	li r0, :text_setCursor
	lih r0, ::text_setCursor
	call [r0]
	addi sp, 2

	li r0, 1
:loop_cursor
	push r0

	li r0, :text_drawCursor
	lih r0, ::text_drawCursor
	call [r0]

	ei
	li r0, :graphics_waitVertRetrace
	lih r0, ::graphics_waitVertRetrace
	call [r0]
	di

	li r0, :text_eraseCursor
	lih r0, ::text_eraseCursor
	call [r0]

	ei
	li r0, :graphics_waitVertRetrace
	lih r0, ::graphics_waitVertRetrace
	call [r0]
	di

	pop r0
	dec r0
	li r1, :loop_cursor
	lih r1, ::loop_cursor
	jpnz [r1]

	li r0, 0
	push r0
	li r0, 10
	push r0
	li r0, :text_setCursor
	lih r0, ::text_setCursor
	call [r0]
	addi sp, 2

	li r0, 0x03
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1

	li r0, :s_bigbat8
	lih r0, ::s_bigbat8
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1

	li r0, 10
	push r0
	li r0, 10
	push r0
	li r0, :text_setCursor
	lih r0, ::text_setCursor
	call [r0]
	addi sp, 2

	li r0, 0xF0
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1
	
	li r0, 0x23
	lih r0, 0x01
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1

	li r0, 0x67
	lih r0, 0x45
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1

	li r0, 0xAB
	lih r0, 0x89
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1

	li r0, 0xEF
	lih r0, 0xCD
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1

	halt

