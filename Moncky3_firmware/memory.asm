; *****
; * This file contains the code of the Moncky-3 memory library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * hardware variables (I/O)
; *****

; *****
; * global variables
; *****
:nextAddress
.data 0x4000

; ***** void memory_init(uint16 nextAddress)
; * sets the nextAddress to the specified value
; *****
:memory_init
	ldi r0, (sp+2) ; nextAddress parameter
	li r1, :nextAddress
	lih r1, ::nextAddress
	st r0, (r1)
	ret

; ***** uint16* malloc(uint16 length)
; * multiplies two signed 8-bit integers.  The result is put in r0 (8 bit)
; *****
:malloc
	li r1, :nextAddress
	lih r1, ::nextAddress
	ld r0, (r1)
	set r2, r0
	ldi r3, (sp+2) ; length
	add r2, r3
	st r2, (r1)
	ret


	
