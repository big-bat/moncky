; *****
; * This file contains the code of the Moncky-3 SPI library.
; * depends on: graphics.asm, text.asm, utilities.asm
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * hardware variables (I/O)
; *****
.def :spi_data 	0x7E00
.def :spi_control	0x7E01

; ***** void spi_init()
; * This will set the slave select pins to high, SPI mode to 0, and speed to 250KHz.
; *****
:spi_init_log
; .data " spi_init "
:spi_init
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :spi_init_log
	; lih r1, ::spi_init_log
	; li r2, 0
	; call [r0]
	
	li r0, :spi_control
	lih r0, ::spi_control
	li r1, 0xF0
	lih r1, 24
	out r1, (r0)
	ret

; ***** void spi_enable(uint8 slave)
; * this will put the selected slave (0, 1, 2, 3) select pin to 0 and the others to 1
; *****
:spi_enable_log
; .data " spi_enable "
:spi_enable
	; log
	; push bp
	; set bp, sp
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :spi_enable_log
	; lih r1, ::spi_enable_log
	; li r2, 1
	; call [r0]
	; pop bp

	ldi r4, (sp+2) ; slave
	andi r4, 3 ; clip
	or r4, r4
	sz r0
	andi r0, 0xE
	cmpi r4, 1
	sz r1
	andi r1, 0xD
	cmpi r4, 2
	sz r2
	andi r2, 0xB
	cmpi r4, 3
	sz r3
	andi r3, 0x7
	or r0, r1
	or r2, r3
	or r0, r2
	shli r0, 4
	li r1, :spi_control
	lih r1, ::spi_control
	in r2, (r1)
	li r3, 0x0F
	lih r3, 0xFF
	and r2, r3
	or r2, r0
	out r2, (r1)
	ret

; ***** void spi_disable()
; * this will put all slave select pins to 1
; *****
:spi_disable_log
; .data " spi_disable "
:spi_disable
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :spi_disable_log
	; lih r1, ::spi_disable_log
	; li r2, 0
	; call [r0]
	
	li r0, 0xF0
	li r1, :spi_control
	lih r1, ::spi_control
	in r2, (r1)
	li r3, 0x0F
	lih r3, 0xFF
	and r2, r3
	or r2, r0
	out r2, (r1)
	ret

; ***** void spi_setSpeed(uint8 speed)
; * this will put the speed of the SPI interface to the given value
; *****
:spi_setSpeed_log
; .data " spi_setSpeed "
:spi_setSpeed
	; log
	; push bp
	; set bp, sp
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :spi_setSpeed_log
	; lih r1, ::spi_setSpeed_log
	; li r2, 1
	; call [r0]
	; pop bp

	ldi r0, (sp+2) ; speed
	shli r0, 8
	li r1, :spi_control
	lih r1, ::spi_control
	in r2, (r1)
	andi r2, 0xFF
	or r2, r0
	out r2, (r1)
	ret

; ***** uint8 spi_transfer(uint8 byte)
; * send and receive 8 bits of data
; * return value in r0
; *****
:spi_transfer_log
; .data " spi_transfer "
:spi_transfer
	push bp
	set bp, sp
	
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :spi_transfer_log
	; lih r1, ::spi_transfer_log
	; li r2, 1
	; call [r0]
	
	li r0, :spi_data
	lih r0, ::spi_data
	ldi r1, (bp+3) ; byte
	out r1, (r0)	; set data to byte
	li r0, :spi_control
	lih r0, ::spi_control
	in r1, (r0)	; read spi_control
	ori r1, 1	; initiate transfer setting transfer bit
	out r1, (r0)	; write spi_control
:spi_transfer_loop 
	in r1, (r0)	; read spi_control
	andif r1, 8	; check ready bit
	li r2, :spi_transfer_loop
	lih r2, ::spi_transfer_loop
	jpz [r2]
	li r2, 1
	not r2, r2
	and r1, r2	; clear transfer bit again
	out r1, (r0)	; write spi_control
	li r0, :spi_data
	lih r0, ::spi_data
	in r0, (r0)	; read received data
	andi r0, 0xFF	; clip
	
	; debug:
	; push r0 ; save return value
	; push r0
	; li r1, :text_printHex
	; lih r1, ::text_printHex
	; call [r1]
	; addi sp, 1
	; li r1, 10 ; newline
	; push r1
	; li r1, :text_printChar
	; lih r1, ::text_printChar
	; call [r1]
	; addi sp, 1
	; pop r0 ; restore return value
	
	pop bp
	ret

