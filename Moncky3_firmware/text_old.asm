; *****
; * This file contains the code of the Moncky-3 text library.
; * depends on: graphics.asm, system.asm
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * firmware variables (I/O)
; *****
.def :text_cursorPosition	0x7D02 ; LSB = X, MSB = Y
.def :text_savedCursor		0x7D05 ; until 0x7D25

; *****
; * 8x8 font for all ASCII characters
; * based on Amstrad CPC 464 font
; *****
:text_font
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0000 (null)
        .data 0x6CFE, 0xFEFE, 0x7C38, 0x1000 ; U+0001 (heart)
        .data 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF ; U+0002 (full, cursor)
        .data 0x0000, 0x0001, 0x071E, 0x3E7E ; U+0003 (bigbat00)
        .data 0x0000, 0x0000, 0x0024, 0x3C24 ; U+0004 (bigbat01)
        .data 0x0000, 0x0080, 0xE078, 0x7C7E ; U+0005 (bigbat02)
        .data 0x7FFF, 0xFF1F, 0x0F07, 0x070F ; U+0006 (bigbat10)
        .data 0x3CFF, 0xFFFF, 0xFFFF, 0xFFFF ; U+0007 (bigbat11)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0008 (backspace)        
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0009 (tab)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000A (newline)
        .data 0xFEFF, 0xFFF8, 0xF0E0, 0xE0F0 ; U+000B (bigbat12)
        .data 0x1810, 0x0000, 0x0000, 0x0000 ; U+000C (bigbat20)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000D (carriage return)
        .data 0x3C18, 0x1818, 0x0000, 0x0000 ; U+000E (bigbat21)
        .data 0x1808, 0x0000, 0x0000, 0x0000 ; U+000F (bigbat22)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0010
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0011
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0012
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0013
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0014
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0015
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0016
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0017
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0018
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0019
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001A
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001B
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001C
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001D
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001E
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001F
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0020 (space)
        .data 0x1818, 0x1818, 0x1800, 0x1800 ; U+0021 (!)
        .data 0x6C6C, 0x6C00, 0x0000, 0x0000 ; U+0022 (")
        .data 0x6C6C, 0xFE6C, 0xFE6C, 0x6C00 ; U+0023 (#)
        .data 0x183E, 0x583C, 0x1A7C, 0x1800 ; U+0024 ($)
        .data 0x00C6, 0xCC18, 0x3066, 0xC600 ; U+0025 (%)
        .data 0x386C, 0x3876, 0xDCCC, 0x7600 ; U+0026 (&)
        .data 0x1818, 0x3000, 0x0000, 0x0000 ; U+0027 (')
        .data 0x0C18, 0x3030, 0x3018, 0x0C00 ; U+0028 (()
        .data 0x3018, 0x0C0C, 0x0C18, 0x3000 ; U+0029 ())
        .data 0x0066, 0x3CFF, 0x3C66, 0x0000 ; U+002A (*)
        .data 0x0018, 0x187E, 0x1818, 0x0000 ; U+002B (+)
        .data 0x0000, 0x0000, 0x0018, 0x1830 ; U+002C (,)
        .data 0x0000, 0x007E, 0x0000, 0x0000 ; U+002D (-)
        .data 0x0000, 0x0000, 0x0018, 0x1800 ; U+002E (.)
        .data 0x060C, 0x1830, 0x60C0, 0x8000 ; U+002F (/)
        .data 0x7CC6, 0xCED6, 0xE6C6, 0x7C00 ; U+0030 (0)
        .data 0x1838, 0x1818, 0x1818, 0x7E00 ; U+0031 (1)
        .data 0x3C66, 0x063C, 0x6066, 0x7E00 ; U+0032 (2)
        .data 0x3C66, 0x061C, 0x0666, 0x3C00 ; U+0033 (3)
        .data 0x1C3C, 0x6CCC, 0xFE0C, 0x1E00 ; U+0034 (4)
        .data 0x7E62, 0x607C, 0x0666, 0x3C00 ; U+0035 (5)
        .data 0x3C66, 0x607C, 0x6666, 0x3C00 ; U+0036 (6)
        .data 0x7E66, 0x060C, 0x1818, 0x1800 ; U+0037 (7)
        .data 0x3C66, 0x663C, 0x6666, 0x3C00 ; U+0038 (8)
        .data 0x3C66, 0x663E, 0x0666, 0x3C00 ; U+0039 (9)
        .data 0x0000, 0x1818, 0x0018, 0x1800 ; U+003A (:)
        .data 0x0000, 0x1818, 0x0018, 0x1830 ; U+003B (;)
        .data 0x0C18, 0x3060, 0x3018, 0x0C00 ; U+003C (<)
        .data 0x0000, 0x7E00, 0x007E, 0x0000 ; U+003D (=)
        .data 0x6030, 0x180C, 0x1830, 0x6000 ; U+003E (>)
        .data 0x3C66, 0x660C, 0x1800, 0x1800 ; U+003F (?)
        .data 0x7CC6, 0xDEDE, 0xDEC0, 0x7C00 ; U+0040 (@)
        .data 0x183C, 0x6666, 0x7E66, 0x6600 ; U+0041 (A)
        .data 0xFC66, 0x667C, 0x6666, 0xFC00 ; U+0042 (B)
        .data 0x3C66, 0xC0C0, 0xC066, 0x3C00 ; U+0043 (C)
        .data 0xF86C, 0x6666, 0x666C, 0xF800 ; U+0044 (D)
        .data 0xFE62, 0x6878, 0x6862, 0xFE00 ; U+0045 (E)
        .data 0xFE62, 0x6878, 0x6860, 0xF000 ; U+0046 (F)
        .data 0x3C66, 0xC0C0, 0xCE66, 0x3E00 ; U+0047 (G)
        .data 0x6666, 0x667E, 0x6666, 0x6600 ; U+0048 (H)
        .data 0x7E18, 0x1818, 0x1818, 0x7E00 ; U+0049 (I)
        .data 0x1E0C, 0x0C0C, 0xCCCC, 0x7800 ; U+004A (J)
        .data 0xE666, 0x6C78, 0x6C66, 0xE600 ; U+004B (K)
        .data 0xF060, 0x6060, 0x6266, 0xFE00 ; U+004C (L)
        .data 0xC6EE, 0xFEFE, 0xD6C6, 0xC600 ; U+004D (M)
        .data 0xC6E6, 0xF6DE, 0xCEC6, 0xC600 ; U+004E (N)
        .data 0x386C, 0xC6C6, 0xC66C, 0x3800 ; U+004F (O)
        .data 0xFC66, 0x667C, 0x6060, 0xF000 ; U+0050 (P)
        .data 0x386C, 0xC6C6, 0xDACC, 0x7600 ; U+0051 (Q)
        .data 0xFC66, 0x667C, 0x6C66, 0xE600 ; U+0052 (R)
        .data 0x3C66, 0x603C, 0x0666, 0x3C00 ; U+0053 (S)
        .data 0x7E5A, 0x1818, 0x1818, 0x3C00 ; U+0054 (T)
        .data 0x6666, 0x6666, 0x6666, 0x3C00 ; U+0055 (U)
        .data 0x6666, 0x6666, 0x663C, 0x1800 ; U+0056 (V)
        .data 0xC6C6, 0xC6D6, 0xFEEE, 0xC600 ; U+0057 (W)
        .data 0xC66C, 0x3838, 0x6CC6, 0xC600 ; U+0058 (X)
        .data 0x6666, 0x663C, 0x1818, 0x3C00 ; U+0059 (Y)
        .data 0xFEC6, 0x8C18, 0x3266, 0xFE00 ; U+005A (Z)
        .data 0x3C30, 0x3030, 0x3030, 0x3C00 ; U+005B ([)
        .data 0xC060, 0x3018, 0x0C06, 0x0200 ; U+005C (\)
        .data 0x3C0C, 0x0C0C, 0x0C0C, 0x3C00 ; U+005D (])
        .data 0x183C, 0x7E18, 0x1818, 0x1800 ; U+005E (^)
        .data 0x0000, 0x0000, 0x0000, 0x00FF ; U+005F (_)
        .data 0x3018, 0x0C00, 0x0000, 0x0000 ; U+0060 (`)
        .data 0x0000, 0x780C, 0x7CCC, 0x7600 ; U+0061 (a)
        .data 0xE060, 0x7C66, 0x6666, 0xDC00 ; U+0062 (b)
        .data 0x0000, 0x3C66, 0x6066, 0x3C00 ; U+0063 (c)
        .data 0x1C0C, 0x7CCC, 0xCCCC, 0x7600 ; U+0064 (d)
        .data 0x0000, 0x3C66, 0x7E60, 0x3C00 ; U+0065 (e)
        .data 0x1C36, 0x3078, 0x3030, 0x7800 ; U+0066 (f)
        .data 0x0000, 0x3E66, 0x663E, 0x067C ; U+0067 (g)
        .data 0xE060, 0x6C76, 0x6666, 0xE600 ; U+0068 (h)
        .data 0x1800, 0x3818, 0x1818, 0x3C00 ; U+0069 (i)
        .data 0x0600, 0x0E06, 0x0666, 0x663C ; U+006A (j)
        .data 0xE060, 0x666C, 0x786C, 0xE600 ; U+006B (k)
        .data 0x3818, 0x1818, 0x1818, 0x3C00 ; U+006C (l)
        .data 0x0000, 0x6CFE, 0xD6D6, 0xC600 ; U+006D (m)
        .data 0x0000, 0xDC66, 0x6666, 0x6600 ; U+006E (n)
        .data 0x0000, 0x3C66, 0x6666, 0x3C00 ; U+006F (o)
        .data 0x0000, 0xDC66, 0x667C, 0x60F0 ; U+0070 (p)
        .data 0x0000, 0x76CC, 0xCC7C, 0x0C1E ; U+0071 (q)
        .data 0x0000, 0xDC60, 0x6060, 0xF000 ; U+0072 (r)
        .data 0x0000, 0x3C60, 0x3C06, 0x7C00 ; U+0073 (s)
        .data 0x3030, 0x7C30, 0x3036, 0x1C00 ; U+0074 (t)
        .data 0x0000, 0x6666, 0x6666, 0x3E00 ; U+0075 (u)
        .data 0x0000, 0x6666, 0x663C, 0x1800 ; U+0076 (v)
        .data 0x0000, 0xC6D6, 0xD6FE, 0x6C00 ; U+0077 (w)
        .data 0x0000, 0xC66C, 0x386C, 0xC600 ; U+0078 (x)
        .data 0x0000, 0x6666, 0x663E, 0x067C ; U+0079 (y)
        .data 0x0000, 0x7E4C, 0x1832, 0x7E00 ; U+007A (z)
        .data 0x0E18, 0x1870, 0x1818, 0x0E00 ; U+007B ({)
        .data 0x1818, 0x1818, 0x1818, 0x1800 ; U+007C (|)
        .data 0x7018, 0x180E, 0x1818, 0x7000 ; U+007D (})
        .data 0x76DC, 0x0000, 0x0000, 0x0000 ; U+007E (~)
        .data 0xCC33, 0xCC33, 0xCC33, 0xCC33 ; U+007F

; ***** void text_init()
; * puts the cursor at (0,0)
; *****
:text_init
	li r0, :text_cursorPosition
	lih r0, ::text_cursorPosition
	li r1, 0
	out r1, (r0)
	ret

; ***** void text_setCursor(uint8 x, uint8 y)
; * sets the cursor at the given coordinates (x<40 and y<25)
; * the cursor is set at (0,0) when illegal values are given
; *****
:text_setCursor
	ldi r0, (sp+3) ; x
	ldi r1, (sp+2) ; y
	cmpi r0, 40
	snc r2
	and r0, r2
	cmpi r1, 25
	snc r3
	and r2, r3
	shli r1, 8
	or r0, r1
	and r0, r3
	li r1, :text_cursorPosition
	lih r1, ::text_cursorPosition
	out r0, (r1)
	ret

; ***** void text_drawCursor()
; * Draws a square cursor at the current position and saves the erased points
; *****
:text_drawCursor
	li r0, :text_cursorPosition
	lih r0, ::text_cursorPosition
	in r0, (r0)
	set r1, r0
	andi r0, 0xFF ; x
	shli r0, 2
	shri r1, 8 ; y
	set r2, r1
	shli r1, 8
	shli r2, 10
	add r1, r2
	add r1, r0
	li r2, :graphics_drawBuffer
	lih r2, ::graphics_drawBuffer
	in r0, (r2)
	andi r0, 1
	shli r0,15
	add r1, r0 ; r1 is now start address in vram
	li r2, :text_savedCursor
	lih r2, ::text_savedCursor
	li r4, :graphics_drawCol
	lih r4, ::graphics_drawCol
	in r3, (r4)
	andi r3, 0xFF
	set r4, r3
	shli r4, 8
	or r3, r4
	li r4, 8
	li r5, :text_drawCursor_loop
	lih r5, ::text_drawCursor_loop
:text_drawCursor_loop
	in r0, (r1)
	out r0, (r2)
	out r3, (r1)
	inc r1
	inc r2
	in r0, (r1)
	out r0, (r2)
	out r3, (r1)
	inc r1
	inc r2
	in r0, (r1)
	out r0, (r2)
	out r3, (r1)
	inc r1
	inc r2
	in r0, (r1)
	out r0, (r2)
	out r3, (r1)
	addi r1, 100
	addi r1, 57
	inc r2
	dec r4
	jpnz [r5]
	ret

; ***** void text_eraseCursor()
; * restores the part of the screen that was overwritten by the cursor
; *****
:text_eraseCursor
	li r0, :text_cursorPosition
	lih r0, ::text_cursorPosition
	in r0, (r0)
	set r1, r0
	andi r0, 0xFF ; x
	shli r0, 2
	shri r1, 8 ; y
	set r2, r1
	shli r1, 8
	shli r2, 10
	add r1, r2
	add r1, r0
	li r3, :graphics_drawBuffer
	lih r3, ::graphics_drawBuffer
	in r0, (r3)
	andi r0, 1
	shli r0,15
	add r1, r0 ; r1 is now start address in vram
	li r2, :text_savedCursor
	lih r2, ::text_savedCursor
	li r4, 8
	li r3, :text_eraseCursor_loop
	lih r3, ::text_eraseCursor_loop
:text_eraseCursor_loop
	in r0, (r2)
	out r0, (r1)
	inc r1
	inc r2
	in r0, (r2)
	out r0, (r1)
	inc r1
	inc r2
	in r0, (r2)
	out r0, (r1)
	inc r1
	inc r2
	in r0, (r2)
	out r0, (r1)
	addi r1, 100
	addi r1, 57
	inc r2
	dec r4
	jpnz [r3]
	ret

; ***** void text_drawChar(uint16 x, uint16 y, uint8 char)
; * draws a character (0-127) at the given (x,y) coordinates
; *****
; * r0 points to bit pattern
; * r1 counter (4 words)
; * r2 bit pattern
; * r3 bitMask
; * r4 temp
; * r5 points to plot function
:text_drawChar
	push bp
	set bp, sp	
	; prepare stack for plot(x,y)
	ldi r0, (bp+5) ; x
	push r0
	ldi r0, (bp+4) ; y
	push r0
	li r5, :graphics_plot
	lih r5, ::graphics_plot
	ldi r0, (bp+3)	; char
	andi r0, 0x7F	; clip
	shli r0, 2	; times 4 to find the offset in the font table
	li r1, :text_font
	lih r1, ::text_font
	add r0, r1	; r0 points now to the start of the bit patterns
	li r1, 4	; r1 = counter: 4 words
:text_drawChar_nextPattern
	ld r2, (r0)	; r2 contains the bit pattern
	li r3, 0	; r3 = bitMask
	lih r3, 0x80
:text_drawChar_nextPixel
	li r4, :text_drawChar_noPixel
	lih r4, ::text_drawChar_noPixel
	andf r2, r3
	jpz r4
	call [r5]
:text_drawChar_noPixel
	ld r4, (bp) ; inc(x)
	inc r4
	st r4, (bp)
	shri r3, 1
	li r4, 128
	cmp r3, r4
	li r4, :text_drawChar_sameRow
	lih r4, ::text_drawChar_sameRow
	jpnz [r4]
	ld r4, (bp) ; x = x - 8
	subi r4, 8
	st r4, (bp)
	ldi r4, (sp+1) ; y++
	inc r4
	sti r4, (sp+1)
:text_drawChar_sameRow
	cmpi r3, 0
	li r4, :text_drawChar_nextPixel
	lih r4, ::text_drawChar_nextPixel
	jpnz [r4]
	ld r4, (bp) ; x = x - 8
	subi r4, 8
	st r4, (bp)
	ldi r4, (sp+1) ; y++
	inc r4
	sti r4, (sp+1)
	inc r0
	li r4, :text_drawChar_nextPattern
	lih r4, ::text_drawChar_nextPattern
	dec r1
	jpnz [r4]
	addi sp, 2
	pop bp
	ret

; ***** void text_printChar(uint8 char)
; * Prints a character (0-127) at the current cursor position
; * The cursor is advanced one position.  If the cursor goes off the screen, the screen is scrolled up one line.
; * This routine also 'prints' cursor movements: arrows and del
; *****
:text_printChar
	push bp
	set bp, sp
	li r0, :text_cursorPosition
	lih r0, ::text_cursorPosition
	in r0, (r0)
	set r1, r0
	andi r0, 0xFF ; x
	shri r1, 8 ; y
	ldi r2, (bp+3) ; char
	cmpi r2, 0x11 ; arrow left
	li r3, :text_printChar_left
	lih r3, ::text_printChar_left
	jpz [r3]
	cmpi r2, 0x12 ; arrow right
	li r3, :text_printChar_right
	lih r3, ::text_printChar_right
	jpz [r3]
	cmpi r2, 0x13 ; arrow up
	li r3, :text_printChar_up
	lih r3, ::text_printChar_up
	jpz [r3]
	cmpi r2, 0x14 ; arrow down
	li r3, :text_printChar_down
	lih r3, ::text_printChar_down
	jpz [r3]
	cmpi r2, 8 ; backspace
	li r3, :text_printChar_backspace
	lih r3, ::text_printChar_backspace
	jpz [r3]
	li r3, :text_printChar_normal
	lih r3, ::text_printChar_normal
	jp [r3]
:text_printChar_left
	dec r0
	set r2, r0
	ashri r2, 15
	not r2, r2
	and r0, r2
	li r2, :text_printChar_store_cursor
	lih r2, ::text_printChar_store_cursor
	jp [r2]
:text_printChar_right
	inc r0
	cmpi r0, 40
	ss r2
	and r0, r2
	li r3, 39
	not r2, r2
	and r3, r2
	or r0, r3
	li r2, :text_printChar_store_cursor
	lih r2, ::text_printChar_store_cursor
	jp [r2]
:text_printChar_up
	dec r1
	set r2, r1
	ashri r2, 15
	not r2, r2
	and r1, r2
	li r2, :text_printChar_store_cursor
	lih r2, ::text_printChar_store_cursor
	jp [r2]
:text_printChar_down
	inc r1
	cmpi r1, 25
	ss r2
	and r1, r2
	li r3, 24
	not r2, r2
	and r3, r2
	or r1, r3
	li r2, :text_printChar_store_cursor
	lih r2, ::text_printChar_store_cursor
	jp [r2]
:text_printChar_backspace ; do arrow left and draw char 2 in background col, then again arrow left
	li r0, 0x11 ; arrow left
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	; save current colour
	li r0, :graphics_drawCol
	lih r0, ::graphics_drawCol
	in r0, (r0)
	push r0 ; save to stack
	; set colour to background colour
	li r0, :graphics_bgCol
	lih r0, :graphics_bgCol
	in r0, (r0)
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1
	li r0, 2 ; cursor character
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	; restore draw colour
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1
	li r0, 0x11 ; arrow left
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	li r0, :text_printChar_end
	lih r0, ::text_printChar_end
	jp [r0]
:text_printChar_normal
	shli r0, 3
	shli r1, 3
	; drawChar(x,y,char)
	push r0
	push r1
	ldi r2, (bp+3) ; char
	push r2
	li r2, :text_drawChar
	lih r2, ::text_drawChar
	call [r2]
	addi sp, 1
	pop r1 ; restore x and y and increase position or move cursor
	pop r0
	shri r0, 3
	shri r1, 3
	ldi r2, (bp+3) ; char
	cmpi r2, 10 ; newline
	li r2, :text_printChar_no_newline
	lih r2, ::text_printChar_no_newline
	jpnz r2
	li r0, 39
:text_printChar_no_newline
	inc r0
	cmpi r0, 40
	li r2, :text_printChar_store_cursor
	lih r2, ::text_printChar_store_cursor
	jpnz [r2]
	li r0, 0
	inc r1
	cmpi r1, 25
	jpnz [r2]
	li r1, 24
	; scroll_up(8)
	push r0 ; save r0 and r1
	push r1
	li r2, 8
	push r2
	li r2, :graphics_scrollUp
	lih r2, ::graphics_scrollUp
	call [r2]
	addi sp, 1
	pop r1 ; restore r1 and r0
	pop r0
:text_printChar_store_cursor
	shli r1, 8
	or r1, r0
	li r0, :text_cursorPosition
	lih r0, ::text_cursorPosition
	out r1, (r0)
:text_printChar_end
	pop bp
	ret

; ***** void text_printString16(uint16* string)
; * prints a null-terminated string on the screen. There is one 16-bit word per character.
; *****
:text_printString16
:text_printString16_loop
	ldi r0, (sp+2) ; string
	ld r1, (r0) ; char to which string points
	nop r1, r1
	li r2, :text_printString16_end
	lih r2, ::text_printString16_end
	jpz [r2]
	push r1
	li r2, :text_printChar
	lih r2, ::text_printChar
	call [r2]
	addi sp, 1
	ldi r0, (sp+2) ; advance string
	inc r0
	sti r0, (sp+2)
	li r0, :text_printString16_loop
	lih r0, ::text_printString16_loop
	jp [r0]
:text_printString16_end
	ret

; ***** void text_printString(uint8* string)
; * prints a null-terminated string on the screen.
; * There are two characters per 16-bit word: the LSB is the first character and the MSB is the second.
; *****
:text_printString
:text_printString_loop
	ldi r0, (sp+2) ; string
	ld r1, (r0) ; word to which string points
	andi r1, 0xFF ; select LSB
	or r1, r1
	li r2, :text_printString_end
	lih r2, ::text_printString_end
	jpz [r2]
	push r1
	li r2, :text_printChar
	lih r2, ::text_printChar
	call [r2]
	addi sp, 1
	ldi r0, (sp+2) ; string
	ld r1, (r0) ; word to which string points
	shri r1, 8 ; select MSB
	or r1, r1
	li r2, :text_printString_end
	lih r2, ::text_printString_end
	jpz [r2]
	push r1
	li r2, :text_printChar
	lih r2, ::text_printChar
	call [r2]
	addi sp, 1
	ldi r0, (sp+2) ; advance string
	inc r0
	sti r0, (sp+2)
	li r0, :text_printString_loop
	lih r0, ::text_printString_loop
	jp [r0]
:text_printString_end
	ret

; ***** void text_printHex(uint16 value)
; * prints the given 16-bit value on the screen in 4 hexadecimal digits (with leading zeroes)
; *****
:text_printHex
	ldi r0, (sp+2) ; value
	shri r0, 12 ; first digit
	cmpi r0, 10
	sc r1
	andi r1, 7
	add r0, r1
	addi r0, 48
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	ldi r0, (sp+2) ; value
	shri r0, 8 ; second digit
	andi r0, 15
	cmpi r0, 10
	sc r1
	andi r1, 7
	add r0, r1
	addi r0, 48
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	ldi r0, (sp+2) ; value
	shri r0, 4 ; third digit
	andi r0, 15
	cmpi r0, 10
	sc r1
	andi r1, 7
	add r0, r1
	addi r0, 48
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	ldi r0, (sp+2) ; value
	andi r0, 15 ; last digit
	cmpi r0, 10
	sc r1
	andi r1, 7
	add r0, r1
	addi r0, 48
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	ret

