; *****
; * This file contains the code of the Moncky-3 FAT library.
; * The code is based on http://www.rjhcoding.com/avrc-sd-interface-1.php
; * depends on: graphics.asm, text.asm, utilities.asm, spi.asm
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * firmware variables (I/O)
; *****
.def :fat_sector 		0xFD00
.def :fat_startPartition_LSB	0xFE00
.def :fat_startPartition_MSB	0xFE01
.def :fat_lengthPartition_LSB 0xFE02
.def :fat_lengthPartition_MSB 0xFE03
.def :fat_sectorsPerFat	0xFE04
.def :fat_fatPosition		0xFE05
.def :fat_rootFolderPosition_LSB 0xFE06
.def :fat_rootFolderPosition_MSB 0xFE07
.def :fat_numberOfSectorsForRootFolder 0xFE08
.def :fat_fileStart		0xFE09
.def :fat_fileLength_LSB	0xFE0A
.def :fat_fileLength_MSB	0xFE0B
.def :fat_currentClusterNumber 0xFE0C
.def :fat_sectorsPerCluster	0xFE0D
.def :fat_currentSectorNumber 0xFE0E

; *****
; * constants
; *****
.def :FAT_OK				0
.def :FAT_ERROR_NOT_FORMATTED		1
.def :FAT_ERROR_PARTITION_TOO_BIG	2
.def :FAT_ERROR_INVALID_SECTOR_SIZE	3
.def :FAT_ERROR_INVALID_NUMBER_OF_FATS 4
.def :FAT_ERROR_FILE_NOT_FOUND	5
.def :FAT_END_OF_FILE			6

:fat_result_strings
.data :fat_s_ok, :fat_s_not_formatted, :fat_s_partition, :fat_s_sector, :fat_s_fats, :fat_s_file
:fat_s_ok
.data "FAT ok"
:fat_s_not_formatted
.data "disk not formatted"
:fat_s_partition
.data "partition is too big (max 32MB)"
:fat_s_sector
.data "invalid sector size (should be 512B)"
:fat_s_fats
.data "invalid number of FATs (should be 2)"
:fat_s_file
.data "cannot find file "

; ***** uint8 fat_init()
; * tries to find and initialise a FAT16 file system on the first partition
; * returns an error code or FAT_OK
; *****
:fat_init_log
; .data " fat_init "
:fat_init
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :fat_init_log
	; lih r1, ::fat_init_log
	; li r2, 0
	; call [r0]

	li r0, :fat_findAndCheckPartition
	lih r0, ::fat_findAndCheckPartition
	call [r0]
	cmpi r0, :FAT_OK
	li r1, :fat_init_return
	lih r1, ::fat_init_return
	jpnz [r1]
	li r0, :fat_findAndCheckFAT
	lih r0, ::fat_findAndCheckFAT
	call [r0]
:fat_init_return
	ret

; ***** uint8 fat_findAndCheckPartition()
; * finds the first logical partition on the disk.
; * if the disk was not formatted or the partition>=32MB an error is returned
; *****
:fat_findAndCheckPartition_log
; .data " fat_findAndCheckPartition "
:fat_findAndCheckPartition
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :fat_findAndCheckPartition_log
	; lih r1, ::fat_findAndCheckPartition_log
	; li r2, 0
	; call [r0]
	
	; fat_startPartition = fat_lengthPartition = 0
	li r1, 0
	li r0, :fat_startPartition_LSB
	lih r0, ::fat_startPartition_LSB
	out r1, (r0)
	li r0, :fat_startPartition_MSB
	lih r0, ::fat_startPartition_MSB
	out r1, (r0)
	li r0, :fat_lengthPartition_LSB
	lih r0, ::fat_lengthPartition_LSB
	out r1, (r0)
	li r0, :fat_lengthPartition_MSB
	lih r0, ::fat_lengthPartition_MSB
	out r1, (r0)

	; sd_read(0, fat_sector); // read MBR
	li r0, 0
	push r0
	push r0
	li r0, :fat_sector
	lih r0, ::fat_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_read
	lih r0, ::sd_read
	call [r0]
	addi sp, 4

	; register (r0) uint16 signature = fat_sector[255];
	li r0, :fat_sector
	lih r0, ::fat_sector
	li r1, 0xFF
	add r0, r1
	in r0, (r0)

	; if (signature != 0xAA55) return FAT_ERROR_NOT_FORMATTED;
	li r1, 0x55
	lih r1, 0xAA
	cmp r0, r1
	li r0, :fat_findAndCheckPartition_sig_ok
	lih r0, ::fat_findAndCheckPartition_sig_ok
	jpz [r0]
	li r0, :FAT_ERROR_NOT_FORMATTED
	li r1, :fat_findAndCheckPartition_return
	lih r1, ::fat_findAndCheckPartition_return
	jp [r1]
:fat_findAndCheckPartition_sig_ok

	; fat_startPartition = fat_sector[0xE3] + fat_sector[0xE4] << 16;
	li r0, :fat_sector
	lih r0, ::fat_sector
	li r1, 0xE3
	add r1, r0 ; fat_sector[0xE3]
	in r2, (r1)
	li r3, :fat_startPartition_LSB
	lih r3, ::fat_startPartition_LSB
	out r2, (r3)
	inc r1 ; fat_sector[0xE4]
	in r2, (r1)
	li r3, :fat_startPartition_MSB
	lih r3, ::fat_startPartition_MSB
	out r2, (r3)
		
	; fat_lengthPartition = fat_sector[0xE5] + fat_sector[0xE6] << 16;
	inc r1 ; fat_sector[0xE5]
	in r2, (r1)
	li r3, :fat_lengthPartition_LSB
	lih r3, ::fat_lengthPartition_LSB
	out r2, (r3)
	inc r1 ; fat_sector[0xE6]
	in r2, (r1)
	li r3, :fat_lengthPartition_MSB
	lih r3, ::fat_lengthPartition_MSB
	out r2, (r3)

	; if (fat_lengthPartition >= 65536) return FAT_ERROR_PARTITION_TOO_BIG;
	nop r2, r2
	li r0, :FAT_OK
	li r1, :fat_findAndCheckPartition_return
	lih r1, ::fat_findAndCheckPartition_return
	jpz [r1]

	; return FAT_ERROR_PARTITION_TOO_BIG 
	li r0, :FAT_ERROR_PARTITION_TOO_BIG

:fat_findAndCheckPartition_return
	ret


; ***** uint8 fat_findAndCheckFAT()
; * finds the FAT on the first logical partition
; * the FAT is expected to have 512 bytes per sector and 2 FAT tables
; * returns FAT_OK if all is ok, otherwise an error is returned
; *****
:fat_findAndCheckFAT_log
; .data	" fat_findAndCheckFAT "
:fat_findAndCheckFAT
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :fat_findAndCheckFAT_log
	; lih r1, ::fat_findAndCheckFAT_log
	; li r2, 0
	; call [r0]
	
	; fatPosition = rootFolderPosition = numberOfSectorsForRootFolder = 0
	li r1, 0
	li r0, :fat_fatPosition
	lih r0, ::fat_fatPosition
	out r1, (r0)
	li r0, :fat_rootFolderPosition_LSB
	lih r0, ::fat_rootFolderPosition_LSB
	out r1, (r0)
	li r0, :fat_rootFolderPosition_MSB
	lih r0, ::fat_rootFolderPosition_MSB
	out r1, (r0)
	li r0, :fat_numberOfSectorsForRootFolder
	lih r0, ::fat_numberOfSectorsForRootFolder
	out r1, (r0)

	; sd_read(fat_startPartition, fat_sector); // VBR
	li r0, :fat_startPartition_MSB
	lih r0, ::fat_startPartition_MSB
	in r0, (r0)
	push r0
	li r0, :fat_startPartition_LSB
	lih r0, ::fat_startPartition_LSB
	in r0, (r0)
	push r0
	li r0, :fat_sector
	lih r0, ::fat_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_read
	lih r0, ::sd_read
	call [r0]
	addi sp, 4
	
	; register (r1) uint16 bytesPerSector = (fat_sector[5] >> 8) | (fat_sector[6] & 0xFF) << 8;
	li r0, :fat_sector
	lih r0, ::fat_sector
	addi r0, 5 ; &fat_sector[5]
	in r1, (r0)
	shri r1, 8
	inc r0 ; &fat_sector[6]
	in r2, (r0)
	andi r2, 0xFF
	shli r2, 8
	or r1, r2 ; bytesPerSector now in r1
	
	; if (bytesPerSector != 512) return FAT_ERROR_INVALID_SECTOR_SIZE;
	li r2, 0x00
	lih r2, 0x02
	cmp r1, r2
	li r2, :fat_findAndCheckFAT_bytesPerSector_OK
	lih r2, ::fat_findAndCheckFAT_bytesPerSector_OK
	jpz [r2]
	li r0, :FAT_ERROR_INVALID_SECTOR_SIZE
	li r1, :fat_findAndCheckFAT_return
	lih r1, ::fat_findAndCheckFAT_return
	jp [r1]
:fat_findAndCheckFAT_bytesPerSector_OK
	; register (r0) uint8 numberOfFATs = fat_sector[8] & 0xFF;
	li r0, :fat_sector
	lih r0, ::fat_sector
	addi r0, 8
	in r0, (r0)
	andi r0, 0xFF

	; if (numberOfFATs != 2) return FAT_ERROR_INVALID_NUMBER_OF_FATS;
	cmpi r0, 2
	li r2, :fat_findAndCheckFAT_numberOfFats_OK
	lih r2, ::fat_findAndCheckFAT_numberOfFats_OK
	jpz [r2]
	li r0, :FAT_ERROR_INVALID_NUMBER_OF_FATS
	li r1, :fat_findAndCheckFAT_return
	lih r1, ::fat_findAndCheckFAT_return
	jp [r1]
:fat_findAndCheckFAT_numberOfFats_OK
	; fat_sectorsPerFat = fat_sector[11];
	li r0, :fat_sector
	lih r0, ::fat_sector
	addi r0, 11
	in r0, (r0)
	li r1, :fat_sectorsPerFat
	lih r1, ::fat_sectorsPerFat
	out r0, (r1)

	; fat_fatPosition = fat_sector[7];
	li r0, :fat_sector
	lih r0, ::fat_sector
	addi r0, 7
	in r0, (r0)
	li r1, :fat_fatPosition
	lih r1, ::fat_fatPosition
	out r0, (r1)

	; fat_rootFolderPosition = fat_startPartition + fat_fatPosition + 2 * fat_sectorsPerFat;
	li r0, :fat_startPartition_LSB
	lih r0, ::fat_startPartition_LSB
	in r1, (r0)
	li r0, :fat_startPartition_MSB
	lih r0, ::fat_startPartition_MSB
	in r2, (r0)	
	li r0, :fat_fatPosition
	lih r0, ::fat_fatPosition
	in r3, (r0)
	add r1, r3
	addci r2, 0
	li r0, :fat_sectorsPerFat
	lih r0, ::fat_sectorsPerFat
	in r3, (r0)
	shli r3, 1
	add r1, r3
	addci r2, 0
	li r0, :fat_rootFolderPosition_LSB
	lih r0, ::fat_rootFolderPosition_LSB
	out r1, (r0)
	li r0, :fat_rootFolderPosition_MSB
	lih r0, ::fat_rootFolderPosition_MSB
	out r2, (r0)

        ; register (r1) uint16 maxNumberOfRootDirEntries = (fat_sector[8] >> 8) | (fat_sector[9] & 0xFF) << 8;
        li r0, :fat_sector
	lih r0, ::fat_sector
	addi r0, 8
	in r1, (r0)
	shri r1, 8
	inc r0
	in r2, (r0)
	andi r2, 0xFF
	shli r2, 8
	or r1, r2

	; fat_numberOfSectorsForRootFolder = maxNumberOfRootDirEntries / 16;
	shri r1, 4
	li r0, :fat_numberOfSectorsForRootFolder
	lih r0, ::fat_numberOfSectorsForRootFolder
	out r1, (r0)

	; fat_sectorsPerCluster = fat_sector[6] >> 8;
	li r0, :fat_sector
	lih r0, ::fat_sector
	addi r0, 6
	in r0, (r0)
	shri r0, 8
	li r1, :fat_sectorsPerCluster
	lih r1, ::fat_sectorsPerCluster
	out r0, (r1)

	; return FAT_OK;
	li r0, :FAT_OK
:fat_findAndCheckFAT_return	
	ret

; ***** uint8 fat_openFile(char* filename)
; * opens a file with a certain filename (11 chars: 8+3)
; * the string is expected to be an array of 11 chars (must not be null terminated)
; * Filenames are all capitals and spaces are used to pad
; * When the file was not found, fileStart is put to zero, FAT_FILE_NOT_FOUND is returned
; *****
:fat_openFile_log
; .data " fat_openFile "
:fat_openFile
	push bp
	set bp, sp
	
	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :fat_openFile_log
	; lih r1, ::fat_openFile_log
	; li r2, 1
	; call [r0]

	; create local vars "i" and "entry"
	subi sp, 2
	
	; fat_fileStart = fat_fileLength = fat_currentSectorNumber = 0;
	li r1, 0
	li r0, :fat_fileStart
	lih r0, ::fat_fileStart
	out r1, (r0)
	li r0, :fat_fileLength_LSB
	lih r0, ::fat_fileLength_LSB
	out r1, (r0)
	li r0, :fat_fileLength_MSB
	lih r0, ::fat_fileLength_MSB
	out r1, (r0)
	li r0, :fat_currentSectorNumber
	lih r0, ::fat_currentSectorNumber
	out r1, (r0)

        ; fat_currentClusterNumber = 0xFFFF; // end of file
        li r1, 0xFF
        lih r1, 0xFF
        li r0, :fat_currentClusterNumber
        lih r0, ::fat_currentClusterNumber
        out r1, (r0)

        ; uint16 i = 0;
        li r0, 0
        sti r0, (sp+2)

        ; while (i < fat_numberOfSectorsForRootFolder) {
:fat_openFile_loop
	ldi r1, (sp+2) ; i
	li r0, :fat_numberOfSectorsForRootFolder
	lih r0, ::fat_numberOfSectorsForRootFolder
	in r0, (r0)
	cmp r1, r0
	li r0, :fat_openFile_endLoop
	lih r0, ::fat_openFile_endLoop
	jpc [r0]

	; sd_read(rootFolderPosition + i, fat_sector); // part of root folder
	li r0, :fat_rootFolderPosition_LSB
	lih r0, ::fat_rootFolderPosition_LSB
	in r2, (r0)
	li r0, :fat_rootFolderPosition_MSB
	lih r0, ::fat_rootFolderPosition_MSB
	in r3, (r0)
	ldi r1, (sp+2) ; i
	add r2, r1
	addci r3, 0
	push r3
	push r2
	li r0, :fat_sector
	lih r0, ::fat_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_read
	lih r0, ::sd_read
	call [r0]
	addi sp, 4

	; uint8 entry = 0;
	li r1, 0
	sti r1, (sp+1)

	; while (entry < 16) {
:fat_openFile_loop2
	ldi r1, (sp+1) ; entry
	cmpi r1, 16
	li r0, :fat_openFile_endLoop2
	lih r0, ::fat_openFile_endLoop2
	jpc [r0]

	; register (r1) uint16 position = entry * 16;
	ldi r1, (sp+1) ; entry
	shli r1, 4

	; register (r0) char* name = fat_sector + position;
	li r0, :fat_sector
	lih r0, ::fat_sector
	add r0, r1

	; if (strcmp(name, filename, 11)==0) {
	ldi r2, (bp+3)	; r2 = filename
	li r5, :fat_openFile_no_match
	lih r5, ::fat_openFile_no_match
	in r3, (r0)
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	ld r4, (r2)
	cmp r3, r4
	jpnz [r5]
	inc r2
	inc r0
	in r3, (r0)
	andi r3, 0xFF
	ld r4, (r2)
	andi r4, 0xFF
	cmp r3, r4
	jpnz [r5]

	; fat_currentClusterNumber = fat_fileStart = fat_sector[position + 13];
	addi r1, 13
	li r2, :fat_sector
	lih r2, ::fat_sector
	add r2, r1 ; &fat_sector[position+13]
	in r0, (r2)
	li r3, :fat_fileStart
	lih r3, ::fat_fileStart
	out r0, (r3)
	li r3, :fat_currentClusterNumber
	lih r3, ::fat_currentClusterNumber
	out r0, (r3)
	
	; fat_fileLength = fat_sector[position + 14] + fat_sector[position + 15] << 16;
	inc r2 ; &fat_sector[position+14]
	in r3, (r2)
	li r0, :fat_fileLength_LSB
	lih r0, ::fat_fileLength_LSB
	out r3, (r0)
	inc r2 ; &fat_sector[position+15]
	in r3, (r2)
	li r0, :fat_fileLength_MSB
	lih r0, ::fat_fileLength_MSB
	out r3, (r0)

	; return FAT_OK;
	li r0, :FAT_OK
	li r1, :fat_openFile_return
	lih r1, ::fat_openFile_return
	jp [r1]

	; }
:fat_openFile_no_match	

	; entry++;
	ldi r0, (sp+1) ; entry
	inc r0
	sti r0, (sp+1)

	; }
	li r0, :fat_openFile_loop2
	lih r0, ::fat_openFile_loop2
	jp [r0]

	; }
:fat_openFile_endLoop2

	; i++;
	ldi r1, (sp+2)
	inc r1
	sti r1, (sp+2)

	; }
	li r0, :fat_openFile_loop
	lih r0, ::fat_openFile_loop
	jp [r0]
:fat_openFile_endLoop
	; return FAT_ERROR_FILE_NOT_FOUND;
	li r0, :FAT_ERROR_FILE_NOT_FOUND
:fat_openFile_return
	addi sp, 2 ; remove "i" and "entry"
	pop bp
	ret

; ***** boolean uint8 fat_readNextFileSector(uint16 buffer[256], boolean io)
; * reads the next sector into the given location
; * the boolean io specifies if the buffer is in io-space
; * returns FAT_END_OF_FILE if end of file is reached, otherwise FAT_OK is returned
; *****
:fat_readNextFileSector_log
; .data " fat_readNextFileSector "
:fat_readNextFileSector
	push bp
	set bp, sp

	; log
	; li r0, :util_log
	; lih r0, ::util_log
	; li r1, :fat_readNextFileSector_log
	; lih r1, ::fat_readNextFileSector_log
	; li r2, 1
	; call [r0]

	; if (fat_currentClusterNumber >= 0xFFF8) return FAT_END_OF_FILE;
	li r0, :fat_currentClusterNumber
	lih r0, ::fat_currentClusterNumber
	in r0, (r0)
	li r1, 0xF8
	lih r1, 0xFF
	cmp r0, r1
	li r0, :FAT_END_OF_FILE
	li r1, :fat_readNextFileSector_return
	lih r1, ::fat_readNextFileSector_return
	jpc [r1]
	
	; sd_read(fat_rootFolderPosition + fat_numberOfSectorsForRootFolder
        ;    + (fat_currentClusterNumber-2)*fat_sectorsPerCluster
        ;    + fat_currentSectorNumber, buffer);
	li r0, :fat_rootFolderPosition_LSB
	lih r0, ::fat_rootFolderPosition_LSB
	in r1, (r0)
	li r0, :fat_rootFolderPosition_MSB
	lih r0, ::fat_rootFolderPosition_MSB
	in r2, (r0)
	li r0, :fat_numberOfSectorsForRootFolder
	lih r0, ::fat_numberOfSectorsForRootFolder
	in r3, (r0)
	add r1, r3
	addci r2, 0
	li r0, :fat_currentClusterNumber
	lih r0, ::fat_currentClusterNumber
	in r3, (r0)
	subi r3, 2
	li r4, 0
	li r0, :fat_sectorsPerCluster
	lih r0, ::fat_sectorsPerCluster
	in r5, (r0)
	nop r5, r5
	li r0, :fat_readNextFileSector_multloop_end
	lih r0, ::fat_readNextFileSector_multloop_end
	jpz [r0]
:fat_readNextFileSector_multloop
	andif r5, 1
	li r0, :fat_readNextFileSector_multloop_end
	lih r0, ::fat_readNextFileSector_multloop_end
	jpnz [r0]
	shri r5, 1
	shli r3, 1
	shlci r4, 1
	li r0, :fat_readNextFileSector_multloop
	lih r0, ::fat_readNextFileSector_multloop
	jp [r0]
:fat_readNextFileSector_multloop_end
	add r1, r3
	addc r2, r4
	li r0, :fat_currentSectorNumber
	lih r0, ::fat_currentSectorNumber
	in r3, (r0)
	add r1, r3
	addci r2, 0
	push r2
	push r1
	ldi r1, (bp+4) ; buffer
	push r1
	ldi r1, (bp+3) ; io
	push r1
	li r0, :sd_read
	lih r0, ::sd_read
	call [r0]
	addi sp, 4

	; fat_currentSectorNumber++;
	li r1, :fat_currentSectorNumber
	lih r1, ::fat_currentSectorNumber
	in r2, (r1)
	inc r2
	out r2, (r1)

	; if (fat_currentSectorNumber >= fat_sectorsPerCluster) {
	li r3, :fat_sectorsPerCluster
	lih r3, ::fat_sectorsPerCluster
	in r3, (r3)
	cmp r2, r3
	li r3, :fat_readNextFileSector_else
	lih r3, ::fat_readNextFileSector_else
	jpnc [r3]

	; fat_currentSectorNumber = 0;
	li r2, 0
	out r2, (r1)

	; register (r1) uint16 fatSectorNumber = fat_currentClusterNumber >> 8;
	li r0, :fat_currentClusterNumber
	lih r0, ::fat_currentClusterNumber
	in r1, (r0)
	shri r1, 8

	; register uint16 (r2) fatOffset = fat_currentClusterNumber & 0xFF;
	in r2, (r0)
	andi r2, 0xFF

	; sd_read(fat_startPartition + fat_fatPosition + fat_fatSectorNumber, fat_sector);
	push r2	; save fatOffset
	li r0, :fat_startPartition_LSB
	lih r0, ::fat_startPartition_LSB
	in r3, (r0)
	li r0, :fat_startPartition_MSB
	lih r0, ::fat_startPartition_MSB
	in r4, (r0)
	li r0, :fat_fatPosition
	lih r0, ::fat_fatPosition
	in r5, (r0)
	add r3, r5
	addci r4, 0
	add r3, r1
	addci r4, 0
	push r4
	push r3
	li r0, :fat_sector
	lih r0, ::fat_sector
	push r0
	li r0, 1
	push r0
	li r0, :sd_read
	lih r0, ::sd_read
	call [r0]
	addi sp, 4
	pop r2	; restore fatOffset

	; fat_currentClusterNumber = fat_sector[fatOffset];
	li r0, :fat_sector
	lih r0, ::fat_sector
	add r0, r2
	in r1, (r0)
	li r0, :fat_currentClusterNumber
	lih r0, ::fat_currentClusterNumber
	out r1, (r0)

:fat_readNextFileSector_else
        ; return FAT_OK;
	li r0, :FAT_OK
:fat_readNextFileSector_return
	pop bp
	ret

