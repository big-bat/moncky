; *****
; * This file contains code to demo the Moncky-3 spi library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * Constants
; *****
:s_control
.data "Control register: "
:s_data
.data "Data register: "

; ***** void printControl()
; * prints the control register
; *****
:printControl
	li r0, :s_control
	lih r0, ::s_control
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, :spi_control
	lih r0, ::spi_control
	in r0, (r0)
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1
	li r0, 10
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	ret

:start_system
	li r0, :graphics_init
	lih r0, ::graphics_init
	call [r0]

	li r0, :text_init
	lih r0, ::text_init
	call [r0]
	
	li r0, :spi_init
	lih r0, ::spi_init
	call [r0]

	li r0, 0x05
	push r0
	li r0, :graphics_setBgCol
	lih r0, ::graphics_setBgCol
	call [r0]
	addi sp, 1
	
	li r0, :graphics_clrscr
	lih r0, ::graphics_clrscr
	call [r0]
	
	li r0, :printControl
	lih r0, ::printControl
	call [r0]
	
	li r0, 0
	push r0
	li r0, :spi_enable
	lih r0, ::spi_enable
	call [r0]
	addi sp, 1
	
	li r0, :printControl
	lih r0, ::printControl
	call [r0]
	
	li r0, 1
	push r0
	li r0, :spi_enable
	lih r0, ::spi_enable
	call [r0]
	addi sp, 1
	
	li r0, :printControl
	lih r0, ::printControl
	call [r0]
	
	li r0, 2
	push r0
	li r0, :spi_enable
	lih r0, ::spi_enable
	call [r0]
	addi sp, 1
	
	li r0, :printControl
	lih r0, ::printControl
	call [r0]
	
	li r0, 3
	push r0
	li r0, :spi_enable
	lih r0, ::spi_enable
	call [r0]
	addi sp, 1
	
	li r0, :printControl
	lih r0, ::printControl
	call [r0]
	
	li r0, :spi_disable
	lih r0, ::spi_disable
	call [r0]
	
	li r0, :printControl
	lih r0, ::printControl
	call [r0]
	
	li r0, 42
	push r0
	li r0, :spi_setSpeed
	lih r0, ::spi_setSpeed
	call [r0]
	addi sp, 1
	
	li r0, :printControl
	lih r0, ::printControl
	call [r0]
	
	li r0, 0xAA
	push r0
	li r0, :spi_transfer
	lih r0, ::spi_transfer
	call [r0]
	addi sp, 1
	
	li r0, :printControl
	lih r0, ::printControl
	call [r0]

	; printData
	li r0, :s_data
	lih r0, ::s_data
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, :spi_data
	lih r0, ::spi_data
	in r0, (r0)
	push r0
	li r0, :text_printHex
	lih r0, ::text_printHex
	call [r0]
	addi sp, 1
	li r0, 10
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	
	halt
