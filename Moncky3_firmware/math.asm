; *****
; * This file contains the code of the Moncky-3 math library.
; * (c) 2021
; * author: Kris Demuynck
; *****

; *****
; * hardware variables (I/O)
; *****

; *****
; * global variables
; *****
:math_mult_tables
.data 0,  0,  0,  0,  0,  0,  0,   0,   0,   0,   0,   0,   0,   0,   0,   0
.data 0,  1,  2,  3,  4,  5,  6,   7,   8,   9,  10,  11,  12,  13,  14,  15
.data 0,  2,  4,  6,  8, 10, 12,  14,  16,  18,  20,  22,  24,  26,  28,  30
.data 0,  3,  6,  9, 12, 15, 18,  21,  24,  27,  30,  33,  36,  39,  42,  45
.data 0,  4,  8, 12, 16, 20, 24,  28,  32,  36,  40,  44,  48,  52,  56,  60
.data 0,  5, 10, 15, 20, 25, 30,  35,  40,  45,  50,  55,  60,  65,  70,  75
.data 0,  6, 12, 18, 24, 30, 36,  42,  48,  54,  60,  66,  72,  78,  84,  90
.data 0,  7, 14, 21, 28, 35, 42,  49,  56,  63,  70,  77,  84,  91,  98, 105
.data 0,  8, 16, 24, 32, 40, 48,  56,  64,  72,  80,  88,  96, 104, 112, 120
.data 0,  9, 18, 27, 36, 45, 54,  63,  72,  81,  90,  99, 108, 117, 126, 135
.data 0, 10, 20, 30, 40, 50, 60,  70,  80,  90, 100, 110, 120, 130, 140, 150
.data 0, 11, 22, 33, 44, 55, 66,  77,  88,  99, 110, 121, 132, 143, 154, 165
.data 0, 12, 24, 36, 48, 60, 72,  84,  96, 108, 120, 132, 144, 156, 168, 180
.data 0, 13, 26, 39, 52, 65, 78,  91, 104, 117, 130, 143, 156, 169, 182, 195
.data 0, 14, 28, 42, 56, 70, 84,  98, 112, 126, 140, 154, 168, 182, 196, 210
.data 0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180, 195, 210, 225

; ***** int16 math_multiply8signed(int8 a, int8 b)
; * multiplies two signed 8-bit integers.  The result is put in r0 (16 bit)
; *****
:math_multiply8signed

; ***** uint16 math_multiply8unsigned(uint8 a, uint8 b)
; * multiplies two unsigned 8-bit integers.  The result is an uint16 and is put in r0
; * 31 clock cycles
; *****
:math_multiply8unsigned
	li r6, :math_mult_tables
	lih r6, ::math_mult_tables
	ldi r2, (sp+3) ; a
	set r3, r2
	andi r2, 0x0F
	shri r3, 4
	andi r3, 0x0F
	ldi r4, (sp+2) ; b
	set r5, r4
	shli r4, 4
	andi r4, 0xF0
	andi r5, 0xF0
	set r0, r2 ; r2 * r4
	or r0, r4
	lda r0, (r0+r6)
	set r1, r2 ; r2 * r5
	or r1, r5
	lda r1, (r1+r6)
	shli r1, 4
	add r0, r1
	set r1, r3 ; r3 * r4
	or r1, r4
	lda r1, (r1+r6)
	shli r1, 4
	add r0, r1
	set r1, r3 ; r3 * r5
	or r1, r5
	lda r1, (r1+r6)
	shli r1, 8
	add r0, r1
	ret

; ***** int16 math_multiply16signed(int16 a, int16 b)
; * multiplies two signed 16-bit integers.  The result is put in r0
; *****
:math_multiply16signed

; ***** uint16 math_multiply16unsigned(uint16 a, uint16 b)
; * multiplies two unsigned 16-bit integers.  The result is put in r0
; * 73 clock cycles
; *****
:math_multiply16unsigned
	li r10, :math_mult_tables
	lih r10, ::math_mult_tables
	ldi r2, (sp+3) ; a
	set r3, r2
	set r4, r2
	set r5, r2
	shri r3, 4
	shri r4, 8
	shri r5, 12
	andi r2, 0x0F
	andi r3, 0x0F
	andi r4, 0x0F
	andi r5, 0x0F
	ldi r6, (sp+2) ; b
	set r7, r6
	set r8, r6
	set r9, r6
	shli r6, 4
	shri r8, 4
	shri r9, 8
	andi r6, 0xF0
	andi r7, 0xF0
	andi r8, 0xF0
	andi r9, 0xF0
	set r0, r2 ; r2 * r6
	or r0, r6
	lda r0, (r0+r10)
	set r11, r3 ; r3 * r6
	or r11, r6
	lda r11, (r11+r10)
	shli r11, 4
	add r0, r11
	set r11, r4 ; r4 * r6
	or r11, r6
	lda r11, (r11+r10)
	shli r11, 8
	add r0, r11
	set r11, r5 ; r5 * r6
	or r11, r6
	lda r11, (r11+r10)
	shli r11, 12
	add r0, r11
	set r11, r2 ; r2 * r7
	or r11, r7
	lda r11, (r11+r10)
	shli r11, 4
	add r0, r11
	set r11, r3 ; r3 * r7
	or r11, r7
	lda r11, (r11+r10)
	shli r11, 8
	add r0, r11
	set r11, r4 ; r4 * r7
	or r11, r7
	lda r11, (r11+r10)
	shli r11, 12
	add r0, r11
	set r11, r2 ; r2 * r8
	or r11, r8
	lda r11, (r11+r10)
	shli r11, 8
	add r0, r11
	set r11, r3 ; r3 * r8
	or r11, r8
	lda r11, (r11+r10)
	shli r11, 12
	add r0, r11
	set r11, r2 ; r2 * r9
	or r11, r9
	lda r11, (r11+r10)
	shli r11, 12
	add r0, r11
	ret

; ***** int32 math_multiply32signed(int32 a, int32 b)
; * multiplies two signed 32-bit integers.  The result is put in r0 (LSB) and r1 (MSB)
; *****
:math_multiply32signed

; ***** uint32 math_multiply32unsigned(uint32 a, uint32 b)
; * multiplies two unsigned 32-bit integers.  The result is put in r0 (LSB) and r1 (MSB)
; * 426 clock cycles
; *****
:math_multiply32unsigned
	ldi r4, (sp+2)  ; b LSB
	ldi r5, (sp+3)  ; b MSB
	ldi r2, (sp+4)  ; a LSB
	ldi r3, (sp+5)  ; a MSB
	li r0, 0
	li r1, 0
	li r8, 32
	li r9, :math_multiply32unsigned_loop
	lih r9, ::math_multiply32unsigned_loop
:math_multiply32unsigned_loop
	andif r4, 1
	snz r6
	set r7, r6
	and r6, r2
	and r7, r3
	add r0, r6
	addc r1, r7
	shli r2, 1
	shlci r3, 1
	shri r5, 1
	shrci r4, 1
	dec r8
	jpnz [r9]
	ret

; ***** int32 math_divideByTen16signed(int16 n)
; * divides a signed 16-bit integer by 10.  The result is put in r0 (LSB) and the rest (modulo) in r1 (MSB)
; * 36 clock cycles
; *****
; * r0 = q
; * r1 = r
; * r2 = n
:math_divideByTen16signed
	ldi r2, (sp+2) ; n
	set r3, r2
	ashri r3, 15
	andi r3, 9
	add r2, r3
	li r3, :math_divideByTen16Common
	lih r3, ::math_divideByTen16Common
	jp [r3]

; ***** uint32 math_divideByTen16unsigned(uint16 n)
; * divides an unsigned 16-bit integer by 10.  The result is put in r0 (LSB) and the rest (modulo) in r1 (MSB)
; * 29 clock cycles
; *****
:math_divideByTen16unsigned
	ldi r2, (sp+2) ; n
:math_divideByTen16Common
	set r0, r2
	shri r0, 1
	set r3, r2
	shri r3, 2
	add r0, r3
	set r3, r0
	shri r3, 4
	add r0, r3
	set r3, r0
	shri r3, 8
	add r0, r3
	shri r0, 3
	set r3, r0
	shli r3, 2
	add r3, r0
	shli r3, 1
	set r1, r2
	sub r1, r3
	set r3, r1
	addi r3, 6
	shri r3, 4
	add r0, r3
	set r4, r3
	shli r3, 2
	add r3, r4
	shli r3, 1
	sub r1, r3
	ret

; ***** uint32 math_divide8unsigned(uint8 a, uint8 b)
; * divides two unsigned 8-bit integers.  The result is put in r0 (LSB) and the remainder (modulo) in r1 (MSB)
; * max 12+16*14 = 236 clock cycles
; *****
:math_divide8unsigned

; ***** uint32 math_divide16unsigned(uint16 a, uint16 b)
; * divides two unsigned 16-bit integers.  The result is put in r0 (LSB) and the remainder (modulo) in r1 (MSB)
; * max 12+16*14 = 236 clock cycles
; *****
; r0 = result
; r1 = remainder
; r2 = a
; r3 = b
; r4 = counter
; r5 = endloop address
; r6 = loop address
:math_divide16unsigned
	ldi r2, (sp+3) ; a
	ldi r3, (sp+2) ; b
:math_divide16common
	li r1, 0
	li r0, 0
	li r4, 17
	li r5, :math_divide16unsigned_endloop
	lih r5, ::math_divide16unsigned_endloop
	li r6, :math_divide16unsigned_loop
	lih r6, ::math_divide16unsigned_loop
:math_divide16unsigned_loop
	dec r4
	jpz [r5]
	shli r1, 1
	or r2, r2
	ss r7
	andi r7, 1
	or r1, r7
	shli r2, 1
	shli r0, 1
	cmp r1, r3
	jpnc [r6]
	sub r1, r3
	ori r0, 1
	jp [r6]
:math_divide16unsigned_endloop
	ret


; ***** uint32 math_divide8signed(int8 a, int8 b)
; * divides two signed 8-bit integers.  The result is put in r0 (LSB) and the remainder (modulo) in r1 (MSB)
; * max 14+236+14 = 264 clock cycles
; *****
:math_divide8signed

; ***** uint32 math_divide16signed(int16 a, int16 b)
; * divides two signed 16-bit integers.  The result is put in r0 (LSB) and the remainder (modulo) in r1 (MSB)
; * max 14+236+14 = 264 clock cycles
; *****
:math_divide16signed
	ldi r2, (sp+3) ; a
	ldi r3, (sp+2) ; b
	or r2, r2
	li r0, :math_divide16signed_apos
	lih r0, ::math_divide16signed_apos
	jpns [r0]
	neg r2, r2
:math_divide16signed_apos
	or r3, r3
	li r0, :math_divide16signed_bpos
	lih r0, ::math_divide16signed_bpos
	jpns [r0]
	neg r3, r3
:math_divide16signed_bpos
	li r0, :math_divide16common
	lih r0, ::math_divide16common
	call [r0]
	ldi r2, (sp+3) ; a
	ldi r3, (sp+2) ; b
	or r2, r2
	li r2, :math_divide16signed_apos2
	lih r2, ::math_divide16signed_apos2
	jpns [r2]
	neg r0, r0
	neg r1, r1
:math_divide16signed_apos2
	or r3, r3
	li r2, :math_divide16signed_bpos2
	lih r2, ::math_divide16signed_bpos2
	jpns [r2]
	neg r0, r0
:math_divide16signed_bpos2
	ret


; ***** uint32 math_divide32unsigned(uint32 a, uint32 b)
; * divides two unsigned 32-bit integers.  The result is put in r0/r1.  The remainder is put in r2/r3
; * 16+32*20 = 656 clock cycles
; *****
; r0 = result (lsb)
; r1 = result (msb)
; r2 = remainder (lsb)
; r3 = remainder (msb)
; r4 = a (lsb)
; r5 = a (msb)
; r6 = b (lsb)
; r7 = b (msb)
; r8 = counter
; r9 = endloop address
; r10 = loop address
:math_divide32unsigned
	ldi r4, (sp+4) ; a
	ldi r5, (sp+5)
	ldi r6, (sp+2) ; b
	ldi r7, (sp+3)
:math_divide32common
	li r0, 0
	li r1, 0
	li r2, 0
	li r3, 0
	li r8, 33
	li r9, :math_divide32unsigned_endloop
	lih r9, ::math_divide32unsigned_endloop
	li r10, :math_divide32unsigned_loop
	lih r10, ::math_divide32unsigned_loop
:math_divide32unsigned_loop
	dec r8
	jpz [r9]
	shli r2, 1
	shlci r3, 1
	or r5, r5
	ss r11
	andi r11, 1
	or r2, r11
	shli r4, 1
	shlci r5, 1
	shli r0, 1
	shlci r1, 1
	set r11, r3
	cmp r2, r6
	subc r11, r7
	jpnc [r10]
	sub r2, r6
	subc r3, r7
	ori r0, 1
	jp [r10]
:math_divide32unsigned_endloop
	ret

; ***** uint32 math_divide32signed(int32 a, int32 b)
; * divides two signed 32-bit integers.  The result is put in r0/r1.  The remainder is put in r2/r3
; * max 23+656+23 = 702 cycles
; *****
:math_divide32signed
	ldi r4, (sp+4) ; a
	ldi r5, (sp+5)
	ldi r6, (sp+2) ; b
	ldi r7, (sp+3)
	or r5, r5
	li r0, :math_divide32signed_apos
	lih r0, ::math_divide32signed_apos
	jpns [r0]
	not r4, r4
	not r5, r5
	inc r4
	addci r5, 0
:math_divide32signed_apos
	or r7, r7
	li r0, :math_divide32signed_bpos
	lih r0, ::math_divide32signed_bpos
	jpns [r0]
	not r6, r6
	not r7, r7
	inc r6
	addci r7, 0
:math_divide32signed_bpos
	li r0, :math_divide32common
	lih r0, ::math_divide32common
	call [r0]
	ldi r5, (sp+5)	; a msb
	ldi r7, (sp+3)	; b msb
	or r5, r5
	li r8, :math_divide32signed_apos2
	lih r8, ::math_divide32signed_apos2
	jpns [r8]
	not r0, r0
	not r1, r1
	inc r0
	addci r1, 0
	not r2, r2
	not r3, r3
	inc r2
	addci r3, 0
:math_divide32signed_apos2
	or r7, r7
	li r8, :math_divide32signed_bpos2
	lih r8, ::math_divide32signed_bpos2
	jpns [r8]
	not r0, r0
	not r1, r1
	inc r0
	addci r1, 0
:math_divide32signed_bpos2
	ret


; ***** float32 math_addFloat(float32 a, float32 b)
; * adds two 32-bit floating point numbers
; * ... cycles
; *****
; a_lsb: bp+5
; a_msb: bp+6
; b_lsb: bp+3
; b_msb: bp+4
:math_addFloat
	push bp
	set bp, sp
	; if ((a & 0x7FFFFFFF) < (b & 0x7FFFFFFF)) { swap(a,b) }
	ldi r0, (bp+5)
	ldi r1, (bp+6)
	ldi r2, (bp+7)
	ldi r3, (bp+8)
	shli r1, 1
	shri r1, 1
	shli r3, 1
	shri r3, 1
	sub r0, r2
	subc r1, r3
	li r0, :math_addFloat_noswap
	lih r0, ::math_addFloat_noswap
	jpc [r0]
	ldi r0, (bp+5)
	ldi r1, (bp+6)
	ldi r2, (bp+7)
	ldi r3, (bp+8)
	xor r0, r2
	xor r1, r3
	xor r2, r0
	xor r3, r1
	xor r0, r2
	xor r1, r3
	sti r0, (bp+5)
	sti r1, (bp+6)
	sti r2, (bp+7)
	sti r3, (bp+8)
:math_addFloat_noswap
	; if (a == NAN) { return NAN }
	ldi r0, (bp+5)
	ldi r1, (bp+6)
	li r4, 0xC0
	lih r4, 0x7F
	subf r1, r4
	li r3, :math_addFloat_nonan
	lih r3, ::math_addFloat_nonan
	jpnz [r3]
	li r0, 0
	set r1, r4
	pop bp
	ret
:math_addFloat_nonan
	; if (a == POS_INF) {
	li r4, 0x80
	lih r4, 0x7F
	subf r1, r4
	li r3, :math_addFloat_noinf
	lih r3, ::math_addFloat_noinf
	jpnz [r3]
	
:math_addFloat_noinf
	; to be continued...


