; *****
; * This file contains code to demo the Moncky-3 fat library.
; * (c) 2021
; * author: Kris Demuynck
; *****

:s_press_key
.data "Insert SD card and press any key to initialise it."
:s_found
.data "Found a "
:s_card
.data " card."
:s_filename
.data "PM5644  IMG"
:s_moncky
.data "MONCKY"

:start_system
	li r0, :graphics_init
	lih r0, ::graphics_init
	call [r0]
	
	li r0, :graphics_clrscr
	lih r0, ::graphics_clrscr
	call [r0]

	li r0, :text_init
	lih r0, ::text_init
	call [r0]

	li r0, :keyboard_init
	lih r0, ::keyboard_init
	call [r0]
	
	li r0, :spi_init
	lih r0, ::spi_init
	call [r0]
	
	li r0, :s_press_key
	lih r0, ::s_press_key
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	
	li r0, 0
	push r0
	li r0, :keyboard_waitKey
	lih r0, ::keyboard_waitKey
	call [r0]
	addi sp, 1
	
	li r0, :sd_init
	lih r0, ::sd_init
	call [r0]
	
	; if (return value != 0) print error and stop
	or r0, r0
	li r1, :else
	lih r1, ::else
	jpz [r1]
	push r0
	li r0, 0xE0 ; col red
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1
	pop r0
	li r1, :sd_result_strings
	lih r1, ::sd_result_strings
	add r1, r0
	ld r0, (r1)
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	halt
:else
	li r0, :fat_init
	lih r0, ::fat_init
	call [r0]

	; if (return value != 0) print error and stop
	or r0, r0
	li r1, :else2
	lih r1, ::else2
	jpz [r1]
	push r0
	li r0, 0xE0 ; col red
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1
	pop r0
	li r1, :fat_result_strings
	lih r1, ::fat_result_strings
	add r1, r0
	ld r0, (r1)
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	halt
:else2
	li r0, :s_filename
	lih r0, ::s_filename
	push r0
	li r0, :fat_openFile
	lih r0, ::fat_openFile
	call [r0]
	addi sp, 1

	; if (return value != 0) print error and stop
	or r0, r0
	li r1, :else3
	lih r1, ::else3
	jpz [r1]
	push r0
	li r0, 0xE0 ; col red
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1
	pop r0
	li r1, :fat_result_strings
	lih r1, ::fat_result_strings
	add r1, r0
	ld r0, (r1)
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	li r0, 10 ; newline
	push r0
	li r0, :text_printChar
	lih r0, ::text_printChar
	call [r0]
	addi sp, 1
	halt
:else3
	li r10, 0 ; counter
:loop
	push r10 ; save counter
	push r10
	li r0, 1
	push r0
	li r0, :fat_readNextFileSector
	lih r0, ::fat_readNextFileSector
	call [r0]
	addi sp, 2
	pop r10 ; restore counter

	; if (return value != 0) print("MONCKY");stop;
	or r0, r0
	li r1, :else4
	lih r1, ::else4
	jpz [r1]
	li r0, 0xFF ; col white
	push r0
	li r0, :graphics_setCol
	lih r0, ::graphics_setCol
	call [r0]
	addi sp, 1
	li r0, 17
	push r0
	li r0, 4
	push r0
	li r0, :text_setCursor
	lih r0, ::text_setCursor
	call [r0]
	addi sp, 2
	li r0, :s_moncky
	lih r0, ::s_moncky
	push r0
	li r0, :text_printString
	lih r0, ::text_printString
	call [r0]
	addi sp, 1
	halt
:else4
	li r1, 0x00
	lih r1, 0x01
	add r10, r1
	li r1, :loop
	lih r1, ::loop
	jp [r1]
	halt

