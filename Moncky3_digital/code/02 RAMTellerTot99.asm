	li r0, :main
	lih r0, ::main
	jp [r0]

:digits
.data 0x3F, 0x06, 0x5D, 0x4F, 0x66, 0x6B, 0x7B, 0x0E
.data 0x7F, 0x6F, 0x7E, 0x73, 0x39, 0x57, 0x79, 0x78

:main
	li r4, :digits
	lih r4, ::digits
	li r0, 0
:lus0	cmpi r0, 10
	li r2, :verder0
	jpns [r2]
	li r1, 0
:lus1	cmpi r1, 10
	li r2, :verder1
	jpns [r2]
	lda r2, (r4+r0)
	shli r2, 8
	lda r3, (r4+r1)
	or r2, r3
	set r13, r2
	addi r1, 1
	jpi [:lus1]
:verder1 addi r0, 1
	jpi [:lus0]
:verder0 halt

