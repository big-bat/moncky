	li sp, 0xFF
	lih sp, 0xBF
	li r0, :main
	lih r0, ::main
	ei
	jp [r0]

.org	0x0010
:interrupt
	push r0
	push r1
	push r11
	sflags r11
	
	li r1, :counter
	lih r1, ::counter
	ld r0, (r1)
	addi r0, 1
	st r0, (r1)
	li r1, :showregister
	lih r1, ::showregister
	call r1

	rflags r11
	pop r11
	pop r1
	pop r0
	reti

:digits
.data 0x3F, 0x06, 0x5D, 0x4F, 0x66, 0x6B, 0x7B, 0x0E
.data 0x7F, 0x6F, 0x7E, 0x73, 0x39, 0x57, 0x79, 0x78
	
:showregister ; r0 contains number
	push r0
	push r1
	push r2
	push r3
	li r3, :digits
	lih r3, ::digits
	set r2, r0
	andi r2, 0xF
	lda r13, (r3+r2)
	shri r0, 4
	set r2, r0
	andi r2, 0xF
	lda r1, (r3+r2)
	shli r1, 8
	or r13, r1
	shri r0, 4
	set r2, r0
	andi r2, 0xF
	lda r12, (r3+r2)
	shri r0, 4
	set r2, r0
	andi r2, 0xF
	lda r1, (r3+r2)
	shli r1, 8
	or r12, r1
	pop r3
	pop r2
	pop r1
	pop r0
	ret
:main
	li r2, :counter
	lih r2, ::counter
	li r0, 0
	st r0, (r2)
	li r1, :showregister
	lih r1, ::showregister
	call r1
:loop
	li r0, 0xFF
	lih r0, 0x7F
	addi r0, 5
	li r1, :loop
	lih r1, ::loop
	jp r1
	halt

.def	:counter 0x4100
