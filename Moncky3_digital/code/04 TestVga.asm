	di
	li sp, 0xFF
	lih sp, 0xBF
	li r0, :main
	lih r0, ::main
	jp [r0]
	
.org 0x10
	reti

; ***** clrscr(buffer, col)
; * clears the specified buffer by replacing all memory locations in that area by col
; *****
:clrscr
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	; determine start of video (r0) and length (r1) to be cleared
	ldi r0, (bp+4) ; buffer
	andi r0, 1
	shli r0, 15
	li r1, 0xFF
	lih r1, 0x7C
	ldi r2, (bp+3) ; col
	li r3, :clrscr_loop
	lih r3, ::clrscr_loop
	add r0, r1
:clrscr_loop
	out r2, (r0)
	dec r0
	dec r1
	jpns [r3]
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

:plot	; plot(x,y,col)
	push r0
	push r1
	push r2
	push r3
	ldi r0, (sp+8) ; x
	ldi r1, (sp+7) ; y
	
	; clipping
	li r2, :returnPlot
	lih r2, ::returnPlot
	nopf r0, r0
	jps [r2]
	nopf r1, r1
	jps  [r2]
	li r3, 160
	shli r3, 1
	cmp r0, r3
	jpns [r2]
	li r3, 200
	cmp r1, r3
	jpns [r2]
	
	; find memory address * 2 (r0)
	set r2, r1
	shli r1, 6
	shli r2, 8
	add r1, r2
	add r0, r1
	
	; find mask
	set r2, r0
	shri r0, 1 ; r0 = memory address
	andi r2, 1
	shli r2, 3
	li r1, 0xFF
	shl r1, r2 ; r1 = mask
	not r1, r1 ; r1 = inverse mask
	
	in r3, (r0)
	and r3, r1
	ldi r1, (sp+6) ; col
	andi r1, 0xFF
	shl r1, r2
	or r3, r1
	out r3, (r0)
:returnPlot
	pop r3
	pop r2
	pop r1
	pop r0
	ret
:line	; line(x0, y0, x1, x2, col)
	push r0
	li r0, 3
	add r0, sp ; r0 points to parameters
	push r1
	push r2
	push r3
	push r4
	push r5
	push r6
	push r7
	push r8
	push r9
	push r10
	push r11
	ldi r1, (r0+4) ; x0
	ldi r2, (r0+3) ; y0
	ldi r3, (r0+2) ; x1
	ldi r4, (r0+1) ; x2
	ldi r5, (r0+0) ; col
	
	set r6, r3
	sub r6, r1 ; dx = x1 - x0
	li r0, :dx
	lih r0, ::dx
	li r7, 0
	jpz r0
	li r7, 1
	jpns r0
	neg r7, r7
	neg r6, r6
:dx
	set r8, r4
	sub r8, r2 ; dy = y1 - y0
	li r0, :dy
	lih r0, ::dy
	li r9, 0
	jpz r0
	li r9, 0xFF
	lih r9, 0xFF
	jps r0
	li r9, 1
	neg r8, r8
:dy
	set r10, r6 ; err = dx + dy
	add r10, r8
:loop	
	cmp r1, r3
	sz r0
	cmp r2, r4
	sz r11
	and r0, r11
	li r0, :end
	lih r0, ::end
	jpnz r0

	push r1
	push r2
	push r5
	li r0, :plot
	lih r0, ::plot
	call [r0]
	addi sp, 3

	set r11, r10
	shli r11, 1
	
	cmp r11, r8
	li r0, :else1
	lih r0, ::else1
	jps r0
	add r10, r8
	add r1, r7
:else1	cmp r6, r11
	li r0, :else2
	lih r0, ::else2
	jps r0
	add r10, r6
	add r2, r9
:else2	li r0, :loop
	lih r0, ::loop
	jp r0
:end	push r1
	push r2
	push r5
	li r0, :plot
	lih r0, ::plot
	call [r0]
	addi sp, 3
	
	pop r11
	pop r10
	pop r9
	pop r8
	pop r7
	pop r6
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	ret
:main
	; setBuffer(0)
	li r0, 0x00
	lih r0, 0x7D
	li r1, 0
	out r1, (r0)
	
	; clrscr(0, 0x0303)
	li r1, 0
	push r1
	li r1, 0x03
	lih r1, 0x03
	push r1
	li r1, :clrscr
	lih r1, ::clrscr
	call [r1]
	addi sp, 2
	
	; plot(10, 10, 0xE0)
	li r0, 10
	push r0
	push r0
	li r0, 0xE0
	push r0
	li r0, :plot
	lih r0, ::plot
	call [r0]
	addi sp, 3
	
	; line(10, 0, 70, 30, 0xE0)
	li r0, 10 ; x0
	push r0
	li r0, 0 ; y0
	push r0
	li r0, 70 ; x1
	push r0
	li r0, 30 ; y1
	push r0
	li r0, 0xE0 ; col
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 5
	
	; line(10, 30, 140, 10, 0x1C)
	li r0, 10 ; x0
	push r0
	li r0, 30 ; y0
	push r0
	li r0, 140 ; x1
	push r0
	li r0, 10 ; y1
	push r0
	li r0, 0x1C
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 5
	
	; line(30, 10, 10, 70, 0x02)
	li r0, 30 ; x0
	push r0
	li r0, 10 ; y0
	push r0
	li r0, 10 ; x1
	push r0
	li r0, 70 ; y1
	push r0
	li r0, 0x02 ; col
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 5
	
	halt

