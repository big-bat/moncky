; *****
; * constants
; *****
.def	:screenMode 0x3F40
.def	:startOfStack 0xBFFF

; *****
; * variables
; *****
.def	:cursorPosition 0x3FFF

; *****
; * start of code
; *****
.org 0x0000
	di
	; jump to start
	li r0, :start_system
	lih r0, ::start_system
	jp [r0]
	
; *****
; * interrupt handler
; *****
.org 0x0010
	push r0
	push r1
	push r2
	sflags r2
	
	li r0, :screenMode
	lih r0, ::screenMode
	in r1, (r0)
	inc r1
	andi r1, 3
	out r1, (r0)
	
	rflags r2
	pop r2
	pop r1
	pop r0
	reti

; *****
; * 8x8 font for all ASCII characters
; * based on Amstrad CPC 464 font
; *****
:ascii
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0000 (nul)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0001
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0002
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0003
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0004
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0005
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0006
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0007
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0008
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0009
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000A
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000B
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000C
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000D
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000E
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000F
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0010
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0011
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0012
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0013
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0014
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0015
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0016
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0017
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0018
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0019
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001A
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001B
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001C
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001D
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001E
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001F
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0020 (space)
        .data 0x1818, 0x1818, 0x1800, 0x1800 ; U+0021 (!)
        .data 0x6C6C, 0x6C00, 0x0000, 0x0000 ; U+0022 (")
        .data 0x6C6C, 0xFE6C, 0xFE6C, 0x6C00 ; U+0023 (#)
        .data 0x183E, 0x583C, 0x1A7C, 0x1800 ; U+0024 ($)
        .data 0x00C6, 0xCC18, 0x3066, 0xC600 ; U+0025 (%)
        .data 0x386C, 0x3876, 0xDCCC, 0x7600 ; U+0026 (&)
        .data 0x1818, 0x3000, 0x0000, 0x0000 ; U+0027 (')
        .data 0x0C18, 0x3030, 0x3018, 0x0C00 ; U+0028 (()
        .data 0x3018, 0x0C0C, 0x0C18, 0x3000 ; U+0029 ())
        .data 0x0066, 0x3CFF, 0x3C66, 0x0000 ; U+002A (*)
        .data 0x0018, 0x187E, 0x1818, 0x0000 ; U+002B (+)
        .data 0x0000, 0x0000, 0x0018, 0x1830 ; U+002C (,)
        .data 0x0000, 0x007E, 0x0000, 0x0000 ; U+002D (-)
        .data 0x0000, 0x0000, 0x0018, 0x1800 ; U+002E (.)
        .data 0x060C, 0x1830, 0x60C0, 0x8000 ; U+002F (/)
        .data 0x7CC6, 0xCED6, 0xE6C6, 0x7C00 ; U+0030 (0)
        .data 0x1838, 0x1818, 0x1818, 0x7E00 ; U+0031 (1)
        .data 0x3C66, 0x063C, 0x6066, 0x7E00 ; U+0032 (2)
        .data 0x3C66, 0x061C, 0x0666, 0x3C00 ; U+0033 (3)
        .data 0x1C3C, 0x6CCC, 0xFE0C, 0x1E00 ; U+0034 (4)
        .data 0x7E62, 0x607C, 0x0666, 0x3C00 ; U+0035 (5)
        .data 0x3C66, 0x607C, 0x6666, 0x3C00 ; U+0036 (6)
        .data 0x7E66, 0x060C, 0x1818, 0x1800 ; U+0037 (7)
        .data 0x3C66, 0x663C, 0x6666, 0x3C00 ; U+0038 (8)
        .data 0x3C66, 0x663E, 0x0666, 0x3C00 ; U+0039 (9)
        .data 0x0000, 0x1818, 0x0018, 0x1800 ; U+003A (:)
        .data 0x0000, 0x1818, 0x0018, 0x1830 ; U+003B (;)
        .data 0x0C18, 0x3060, 0x3018, 0x0C00 ; U+003C (<)
        .data 0x0000, 0x7E00, 0x007E, 0x0000 ; U+003D (=)
        .data 0x6030, 0x180C, 0x1830, 0x6000 ; U+003E (>)
        .data 0x3C66, 0x660C, 0x1800, 0x1800 ; U+003F (?)
        .data 0x7CC6, 0xDEDE, 0xDEC0, 0x7C00 ; U+0040 (@)
        .data 0x183C, 0x6666, 0x7E66, 0x6600 ; U+0041 (A)
        .data 0xFC66, 0x667C, 0x6666, 0xFC00 ; U+0042 (B)
        .data 0x3C66, 0xC0C0, 0xC066, 0x3C00 ; U+0043 (C)
        .data 0xF86C, 0x6666, 0x666C, 0xF800 ; U+0044 (D)
        .data 0xFE62, 0x6878, 0x6862, 0xFE00 ; U+0045 (E)
        .data 0xFE62, 0x6878, 0x6860, 0xF000 ; U+0046 (F)
        .data 0x3C66, 0xC0C0, 0xCE66, 0x3E00 ; U+0047 (G)
        .data 0x6666, 0x667E, 0x6666, 0x6600 ; U+0048 (H)
        .data 0x7E18, 0x1818, 0x1818, 0x7E00 ; U+0049 (I)
        .data 0x1E0C, 0x0C0C, 0xCCCC, 0x7800 ; U+004A (J)
        .data 0xE666, 0x6C78, 0x6C66, 0xE600 ; U+004B (K)
        .data 0xF060, 0x6060, 0x6266, 0xFE00 ; U+004C (L)
        .data 0xC6EE, 0xFEFE, 0xD6C6, 0xC600 ; U+004D (M)
        .data 0xC6E6, 0xF6DE, 0xCEC6, 0xC600 ; U+004E (N)
        .data 0x386C, 0xC6C6, 0xC66C, 0x3800 ; U+004F (O)
        .data 0xFC66, 0x667C, 0x6060, 0xF000 ; U+0050 (P)
        .data 0x386C, 0xC6C6, 0xDACC, 0x7600 ; U+0051 (Q)
        .data 0xFC66, 0x667C, 0x6C66, 0xE600 ; U+0052 (R)
        .data 0x3C66, 0x603C, 0x0666, 0x3C00 ; U+0053 (S)
        .data 0x7E5A, 0x1818, 0x1818, 0x3C00 ; U+0054 (T)
        .data 0x6666, 0x6666, 0x6666, 0x3C00 ; U+0055 (U)
        .data 0x6666, 0x6666, 0x663C, 0x1800 ; U+0056 (V)
        .data 0xC6C6, 0xC6D6, 0xFEEE, 0xC600 ; U+0057 (W)
        .data 0xC66C, 0x3838, 0x6CC6, 0xC600 ; U+0058 (X)
        .data 0x6666, 0x663C, 0x1818, 0x3C00 ; U+0059 (Y)
        .data 0xFEC6, 0x8C18, 0x3266, 0xFE00 ; U+005A (Z)
        .data 0x3C30, 0x3030, 0x3030, 0x3C00 ; U+005B ([)
        .data 0xC060, 0x3018, 0x0C06, 0x0200 ; U+005C (\)
        .data 0x3C0C, 0x0C0C, 0x0C0C, 0x3C00 ; U+005D (])
        .data 0x183C, 0x7E18, 0x1818, 0x1800 ; U+005E (^)
        .data 0x0000, 0x0000, 0x0000, 0x00FF ; U+005F (_)
        .data 0x3018, 0x0C00, 0x0000, 0x0000 ; U+0060 (`)
        .data 0x0000, 0x780C, 0x7CCC, 0x7600 ; U+0061 (a)
        .data 0xE060, 0x7C66, 0x6666, 0xDC00 ; U+0062 (b)
        .data 0x0000, 0x3C66, 0x6066, 0x3C00 ; U+0063 (c)
        .data 0x1C0C, 0x7CCC, 0xCCCC, 0x7600 ; U+0064 (d)
        .data 0x0000, 0x3C66, 0x7E60, 0x3C00 ; U+0065 (e)
        .data 0x1C36, 0x3078, 0x3030, 0x7800 ; U+0066 (f)
        .data 0x0000, 0x3E66, 0x663E, 0x067C ; U+0067 (g)
        .data 0xE060, 0x6C76, 0x6666, 0xE600 ; U+0068 (h)
        .data 0x1800, 0x3818, 0x1818, 0x3C00 ; U+0069 (i)
        .data 0x0600, 0x0E06, 0x0666, 0x663C ; U+006A (j)
        .data 0xE060, 0x666C, 0x786C, 0xE600 ; U+006B (k)
        .data 0x3818, 0x1818, 0x1818, 0x3C00 ; U+006C (l)
        .data 0x0000, 0x6CFE, 0xD6D6, 0xC600 ; U+006D (m)
        .data 0x0000, 0xDC66, 0x6666, 0x6600 ; U+006E (n)
        .data 0x0000, 0x3C66, 0x6666, 0x3C00 ; U+006F (o)
        .data 0x0000, 0xDC66, 0x667C, 0x60F0 ; U+0070 (p)
        .data 0x0000, 0x76CC, 0xCC7C, 0x0C1E ; U+0071 (q)
        .data 0x0000, 0xDC60, 0x6060, 0xF000 ; U+0072 (r)
        .data 0x0000, 0x3C60, 0x3C06, 0x7C00 ; U+0073 (s)
        .data 0x3030, 0x7C30, 0x3036, 0x1C00 ; U+0074 (t)
        .data 0x0000, 0x6666, 0x6666, 0x3E00 ; U+0075 (u)
        .data 0x0000, 0x6666, 0x663C, 0x1800 ; U+0076 (v)
        .data 0x0000, 0xC6D6, 0xD6FE, 0x6C00 ; U+0077 (w)
        .data 0x0000, 0xC66C, 0x386C, 0xC600 ; U+0078 (x)
        .data 0x0000, 0x6666, 0x663E, 0x067C ; U+0079 (y)
        .data 0x0000, 0x7E4C, 0x1832, 0x7E00 ; U+007A (z)
        .data 0x0E18, 0x1870, 0x1818, 0x0E00 ; U+007B ({)
        .data 0x1818, 0x1818, 0x1818, 0x1800 ; U+007C (|)
        .data 0x7018, 0x180E, 0x1818, 0x7000 ; U+007D (})
        .data 0x76DC, 0x0000, 0x0000, 0x0000 ; U+007E (~)
        .data 0xCC33, 0xCC33, 0xCC33, 0xCC33 ; U+007F

; ***** clrscr(buffer, col)
; * clears the specified buffer by replacing all memory locations in that area by col
; *****
:clrscr
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	; determine start (r0) of video and length (r1) to be cleared
	li r0, 0
	li r1, 0x40
	lih r1, 0x1F
	li r2, :screenMode
	lih r2, ::screenMode
	in r2, (r2) ; screenMode
	andi r2, 2
	shri r2, 1
	shl r1, r2
	dec r1
	ldi r2, (bp+4) ; buffer
	cmpi r2, 1
	li r3, :clrscr_buffer0
	lih r3, ::clrscr_buffer0
	jpnz [r3]
	li r2, 0x00
	lih r2, 0x20
	add r0, r2
:clrscr_buffer0
	ldi r2, (bp+3) ; col
	li r3, :clrscr_loop
	lih r3, ::clrscr_loop
:clrscr_loop
	add r0, r1
	out r2, (r0)
	sub r0, r1
	dec r1
	jpnz [r3]
	out r2, (r0)
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	out r2, (r0)
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

:plot_xRes
.data 160, 160, 160, 160
:plot_yRes
.data 50, 100, 100, 400
:plot_shiftValues1
.data 0, 1, 0, 4
:plot_shiftValues2
.data 0, 0, 0, 2

; ***** plot(x,y,col)	
; * plots a pixel at location (x,y) with the given colour
; *****
:plot
	push r0
	push r1
	push r2
	push r3
	push r4
	li r4, :screenMode
	lih r4, ::screenMode
	in r4, (r4) ; screenMode
	andi r4, 3
	ldi r0, (sp+9) ; x
	ldi r1, (sp+8) ; y
	
	; clipping
	li r2, :plot_return
	lih r2, ::plot_return
	nopf r0, r0
	jps [r2]
	nopf r1, r1
	jps [r2]
	li r3, :plot_xRes
	lih r3, ::plot_xRes
	lda r3, (r3+r4)
	cmp r0, r3
	jpns [r2]
	li r3, :plot_yRes
	lih r3, ::plot_yRes
	lda r3, (r3+r4)
	cmp r1, r3
	jpns [r2]
	
	; find memory location
	set r2, r1
	shli r1, 5
	shli r2, 7
	add r1, r2
	li r2, :plot_shiftValues2
	lih r2, ::plot_shiftValues2
	lda r2, (r2+r4)
	shl r1, r2
	add r0, r1
	li r2, :plot_shiftValues1
	lih r2, ::plot_shiftValues1
	lda r2, (r2+r4)
	shr r0, r2

	andif r4, 1
	li r1, :plot_notMode0or2
	lih r1, ::plot_notMode0or2
	jpnz [r1]

	; store col in memory (16 bits)	
	ldi r1, (sp+7) ; col
	out r1, (r0)
	
	li r0, :plot_return
	lih r0, ::plot_return
	jp [r0]

:plot_notMode0or2
	andif r4, 2
	li r1, :plot_mode3
	lih r1, ::plot_mode3
	jpnz [r1]
	
	ldi r1, (sp+9) ; x
	andi r1, 1
	shli r1, 3
	li r3, 0xFF ; mask: 8 bits per pixel
	ldi r2, (sp+7) ; col
	and r2, r3
	shl r2, r1
	shl r3, r1
	not r3, r3
	in r1, (r0)
	and r1, r3
	or r1, r2
	out r1, (r0)
	
	li r0, :plot_return
	lih r0, ::plot_return
	jp [r0]
	
:plot_mode3
	ldi r1, (sp+9) ; x
	andi r1, 15
	li r3, 1 ; mask: 1 bit per pixel
	ldi r2, (sp+7) ; col
	and r2, r3
	shl r2, r1
	shl r3, r1
	not r3, r3
	in r1, (r0)
	and r1, r3
	or r1, r2
	out r1, (r0)

:plot_return
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	ret

; ***** display_char(x, y, char, col)
; * draws a character (0-127) at the given (x,y) coordinates
; *****
; r0 points to bit pattern
; r1 counter (4 words)
; r2 bit pattern
; r3 bitMask
; r4 temp
; r5 points to plot function
:display_char
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	push r5
	
	; prepare stack for plot(x,y,col)
	ldi r0, (bp+6) ; x
	push r0
	ldi r0, (bp+5) ; y
	push r0
	li r0, 0	; placeholder for col
	push r0
	li r5, :plot
	lih r5, ::plot
	
	ldi r0, (bp+4) ; char
	shli r0, 2	; times 4
	li r1, :ascii
	lih r1, ::ascii
	add r0, r1	; r0 points now to the start of the bit patterns
	li r1, 4	; r1 = counter: 4 words
:display_char_nextPattern
	ld r2, (r0)	; r2 contains the bit pattern
	li r3, 0	; r3 = bitMask
	lih r3, 0x80
:display_char_nextPixel
	li r4, :display_char_noPixel
	lih r4, ::display_char_noPixel
	andf r2, r3
	jpz r4
	ldi r4, (bp+3) ; col
	sti r4, (sp+1)
	call [r5]
:display_char_noPixel
	ldi r4, (sp+3) ; inc(x)
	inc r4
	sti r4, (sp+3)
	shri r3, 1
	li r4, 128
	cmp r3, r4
	li r4, :display_char_sameRow
	lih r4, ::display_char_sameRow
	jpnz [r4]
	ldi r4, (sp+3) ; x = x - 8
	subi r4, 8
	sti r4, (sp+3)
	ldi r4, (sp+2) ; y++
	inc r4
	sti r4, (sp+2)
:display_char_sameRow
	cmpi r3, 0
	li r4, :display_char_nextPixel
	lih r4, ::display_char_nextPixel
	jpnz [r4]
	ldi r4, (sp+3) ; x = x - 8
	subi r4, 8
	sti r4, (sp+3)
	ldi r4, (sp+2) ; y++
	inc r4
	sti r4, (sp+2)
	inc r0
	li r4, :display_char_nextPattern
	lih r4, ::display_char_nextPattern
	dec r1
	jpnz [r4]
	
	addi sp, 3
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

:start_system
	; initialize stack
	li sp, :startOfStack
	lih sp, ::startOfStack
	; set graphics mode
	li r0, :screenMode
	lih r0, ::screenMode
	li r1, 1
	out r1, (r0)
	ei
	
:redraw
	; clrscr(0, 0x00)
	li r0, 0
	push r0
	li r0, 0x00
	lih r0, 0x0
	push r0
	li r0, :clrscr
	lih r0, ::clrscr
	call [r0]
	addi sp, 2

	li r0, 0
	push r0
	li r0, 0
	push r0	
	li r0, 'H'
	push r0
	li r0, 0xF
	lih r0, 0x00
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4
	
	li r0, 8
	push r0
	li r0, 0
	push r0	
	li r0, 'e'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4
	
	li r0, 16
	push r0
	li r0, 0
	push r0	
	li r0, 'l'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4

	li r0, 24
	push r0
	li r0, 0
	push r0	
	li r0, 'l'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4

	li r0, 32
	push r0
	li r0, 0
	push r0	
	li r0, 'o'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4
	
	li r0, 40
	push r0
	li r0, 0
	push r0	
	li r0, 44
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4
	
	li r0, 0
	push r0
	li r0, 8
	push r0	
	li r0, 'W'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4
	
	li r0, 8
	push r0
	li r0, 8
	push r0	
	li r0, 'o'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4
	
	li r0, 16
	push r0
	li r0, 8
	push r0	
	li r0, 'r'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4
	
	li r0, 24
	push r0
	li r0, 8
	push r0	
	li r0, 'l'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4

	li r0, 32
	push r0
	li r0, 8
	push r0	
	li r0, 'd'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4
	
	li r0, 40
	push r0
	li r0, 8
	push r0	
	li r0, '!'
	push r0
	li r0, 0xFF
	lih r0, 0xFF
	push r0
	li r0, :display_char
	lih r0, ::display_char
	call [r0]
	addi sp, 4

	li r1, :screenMode
	lih r1, ::screenMode
	in r2, (r1)
	li r0, :redraw
	lih r0, ::redraw
	li r4, :loop
	lih r4, ::loop

:loop
	in r3, (r1)
	cmp r2, r3
	jpnz [r0]	
	
	jp [r4]

