; *****
; * This file is used to do experiments and develop the firmware for the Moncky-3 processor.
; * author: Kris Demuynck

; *****
; * constants
; *****
.def	:startOfStack 0xFFFF

; *****
; * variables (I/O)
; *****
.def	:screenConfig 0x7D00
.def	:keyboardLastKey 0x7D01
.def	:cursorPosition 0x7D02 ; LSB = X, MSB = Y
.def	:shiftPressed 0x7D03
.def	:bgCol 0x7D04
.def	:savedCursor 0x7D05 ; until 0x7D25
.def	:spiData 0x7E00
.def	:spiControl 0x7E01
.def	:usecLSB 0x7E02
.def	:usecMSB 0x7E03
.def	:msecLSB 0x7E04
.def	:msecMSB 0x7E05

; *****
; * start of code
; *****
.org 0x0000
	di
	; jump to start
	li r0, :start_system
	lih r0, ::start_system
	jp [r0]
	
; *****
; * interrupt handler
; *****
.org 0x0010
	reti

; *****
; * 8x8 font for all ASCII characters
; * based on Amstrad CPC 464 font
; *****
:font
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0000 (nul)
        .data 0x6CFE, 0xFEFE, 0x7C38, 0x1000 ; U+0001 (heart)
        .data 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF ; U+0002 (full, cursor)
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0003
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0004
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0005
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0006
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0007
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0008
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0009
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000A
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000B
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000C
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000D
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000E
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+000F
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0010
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0011
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0012
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0013
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0014
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0015
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0016
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0017
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0018
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0019
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001A
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001B
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001C
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001D
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001E
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+001F
        .data 0x0000, 0x0000, 0x0000, 0x0000 ; U+0020 (space)
        .data 0x1818, 0x1818, 0x1800, 0x1800 ; U+0021 (!)
        .data 0x6C6C, 0x6C00, 0x0000, 0x0000 ; U+0022 (")
        .data 0x6C6C, 0xFE6C, 0xFE6C, 0x6C00 ; U+0023 (#)
        .data 0x183E, 0x583C, 0x1A7C, 0x1800 ; U+0024 ($)
        .data 0x00C6, 0xCC18, 0x3066, 0xC600 ; U+0025 (%)
        .data 0x386C, 0x3876, 0xDCCC, 0x7600 ; U+0026 (&)
        .data 0x1818, 0x3000, 0x0000, 0x0000 ; U+0027 (')
        .data 0x0C18, 0x3030, 0x3018, 0x0C00 ; U+0028 (()
        .data 0x3018, 0x0C0C, 0x0C18, 0x3000 ; U+0029 ())
        .data 0x0066, 0x3CFF, 0x3C66, 0x0000 ; U+002A (*)
        .data 0x0018, 0x187E, 0x1818, 0x0000 ; U+002B (+)
        .data 0x0000, 0x0000, 0x0018, 0x1830 ; U+002C (,)
        .data 0x0000, 0x007E, 0x0000, 0x0000 ; U+002D (-)
        .data 0x0000, 0x0000, 0x0018, 0x1800 ; U+002E (.)
        .data 0x060C, 0x1830, 0x60C0, 0x8000 ; U+002F (/)
        .data 0x7CC6, 0xCED6, 0xE6C6, 0x7C00 ; U+0030 (0)
        .data 0x1838, 0x1818, 0x1818, 0x7E00 ; U+0031 (1)
        .data 0x3C66, 0x063C, 0x6066, 0x7E00 ; U+0032 (2)
        .data 0x3C66, 0x061C, 0x0666, 0x3C00 ; U+0033 (3)
        .data 0x1C3C, 0x6CCC, 0xFE0C, 0x1E00 ; U+0034 (4)
        .data 0x7E62, 0x607C, 0x0666, 0x3C00 ; U+0035 (5)
        .data 0x3C66, 0x607C, 0x6666, 0x3C00 ; U+0036 (6)
        .data 0x7E66, 0x060C, 0x1818, 0x1800 ; U+0037 (7)
        .data 0x3C66, 0x663C, 0x6666, 0x3C00 ; U+0038 (8)
        .data 0x3C66, 0x663E, 0x0666, 0x3C00 ; U+0039 (9)
        .data 0x0000, 0x1818, 0x0018, 0x1800 ; U+003A (:)
        .data 0x0000, 0x1818, 0x0018, 0x1830 ; U+003B (;)
        .data 0x0C18, 0x3060, 0x3018, 0x0C00 ; U+003C (<)
        .data 0x0000, 0x7E00, 0x007E, 0x0000 ; U+003D (=)
        .data 0x6030, 0x180C, 0x1830, 0x6000 ; U+003E (>)
        .data 0x3C66, 0x660C, 0x1800, 0x1800 ; U+003F (?)
        .data 0x7CC6, 0xDEDE, 0xDEC0, 0x7C00 ; U+0040 (@)
        .data 0x183C, 0x6666, 0x7E66, 0x6600 ; U+0041 (A)
        .data 0xFC66, 0x667C, 0x6666, 0xFC00 ; U+0042 (B)
        .data 0x3C66, 0xC0C0, 0xC066, 0x3C00 ; U+0043 (C)
        .data 0xF86C, 0x6666, 0x666C, 0xF800 ; U+0044 (D)
        .data 0xFE62, 0x6878, 0x6862, 0xFE00 ; U+0045 (E)
        .data 0xFE62, 0x6878, 0x6860, 0xF000 ; U+0046 (F)
        .data 0x3C66, 0xC0C0, 0xCE66, 0x3E00 ; U+0047 (G)
        .data 0x6666, 0x667E, 0x6666, 0x6600 ; U+0048 (H)
        .data 0x7E18, 0x1818, 0x1818, 0x7E00 ; U+0049 (I)
        .data 0x1E0C, 0x0C0C, 0xCCCC, 0x7800 ; U+004A (J)
        .data 0xE666, 0x6C78, 0x6C66, 0xE600 ; U+004B (K)
        .data 0xF060, 0x6060, 0x6266, 0xFE00 ; U+004C (L)
        .data 0xC6EE, 0xFEFE, 0xD6C6, 0xC600 ; U+004D (M)
        .data 0xC6E6, 0xF6DE, 0xCEC6, 0xC600 ; U+004E (N)
        .data 0x386C, 0xC6C6, 0xC66C, 0x3800 ; U+004F (O)
        .data 0xFC66, 0x667C, 0x6060, 0xF000 ; U+0050 (P)
        .data 0x386C, 0xC6C6, 0xDACC, 0x7600 ; U+0051 (Q)
        .data 0xFC66, 0x667C, 0x6C66, 0xE600 ; U+0052 (R)
        .data 0x3C66, 0x603C, 0x0666, 0x3C00 ; U+0053 (S)
        .data 0x7E5A, 0x1818, 0x1818, 0x3C00 ; U+0054 (T)
        .data 0x6666, 0x6666, 0x6666, 0x3C00 ; U+0055 (U)
        .data 0x6666, 0x6666, 0x663C, 0x1800 ; U+0056 (V)
        .data 0xC6C6, 0xC6D6, 0xFEEE, 0xC600 ; U+0057 (W)
        .data 0xC66C, 0x3838, 0x6CC6, 0xC600 ; U+0058 (X)
        .data 0x6666, 0x663C, 0x1818, 0x3C00 ; U+0059 (Y)
        .data 0xFEC6, 0x8C18, 0x3266, 0xFE00 ; U+005A (Z)
        .data 0x3C30, 0x3030, 0x3030, 0x3C00 ; U+005B ([)
        .data 0xC060, 0x3018, 0x0C06, 0x0200 ; U+005C (\)
        .data 0x3C0C, 0x0C0C, 0x0C0C, 0x3C00 ; U+005D (])
        .data 0x183C, 0x7E18, 0x1818, 0x1800 ; U+005E (^)
        .data 0x0000, 0x0000, 0x0000, 0x00FF ; U+005F (_)
        .data 0x3018, 0x0C00, 0x0000, 0x0000 ; U+0060 (`)
        .data 0x0000, 0x780C, 0x7CCC, 0x7600 ; U+0061 (a)
        .data 0xE060, 0x7C66, 0x6666, 0xDC00 ; U+0062 (b)
        .data 0x0000, 0x3C66, 0x6066, 0x3C00 ; U+0063 (c)
        .data 0x1C0C, 0x7CCC, 0xCCCC, 0x7600 ; U+0064 (d)
        .data 0x0000, 0x3C66, 0x7E60, 0x3C00 ; U+0065 (e)
        .data 0x1C36, 0x3078, 0x3030, 0x7800 ; U+0066 (f)
        .data 0x0000, 0x3E66, 0x663E, 0x067C ; U+0067 (g)
        .data 0xE060, 0x6C76, 0x6666, 0xE600 ; U+0068 (h)
        .data 0x1800, 0x3818, 0x1818, 0x3C00 ; U+0069 (i)
        .data 0x0600, 0x0E06, 0x0666, 0x663C ; U+006A (j)
        .data 0xE060, 0x666C, 0x786C, 0xE600 ; U+006B (k)
        .data 0x3818, 0x1818, 0x1818, 0x3C00 ; U+006C (l)
        .data 0x0000, 0x6CFE, 0xD6D6, 0xC600 ; U+006D (m)
        .data 0x0000, 0xDC66, 0x6666, 0x6600 ; U+006E (n)
        .data 0x0000, 0x3C66, 0x6666, 0x3C00 ; U+006F (o)
        .data 0x0000, 0xDC66, 0x667C, 0x60F0 ; U+0070 (p)
        .data 0x0000, 0x76CC, 0xCC7C, 0x0C1E ; U+0071 (q)
        .data 0x0000, 0xDC60, 0x6060, 0xF000 ; U+0072 (r)
        .data 0x0000, 0x3C60, 0x3C06, 0x7C00 ; U+0073 (s)
        .data 0x3030, 0x7C30, 0x3036, 0x1C00 ; U+0074 (t)
        .data 0x0000, 0x6666, 0x6666, 0x3E00 ; U+0075 (u)
        .data 0x0000, 0x6666, 0x663C, 0x1800 ; U+0076 (v)
        .data 0x0000, 0xC6D6, 0xD6FE, 0x6C00 ; U+0077 (w)
        .data 0x0000, 0xC66C, 0x386C, 0xC600 ; U+0078 (x)
        .data 0x0000, 0x6666, 0x663E, 0x067C ; U+0079 (y)
        .data 0x0000, 0x7E4C, 0x1832, 0x7E00 ; U+007A (z)
        .data 0x0E18, 0x1870, 0x1818, 0x0E00 ; U+007B ({)
        .data 0x1818, 0x1818, 0x1818, 0x1800 ; U+007C (|)
        .data 0x7018, 0x180E, 0x1818, 0x7000 ; U+007D (})
        .data 0x76DC, 0x0000, 0x0000, 0x0000 ; U+007E (~)
        .data 0xCC33, 0xCC33, 0xCC33, 0xCC33 ; U+007F
        
:scancode_to_ascii
 	.data 0x00,0x0C,0x00,0x05,0x03,0x01,0x02,0x10
 	.data 0x00,0x00,0x0B,0x06,0x04,0x09,0x60,0x00
 	.data 0x00,0x00,0x00,0x00,0x00,0x71,0x31,0x00
 	.data 0x00,0x00,0x7A,0x73,0x61,0x77,0x32,0x00
 	.data 0x00,0x63,0x78,0x64,0x65,0x34,0x33,0x00
 	.data 0x00,0x20,0x76,0x66,0x74,0x72,0x35,0x00
 	.data 0x00,0x6E,0x62,0x68,0x67,0x79,0x36,0x00
 	.data 0x00,0x00,0x6D,0x6A,0x75,0x37,0x38,0x00
 	.data 0x00,0x2C,0x6B,0x69,0x6F,0x30,0x39,0x00
 	.data 0x00,0x2E,0x2F,0x6C,0x3B,0x70,0x2D,0x00
 	.data 0x00,0x00,0x27,0x00,0x5B,0x3D,0x00,0x00
 	.data 0x00,0x00,0x0A,0x5D,0x00,0x5C,0x00,0x00
 	.data 0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00
	.data 0x00,0x31,0x00,0x34,0x37,0x00,0x00,0x00
	.data 0x30,0x2E,0x32,0x35,0x36,0x38,0x1B,0x00
	.data 0x0F,0x2B,0x33,0x2D,0x2A,0x39,0x00,0x00
:scancode_to_ascii_shifted
	.data 0x00,0x0C,0x00,0x05,0x03,0x01,0x02,0x10
	.data 0x00,0x00,0x0B,0x06,0x04,0x09,0x7E,0x00
	.data 0x00,0x00,0x00,0x00,0x00,0x51,0x21,0x00
	.data 0x00,0x00,0x5A,0x53,0x41,0x57,0x40,0x00
	.data 0x00,0x43,0x58,0x44,0x45,0x24,0x23,0x00
	.data 0x00,0x20,0x56,0x46,0x54,0x52,0x25,0x00
	.data 0x00,0x4E,0x42,0x48,0x47,0x59,0x5E,0x00
	.data 0x00,0x00,0x4D,0x4A,0x55,0x26,0x2A,0x00
	.data 0x00,0x3C,0x4B,0x49,0x4F,0x29,0x28,0x00
	.data 0x00,0x3E,0x3F,0x4C,0x3A,0x50,0x5F,0x00
	.data 0x00,0x00,0x22,0x00,0x7B,0x2B,0x00,0x00
	.data 0x00,0x00,0x0A,0x7D,0x00,0x7C,0x00,0x00
	.data 0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00
	.data 0x00,0x31,0x00,0x34,0x37,0x00,0x00,0x00
	.data 0x30,0x2E,0x32,0x35,0x36,0x38,0x1B,0x00
	.data 0x0F,0x2B,0x33,0x2D,0x2A,0x39,0x00,0x00
:ext_scancode_to_ascii_keys
	.data 0x012F,0x014A,0x015A,0x169,0x016B,0x16C
	.data 0x0170,0x0171,0x172,0x0174,0x0175,0x17A
	.data 0x017D
:ext_scancode_to_ascii_values
	.data 0x1C,0x2F,0x0A,0x1A,0x11,0x19,0x16,0x15
	.data 0x14,0x12,0x13,0x18,0x17

; ***** clrscr(buffer)
; * clears the specified buffer with the background colour (:bgCol)
; * uses r0, r1, r2, r3
; * 27+4*32000 = 128027 clock cycles
; *****
:clrscr
	push bp
	set bp, sp
	push r0 ; save registers
	push r1
	push r2
	push r3
	; determine start of video (r0) and length (r1) to be cleared
	ldi r0, (bp+3) ; buffer
	andi r0, 1
	shli r0, 15
	li r1, 0xFF
	lih r1, 0x7C
	; prepare for loop
	li r3, :bgCol
	lih r3, ::bgCol
	in r2, (r3)
	andi r2, 0xFF
	set r3, r2
	shli r3, 8
	or r2, r3
	li r3, :clrscr_loop
	lih r3, ::clrscr_loop
	add r0, r1	; r0 now points to the end of the ram that needs to be cleared
:clrscr_loop
	out r2, (r0)
	dec r0
	dec r1
	jpns [r3]
	pop r3 ; restore registers
	pop r2
	pop r1
	pop r0
	pop bp
	ret
	
; ***** setCursor(x, y)
; * sets the cursor at the given position
; * uses r0, r1, r2
; * 27 clock cycles
; *****
	push bp
	set bp, sp
	push r0 ; save registers
	push r1
	push r2
	ldi r0, (bp+4) ; x
	ldi r1, (bp+3) ; y
	; clipping
	li r2, :setCursor_return
	lih r2, ::setCursor_return
	nopf r0, r0
	jps [r2]
	nopf r1, r1
	jps [r2]
	cmpi r0, 40
	jpns [r2]
	cmpi r1, 25
	jpns [r2]
	shli r1, 8
	or r0, r1
	li r1, :cursorPosition
	lih r1, ::cursorPosition
	out r0, (r1)
:setCursor_return
	pop r2 ; restore registers
	pop r1
	pop r0
	pop bp
	ret

; ***** draw_cursor(col, buffer)
; * Draws a square cursor and saves the erased points
; *****
:draw_cursor
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	push r5
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	in r0, (r0)
	set r1, r0
	andi r0, 0xFF ; x
	shli r0, 2
	shri r1, 8 ; y
	set r2, r1
	shli r1, 8
	shli r2, 10
	add r1, r2
	add r1, r0
	ldi r0, (bp+3) ; buffer
	andi r0, 1
	shli r0,15
	add r1, r0 ; r1 is now start address in vram
	li r2, :savedCursor
	lih r2, ::savedCursor
	ldi r3, (bp+4) ; col
	andi r3, 0xFF
	set r4, r3
	shli r4, 8
	or r3, r4
	li r4, 8
	li r5, :draw_cursor_loop
	lih r5, ::draw_cursor_loop
:draw_cursor_loop
	in r0, (r1)
	out r0, (r2)
	out r3, (r1)
	inc r1
	inc r2
	in r0, (r1)
	out r0, (r2)
	out r3, (r1)
	inc r1
	inc r2
	in r0, (r1)
	out r0, (r2)
	out r3, (r1)
	inc r1
	inc r2
	in r0, (r1)
	out r0, (r2)
	out r3, (r1)
	addi r1, 100
	addi r1, 57
	inc r2
	dec r4
	jpnz [r5]
	
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

; ***** erase_cursor(buffer)
; * removes the cursor by restoring the erased points
; *****
:erase_cursor
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	in r0, (r0)
	set r1, r0
	andi r0, 0xFF ; x
	shli r0, 2
	shri r1, 8 ; y
	set r2, r1
	shli r1, 8
	shli r2, 10
	add r1, r2
	add r1, r0
	ldi r0, (bp+3) ; buffer
	andi r0, 1
	shli r0,15
	add r1, r0 ; r1 is now start address in vram
	li r2, :savedCursor
	lih r2, ::savedCursor
	li r4, 8
	li r3, :erase_cursor_loop
	lih r3, ::erase_cursor_loop
:erase_cursor_loop
	in r0, (r2)
	out r0, (r1)
	inc r1
	inc r2
	in r0, (r2)
	out r0, (r1)
	inc r1
	inc r2
	in r0, (r2)
	out r0, (r1)
	inc r1
	inc r2
	in r0, (r2)
	out r0, (r1)
	addi r1, 100
	addi r1, 57
	inc r2
	dec r4
	jpnz [r3]

	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

; ***** plot(x, y, col, buffer)
; * plots a pixel at location (x,y) with the given colour in the given screen buffer
; * uses r0, r1, r2, r3
; * 48 clock cycles
; *****
:plot
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3

	ldi r0, (bp+6) ; x
	ldi r1, (bp+5) ; y
	
	; clipping
	li r2, :plot_return
	lih r2, ::plot_return
	nopf r0, r0
	jps [r2]
	nopf r1, r1
	jps [r2]
	li r3, 0x40 ; li r3, 320
	lih r3, 0x01
	cmp r0, r3
	jpns [r2]
	li r3, 200
	cmp r1, r3
	jpns [r2]
	
	; find memory location (r1 = (320*y+x)/2+buffer<<15)
	set r2, r1
	shli r1, 6
	shli r2, 8
	add r1, r2
	add r1, r0
	shri r1, 1
	ldi r2, (bp+3) ; screen buffer
	shli r2, 15	; r2 points to start of screen buffer
	add r1, r2

	; set right byte to colour
	andi r0, 1
	shli r0, 3
	li r3, 0xFF ; mask: 8 bits per pixel
	ldi r2, (bp+4) ; col
	and r2, r3
	shl r2, r0
	shl r3, r0
	not r3, r3
	in r0, (r1)
	and r0, r3
	or r0, r2
	out r0, (r1)

:plot_return
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

; ***** line(x0, y0, x1, x2, col, buffer)
; * draws a line from (x0, y0) to (x1, y1) in the given colour on the given screen buffer
; * uses Bresenham's algorithm:
; * dx = abs(x1-x0)
; * sx = sign(x1-x0)
; * dy = -abs(y1-y0)
; * sy = sign(y1-y0)
; * err = dx + dy
; * while (x0 != x1 || y0 != y1) {
; *   plot(x0, y0, col, buffer)
; *   e2 = 2 * err
; *   if (e2 > dy) {
; *     err += dy;
; *     x0 += sx;
; *   }
; *   if (e2 <= dx) {
; *     err += dx;
; *     y0 += sy;
; *   }
; * }
; * plot(x0, y0, col, buffer)
; *
; * uses r0, r1 (x0), r2 (y0), r3 (x1), r4 (y1), r5 (:plot), r6 (dx), r7 (sx), r8 (dy), r9 (sy), r10 (err), r11 (e2)
; *****
.alias $x0 r1
.alias $y0 r2
.alias $x1 r3
.alias $y1 r4
.alias $dx r6
.alias $sx r7
.alias $dy r8
.alias $sy r9
.alias $err r10
.alias $e2 r11
:line
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	push r5
	push r6
	push r7
	push r8
	push r9
	push r10
	push r11
	ldi $x0, (bp+8)
	ldi $y0, (bp+7)
	ldi $x1, (bp+6)
	ldi $y1, (bp+5)

	; dx and sx
	set r0, $x1
	sub r0, $x0
	set $dx, r0
	set $sx, r0
	ashri $sx, 15
	xor $dx, $sx
	sub $dx, $sx
	nop r0, r0
	sns r0
	snz r5
	and r0, r5
	andi r0, 1
	add $sx, r0
	
	; dy and sy
	set r0, $y1
	sub r0, $y0
	set $dy, r0
	set $sy, r0
	ashri $sy, 15
	xor $dy, $sy
	sub $dy, $sy
	neg $dy, $dy
	nop r0, r0
	sns r0
	snz r5
	and r0, r5
	andi r0, 1
	add $sy, r0

	; err = dx + dy
	set $err, $dx
	add $err, $dy
	
	; prepare stack for plot(x,y,col,buffer)
	push $x0 ; x
	push $y0 ; y
	ldi r0, (bp+4) ; col
	push r0
	ldi r0, (bp+3) ; buffer
	push r0
	li r5, :plot
	lih r5, ::plot
:line_loop
	cmp $x0, $x1
	sz r0
	cmp $y0, $y1
	sz $e2
	and r0, $e2
	li r0, :line_end
	lih r0, ::line_end
	jpnz r0

	sti $x0, (sp+4)
	sti $y0, (sp+3)
	call [r5]

	set $e2, $err
	shli $e2, 1
	
	cmp $e2, $dy
	li r0, :line_else1
	lih r0, ::line_else1
	jps r0
	add $err, $dy
	add $x0, $sx
:line_else1
	cmp $dx, $e2
	li r0, :line_else2
	lih r0, ::line_else2
	jps r0
	add $err, $dx
	add $y0, $sy
:line_else2
	li r0, :line_loop
	lih r0, ::line_loop
	jp r0
:line_end
	sti $x0, (sp+4)
	sti $y0, (sp+3)
	call [r5]
	addi sp, 4
	
	pop r11
	pop r10
	pop r9
	pop r8
	pop r7
	pop r6
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret
.rmAliases

; ***** display_char(x, y, char, col, buffer)
; * draws a character (0-127) at the given (x,y) coordinates with the given colour on the given screen buffer
; *****
; * r0 points to bit pattern
; * r1 counter (4 words)
; * r2 bit pattern
; * r3 bitMask
; * r4 temp
; * r5 points to plot function
:display_char
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	push r5
	
	; prepare stack for plot(x,y,col,buffer)
	ldi r0, (bp+7) ; x
	push r0
	ldi r0, (bp+6) ; y
	push r0
	ldi r0, (bp+4) ; col
	push r0
	ldi r0, (bp+3) ; buffer
	push r0
	li r5, :plot
	lih r5, ::plot
	
	ldi r0, (bp+5)	; char
	andi r0, 0x7F	; clip
	shli r0, 2	; times 4
	li r1, :font
	lih r1, ::font
	add r0, r1	; r0 points now to the start of the bit patterns
	li r1, 4	; r1 = counter: 4 words
:display_char_nextPattern
	ld r2, (r0)	; r2 contains the bit pattern
	li r3, 0	; r3 = bitMask
	lih r3, 0x80
:display_char_nextPixel
	li r4, :display_char_noPixel
	lih r4, ::display_char_noPixel
	andf r2, r3
	jpz r4
	call [r5]
:display_char_noPixel
	ldi r4, (sp+4) ; inc(x)
	inc r4
	sti r4, (sp+4)
	shri r3, 1
	li r4, 128
	cmp r3, r4
	li r4, :display_char_sameRow
	lih r4, ::display_char_sameRow
	jpnz [r4]
	ldi r4, (sp+4) ; x = x - 8
	subi r4, 8
	sti r4, (sp+4)
	ldi r4, (sp+3) ; y++
	inc r4
	sti r4, (sp+3)
:display_char_sameRow
	cmpi r3, 0
	li r4, :display_char_nextPixel
	lih r4, ::display_char_nextPixel
	jpnz [r4]
	ldi r4, (sp+4) ; x = x - 8
	subi r4, 8
	sti r4, (sp+4)
	ldi r4, (sp+3) ; y++
	inc r4
	sti r4, (sp+3)
	inc r0
	li r4, :display_char_nextPattern
	lih r4, ::display_char_nextPattern
	dec r1
	jpnz [r4]
	
	addi sp, 4
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret

; ***** scroll_up(amount, buffer)
; * Scrolls up the screen buffer with the specified number of lines.
; * The new lines on the bottom are filled with the background colour.
; *****
:scroll_up
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	; r1 = source, r2 = dest, r3 = count
	ldi r0, (bp+4) ; amount
	set r1, r0
	shli r0, 7
	shli r1, 5
	add r0, r1
	ldi r1, (bp+3) ; buffer
	andi r1, 1
	shli r1, 15
	set r2, r1
	add r1, r0
	li r3, 0x00
	lih r3, 0x7D
	sub r3, r0
	li r0, :scroll_up_loop
	lih r0, ::scroll_up_loop	
:scroll_up_loop
	in r4, (r1)
	out r4, (r2)
	inc r1
	inc r2
	dec r3
	jpnz [r0]
	li r3, :bgCol
	lih r3, ::bgCol
	in r3, (r3)
	set r4, r3
	shli r4, 8
	or r3, r4
	li r4, 0x00
	lih r4, 0x7D
	li r0, :scroll_up_loop2
	lih r0, ::scroll_up_loop2
:scroll_up_loop2
	out r3, (r2)
	inc r2
	cmp r2, r4
	jpnz [r0]
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret
	
; ***** print_char(char, col, buffer)
; * Prints a character (0-127) at the current cursor position with the given colour on the given screen buffer.
; * The cursor is advanced one position.  If the cursor goes off the screen, the screen is scrolled up one line.
; * This routine also 'prints' cursor movements: arrows and del
; *****
:print_char
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	in r0, (r0)
	set r1, r0
	andi r0, 0xFF ; x
	shri r1, 8 ; y
	
	ldi r2, (bp+5) ; char
	cmpi r2, 0x11 ; arrow left
	li r3, :print_char_left
	lih r3, ::print_char_left
	jpz [r3]
	cmpi r2, 0x12 ; arrow right
	li r3, :print_char_right
	lih r3, ::print_char_right
	jpz [r3]
	cmpi r2, 0x13 ; arrow up
	li r3, :print_char_up
	lih r3, ::print_char_up
	jpz [r3]
	cmpi r2, 0x14 ; arrow down
	li r3, :print_char_down
	lih r3, ::print_char_down
	jpz [r3]
	cmpi r2, 8 ; backspace
	li r3, :print_char_backspace
	lih r3, ::print_char_backspace
	jpz [r3]
	li r3, :print_char_normal
	lih r3, ::print_char_normal
	jp [r3]
:print_char_left
	dec r0
	set r2, r0
	ashri r2, 15
	not r2, r2
	and r0, r2
	li r2, :print_char_store_cursor
	lih r2, ::print_char_store_cursor
	jp [r2]
:print_char_right
	inc r0
	cmpi r0, 40
	ss r2
	and r0, r2
	li r3, 39
	not r2, r2
	and r3, r2
	or r0, r3
	li r2, :print_char_store_cursor
	lih r2, ::print_char_store_cursor
	jp [r2]
:print_char_up
	dec r1
	set r2, r1
	ashri r2, 15
	not r2, r2
	and r1, r2
	li r2, :print_char_store_cursor
	lih r2, ::print_char_store_cursor
	jp [r2]
:print_char_down
	inc r1
	cmpi r1, 25
	ss r2
	and r1, r2
	li r3, 24
	not r2, r2
	and r3, r2
	or r1, r3
	li r2, :print_char_store_cursor
	lih r2, ::print_char_store_cursor
	jp [r2]
:print_char_backspace ; do arrow left and draw char 2 in background col
	li r0, 0x11
	push r0
	li r0, :bgCol
	lih r0, :bgCol
	in r0, (r0)
	push r0
	ldi r0, (bp+3) ; buffer
	push r0
	li r0, :print_char
	lih r0, ::print_char
	call [r0]
	li r1, 2
	sti r1, (sp+3)
	call [r0]
	li r1, 0x11
	sti r1, (sp+3)
	call [r0]
	addi sp, 3
	li r0, :print_char_end
	lih r0, ::print_char_end
	jp [r0]
:print_char_normal
	shli r0, 3
	shli r1, 3
	; display_char(x,y,char,col)
	push r0
	push r1
	ldi r2, (bp+5) ; char
	push r2
	ldi r2, (bp+4) ; col
	push r2
	ldi r2, (bp+3) ; buffer
	push r2
	li r2, :display_char
	lih r2, ::display_char
	call [r2]
	addi sp, 3
	pop r1 ; restore x and y and increase position or move cursor
	pop r0
	shri r0, 3
	shri r1, 3
	ldi r2, (bp+5) ; char
	cmpi r2, 10 ; newline
	li r2, :print_char_no_newline
	lih r2, ::print_char_no_newline
	jpnz r2
	li r0, 39
:print_char_no_newline
	inc r0
	cmpi r0, 40
	li r2, :print_char_store_cursor
	lih r2, ::print_char_store_cursor
	jpnz [r2]
	li r0, 0
	inc r1
	cmpi r1, 25
	jpnz [r2]
	li r1, 24
	; scroll_up(8, buffer)
	li r2, 8
	push r2
	ldi r2, (bp+3) ; buffer
	push r2
	li r2, :scroll_up
	lih r2, ::scroll_up
	call [r2]
	addi sp, 2
:print_char_store_cursor
	shli r1, 8
	or r1, r0
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	out r1, (r0)
:print_char_end
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret


; ***** print_string(string, col, buffer)
; * prints a null-terminated string at the current cursor position with the given colour on the given screen buffer
; *****
:print_string
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	; prepare stack for print_char
	push r0 ; dummy for char
	ldi r0, (bp+4) ; col
	push r0
	ldi r0, (bp+3) ; buffer
	push r0	
	ldi r0, (bp+5) ; string
:print_string_loop
	ld r1, (r0)
	nop r1, r1
	li r2, :print_string_end
	lih r2, ::print_string_end
	jpz [r2]
	sti r1, (sp+3)
	li r2, :print_char
	lih r2, ::print_char
	call [r2]
	inc r0
	li r2, :print_string_loop
	lih r2, ::print_string_loop
	jp [r2]
:print_string_end
	addi sp, 3
	pop r2
	pop r1
	pop r0
	pop bp
	ret
	
; ***** print_hex(i, col, buffer)
; * prints an int (i) at the current cursor position with the given colour on the given screen buffer
; *****
:print_hex
	push bp
	set bp, sp
	push r0
	push r1
	
	; prepare stack for print_char(char,col,buffer)
	push r0	; char
	ldi r0, (bp+4) ; col
	push r0
	ldi r0, (bp+3) ; buffer
	push r0
	
	ldi r0, (bp+5) ; i
	shri r0, 12
	cmpi r0, 10
	li r1, :display_hex_nodigit1
	lih r1, ::display_hex_nodigit1
	jps [r1]
	addi r0, 7
:display_hex_nodigit1
	addi r0, 48
	sti r0, (sp+3) ; set char in stack
	li r1, :print_char
	lih r1, ::print_char
	call [r1]
	
	ldi r0, (bp+5) ; i
	shri r0, 8
	andi r0, 15
	cmpi r0, 10
	li r1, :display_hex_nodigit2
	lih r1, ::display_hex_nodigit2
	jps [r1]
	addi r0, 7
:display_hex_nodigit2
	addi r0, 48
	sti r0, (sp+3) ; set char in stack
	li r1, :print_char
	lih r1, ::print_char
	call [r1]
	
	ldi r0, (bp+5) ; i
	shri r0, 4
	andi r0, 15
	cmpi r0, 10
	li r1, :display_hex_nodigit3
	lih r1, ::display_hex_nodigit3
	jps [r1]
	addi r0, 7
:display_hex_nodigit3
	addi r0, 48
	sti r0, (sp+3) ; set char in stack
	li r1, :print_char
	lih r1, ::print_char
	call [r1]
	
	ldi r0, (bp+5) ; i
	andi r0, 15
	cmpi r0, 10
	li r1, :display_hex_nodigit4
	lih r1, ::display_hex_nodigit4
	jps [r1]
	addi r0, 7
:display_hex_nodigit4
	addi r0, 48
	sti r0, (sp+3) ; set char in stack
	li r1, :print_char
	lih r1, ::print_char
	call [r1]
	
	addi sp, 3
	pop r1
	pop r0
	pop bp
	ret
	
; ***** read_keycode()
; * gets the last key code pressed on the keyboard
; *****
:read_keycode
	push bp
	set bp, sp
	push r0	
	li r0, :keyboardLastKey
	lih r0, ::keyboardLastKey
	in r0, (r0)
	sti r0, (bp+3)
	pop r0
	pop bp
	ret

; ***** read_char()
; * reads the last character pressed on the keyboard
; * char c = 0
; * int code = read_keycode()
; * if (code == 0x12 || code == 0x59) shiftPressed = 1; return c
; * if (code == 0x212 || code == 0x259) shiftPressed = 0; return c
; * if (code < 0x80)
; *     c = scancode_to_ascii[code+shiftPressed<<7]; return c
; * for(int i in 12:0)
; *   if (ext_scancode_to_ascii_keys[i] == code)
; *     c = :ext_scancode_to_ascii_values[i]; return c
.alias $code r0
.alias $c r1
.alias $i r2
:read_char
	push bp
	set bp, sp
	push r0
	push r1
	push r2
	push r3
	push r4
	
	subi sp, 1
	li r3, :read_keycode
	lih r3, ::read_keycode
	call [r3]
	addi sp, 1
	ld $code, (sp)
	
	li r3 :read_char_shiftPressedOrReleased
	lih r3, ::read_char_shiftPressedOrReleased
	li $c, 1
	cmpi $code, 0x12
	jpz [r3]
	cmpi $code, 0x59
	jpz [r3]
	li $c, 0
	li r4, 0x12
	lih r4, 2
	cmp $code, r4
	jpz [r3]
	li r4, 0x59
	lih r4, 2
	cmp $code, r4
	jpz [r3]
	
	li r4, 0x80
	cmp $code, r4
	li r3, :read_char_ext
	lih r3, ::read_char_ext
	jpns [r3]
	li r3, :shiftPressed
	lih r3, ::shiftPressed
	in r3, (r3)
	shli r3, 7
	add $code, r3
	li r3, :scancode_to_ascii
	lih r3, ::scancode_to_ascii
	lda $c, ($code+r3)
	li r3, :read_char_return
	lih r3, ::read_char_return
	jp [r3]

	li $c, 0
:read_char_ext	
	li $i, 12
:read_char_loop
	nop $i, $i
	li r3, :read_char_return
	lih r3, ::read_char_return
	jps [r3]	
	
	li r3, :ext_scancode_to_ascii_keys
	lih r3, ::ext_scancode_to_ascii_keys
	lda r3, (r3+$i)
	cmp $code, r3
	li r3, :read_char_next
	lih r3, ::read_char_next
	jpnz [r3]
	li r3, :ext_scancode_to_ascii_values
	lih r3, ::ext_scancode_to_ascii_values
	lda $c, (r3+$i)
	li r3, :read_char_return
	lih r3, ::read_char_return
	jp [r3]
:read_char_next
	dec $i
	li r4, :read_char_loop
	lih r4, ::read_char_loop
	jp [r4]
	
:read_char_shiftPressedOrReleased
	li r2, :shiftPressed
	lih r2, ::shiftPressed
	out $c, (r2)
	li $c, 0
:read_char_return
	sti $c, (bp+3)
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	pop bp
	ret
.rmAliases

; *****
; * variables (data memory)
; *****
.def	:foo	0x3000
:message
.data	'H','e','l','l','o',32,'t','h','e','r','e','!','!','!',32,0

; ***** start_system()
; * initializes the system and jumps to main()
; *****
:start_system
	; initialize stack
	li sp, :startOfStack
	lih sp, ::startOfStack
	; set graphics to buffer 0
	li r0, :screenConfig
	lih r0, ::screenConfig
	li r1, 0
	out r1, (r0)
	; set cursor to (0,0)
	li r0, :cursorPosition
	lih r0, ::cursorPosition
	li r1, 0
	out r1, (r0)
	; initialize keyboard
	li r0, :shiftPressed
	lih r0, ::shiftPressed
	li r1, 0
	out r1, (r0)
	di
	
:main
	; set background colour
	li r0, :bgCol
	lih r0, ::bgCol
	li r1, 0x02
	out r1, (r0)
	; clrscr(0)
	li r1, 0
	push r1
	li r1, :clrscr
	lih r1, ::clrscr
	call [r1]
	addi sp, 1

	; plot(10, 10, 0xE0, 0)
	li r0, 10
	push r0
	push r0
	li r0, 0xE0
	push r0
	li r0, 0
	push r0
	li r0, :plot
	lih r0, ::plot
	call [r0]
	addi sp, 4
	
	; line(0, 0, 319, 0, 0xFF, 0)
	li r0, 0 ; x0
	push r0
	li r0, 0 ; y0
	push r0
	li r0, 0x3F ; x1
	lih r0, 1
	push r0
	li r0, 0 ; y1
	push r0
	li r0, 0xFF ; col
	push r0
	li r0, 0
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 6
	
	; line(319, 0, 319, 199, 0xFF, 0)
	li r0, 0x3F ; x0
	lih r0, 1
	push r0
	li r0, 0 ; y0
	push r0
	li r0, 0x3F ; x1
	lih r0, 1
	push r0
	li r0, 199 ; y1
	push r0
	li r0, 0xFF ; col
	push r0
	li r0, 0
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 6
	
	; line(319, 199, 0, 199, 0xFF, 0)
	li r0, 0x3F ; x0
	lih r0, 1
	push r0
	li r0, 199 ; y0
	push r0
	li r0, 0 ; x1
	push r0
	li r0, 199 ; y1
	push r0
	li r0, 0xFF ; col
	push r0
	li r0, 0
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 6
	
	; line(0, 199, 0, 0, 0xFF, 0)
	li r0, 0 ; x0
	push r0
	li r0, 199 ; y0
	push r0
	li r0, 0 ; x1
	push r0
	li r0, 0 ; y1
	push r0
	li r0, 0xFF ; col
	push r0
	li r0, 0
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 6
	
	; line(10, 0, 70, 30, 0xE0, 0)
	li r0, 10 ; x0
	push r0
	li r0, 0 ; y0
	push r0
	li r0, 70 ; x1
	push r0
	li r0, 30 ; y1
	push r0
	li r0, 0xE0 ; col
	push r0
	li r0, 0
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 6
	
	; line(10, 30, 140, 10, 0x1C, 0)
	li r0, 10 ; x0
	push r0
	li r0, 30 ; y0
	push r0
	li r0, 140 ; x1
	push r0
	li r0, 10 ; y1
	push r0
	li r0, 0x1C
	push r0
	li r0, 0
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 6
	
	; line(30, 10, 10, 70, 0x03, 0)
	li r0, 30 ; x0
	push r0
	li r0, 10 ; y0
	push r0
	li r0, 10 ; x1
	push r0
	li r0, 70 ; y1
	push r0
	li r0, 0x03 ; col
	push r0
	li r0, 0
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 6
	
	; line(40, 150, 100, 20, 0xFC, 0)
	li r0, 40 ; x0
	push r0
	li r0, 150 ; y0
	push r0
	li r0, 100 ; x1
	push r0
	li r0, 20 ; y1
	push r0
	li r0, 0xFC ; col
	push r0
	li r0, 0
	push r0
	li r0, :line
	lih r0, ::line
	call [r0]
	addi sp, 6
:loop2
	; draw_cursor(0xFF, 0)
	li r0, 0xFF
	push r0
	li r0, 0
	push r0
	li r0, :draw_cursor
	lih r0, ::draw_cursor
	call [r0]
	addi sp, 2
:loop
	; read_char()
	subi sp, 1
	li r0, :read_char
	lih r0, ::read_char
	call [r0]
	pop r0
	
	nop r0, r0
	li r1, :loop
	lih r1, ::loop
	jpz [r1]
	
	; erase_cursor(0)
	li r1, 0
	push r1
	li r1, :erase_cursor
	lih r1, ::erase_cursor
	call [r1]
	addi sp, 1
	
	; print_char(r0, 0xFF, 0)
	push r0
	li r0, 0xFF
	push r0
	li r0, 0
	push r0
	li r0, :print_char
	lih r0, ::print_char
	call [r0]
	addi sp, 3
	li r0, :loop2
	lih r0, ::loop2
	jp [r0]


	halt
