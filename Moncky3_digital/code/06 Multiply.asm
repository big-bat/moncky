	li sp, 0xFF
	lih sp, 0xBF
	li r0, :main
	lih r0, ::main
	jp [r0]
	
.org 0x0010
	reti

:umult	; *** unsigned multiplication r0*r1 = (r3, r2)
	li r4, 0
	li r2, 0
	li r3, 0
	li r5, 17
:loop	li r6, :endloop
	lih r6, ::endloop
	subi r5, 1
	jpz r6
	andif r1, 1
	li r6, :shift
	lih r6, ::shift
	jpz r6
	add r2, r0
	addc r3, r4
:shift	shli r0, 1
	shlci r4, 1
	shri r1, 1
	li r6, :loop
	lih r6, ::loop
	jp r6
:endloop
	ret

:main
	li r0, 0x00
	lih r0, 0x50
	li r1, 0x00
	lih r1, 0xC0
	li r6, :umult
	lih r6, ::umult
	call [r6]
	halt

