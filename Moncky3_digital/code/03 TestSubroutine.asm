	li sp, 0xFF
	li r0, :main
	lih r0, ::main
	jp [r0]

:digits
.data 0x3F, 0x06, 0x5D, 0x4F, 0x66, 0x6B, 0x7B, 0x0E
.data 0x7F, 0x6F, 0x7E, 0x73, 0x39, 0x57, 0x79, 0x78

:showregister ; r0 contains number
	push r0
	push r1
	push r2
	push r3
	li r3, :digits
	lih r3, ::digits
	set r2, r0
	andi r2, 0xF
	lda r13, (r3+r2)
	shri r0, 4
	set r2, r0
	andi r2, 0xF
	lda r1, (r3+r2)
	shli r1, 8
	or r13, r1
	shri r0, 4
	set r2, r0
	andi r2, 0xF
	lda r12, (r3+r2)
	shri r0, 4
	set r2, r0
	andi r2, 0xF
	lda r1, (r3+r2)
	shli r1, 8
	or r12, r1
	pop r3
	pop r2
	pop r1
	pop r0
	ret

:main	li r0, 0xFE
	lih r1, 0xCA
	li r1, :showregister
	call [r1]
	halt

