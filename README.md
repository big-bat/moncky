Hi, welcome to the home of the Moncky project! (yes, without the 'e' :-) )

The goal is to create a complete working retro 16-bit computer in the style of the C64, CPC 464 and alike, but with modern technology.  It is a work in progress.  A lot of ideas are still floating around.

At the heart is a 16-bit single cycle self-made RISC processor: the Moncky-3.  In this project you can also find its predecessors (Moncky-1 and Moncky-2).  The Moncky-1 was first conceived to teach students about digital logic and processors.  The course text can be found here: https://drive.google.com/file/d/1ngHiIrxpOfb0dO7pm3ZOe7q8XEXCb-b6/view

The Moncky-3 computer is currently implemented on an Arty S7: Spartan-7 FPGA Development Board.  The Verilog code can be found in the verilog folder.  The Moncky-3 processor interfaces with 2x128KB dual-port RAM, a VGA output, a PS/2 keyboard input, and an SPI interface.  The computer can read files from an SD card connected to the SPI port.

Next to the computer there is a Moncky assembler (written in Java) to ease the programming in assembly.
And because assembly can become very cumbersome if you want to write real applications, there is also a complete programming language with compiler.  The language is a bit of a mix between pascal and c.  The main goal was to be able to parse it very easily and create compact code.

All documentation can be found in the documentation folder.  It is still under development.

you can find the following subfolders:
- Moncky1_digital: a Digital (https://github.com/hneemann/Digital) implementation of the Moncky-1 processor
- Moncky1_logisim: a Logisim implementation of the Moncky-1 processor
- Moncky2_digital: a Digital implementation of the Moncky-2 processor
- Moncky3_digital: a Digital implementation of the Moncky-3 processor
- Moncky3_verilog: a verilog implementation of the Moncky-3 computer
- documentation: a pdf file containing all information on this project
- moncky3assembler: an assembler that can be used with all Moncky processors
- Moncky3_firmware: this contains the source code for the ROM and the firmware that can be linked to your own code.  There is also a demo source per library
- Moncky3Compiler: the compiler for the Moncky programming language, written in java.  As a proof of concept you can find the pacman game, completely written in Moncky language!
- Moncky3Simulator: a complete simulation of the moncky-3 computer, written in java.  It also includes the VGA output and keyboard input. It allows to set breakpoints and do profiling
- images: images and short videos that demonstrate the working of the Moncky-3 computer

Feel free to leave any comment at kris.demuynck@gmail.com.

Kris Demuynck (kris.demuynck@gmail.com)
(c) 2022-07-25

ideas:
- create libraries for floating point math, fdlibm, ...
- implement sd card write block and fat write file
- make an interpreter for some language (BASIC, python, own language)
- clean up Verilog code (really necessary!)
- replace MUXs by OR gates and let every component emit a 0 when not selected (should make the processor faster)
- put other slave-select pins on the output pins so more SPI devices can be hooked up
- add sound controller (with own memory to hold samples, at least 3 channels, implement a tracker in hardware)
- add instructions to quickly read from bp+... and bp-...
- add instructions pushi and popi to quickly push and pop constant values
- add instructions to manipulate bytes more quickly
- make interrupt lines for counters (configurable timing), keyboard, ... (add interrupt controller?)
- use grapics engine connected by SPI?
- remove uint8 and int8 datatypes from the programming language as they do not render any advantage (except for compacting arrays)
- enable inline assembly in the programming language
- ...
