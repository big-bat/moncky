package moncky3.monckycompiler;

import moncky3.monckycompiler.lex.Token;
import moncky3.monckycompiler.parsing.Destination;
import moncky3.monckycompiler.parsing.DestinationValue;
import moncky3.monckycompiler.parsing.Value;
import moncky3.monckycompiler.resources.ResourceManager;
import moncky3.monckycompiler.types.*;

import java.io.PrintStream;

import static moncky3.monckycompiler.types.PrimitiveType.*;

/**
 * This class bundles all logic to perform calculations.
 */
public class CalculationGenerator {
    private final CodeGenerator codeGenerator;
    private final ResourceManager resourceManager;
    private PrintStream output;

    public CalculationGenerator(CodeGenerator codeGenerator, ResourceManager resourceManager) {
        this.codeGenerator = codeGenerator;
        this.resourceManager = resourceManager;
        this.output = System.out;
    }

    public void setPrintStream(PrintStream printStream) {
        this.output = printStream;
    }

    private void not(DestinationValue destval) {
        if (!destval.inRegister()) {
            throw new InternalCompilerError("values must be loaded in registers");
        }
        if (destval instanceof Destination) {
            destval = codeGenerator.loadValueInRegisters((Destination) destval, 0, 1);
        }
        Value value = (Value) destval;
        if (value.getType().sizeof() == 1) {
            output.println("\tor r" + value.getRegisterLSB() + ", r" + value.getRegisterLSB());
        } else {
            output.println("\tor r" + value.getRegisterLSB() + ", r" + value.getRegisterMSB());
        }
        output.println("\tsz r" + value.getRegisterLSB());
        if (value.getType().sizeof() == 2) {
            output.println("\tsz r" + value.getRegisterMSB());
        }
    }

    private void invert(DestinationValue destval) {
        if (!destval.inRegister()) {
            throw new InternalCompilerError("values must be loaded in registers");
        }
        if (destval instanceof Destination) {
            destval = codeGenerator.loadValueInRegisters((Destination) destval, 0, 1);
        }
        Value value = (Value) destval;
        if (!value.inRegister()) {
            throw new InternalCompilerError("values must be loaded in registers");
        }
        output.println("\tnot r" + value.getRegisterLSB() + ", r" + value.getRegisterLSB());
        if (value.getType().sizeof() == 2) {
            output.println("\tnot r" + value.getRegisterMSB() + ", r" + value.getRegisterMSB());
        }
    }

    private DestinationValue followPointer(DestinationValue destval) {
        if (!destval.inRegister()) {
            throw new InternalCompilerError("values must be loaded in registers");
        }
        if (destval instanceof Destination) {
            Destination destination = (Destination) destval;
            if (!(destination.getType() instanceof PointerType)) {
                throw new InternalCompilerError("unary operator '^' can only be used on pointer types");
            }
            PointerType pointerType = (PointerType) destination.getType();
            destination = codeGenerator.loadDestinationInRegister(destination, 2);
            codeGenerator.loadPointer(2, pointerType, 2, -1);
            return new Destination(pointerType.getType(), destination.isLocal(), 2);
        } else {
            Value value = (Value) destval;
            if (!(value.getType() instanceof PointerType)) {
                throw new InternalCompilerError("can only follow a pointer");
            }
            PointerType pointerType = (PointerType) value.getType();
            if (pointerType.getType().sizeof() > 2) {
                throw new InternalCompilerError("cannot load value of pointer in 2 registers (" + pointerType.getType() + ")");
            }
            if (pointerType.getType().sizeof() == 2) {
                output.println("\tldi r" + value.getRegisterMSB() + ", (r" + value.getRegisterLSB() + "+1)");
            }
            output.println("\tld r" + value.getRegisterLSB() + ", (r" + value.getRegisterLSB() + ")");
            return new Value(value.getRegisterLSB(), value.getRegisterMSB(), pointerType.getType());
        }
    }

    private void add(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("value1 must be loaded in registers");
        }
        Type type = value1.getType();
        Type type2 = value2.getType();
        if (!type.equals(type2)) {
            throw new InternalCompilerError("Can only add two numbers of the same type");
        }
        if (type.sizeof() == 1) {
            if (value2.inRegister()) {
                output.println("\tadd r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
            } else {
                long value = value2.getLongValue();
                if (value == 0) {
                    codeGenerator.comment("add 0 to r" + value1.getRegisterLSB());
                } else if ((value <= 127) && (value >= -128)) {
                    output.println("\taddi r" + value1.getRegisterLSB() + ", " + value);
                } else if ((value <= 254) && (value >= -256)) {
                    output.println("\taddi r" + value1.getRegisterLSB() + ", " + (value / 2));
                    output.println("\taddi r" + value1.getRegisterLSB() + ", " + (value - (value / 2)));
                } else {
                    int reg = resourceManager.getFreeRegister();
                    codeGenerator.loadValueInRegisters(value2, reg, -1);
                    output.println("\tadd r" + value1.getRegisterLSB() + ", r" + reg);
                    resourceManager.setRegisterFree(reg);
                }
            }
            if (type == UINT8) {
                output.println("\tandi r" + value1.getRegisterLSB() + ", 0xFF");
            }
        } else if (type.sizeof() == 2) {
            if (value2.inRegister()) {
                output.println("\tadd r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
                output.println("\taddc r" + value1.getRegisterMSB() + ", r" + value2.getRegisterMSB());
            } else {
                long value = value2.getLongValue();
                if (value == 0) {
                    codeGenerator.comment("add 0 to r" + value1.getRegisterLSB() + " and r" + value1.getRegisterMSB());
                } else if ((value <= 127) && (value >= -128)) {
                    output.println("\taddi r" + value1.getRegisterLSB() + ", " + value);
                    output.println("\taddci r" + value1.getRegisterMSB() + ", " + 0);
                } else if ((value <= 65535 && (value >= -32768))) {
                    int reg = resourceManager.getFreeRegister();
                    value2 = new Value(PrimitiveType.INT16, value);
                    codeGenerator.loadValueInRegisters(value2, reg, -1);
                    output.println("\tadd r" + value1.getRegisterLSB() + ", r" + reg);
                    output.println("\taddci r" + value1.getRegisterMSB() + ", " + 0);
                    resourceManager.setRegisterFree(reg);
                } else {
                    int lsb = resourceManager.getFreeRegister();
                    int msb = resourceManager.getFreeRegister();
                    codeGenerator.loadValueInRegisters(value2, lsb, msb);
                    output.println("\tadd r" + value1.getRegisterLSB() + ", r" + lsb);
                    output.println("\taddc r" + value1.getRegisterMSB() + ", r" + msb);
                    resourceManager.setRegisterFree(msb);
                    resourceManager.setRegisterFree(lsb);
                }
            }
        } else {
            throw new InternalCompilerError("Can only add two primitive types (" + type + " was given)");
        }
    }

    private void subtract(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("value1 must be loaded in registers");
        }
        Type type = value1.getType();
        Type type2 = value2.getType();
        if (!type.equals(type2)) {
            throw new InternalCompilerError("Can only subtract two numbers of the same type");
        }
        if (type.sizeof() == 1) {
            if (value2.inRegister()) {
                output.println("\tsub r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
            } else {
                long value = value2.getLongValue();
                if ((value <= 15) && (value >= 0)) {
                    output.println("\tsubi r" + value1.getRegisterLSB() + ", " + value);
                } else {
                    int reg = resourceManager.getFreeRegister();
                    codeGenerator.loadValueInRegisters(value2, reg, -1);
                    output.println("\tsub r" + value1.getRegisterLSB() + ", r" + reg);
                    resourceManager.setRegisterFree(reg);
                }
            }
            //if (type == UINT8) {
            //    output.println("\tandi r" + value1.getRegisterLSB() + ", 0xFF");
            //}
        } else if (type.sizeof() == 2) {
            if (value2.inRegister()) {
                output.println("\tsub r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
                output.println("\tsubc r" + value1.getRegisterMSB() + ", r" + value2.getRegisterMSB());
            } else {
                long value = value2.getLongValue();
                if ((value >= 0) && (value <= 15)) {
                    output.println("\tsubi r" + value1.getRegisterLSB() + ", " + value);
                    output.println("\tsubci r" + value1.getRegisterMSB() + ", " + 0);
                } else if ((value <= 65535) && (value >= -32768)) {
                    int reg = resourceManager.getFreeRegister();
                    value2 = new Value(PrimitiveType.INT16, value);
                    codeGenerator.loadValueInRegisters(value2, reg, -1);
                    output.println("\tsub r" + value1.getRegisterLSB() + ", r" + reg);
                    output.println("\tsubci r" + value1.getRegisterMSB() + ", " + 0);
                    resourceManager.setRegisterFree(reg);
                } else {
                    int lsb = resourceManager.getFreeRegister();
                    int msb = resourceManager.getFreeRegister();
                    codeGenerator.loadValueInRegisters(value2, lsb, msb);
                    output.println("\tsub r" + value1.getRegisterLSB() + ", r" + lsb);
                    output.println("\tsubc r" + value1.getRegisterMSB() + ", r" + msb);
                    resourceManager.setRegisterFree(msb);
                    resourceManager.setRegisterFree(lsb);
                }
            }
        } else {
            throw new InternalCompilerError("Can only add two primitive types (" + type + " was given)");
        }
    }

    private void multiply(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("values must be loaded in registers");
        }
        Type type = value1.getType();
        Type type2 = value2.getType();
        if (type instanceof PointerType) {
            type = UINT16;
        }
        if (type2 instanceof PointerType) {
            type2 = UINT16;
        }
        if (!type.equals(type2)) {
            throw new InternalCompilerError("Can only multiply two numbers of the same type");
        }
        if (!(type instanceof PrimitiveType)) {
            throw new InternalCompilerError("Can only multiply primitive types");
        }
        if (value2.inRegister()) {
            codeGenerator.pushValue(value1);
            codeGenerator.pushValue(value2);
            String label;
            if (type == UINT8) {
                label = "math_multiply8unsigned";
            } else if (type == INT8) {
                label = "math_multiply8signed";
            } else if (type == UINT16) {
                label = "math_multiply16unsigned";
            } else if (type == INT16) {
                label = "math_multiply16signed";
            } else if (type == UINT32) {
                label = "math_multiply32unsigned";
            } else if (type == INT32) {
                label = "math_multiply32signed";
            } else {
                throw new InternalCompilerError("Unknown type encountered for multiplication: " + type);
            }
            output.println("\tli r0, :" + label);
            output.println("\tlih r0, ::" + label);
            output.println("\tcall [r0]");
            removeValuesFromStack(value1, value2);
        } else {
            if (type.sizeof() == 1) {
                long value = value2.getLongValue();
                codeGenerator.multiplyRegisterByConstant(value1.getRegisterLSB(), (int)(value & 0xFFFF));
            } else {
                int lsb = resourceManager.getFreeRegister();
                int msb = resourceManager.getFreeRegister();
                value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
                multiply(value1, value2);
                resourceManager.setRegisterFree(msb);
                resourceManager.setRegisterFree(lsb);
            }
        }
    }

    private void removeValuesFromStack(Value... values) {
        int size = 0;
        for (Value value : values) {
            size += value.getType().sizeof();
        }
        output.println("\taddi sp, " + size);
    }

    private void divide(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("values must be loaded in registers");
        }
        Type type = value1.getType();
        Type type2 = value2.getType();
        if (type instanceof PointerType) {
            type = UINT16;
        }
        if (type2 instanceof PointerType) {
            type2 = UINT16;
        }
        if (!type.equals(type2)) {
            throw new InternalCompilerError("Can only divide two numbers of the same type");
        }
        if (!(type instanceof PrimitiveType)) {
            throw new InternalCompilerError("Can only divide primitive types");
        }
        if (value2.inRegister()) {
            codeGenerator.pushValue(value1);
            codeGenerator.pushValue(value2);
            String label;
            if (type == UINT8) {
                label = "math_divide8unsigned";
            } else if (type == INT8) {
                label = "math_divide8signed";
            } else if (type == UINT16) {
                label = "math_divide16unsigned";
            } else if (type == INT16) {
                label = "math_divide16signed";
            } else if (type == UINT32) {
                label = "math_divide32unsigned";
            } else if (type == INT32) {
                label = "math_divide32signed";
            } else {
                throw new InternalCompilerError("Unknown type encountered for division: " + type);
            }
            output.println("\tli r0, :" + label);
            output.println("\tlih r0, ::" + label);
            output.println("\tcall [r0]");
            removeValuesFromStack(value1, value2);
        } else {
            if (type.sizeof() == 1) {
                long value = value2.getLongValue();
                codeGenerator.divideRegisterByConstant(type, value1.getRegisterLSB(), (int)(value & 0xFFFF));
            } else {
                int powerOfTwo = codeGenerator.isPowerOfTwo((int)(value2.getLongValue()));
                if (powerOfTwo > 0) {
                    if (type == UINT32) {
                        shiftRight(value1, new Value(type, (long) powerOfTwo));
                    } else {
                        arithmeticShiftRight(value1, new Value(type, (long) powerOfTwo));
                    }
                } else {
                    int lsb = resourceManager.getFreeRegister();
                    int msb = resourceManager.getFreeRegister();
                    value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
                    divide(value1, value2);
                    resourceManager.setRegisterFree(msb);
                    resourceManager.setRegisterFree(lsb);
                }
            }
        }
    }

    private void modulo(Value value1, Value value2) {
        int lsb = 16, msb = 16;
        if (!value2.inRegister()) {
            lsb = resourceManager.getFreeRegister();
            msb = resourceManager.getFreeRegister();
            value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
        }
        divide(value1, value2);
        if (value1.getType().sizeof() == 1) {
            output.println("\tset r0, r1");
        } else {
            output.println("\tset r0, r2");
            output.println("\tset r1, r3");
        }
        resourceManager.setRegisterFree(msb);
        resourceManager.setRegisterFree(lsb);
    }

    private void or(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("value1 must be loaded in registers");
        }
        Type type = value1.getType();
        Type type2 = value2.getType();
        if (!type.equals(type2)) {
            throw new InternalCompilerError("Can only 'or' two numbers of the same type");
        }
        if (value2.inRegister()) {
            output.println("\tor r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
            if (type.sizeof() == 2) {
                output.println("\tor r" + value1.getRegisterMSB() + ", r" + value2.getRegisterMSB());
            }
        } else {
            long value = value2.getLongValue();
            if (value < 0 || value > 255) {
                int lsb = resourceManager.getFreeRegister();
                int msb = resourceManager.getFreeRegister();
                value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
                or(value1, value2);
                resourceManager.setRegisterFree(msb);
                resourceManager.setRegisterFree(lsb);
            } else {
                if (value != 0) {
                    output.println("\tori r" + value1.getRegisterLSB() + ", " + (value & 0xFF));
                }
            }
        }
    }

    private void and(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("value1 must be loaded in registers");
        }
        Type type = value1.getType();
        Type type2 = value2.getType();
        if (!type.equals(type2)) {
            throw new InternalCompilerError("Can only 'and' two numbers of the same type");
        }
        if (value2.inRegister()) {
            output.println("\tand r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
            if (type.sizeof() == 2) {
                output.println("\tand r" + value1.getRegisterMSB() + ", r" + value2.getRegisterMSB());
            }
        } else {
            long value = value2.getLongValue();
            if (value < 0 || value > 255) {
                int lsb = resourceManager.getFreeRegister();
                int msb = resourceManager.getFreeRegister();
                value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
                and(value1, value2);
                resourceManager.setRegisterFree(msb);
                resourceManager.setRegisterFree(lsb);
            } else {
                output.println("\tandi r" + value1.getRegisterLSB() + ", " + (value & 0xFF));
                if (type.sizeof() == 2) {
                    output.println("\tli r" + value1.getRegisterMSB() + ", 0");
                }
            }
        }
    }

    private void xor(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("value1 must be loaded in registers");
        }
        Type type = value1.getType();
        Type type2 = value2.getType();
        if (!type.equals(type2)) {
            throw new InternalCompilerError("Can only 'xor' two numbers of the same type");
        }
        if (value2.inRegister()) {
            output.println("\txor r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
            if (type.sizeof() == 2) {
                output.println("\txor r" + value1.getRegisterMSB() + ", r" + value2.getRegisterMSB());
            }
        } else {
            long value = value2.getLongValue();
            if (value < 0 || value > 15) {
                int lsb = resourceManager.getFreeRegister();
                int msb = resourceManager.getFreeRegister();
                value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
                xor(value1, value2);
                resourceManager.setRegisterFree(msb);
                resourceManager.setRegisterFree(lsb);
            } else {
                if (value != 0) {
                    output.println("\txori r" + value1.getRegisterLSB() + ", " + (value & 0xFF));
                }
            }
        }
    }

    private void shiftLeft(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("value1 must be loaded in registers");
        }
        Type type = value1.getType();
        if (value2.inRegister()) {
            output.println("\tshl r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
            if (type.sizeof() == 2) {
                int lsb = value1.getRegisterLSB();
                int msb = value1.getRegisterMSB();
                int counter = value2.getRegisterLSB();
                int address = resourceManager.getFreeRegister();
                String loopLabel = resourceManager.getNextLabel("shiftLeft_loop");
                String doneLabel = resourceManager.getNextLabel("shiftLeft_done");
                codeGenerator.writeLabel(loopLabel);
                output.println("\tor r" + counter + ", r" + counter);
                output.println("\tli r" + address + ", :" + doneLabel);
                output.println("\tlih r" + address + ", ::" + doneLabel);
                output.println("\tjpz [r" + address + "]");
                output.println("\tshli r" + lsb + ", 1");
                output.println("\tshlci r" + msb + ", 1");
                output.println("\tdec r" + counter);
                output.println("\tli r" + address + ", :" + loopLabel);
                output.println("\tlih r" + address + ", ::" + loopLabel);
                output.println("\tjp [r" + address + "]");
                codeGenerator.writeLabel(doneLabel);
                resourceManager.setRegisterFree(address);
            }
        } else {
            long value = value2.getLongValue();
            if (type.sizeof() == 2) {
                int lsb = value1.getRegisterLSB();
                int msb = value1.getRegisterMSB();
                if (value >= 16) {
                    output.println("\tset r" + msb + ", r" + lsb);
                    if (value > 16) output.println("\tshli r" + msb + ", " + (value - 16));
                    output.println("\tli r" + lsb + ", 0");
                } else {
                    if (value <= 2) {
                        for (int i = 0; i < value; i++) {
                            output.println("\tshli r" + lsb + ", 1");
                            output.println("\tshlci r" + msb + ", 1");
                        }
                    } else {
                        int reg = resourceManager.getFreeRegister();
                        output.println("\tset r" + reg + ", r" + lsb);
                        output.println("\tshli r" + lsb + ", " + value);
                        output.println("\tshli r" + msb + ", " + value);
                        output.println("\tshri r" + reg + ", " + (16-value));
                        output.println("\tor r" + msb + ", r" + reg);
                        resourceManager.setRegisterFree(reg);
                    }
                }
            } else {
                if (value > 15) {
                    output.println("\tli r" + value1.getRegisterLSB() + ", 0");
                } else {
                    output.println("\tshli r" + value1.getRegisterLSB() + ", " + (value & 15));
                }
            }
        }
    }

    private void shiftRight(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("value1 must be loaded in registers");
        }
        Type type = value1.getType();
        if (value2.inRegister()) {
            if (type.sizeof() == 2) {
                int lsb = value1.getRegisterLSB();
                int msb = value1.getRegisterMSB();
                int counter = value2.getRegisterLSB();
                int address = resourceManager.getFreeRegister();
                String loopLabel = resourceManager.getNextLabel("shiftRight_loop");
                String doneLabel = resourceManager.getNextLabel("shiftRight_done");
                codeGenerator.writeLabel(loopLabel);
                output.println("\tor r" + counter + ", r" + counter);
                output.println("\tli r" + address + ", :" + doneLabel);
                output.println("\tlih r" + address + ", ::" + doneLabel);
                output.println("\tjpz [r" + address + "]");
                output.println("\tshri r" + msb + ", 1");
                output.println("\tshrci r" + lsb + ", 1");
                output.println("\tdec r" + counter);
                output.println("\tli r" + address + ", :" + loopLabel);
                output.println("\tlih r" + address + ", ::" + loopLabel);
                output.println("\tjp [r" + address + "]");
                codeGenerator.writeLabel(doneLabel);
                resourceManager.setRegisterFree(address);
            } else {
                output.println("\tshr r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
            }
        } else {
            long value = value2.getLongValue();
            if (type.sizeof() == 2) {
                int lsb = value1.getRegisterLSB();
                int msb = value1.getRegisterMSB();
                if (value >= 16) {
                    output.println("\tset r" + lsb + ", r" + msb);
                    if (value > 16) output.println("\tshri r" + lsb + ", " + (value - 16));
                    output.println("\tli r" + msb + ", 0");
                } else {
                    if (value == 0) return;
                    if (value <= 2) {
                        for (int i = 0; i < value; i++) {
                            output.println("\tshri r" + msb + ", 1");
                            output.println("\tshrci r" + lsb + ", 1");
                        }
                    } else {
                        int reg = resourceManager.getFreeRegister();
                        output.println("\tset r" + reg + ", r" + msb);
                        output.println("\tshri r" + lsb + ", " + value);
                        output.println("\tshri r" + msb + ", " + value);
                        output.println("\tshli r" + reg + ", " + (16-value));
                        output.println("\tor r" + lsb + ", r" + reg);
                        resourceManager.setRegisterFree(reg);
                    }
                }
            } else {
                if (value > 15) {
                    output.println("\tset r" + value1.getRegisterLSB() + ", 0");
                } else {
                    output.println("\tshri r" + value1.getRegisterLSB() + ", " + value);
                }
            }
        }
    }

    private void arithmeticShiftRight(Value value1, Value value2) {
        if (!value1.inRegister()) {
            throw new InternalCompilerError("value1 must be loaded in registers");
        }
        Type type = value1.getType();
        if (value2.inRegister()) {
            if (type.sizeof() == 2) {
                int lsb = value1.getRegisterLSB();
                int msb = value1.getRegisterMSB();
                int counter = value2.getRegisterLSB();
                int address = resourceManager.getFreeRegister();
                String loopLabel = resourceManager.getNextLabel("shiftRight_loop");
                String doneLabel = resourceManager.getNextLabel("shiftRight_done");
                codeGenerator.writeLabel(loopLabel);
                output.println("\tor r" + counter + ", r" + counter);
                output.println("\tli r" + address + ", :" + doneLabel);
                output.println("\tlih r" + address + ", ::" + doneLabel);
                output.println("\tjpz [r" + address + "]");
                output.println("\tashri r" + msb + ", 1");
                output.println("\tshrci r" + lsb + ", 1");
                output.println("\tdec r" + counter);
                output.println("\tli r" + address + ", :" + loopLabel);
                output.println("\tlih r" + address + ", ::" + loopLabel);
                output.println("\tjp [r" + address + "]");
                codeGenerator.writeLabel(doneLabel);
                resourceManager.setRegisterFree(address);
            } else {
                output.println("\tashr r" + value1.getRegisterLSB() + ", r" + value2.getRegisterLSB());
            }
        } else {
            long value = value2.getLongValue();
            if (type.sizeof() == 2) {
                int lsb = value1.getRegisterLSB();
                int msb = value1.getRegisterMSB();
                if (value >= 16) {
                    output.println("\tset r" + lsb + ", r" + msb);
                    if (value > 16) output.println("\tshri r" + lsb + ", " + (value - 16));
                    output.println("\tor r" + msb + ", r" + msb);
                    output.println("\tss r" + msb);
                } else {
                    if (value == 0) return;
                    if (value <= 2) {
                        for (int i = 0; i < value; i++) {
                            output.println("\tashri r" + msb + ", 1");
                            output.println("\tshrci r" + lsb + ", 1");
                        }
                    } else {
                        int reg = resourceManager.getFreeRegister();
                        output.println("\tset r" + reg + ", r" + msb);
                        output.println("\tshri r" + lsb + ", " + value);
                        output.println("\tashri r" + msb + ", " + value);
                        output.println("\tshli r" + reg + ", " + (16 - value));
                        output.println("\tor r" + lsb + ", r" + reg);
                        resourceManager.setRegisterFree(reg);
                    }
                }
            } else {
                if (value > 15) {
                    output.println("\tor r" + value1.getRegisterLSB() + ", r" + value1.getRegisterLSB());
                    output.println("\tss r" + value1.getRegisterLSB());
                } else {
                    output.println("\tashri r" + value1.getRegisterLSB() + ", " + value);
                }
            }
        }
    }

    private void booleanEquals(Value value1, Value value2) {
        if (value1.getType().sizeof() == 2) {
            xor(value1, value2);
            output.println("\tor r" + value1.getRegisterLSB() + ", r" + value1.getRegisterMSB());
        } else {
            subtract(value1, value2);
        }
        output.println("\tsz r" + value1.getRegisterLSB());
    }

    private void booleanNotEquals(Value value1, Value value2) {
        if (value1.getType().sizeof() == 2) {
            xor(value1, value2);
            output.println("\tor r" + value1.getRegisterLSB() + ", r" + value1.getRegisterMSB());
        } else {
            subtract(value1, value2);
        }
        output.println("\tsnz r" + value1.getRegisterLSB());
    }

    private void smallerThan(Value value1, Value value2) {
        subtract(value1, value2);
        Type type = value1.getType();
        if (type == INT8 || type == INT16 || type == INT32) {
            output.println("\tss r" + value1.getRegisterLSB());
        } else {
            output.println("\tsnc r" + value1.getRegisterLSB());
        }
    }

    private void greaterThan(Value value1, Value value2) {
        if (value2.inRegister()) {
            subtract(value2, value1);
        } else {
            int lsb = resourceManager.getFreeRegister();
            int msb = resourceManager.getFreeRegister();
            value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
            subtract(value2, value1);
            resourceManager.setRegisterFree(msb);
            resourceManager.setRegisterFree(lsb);
        }
        Type type = value1.getType();
        if (type == INT8 || type == INT16 || type == INT32) {
            output.println("\tss r" + value1.getRegisterLSB());
        } else {
            output.println("\tsnc r" + value1.getRegisterLSB());
        }
    }

    private void smallerThanOrEqual(Value value1, Value value2) {
        if (value2.inRegister()) {
            subtract(value2, value1);
        } else {
            int lsb = resourceManager.getFreeRegister();
            int msb = resourceManager.getFreeRegister();
            value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
            subtract(value2, value1);
            resourceManager.setRegisterFree(msb);
            resourceManager.setRegisterFree(lsb);
        }
        Type type = value1.getType();
        if (type == INT8 || type == INT16 || type == INT32) {
            output.println("\tsns r" + value1.getRegisterLSB());
        } else {
            output.println("\tsc r" + value1.getRegisterLSB());
        }
    }

    private void greaterThanOrEqual(Value value1, Value value2) {
        subtract(value1, value2);
        Type type = value1.getType();
        if (type == INT8 || type == INT16 || type == INT32) {
            output.println("\tsns r" + value1.getRegisterLSB());
        } else {
            output.println("\tsc r" + value1.getRegisterLSB());
        }
    }

    /**
     * Perform an operation on two values that are (were) present in registers.
     * Both values should be loaded in registers.  Value1 will hold the result.
     *
     * @param operation
     * @param destval1
     * @param destval2
     * @return
     */
    public Value performOperationOnRegister(Token operation, DestinationValue destval1, DestinationValue destval2) {
        if (!destval1.inRegister()) {
            throw new InternalCompilerError("Destval1 is supposed to be saved in registers...");
        }
        Value value1, value2;
        if (destval1 instanceof Destination) {
            Destination destination = (Destination) destval1;
            value1 = codeGenerator.loadValueInRegisters(destination, 0, 1);
        } else {
            value1 = (Value) destval1;
        }
        if (destval2 instanceof Destination) {
            Destination destination = (Destination) destval2;
            value2 = codeGenerator.loadValueInRegisters(destination, 0, 1);
        } else {
            value2 = (Value) destval2;
        }
        int msb1 = 16, msb2 = 16;
        // assign extra register if only one was reserved
        if (value1.inRegister() && value1.getRegisterMSB() == -1) {
            msb1 = resourceManager.getFreeRegister();
            value1 = new Value(value1.getRegisterLSB(), msb1, value1.getType());
        }
        if (value2.inRegister() && value2.getRegisterMSB() == -1) {
            msb2 = resourceManager.getFreeRegister();
            value2 = new Value(value2.getRegisterLSB(), msb2, value2.getType());
        }

        // cast both values up to the resulting type
        Type resultType = findResultType(value1.getType(), value2.getType(), operation);
        value1 = castUp(value1, resultType);
        if (operation != Token.SHL && operation != Token.SHR && operation != Token.ASHR) {
            value2 = castUp(value2, resultType);
        }

        // perform the calculation
        switch (operation) {
            case PLUS:
                add(value1, value2);
                break;
            case MINUS:
                subtract(value1, value2);
                break;
            case ASTERISK:
                multiply(value1, value2);
                break;
            case SLASH:
                divide(value1, value2);
                break;
            case PERCENT:
                modulo(value1, value2);
                break;
            case PIPE:
                or(value1, value2);
                break;
            case AMPERSAND:
                and(value1, value2);
                break;
            case CIRCUMFLEX:
                xor(value1, value2);
                break;
            case SHL:
                shiftLeft(value1, value2);
                break;
            case SHR:
                shiftRight(value1, value2);
                break;
            case ASHR:
                arithmeticShiftRight(value1, value2);
                break;
            case BOOL_EQUALS:
                booleanEquals(value1, value2);
                break;
            case BOOL_NOT_EQUALS:
                booleanNotEquals(value1, value2);
                break;
            case SMALLER_THAN:
                smallerThan(value1, value2);
                break;
            case GREATER_THAN:
                greaterThan(value1, value2);
                break;
            case SMALLER_THAN_OR_EQUAL:
                smallerThanOrEqual(value1, value2);
                break;
            case GREATER_THAN_OR_EQUAL:
                greaterThanOrEqual(value1, value2);
                break;
            default:
                throw new InternalCompilerError("operation " + operation + " (on integers) not implemented yet");
        }
        if ((operation == Token.BOOL_EQUALS) || (operation == Token.BOOL_NOT_EQUALS) || (operation == Token.SMALLER_THAN) || (operation == Token.GREATER_THAN) || (operation == Token.SMALLER_THAN_OR_EQUAL) || (operation == Token.GREATER_THAN_OR_EQUAL)) {
            resultType = PrimitiveType.UINT16;
        }
        Value result = new Value(value1.getRegisterLSB(), value1.getRegisterMSB(), resultType);

        resourceManager.setRegisterFree(msb2);
        resourceManager.setRegisterFree(msb1);

        return result;
    }

    /**
     * Casts a value to a certain type.
     * It is assumed that the cast is always upwards.
     * If the value is saved in registers, it is assumed 2 registers were allocated to it
     *
     * @param value
     * @param resultType
     * @return
     */
    private Value castUp(Value value, Type resultType) {
        if ((value.getType() instanceof RecordType) || (value.getType() instanceof ArrayType)) {
            throw new InternalCompilerError("Cannot cast a record or array type to anything else");
        }
        if (!value.inRegister()) {
            return new Value(resultType, value.getValue());
        }
        if (value.getType().sizeof() == resultType.sizeof()) {
            return new Value(value.getRegisterLSB(), value.getRegisterMSB(), resultType);
        } else if (value.getType().sizeof() < resultType.sizeof()) {
            if (resultType == UINT32) {
                output.println("\tli r" + value.getRegisterMSB() + ", 0");
            } else {
                output.println("\tor r" + value.getRegisterLSB() + ", r" + value.getRegisterLSB());
                output.println("\tss r" + value.getRegisterMSB());
            }
            return new Value(value.getRegisterLSB(), value.getRegisterMSB(), resultType);
        } else {
            throw new InternalCompilerError("can only cast up...");
        }
    }

    /**
     * Casts a value to a certain type.
     * Information can be lost if the cast is to a smaller type.
     */
    public Value castTo(Value value, Type resultType) {
        if ((value.getType() instanceof RecordType) || (value.getType() instanceof ArrayType)) {
            throw new InternalCompilerError("Cannot cast a record or array type to anything else");
        }
        if ((resultType instanceof RecordType) || (resultType instanceof ArrayType)) {
            throw new InternalCompilerError("Cannot cast to a record or array type");
        }
        if (!value.inRegister()) {
            return new Value(resultType, value.getValue());
        }
        if (resultType.sizeof() >= value.getType().sizeof()) {
            return castUp(value, resultType);
        } else {
            return new Value(value.getRegisterLSB(), -1, resultType);
        }
    }

    /**
     * Perform an operation on two values that are constants.
     *
     * @param operation
     * @param value1
     * @param value2
     * @return
     */
    public Value performOperationOnConstants(Token operation, Value value1, Value value2) {
        if (value1.inRegister() || value2.inRegister()) {
            throw new InternalCompilerError("values are supposed to be constants...");
        }
        Type type1 = value1.getType();
        Type type2 = value2.getType();
        if ((!(type1 instanceof PrimitiveType) && !(type1 instanceof PointerType)) || (!(type2 instanceof PrimitiveType) && !(type2 instanceof PointerType))) {
            throw new InternalCompilerError("can only perform operations on primitive or pointer types");
        }
        if (type1 instanceof PointerType) {
            value1 = new Value(PrimitiveType.UINT16, value1.getValue());
        }
        if (type2 instanceof PointerType) {
            value2 = new Value(PrimitiveType.UINT16, value2.getValue());
        }
        if (value1.isLabel() || value2.isLabel()) {
            int lsb = resourceManager.getFreeRegister();
            int msb = resourceManager.getFreeRegister();
            value1 = codeGenerator.loadValueInRegisters(value1, 0, 1);
            value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
            Value result = performOperationOnRegister(operation, value1, value2);
            resourceManager.setRegisterFree(lsb);
            resourceManager.setRegisterFree(msb);
            return result;
        }
        long val1, val2;
        val1 = value1.getLongValue();
        val2 = value2.getLongValue();
        long result;
        switch (operation) {
            case PLUS:
                result = val1 + val2;
                break;
            case MINUS:
                result = val1 - val2;
                break;
            case ASTERISK:
                result = val1 * val2;
                break;
            case SLASH:
                result = val1 / val2;
                break;
            case PERCENT:
                result = val1 % val2;
                break;
            case PIPE:
                result = val1 | val2;
                break;
            case AMPERSAND:
                result = val1 & val2;
                break;
            case CIRCUMFLEX:
                result = val1 ^ val2;
                break;
            case SHL:
                result = val1 << val2;
                break;
            case SHR:
                result = val1 >>> val2;
                break;
            case ASHR:
                result = val1 >> val2;
                break;
            case BOOL_EQUALS:
                result = (val1 == val2) ? -1 : 0;
                break;
            case BOOL_NOT_EQUALS:
                result = (val1 != val2) ? -1 : 0;
                break;
            case SMALLER_THAN:
                result = (val1 < val2) ? -1 : 0;
                break;
            case GREATER_THAN:
                result = (val1 > val2) ? -1 : 0;
                break;
            case SMALLER_THAN_OR_EQUAL:
                result = (val1 <= val2) ? -1 : 0;
                break;
            case GREATER_THAN_OR_EQUAL:
                result = (val1 >= val2) ? -1 : 0;
                break;
            default:
                throw new InternalCompilerError("unsupported operation " + operation);
        }
        Type resultType = findResultType(value1.getType(), value2.getType(), operation);
        if (resultType instanceof PointerType) {
            result = result & 0xFFFF;
        }
        if (result < -32768) {
            resultType = PrimitiveType.INT32;
        } else if (result < -128) {
            resultType = PrimitiveType.INT16;
        } else if (result < 0) {
            resultType = INT8;
        } else if (result > Integer.MAX_VALUE) {
            resultType = PrimitiveType.UINT32;
        } else if (result > 65535) {
            resultType = PrimitiveType.UINT32;
        } else if (result > 127) {
            resultType = PrimitiveType.UINT16;
        } else {
            resultType = UINT8;
        }
        return new Value(resultType, result);
    }

    private Type findResultType(Type type1, Type type2, Token operation) {
        if (type1 instanceof ArrayType || type1 instanceof RecordType || type2 instanceof ArrayType || type2 instanceof RecordType) {
            throw new InternalCompilerError("cannot do an operation on arrays or records");
        }
        if (operation == Token.SHL || operation == Token.SHR || operation == Token.ASHR) {
            return type1;
        }
        if (type1 instanceof PointerType) {
            return type1;
        }
        if (type2 instanceof PointerType) {
            return type2;
        }
        if (type1 == PrimitiveType.INT32 || type2 == PrimitiveType.INT32) {
            return PrimitiveType.INT32;
        }
        if (type1 == PrimitiveType.UINT32 || type2 == PrimitiveType.UINT32) {
            return PrimitiveType.UINT32;
        }
        if (type1 == PrimitiveType.INT16 || type2 == PrimitiveType.INT16) {
            return PrimitiveType.INT16;
        }
        if (type1 == PrimitiveType.UINT16 || type2 == PrimitiveType.UINT16) {
            return PrimitiveType.UINT16;
        }
        if (type1 == INT8 || type2 == INT8) {
            return INT8;
        }
        return UINT8;
    }

    /**
     * assumes that the value is loaded in registers
     *
     * @param operation
     * @param value
     * @return
     */
    public DestinationValue performUnaryOperation(Token operation, DestinationValue value) {
        if (!value.inRegister()) {
            throw new InternalCompilerError("can only perform unary operation on value in registers");
        }
        switch (operation) {
            case EXCLAMATION:
                not(value);
                return value;
            case TILDE:
                invert(value);
                return value;
            case CIRCUMFLEX:
                return followPointer(value);
            default:
                throw new InternalCompilerError("unsupported operation: " + operation);
        }
    }

    public Value performUnaryOperationOnConstant(Token operation, Value value) {
        if (value.inRegister()) {
            throw new InternalCompilerError("can only perform unary operation on a constant");
        }
        long val = value.getLongValue();
        switch (operation) {
            case EXCLAMATION:
                return new Value(value.getType(), ((val == 0) ? -1L : 0L));
            case TILDE:
                return new Value(value.getType(), (~val));
            case CIRCUMFLEX:
                throw new InternalCompilerError("cannot follow a pointer that is a constant (unknown type)");
            default:
                throw new InternalCompilerError("unsupported operation: " + operation);
        }
    }
}
