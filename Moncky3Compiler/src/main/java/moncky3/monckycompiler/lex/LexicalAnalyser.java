package moncky3.monckycompiler.lex;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;

import static java.io.StreamTokenizer.*;
import static moncky3.monckycompiler.lex.Token.*;

public class LexicalAnalyser {
    public String stringValue;
    public long intValue;
    public double floatValue;
    public Token lastToken;
    private StreamTokenizer tokenizer;
    private boolean putBack;
    private int lastLine;

    public LexicalAnalyser() {
        this.lastLine = 0;
    }

    public void setFilename(String filename) throws FileNotFoundException {
        this.tokenizer = new StreamTokenizer(new FileReader(filename));
        tokenizer.eolIsSignificant(false);
        tokenizer.lowerCaseMode(false);
        tokenizer.parseNumbers();
        tokenizer.quoteChar('\"');
        tokenizer.slashSlashComments(true);
        tokenizer.slashStarComments(true);
        tokenizer.ordinaryChar(';');
        tokenizer.ordinaryChar('/');
        tokenizer.wordChars('_', '_');
        tokenizer.ordinaryChar('.');
        tokenizer.wordChars(128, 255);
        putBack = false;
    }

    public int lineNumber() {
        return lastLine;
    }

    public void putBack() {
        putBack = true;
    }

    public Token nextToken() {
        if (!putBack) {
            lastLine = tokenizer.lineno();
        }
        try {
            if (putBack) {
                putBack = false;
                return lastToken;
            }
            int token = TT_EOL;
            while (token == TT_EOL) {
                token = tokenizer.nextToken();
            }
            stringValue = tokenizer.sval;
            switch (token) {
                case TT_EOF:
                    lastToken = EOF;
                    return lastToken;
                case TT_NUMBER:
                    lastToken = processNumber();
                    return lastToken;
                case TT_WORD:
                    lastToken = processString();
                    return lastToken;
                case '\"':
                    processStringLiteral();
                    lastToken = STRING_LITERAL;
                    return lastToken;
                case '\'':
                    lastToken = processCharLiteral();
                    return lastToken;
                default:
                    lastToken = processCharacter(token);
                    return lastToken;
            }
        } catch (IOException e) {
            lastToken = EOF;
            return lastToken;
        }
    }

    private Token processCharLiteral() {
        stringValue = tokenizer.sval;
        lastToken = CHAR_LITERAL;
        return lastToken;
    }

    private void processStringLiteral() {
        stringValue = tokenizer.sval;
    }

    private Token processCharacter(int token) throws IOException {
        char[] value = new char[1];
        value[0] = (char) token;
        stringValue = new String(value);

        char nextToken = (char) tokenizer.nextToken();

        switch (value[0]) {
            case '=': {
                if (nextToken == '=') {
                    stringValue = "==";
                    return BOOL_EQUALS;
                } else {
                    tokenizer.pushBack();
                    return EQUALS;
                }
            }
            case '+':
                tokenizer.pushBack();
                return PLUS;
            case ',':
                tokenizer.pushBack();
                return COMMA;
            case '?':
                tokenizer.pushBack();
                return QUESTION_MARK;
            case ':':
                tokenizer.pushBack();
                return COLON;
            case '-':
                tokenizer.pushBack();
                return MINUS;
            case '*':
                tokenizer.pushBack();
                return ASTERISK;
            case '/':
                tokenizer.pushBack();
                return SLASH;
            case '%':
                tokenizer.pushBack();
                return PERCENT;
            case '|':
                if (nextToken == '|') {
                    stringValue = "||";
                    return BOOL_OR;
                } else {
                    tokenizer.pushBack();
                    return PIPE;
                }
            case '&':
                if (nextToken == '&') {
                    stringValue = "&&";
                    return BOOL_AND;
                } else {
                    tokenizer.pushBack();
                    return AMPERSAND;
                }
            case '^':
                tokenizer.pushBack();
                return CIRCUMFLEX;
            case '<': {
                if (nextToken == '<') {
                    stringValue = "<<";
                    return SHL;
                } else if (nextToken == '=') {
                    stringValue = "<=";
                    return SMALLER_THAN_OR_EQUAL;
                } else {
                    tokenizer.pushBack();
                    return SMALLER_THAN;
                }
            }
            case '>': {
                if (nextToken == '>') {
                    nextToken = (char) tokenizer.nextToken();
                    if (nextToken == '>') {
                        stringValue = ">>>";
                        return ASHR;
                    } else {
                        stringValue = ">>";
                        tokenizer.pushBack();
                        return SHR;
                    }
                }
                if (nextToken == '=') {
                    stringValue = ">=";
                    return GREATER_THAN_OR_EQUAL;
                } else {
                    tokenizer.pushBack();
                    return GREATER_THAN;
                }
            }
            case '!': {
                if (nextToken == '=') {
                    stringValue = "!=";
                    return BOOL_NOT_EQUALS;
                } else {
                    tokenizer.pushBack();
                    return EXCLAMATION;
                }
            }
            case '(':
                tokenizer.pushBack();
                return LEFT_BRACKET;
            case ')':
                tokenizer.pushBack();
                return RIGHT_BRACKET;
            case '{':
                tokenizer.pushBack();
                return LEFT_CURLY;
            case '}':
                tokenizer.pushBack();
                return RIGHT_CURLY;
            case ';':
                tokenizer.pushBack();
                return SEMICOLON;
            case '[':
                tokenizer.pushBack();
                return LEFT_SQUARE;
            case ']':
                tokenizer.pushBack();
                return RIGHT_SQUARE;
            case '.':
                tokenizer.pushBack();
                return POINT;
            case '~':
                tokenizer.pushBack();
                return TILDE;
            default:
                return UNKNOWN;
        }
    }

    private Token processString() {
        switch (tokenizer.sval) {
            case "uint8":
                return UINT8;
            case "uint16":
                return UINT16;
            case "uint32":
                return UINT32;
            case "int8":
                return INT8;
            case "int16":
                return INT16;
            case "int32":
                return INT32;
            case "if":
                return IF;
            case "else":
                return ELSE;
            case "while":
                return WHILE;
            case "return":
                return RETURN;
            case "pointer":
                return POINTER;
            case "to":
                return TO;
            case "array":
                return ARRAY;
            case "of":
                return OF;
            case "call":
                return CALL;
            case "false":
                return FALSE;
            case "true":
                return TRUE;
            case "let":
                return LET;
            case "fun":
                return FUN;
            case "var":
                return VAR;
            case "const":
                return CONST;
            case "main":
                return MAIN;
            case "record":
                return RECORD;
            default:
                return IDENTIFIER;
        }
    }

    private Token processNumber() throws IOException {
        double nval = tokenizer.nval;
        int token = tokenizer.nextToken();
        if (token == TT_WORD && tokenizer.sval.startsWith("x")) {
            intValue = Long.valueOf(tokenizer.sval.substring(1), 16);
            floatValue = intValue;
            stringValue = "" + intValue;
            return INTEGER_LITERAL;
        } else {
            tokenizer.pushBack();
            floatValue = nval;
            intValue = (long) nval;
            if (intValue == floatValue) {
                stringValue = "" + intValue;
                return INTEGER_LITERAL;
            } else {
                stringValue = "" + floatValue;
                return FLOAT_LITERAL;
            }
        }
    }
}
