package moncky3.monckycompiler;

import java.io.PrintStream;

public class Optimizer extends PrintStream {
    private PrintStream assemblyFile;
    private int reg;
    private boolean pushDetected;

    public Optimizer(PrintStream assemblyFile) {
        super(assemblyFile);
        this.assemblyFile = assemblyFile;
        this.reg = 0;
        this.pushDetected = false;
    }

    @Override
    public void println(String s) {
        if (s.startsWith("\tpush r")) {
            if (pushDetected) {
                assemblyFile.println("\tpush r" + reg);
            }
            reg = Integer.parseInt(s.substring(7));
            pushDetected = true;
        } else if (s.startsWith("\tpop r")) {
            if (pushDetected) {
                int reg2 = Integer.parseInt(s.substring(6));
                if (reg2 != reg) {
                    assemblyFile.println("\tpush r" + reg);
                    assemblyFile.println(s);
                }
            } else {
                assemblyFile.println(s);
            }
            pushDetected = false;
        } else {
            if (pushDetected) {
                assemblyFile.println("\tpush r" + reg);
            }
            pushDetected = false;
            assemblyFile.println(s);
        }
    }
}
