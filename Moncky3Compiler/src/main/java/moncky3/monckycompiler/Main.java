package moncky3.monckycompiler;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Compiler compiler = new Compiler();
        compiler.compile("files/test.src");
    }
}
