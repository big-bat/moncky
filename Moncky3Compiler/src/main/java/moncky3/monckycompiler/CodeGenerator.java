package moncky3.monckycompiler;

import moncky3.monckycompiler.lex.Token;
import moncky3.monckycompiler.parsing.Destination;
import moncky3.monckycompiler.parsing.DestinationValue;
import moncky3.monckycompiler.parsing.Value;
import moncky3.monckycompiler.resources.Function;
import moncky3.monckycompiler.resources.ResourceManager;
import moncky3.monckycompiler.resources.Variable;
import moncky3.monckycompiler.types.*;
import sun.security.krb5.internal.crypto.Des;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CodeGenerator {
    private final ResourceManager resourceManager;
    private final CalculationGenerator calculationGenerator;
    private PrintStream output;

    public CodeGenerator(ResourceManager resourceManager) {
        this.output = System.out;
        this.resourceManager = resourceManager;
        this.calculationGenerator = new CalculationGenerator(this, resourceManager);
    }

    public void setPrintStream(PrintStream printStream) {
        this.output = printStream;
        calculationGenerator.setPrintStream(printStream);
    }

    public void createStringConstants() {
        Map<String, String> stringConstants = resourceManager.getStringConstants();
        if (stringConstants.size() == 0) return;
        output.println("; *****");
        output.println("; * String constants");
        output.println("; *****");
        for (String stringValue : stringConstants.keySet()) {
            String label = stringConstants.get(stringValue);
            output.println(":" + label);
            output.println(".data \"" + stringValue + "\"");
        }
        output.println();
    }

    public void createArrayConstants() {
        Map<String, List<Value>> arrayConstants = resourceManager.getArrayConstants();
        if (arrayConstants.size() == 0) return;
        output.println("; *****");
        output.println("; * Array constants");
        output.println("; *****");
        for (String label : arrayConstants.keySet()) {
            List<Value> array = arrayConstants.get(label);
            output.println(":" + label);
            printArrayValues(array);
        }
        output.println();
    }

    private void printArrayValues(List array) {
        Object element = array.get(0);
        if (!(element instanceof Value)) {
            throw new InternalCompilerError("elements of array should be values");
        }
        Type type = ((Value) element).getType();
        output.print(".data [ ");
        for (int i = 0; i < array.size() - 1; i++) {
            printArrayElement(array, type, i, ", ");
        }
        printArrayElement(array, type, array.size() - 1, " ]");
        output.println();
    }

    private void printArrayElement(List array, Type type, int i, String end) {
        Object element = array.get(i);
        if (!(element instanceof Value)) {
            throw new InternalCompilerError("elements of an array should be Values");
        }
        Value value = (Value) element;
        if (type.sizeof() == 1) {
            output.print(value.getValue() + end);
        } else {
            if (type == PrimitiveType.INT32 || type == PrimitiveType.UINT32) {
                long intValue = value.getLongValue();
                output.print((intValue & 0xFFFF) + ", " + (intValue >>> 16) + end);
            } else {
                throw new InternalCompilerError("array literal can only consist of integer values");
            }
        }
    }

    public void defineGlobalVariable(Variable variable) {
        if (variable.isLocal()) {
            throw new InternalCompilerError("trying to define a local variable as global");
        }
        output.println(":globalVar_" + variable.getName());
        Type type = variable.getType();
        if (type instanceof RecordType) {
            ArrayType arrayType = new ArrayType(PrimitiveType.UINT16, type.sizeof());
            variable = new Variable(variable.getName(), arrayType, variable.getAddress(), false, variable.getInitValue());
            type = arrayType;
        }
        Object initValue = variable.getInitValue();
        long val = -1;
        if (initValue instanceof Long) {
            val = (Long)initValue;
        }
        if (type instanceof ArrayType) {
            ArrayType arrayType = (ArrayType) type;
            if (!(initValue instanceof List)) {
                initValue = new ArrayList((int)arrayType.getLength());
                for(int i = 0; i<arrayType.sizeof(); i++) {
                    ((ArrayList<Value>) initValue).add(new Value(PrimitiveType.UINT16, 0L));
                }
            }
            printArrayValues((List) initValue);
        } else if (type instanceof PointerType) {
            output.println(".data :" + initValue);
        } else if (type.sizeof() == 1) {
            output.println(".data " + (val & 0xFFFF));
        } else if (type.sizeof() == 2) {
            output.println(".data " + (val & 0xFFFF) + ", " + ((val >>> 16) & 0xFFFF));
        } else {
            throw new InternalCompilerError("strange error: length of type should be ok");
        }
    }

    public void generateInitialCode(String sourceFile) {
        output.println("; *****");
        output.println("; * This file was generated by the Moncky-3 compiler.");
        output.println("; * Source file: " + sourceFile);
        output.println("; *****");
        output.println();
        output.println(":start_system");
        output.println("\tli r0, :graphics_init");
        output.println("\tlih r0, ::graphics_init");
        output.println("\tcall [r0]");
        output.println();
        output.println("\tli r0, :text_init");
        output.println("\tlih r0, ::text_init");
        output.println("\tcall [r0]");
        output.println();
        output.println("\tli r0, :keyboard_init");
        output.println("\tlih r0, ::keyboard_init");
        output.println("\tcall [r0]");
        output.println();
        output.println("\tli r0, :spi_init");
        output.println("\tlih r0, ::spi_init");
        output.println("\tcall [r0]");
        output.println();
        output.println("\tli r0, :begin_heap");
        output.println("\tlih r0, ::begin_heap");
        output.println("\tpush r0");
        output.println("\tli r0, :memory_init");
        output.println("\tlih r0, ::memory_init");
        output.println("\tcall [r0]");
        output.println("\taddi sp, 1");
        output.println();
        output.println("\tli r0, :function_main");
        output.println("\tlih r0, ::function_main");
        output.println("\tcall [r0]");
        output.println("\thalt");
        output.println();
    }

    /**
     * Generates code (if necessary) to load the given destination into a register
     *
     * @return the modified destination object
     */
    public Destination loadDestinationInRegister(Destination destination, int register) {
        if (!destination.inRegister()) {
            if (destination.isLocal()) {
                int offset = destination.getAddressAsInt();
                output.println("\tset r" + register + ", bp");
                addConstantToRegister(offset, register);
            } else {
                String label = destination.getAddressAsString();
                loadLabelInRegister(register, label);
            }
            destination = new Destination(destination.getType(), null, destination.isLocal(), register);
        }
        return destination;
    }

    private void loadLabelInRegister(int register, String label) {
        output.println("\tli r" + register + ", :" + label);
        output.println("\tlih r" + register + ", ::" + label);
    }

    /**
     * loads the value of a pointer into registers.
     *
     * @param pointerReg the register number containing the pointer
     * @param type       the type of the value where the pointer is pointing to
     * @param lsb        the lsb destination register in which the value needs to be loaded (it can be equal to pointerReg)
     * @param msb        the msb destination register in which the value needs to be loaded (not equal to pointerReg) (can be -1 if not needed)
     */
    public void loadPointer(int pointerReg, Type type, int lsb, int msb) {
        if (type.sizeof() > 2) {
            throw new InternalCompilerError("cannot load type " + type + " in 2 registers");
        }
        if (type.sizeof() == 2) {
            output.println("\tldi r" + msb + ", (r" + pointerReg + "+1)");
        }
        output.println("\tld r" + lsb + ", (r" + pointerReg + ")");
    }

    /**
     * Adds a constant value to a register.
     *
     * @param value
     * @param register
     */
    public void addConstantToRegister(int value, int register) {
        if (value == 0) return;
        if (value < 0) {
            if (value > -129) {
                output.println("\taddi r" + register + ", " + value);
            } else {
                int reg = resourceManager.getFreeRegister();
                output.println("\tli r" + reg + ", " + ((-value) & 0xFF));
                if (value < -255) {
                    output.println("\tlih r" + reg + ", " + ((-value) / 256));
                }
                output.println("\tsub r" + register + ", r" + reg);
                resourceManager.setRegisterFree(reg);
            }
        } else if (value < 128) {
            output.println("\taddi r" + register + ", " + value);
        } else {
            int reg = resourceManager.getFreeRegister();
            output.println("\tli r" + reg + ", " + (value & 0xFF));
            if (value > 255) {
                output.println("\tlih r" + reg + ", " + (value / 256));
            }
            output.println("\tadd r" + register + ", r" + reg);
            resourceManager.setRegisterFree(reg);
        }
    }

    /**
     * puts a comment in the assembly file
     *
     * @param comment
     */
    public void comment(String comment) {
        output.println("; " + comment);
    }

    /**
     * adds register2 to register1.
     *
     * @param register1
     * @param register2
     */
    public void addRegisters(int register1, int register2) {
        output.println("\tadd r" + register1 + ", r" + register2);
    }

    public Value loadValueInRegisters(Value value, int lsb, int msb) {
        if (value.inRegister()) {
            if (value.getRegisterLSB() != lsb) {
                output.println("\tset r" + lsb + ", r" + value.getRegisterLSB());
            }
            if (value.getRegisterMSB() != msb && value.getType().sizeof() == 2) {
                output.println("\tset r" + msb + ", r" + value.getRegisterMSB());
            }
        } else {
            Type type = value.getType();
            if (type.sizeof() > 2) {
                throw new InternalCompilerError("cannot load a value in registers that is larger than 32 bits (" + type + ")");
            }
            if (type instanceof PrimitiveType) {
                value = loadPrimitiveConstantInRegisters(value, lsb, msb);
            } else if (type instanceof PointerType) {
                value = loadPointerInRegisters(value, lsb, msb);
            } else {
                throw new InternalCompilerError("Loading of non-primitive value in registers not implemented");
            }
        }
        return new Value(lsb, msb, value.getType());
    }

    public Value loadValueInRegisters(DestinationValue destval, int lsb, int msb) {
        if (destval instanceof Destination) {
            return loadValueInRegisters((Destination) destval, lsb, msb);
        } else {
            return loadValueInRegisters((Value) destval, lsb, msb);
        }
    }

    public Value loadValueInRegisters(Destination destination, int lsb, int msb) {
        Type type = destination.getType();
        if (type.sizeof() > 2) {
            throw new InternalCompilerError("cannot load a value in registers that is larger than 32 bits (" + type + ")");
        }
        if (type instanceof PrimitiveType) {
            loadPrimitiveInRegisters(destination, lsb, msb);
        } else if (type instanceof ArrayType) {
            destination = new Destination(type, destination.getAddress(), destination.isLocal(), destination.getRegisterLSB());
            loadValueInRegisters(destination, lsb, msb);
        } else if (type instanceof PointerType) {
            if (!destination.inRegister()) {
                destination = loadDestinationInRegister(destination, 2);
            }
            // change pointer type to uint16 so it can be loaded as a primitive type
            destination = new Destination(PrimitiveType.UINT16, destination.getAddress(), destination.isLocal(), destination.getRegisterLSB());
            loadValueInRegisters(destination, lsb, msb);
        } else if (type instanceof RecordType) {
            throw new InternalCompilerError("cannot load a record into r0/r1");
        } else {
            throw new InternalCompilerError("unknown type");
        }
        return new Value(lsb, msb, type);
    }

    private Value loadPrimitiveConstantInRegisters(Value value, int lsb, int msb) {
        if (value.inRegister()) {
            throw new InternalCompilerError("value already loaded in registers");
        }
        if (!(value.getType() instanceof PrimitiveType)) {
            throw new InternalCompilerError("primitive type expected");
        }
        PrimitiveType type = (PrimitiveType) value.getType();
        if (value.isLabel()) {
            loadLabelInRegister(lsb, (String) (value.getValue()));
            if (type.sizeof() == 2) {
                output.println("\tli r" + msb + ", 0");
            }
            return new Value(lsb, msb, type);
        }
        long v = (long) (value.getValue());
        long b0 = v & 0xFF;
        long b1 = (v >> 8) & 0xFF;
        long b2 = (v >> 16) & 0xFF;
        long b3 = (v >> 24) & 0xFF;
        output.println("\tli r" + lsb + ", " + b0);
        if (b1 != 0) output.println("\tlih r" + lsb + ", " + b1);
        if (type.sizeof() == 2) {
            output.println("\tli r" + msb + ", " + b2);
            if (b3 != 0) output.println("\tlih r" + msb + ", " + b3);
        }
        return new Value(lsb, msb, type);
    }

    private Value loadPointerInRegisters(Value value, int lsb, int msb) {
        if (value.inRegister()) {
            throw new InternalCompilerError("value already loaded in registers");
        }
        if (!(value.getType() instanceof PointerType)) {
            throw new InternalCompilerError("pointer type expected");
        }
        PointerType type = (PointerType) value.getType();
        if (value.isLabel()) {
            loadLabelInRegister(lsb, (String) (value.getValue()));
            if (type.sizeof() == 2) {
                output.println("\tli r" + msb + ", 0");
            }
            return new Value(lsb, msb, type);
        }
        long v = (long) (value.getValue());
        long b0 = v & 0xFF;
        long b1 = (v >> 8) & 0xFF;
        output.println("\tli r" + lsb + ", " + b0);
        if (b1 != 0) output.println("\tlih r" + lsb + ", " + b1);
        return new Value(lsb, msb, type);
    }

    private void loadPrimitiveInRegisters(Destination destination, int lsb, int msb) {
        PrimitiveType type = (PrimitiveType) destination.getType();
        if (destination.inRegister()) {
            output.println("\tld r" + lsb + ", (r" + destination.getRegisterLSB() + ")");
            if (type.sizeof() == 2) {
                output.println("\tldi r" + msb + ", (r" + destination.getRegisterLSB() + "+1)");
            } else if (type.sizeof() > 2) {
                throw new InternalCompilerError("cannot load a variable with length " + type.sizeof() + " into registers");
            }
        } else {
            if (!destination.isLocal() || destination.getAddressAsInt() < 0 || destination.getAddressAsInt() > 14) {
                destination = this.loadDestinationInRegister(destination, 2);
                loadPrimitiveInRegisters(destination, lsb, msb);
            } else {
                if (destination.getAddressAsInt() < 15) {
                    output.println("\tldi r" + lsb + ", (bp+" + destination.getAddressAsInt() + ")");
                    if (type.sizeof() == 2) {
                        output.println("\tldi r" + msb + ", (bp+" + (destination.getAddressAsInt() + 1) + ")");
                    } else if (type.sizeof() > 2) {
                        throw new InternalCompilerError("cannot load a variable with length " + type.sizeof() + " into registers");
                    }
                } else {
                    int reg = resourceManager.getFreeRegister();
                    loadConstantInRegister(reg, destination.getAddressAsInt());
                    output.println("\tlda r" + lsb + ", (bp+r" + reg + ")");
                    if (type.sizeof() == 2) {
                        output.println("\tinc r" + reg);
                        output.println("\tlda r" + msb + ", (bp+r" + reg + ")");
                    } else if (type.sizeof() > 2) {
                        throw new InternalCompilerError("cannot load a variable with length " + type.sizeof() + " into registers");
                    }
                    resourceManager.setRegisterFree(reg);
                }
            }
        }
    }

    public void multiplyRegisterByConstant(int register, int constant) {
        if (constant < 0) {
            multiplyRegisterByConstant(register, -constant);
            output.println("\tneg r" + register + ", r" + register);
        }
        if (constant == 0) {
            output.println("\tli r" + register + ", 0");
        } else if (constant > 1) {
            int powerOfTwo = isPowerOfTwo(constant);
            if (powerOfTwo > 0) {
                output.println("\tshli r" + register + ", " + powerOfTwo);
            } else {
                int zeros = 0;
                while ((constant % 2) == 0) {
                    zeros ++;
                    constant = constant >> 1;
                }
                if (zeros == 0) {
                    int reg = resourceManager.getFreeRegister();
                    output.println("\tset r" + reg + ", r" + register);
                    int lastShift = 0;
                    for (int i = 1; i < 16; i++) {
                        if ((constant & (1 << i)) != 0) {
                            output.println("\tshli r" + reg + ", " + (i - lastShift));
                            output.println("\tadd r" + register + ", r" + reg);
                            lastShift = i;
                        }
                    }
                    resourceManager.setRegisterFree(reg);
                } else {
                    multiplyRegisterByConstant(register, constant);
                    output.println("\tshli r" + register + ", " + zeros);
                }
            }
        }
    }

    public int isPowerOfTwo(int constant) {
        for(int i=1; i<32; i++) {
            if (constant == (1 << i)) {
                return i;
            }
        }
        return 0;
    }

    public void loadConstantInRegister(int register, int value) {
        if (value < -32768 || value > 65535) {
            throw new InternalCompilerError("cannot load the value " + value + " in a register");
        }
        if (value < 0) {
            value = value + 65536;
        }
        output.println("\tli r" + register + ", " + (value & 0xFF));
        if (value > 255) {
            output.println("\tlih r" + register + ", " + (value / 256));
        }
    }

    public void pushValue(Value value) {
        int sizeof = value.getType().sizeof();
        if (sizeof > 2) {
            throw new InternalCompilerError("Cannot push a value on stack that is bigger than 32 bits");
        }
        int lsb = 16, msb = 16;
        if (!value.inRegister()) {
            lsb = resourceManager.getFreeRegister();
            if (sizeof == 2) {
                msb = resourceManager.getFreeRegister();
            }
            value = loadValueInRegisters(value, lsb, msb);
        }
        if (sizeof == 2) {
            output.println("\tpush r" + value.getRegisterMSB());
        }
        output.println("\tpush r" + value.getRegisterLSB());
        resourceManager.setRegisterFree(lsb);
        if (sizeof == 2) {
            resourceManager.setRegisterFree(msb);
        }
    }

    /**
     * pops a value from the stack and puts it in the registers lsb and msb
     *
     * @param value
     * @param lsb
     * @param msb
     * @return modified value
     */
    public Value popValue(Value value, int lsb, int msb) {
        int sizeof = value.getType().sizeof();
        if (sizeof > 2) {
            throw new InternalCompilerError("Cannot pop a value of more than 2 words from the stack");
        }
        output.println("\tpop r" + lsb);
        if (sizeof == 2) {
            output.println("\tpop r" + msb);
        }
        return new Value(lsb, msb, value.getType());
    }

    public Value performOperationOnRegister(Token operation, Value value1, Value value2) {
        return calculationGenerator.performOperationOnRegister(operation, value1, value2);
    }

    public Value performOperationOnConstants(Token operation, Value value1, Value value2) {
        return calculationGenerator.performOperationOnConstants(operation, value1, value2);
    }

    /**
     * assumes that the value is loaded in registers
     *
     * @param operation
     * @param value
     * @return
     */
    public DestinationValue performUnaryOperation(Token operation, DestinationValue value) {
        return calculationGenerator.performUnaryOperation(operation, value);
    }

    public Destination saveDestinationToRegister(Destination destination, int register) {
        if (destination.inRegister()) {
            if (register != destination.getRegisterLSB()) {
                output.println("\tset r" + register + ", r" + destination.getRegisterLSB());
            }
        } else {
            if (destination.isLocal()) {
                int offset = destination.getAddressAsInt();
                loadConstantInRegister(register, offset);
                output.println("\tadd r" + register + ", bp");
            } else {
                loadLabelInRegister(register, destination.getAddressAsString());
            }
        }
        PointerType pointerType = new PointerType(destination.getType());
        return new Destination(pointerType, destination.isLocal(), register);
    }

    public Value performUnaryOperationOnConstant(Token operation, Value result) {
        return calculationGenerator.performUnaryOperationOnConstant(operation, result);
    }

    public Value castTo(Value value, Type resultType) {
        return calculationGenerator.castTo(value, resultType);
    }

    public void callFunction(Function function) {
        loadLabelInRegister(2, function.getLabel());
        output.println("\tcall [r2]");
    }

    public void removeParametersFromStack(Function function) {
        int size = function.getParameterSize();
        addConstantToStackPointer(size);
    }

    private void addConstantToStackPointer(int size) {
        if (size == 0) return;
        if (size < 128) {
            output.println("\taddi sp, " + size);
        } else {
            output.println("\tli r2" + ", " + (size & 0xFF));
            output.println("\tlih r2" + ", " + (size >>> 8));
            output.println("\tadd sp, r2");
        }
    }

    private void subtractConstantFromStackPointer(int size) {
        if (size == 0) return;
        if (size < 129) {
            output.println("\taddi sp, " + (-size));
        } else {
            output.println("\tli r2" + ", " + (size & 0xFF));
            output.println("\tlih r2" + ", " + (size >>> 8));
            output.println("\tsub sp, r2");
        }
    }

    private void removeLocalVariablesFromStack(Function function) {
        int size = function.getLocalVariableSize();
        addConstantToStackPointer(size);
    }

    private void addLocalVariablesToStack(Function function) {
        int size = function.getLocalVariableSize();
        subtractConstantFromStackPointer(size);
    }

    public void writeLabel(String label) {
        output.println(":" + label);
    }

    public void jumpIfFalse(Value bool, String label) {
        if (!bool.inRegister()) {
            if (bool.isLabel() || bool.getLongValue() == 0) {
                jumpToLabel(label);
            }
            return;
        }
        int reg = resourceManager.getFreeRegister();
        loadLabelInRegister(reg, label);
        if (bool.getType().sizeof() == 2) {
            output.println("\tor r" + bool.getRegisterLSB() + ", r" + bool.getRegisterMSB());
        } else {
            output.println("\tor r" + bool.getRegisterLSB() + ", r" + bool.getRegisterLSB());
        }
        output.println("\tjpz r" + reg);
        resourceManager.setRegisterFree(reg);
    }

    public void jumpToLabel(String label) {
        int reg = resourceManager.getFreeRegister();
        loadLabelInRegister(reg, label);
        output.println("\tjp r" + reg);
        resourceManager.setRegisterFree(reg);
    }

    public void returnFromFunction(Function function) {
        if (function != null) {
            removeLocalVariablesFromStack(function);
        }
        output.println("\tpop bp");
        output.println("\tret");
    }

    public void storeValueInDestination(Value value, Destination destination) {
        Type type = value.getType();
        if (type instanceof PointerType) {
            type = PrimitiveType.UINT16;
        }
        if (!(type instanceof PrimitiveType)) {
            throw new InternalCompilerError("cannot store a value that is not a primitive type");
        }
        type = destination.getType();
        if (type instanceof PointerType) {
            type = PrimitiveType.UINT16;
        }
        if (!(type instanceof PrimitiveType)) {
            throw new InternalCompilerError("cannot store a value to a destination that is not a primitive type (got " + type + ")");
        }
        if (!value.inRegister()) {
            value = loadValueInRegisters(value, 0, 1);
        }
        value = castTo(value, destination.getType());
        if (!destination.inRegister()) {
            if (destination.isLocal()) {
                if (destination.getAddressAsInt() < 15) {
                    output.println("\tsti r" + value.getRegisterLSB() + ", (bp+" + destination.getAddress() + ")");
                    if (type.sizeof() == 2) {
                        output.println("\tsti r" + value.getRegisterMSB() + ", (bp+" + (destination.getAddressAsInt() + 1) + ")");
                    }
                    return;
                }
            }
            destination = this.loadDestinationInRegister(destination, 2);
        }
        output.println("\tst r" + value.getRegisterLSB() + ", (r" + destination.getRegisterLSB() + ")");
        if (type.sizeof() == 2) {
            output.println("\tsti r" + value.getRegisterMSB() + ", (r" + destination.getRegisterLSB() + "+1)");
        }
    }

    public void startOfFunction(Function function) {
        writeLabel(function.getLabel());
        output.println("\tpush bp");
        addLocalVariablesToStack(function);
        output.println("\tset bp, sp");
    }

    public void pushRegister(int reg) {
        output.println("\tpush r" + reg);
    }

    public void popRegister(int reg) {
        output.println("\tpop r" + reg);
    }

    public Value performOperationOnRegister(Token operation, DestinationValue destval1, DestinationValue destval2) {
        return calculationGenerator.performOperationOnRegister(operation, destval1, destval2);
    }

    public void divideRegisterByConstant(Type type, int register, int constant) {
        if (constant == 0) {
            throw new InternalCompilerError("division by zero");
        }
        if (type.sizeof() > 1) {
            throw new InternalCompilerError("cannot divide " + type + " by constant " + constant);
        }
        int powerOfTwo = isPowerOfTwo(constant);
        if (powerOfTwo > 0) {
            if (type == PrimitiveType.INT16 || type == PrimitiveType.INT8) {
                output.println("\tashri r" + register + ", " + powerOfTwo);
            } else {
                output.println("\tshri r" + register + ", " + powerOfTwo);
            }
        } else if (constant == 10) {
            String operation;
            if (type == PrimitiveType.INT16 || type == PrimitiveType.INT8) {
                operation = "math_divideByTen16signed";
            } else {
                operation = "math_divideByTen16unsigned";
            }
            output.println("\tpush r" + register);
            output.println("\tli r0, :" + operation);
            output.println("\tlih r0, ::" + operation);
            output.println("\tcall [r0]");
            output.println("\taddi sp, 1");
            if (register != 0) {
                output.println("set r" + register + ", r0");
            }
        } else {
            output.println("\tpush r" + register);
            loadConstantInRegister(0, constant);
            output.println("\tpush r0");
            String operation;
            if (type == PrimitiveType.INT16 || type == PrimitiveType.INT8) {
                operation = "math_divide16signed";
            } else {
                operation = "math_divide16unsigned";
            }
            output.println("\tli r0, :" + operation);
            output.println("\tlih r0, ::" + operation);
            output.println("\tcall [r0]");
            output.println("\taddi sp, 2");
            if (register != 0) {
                output.println("set r" + register + ", r0");
            }
        }
    }
}
