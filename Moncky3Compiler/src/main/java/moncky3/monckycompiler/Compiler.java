package moncky3.monckycompiler;

import moncky3.assembler.AssemblerExternal;
import moncky3.assembler.backend.Assembler;
import moncky3.assembler.backend.Moncky3InstructionSet;
import moncky3.monckycompiler.lex.LexicalAnalyser;
import moncky3.monckycompiler.lex.Token;
import moncky3.monckycompiler.parsing.Parser;
import moncky3.monckycompiler.resources.Function;
import moncky3.monckycompiler.resources.ResourceManager;
import moncky3.monckycompiler.resources.Variable;
import moncky3.monckycompiler.types.ArrayType;
import moncky3.monckycompiler.types.PointerType;
import moncky3.monckycompiler.types.PrimitiveType;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.List;

import static moncky3.monckycompiler.lex.Token.EOF;

public class Compiler {
    private final LexicalAnalyser analyser;
    private final AssemblerExternal assembler;
    private final CodeGenerator codeGenerator;
    private final ResourceManager resourceManager;
    private final Parser parser;

    public Compiler() {
        this.assembler = new AssemblerExternal(new Assembler(new Moncky3InstructionSet()), "../Moncky3_firmware");
        this.analyser = new LexicalAnalyser();
        this.resourceManager = new ResourceManager();
        this.codeGenerator = new CodeGenerator(resourceManager);
        this.parser = new Parser(analyser, resourceManager, codeGenerator);
    }

    public void compile(String filename) throws FileNotFoundException {
        String baseName = filename.substring(0, filename.indexOf(".src"));
        String assemblyFilename = baseName + ".asm";
        PrintStream assemblyFile = new PrintStream(assemblyFilename);
        //PrintStream assemblyFile = System.out;
        PrintStream optimizer = new Optimizer(new Optimizer(assemblyFile));
        codeGenerator.setPrintStream(optimizer);
        codeGenerator.generateInitialCode(filename);
        this.analyser.setFilename(filename);

        defineFirmwareFunctions();

        Token token = analyser.nextToken();
        parser.parseMonckySourceFile(token);
        token = analyser.nextToken();
        if (token != EOF) {
            parser.error("end of file expected", token, "source file");
        }

        codeGenerator.createStringConstants();
        codeGenerator.createArrayConstants();
        codeGenerator.writeLabel("begin_heap");
        assemblyFile.close();
        assembler.translate(assemblyFilename);
        List<Function> uncalledFunctions = resourceManager.getUncalledFunctions();
        for(Function function : uncalledFunctions) {
            if (!function.isFirmware() && !function.getName().equals("main")) {
                System.out.println("Warning: function " + function.getName() + " was never used");
            }
        }
    }

    private void defineFirmwareFunctions() {
        Function function = new Function("text_setCursor", null, true);
        function.addParameter(new Variable("x", PrimitiveType.UINT8));
        function.addParameter(new Variable("y", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("text_drawCursor", null, true);
        resourceManager.addFunction(function);
        function = new Function("text_eraseCursor", null, true);
        resourceManager.addFunction(function);
        function = new Function("text_drawChar_alt", null, true);
        function.addParameter(new Variable("x", PrimitiveType.UINT16));
        function.addParameter(new Variable("y", PrimitiveType.UINT16));
        function.addParameter(new Variable("char", PrimitiveType.UINT8));
        ArrayType arrayType = new ArrayType(PrimitiveType.UINT8, 0);
        PointerType pointerType = new PointerType(arrayType);
        function.addParameter(new Variable("font", pointerType));
        resourceManager.addFunction(function);
        function = new Function("text_drawChar", null, true);
        function.addParameter(new Variable("x", PrimitiveType.UINT16));
        function.addParameter(new Variable("y", PrimitiveType.UINT16));
        function.addParameter(new Variable("char", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("text_printChar", null, true);
        function.addParameter(new Variable("char", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("text_printString16", null, true);
        pointerType = new PointerType(PrimitiveType.UINT16);
        function.addParameter(new Variable("string", pointerType));
        resourceManager.addFunction(function);
        function = new Function("text_printString", null, true);
        pointerType = new PointerType(PrimitiveType.UINT8);
        function.addParameter(new Variable("string", pointerType));
        resourceManager.addFunction(function);
        function = new Function("text_printHex", null, true);
        function.addParameter(new Variable("value", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("text_printDecimal", null, true);
        function.addParameter(new Variable("value", PrimitiveType.UINT16));
        resourceManager.addFunction(function);

        function = new Function("util_sleepMillis", null, true);
        function.addParameter(new Variable("time", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("util_memcopy", null, true);
        pointerType = new PointerType(PrimitiveType.UINT16);
        function.addParameter(new Variable("source", pointerType));
        function.addParameter(new Variable("destination", pointerType));
        function.addParameter(new Variable("length", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("util_memclear", null, true);
        pointerType = new PointerType(PrimitiveType.UINT16);
        function.addParameter(new Variable("destination", pointerType));
        function.addParameter(new Variable("length", PrimitiveType.UINT16));
        function.addParameter(new Variable("value", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("util_sleepMillis", null, true);
        function.addParameter(new Variable("millis", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("util_getMillis", PrimitiveType.UINT32, true);
        resourceManager.addFunction(function);
        function = new Function("util_getMicros", PrimitiveType.UINT32, true);
        resourceManager.addFunction(function);
        function = new Function("util_enableInterrupts", null, true);
        resourceManager.addFunction(function);
        function = new Function("util_disableInterrupts", null, true);
        resourceManager.addFunction(function);

        function = new Function("spi_enable", null, true);
        function.addParameter(new Variable("slave", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("spi_disable", null, true);
        resourceManager.addFunction(function);
        function = new Function("spi_setSpeed", null, true);
        function.addParameter(new Variable("speed", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("spi_transfer", PrimitiveType.UINT8, true);
        function.addParameter(new Variable("byte", PrimitiveType.UINT8));
        resourceManager.addFunction(function);

        function = new Function("sd_disable", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("sd_enable", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("sd_powerUpSeq", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("sd_command", null, true);
        function.addParameter(new Variable("cmd", PrimitiveType.UINT8));
        function.addParameter(new Variable("arg", PrimitiveType.UINT32));
        function.addParameter(new Variable("crc", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("sd_readRes1", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("sd_readUntil", PrimitiveType.UINT8, true);
        function.addParameter(new Variable("expected", PrimitiveType.UINT8));
        function.addParameter(new Variable("timeout", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("sd_goIdleState", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("sd_readRes3_7", null, true);
        arrayType = new ArrayType(PrimitiveType.UINT8, 0);
        pointerType = new PointerType(arrayType);
        function.addParameter(new Variable("res", pointerType));
        resourceManager.addFunction(function);
        function = new Function("sd_sendIfCond", null, true);
        function.addParameter(new Variable("res", pointerType));
        resourceManager.addFunction(function);
        function = new Function("sd_readOCR", null, true);
        function.addParameter(new Variable("res", pointerType));
        resourceManager.addFunction(function);
        function = new Function("sd_sendApp", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("sd_sendOpCond", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("sd_sendBlockSize", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("sd_init", null, true);
        resourceManager.addFunction(function);
        function = new Function("sd_read", PrimitiveType.UINT8, true);
        function.addParameter(new Variable("blockNumber", PrimitiveType.UINT32));
        arrayType = new ArrayType(PrimitiveType.UINT16, 0);
        pointerType = new PointerType(arrayType);
        function.addParameter(new Variable("destination", pointerType));
        function.addParameter(new Variable("io", PrimitiveType.UINT8));
        resourceManager.addFunction(function);

        arrayType = new ArrayType(PrimitiveType.UINT16, 0);
        pointerType = new PointerType(arrayType);
        function = new Function("malloc", pointerType, true);
        function.addParameter(new Variable("length", PrimitiveType.UINT16));
        resourceManager.addFunction(function);

        function = new Function("keyboard_readKeyCode", PrimitiveType.UINT16, true);
        resourceManager.addFunction(function);
        function = new Function("keyboard_waitKey", PrimitiveType.UINT16, true);
        function.addParameter(new Variable("timeout", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("keyboard_toChar", PrimitiveType.UINT16, true);
        function.addParameter(new Variable("keycode", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("keyboard_readChar", PrimitiveType.UINT16, true);
        resourceManager.addFunction(function);
        function = new Function("keyboard_waitChar", PrimitiveType.UINT16, true);
        function.addParameter(new Variable("timeout", PrimitiveType.UINT16));
        resourceManager.addFunction(function);

        function = new Function("graphics_setBgCol", null, true);
        function.addParameter(new Variable("col", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("graphics_clrscr", null, true);
        resourceManager.addFunction(function);
        function = new Function("graphics_setCol", null, true);
        function.addParameter(new Variable("col", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("graphics_setScreenBuffer", null, true);
        function.addParameter(new Variable("buffer", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("graphics_waitVertRetrace", null, true);
        resourceManager.addFunction(function);
        function = new Function("graphics_setDrawBuffer", null, true);
        function.addParameter(new Variable("buffer", PrimitiveType.UINT8));
        resourceManager.addFunction(function);
        function = new Function("graphics_getDrawBuffer", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("graphics_plot", null, true);
        function.addParameter(new Variable("x", PrimitiveType.UINT16));
        function.addParameter(new Variable("y", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_line", null, true);
        function.addParameter(new Variable("x0", PrimitiveType.UINT16));
        function.addParameter(new Variable("y0", PrimitiveType.UINT16));
        function.addParameter(new Variable("x1", PrimitiveType.UINT16));
        function.addParameter(new Variable("y1", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_drawHorizontalEven", null, true);
        function.addParameter(new Variable("x", PrimitiveType.UINT16));
        function.addParameter(new Variable("y", PrimitiveType.UINT16));
        function.addParameter(new Variable("width", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_drawHorizontal", null, true);
        function.addParameter(new Variable("x", PrimitiveType.UINT16));
        function.addParameter(new Variable("y", PrimitiveType.UINT16));
        function.addParameter(new Variable("width", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_drawVertical", null, true);
        function.addParameter(new Variable("x", PrimitiveType.UINT16));
        function.addParameter(new Variable("y", PrimitiveType.UINT16));
        function.addParameter(new Variable("height", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_drawRectangle", null, true);
        function.addParameter(new Variable("x1", PrimitiveType.UINT16));
        function.addParameter(new Variable("y1", PrimitiveType.UINT16));
        function.addParameter(new Variable("width", PrimitiveType.UINT16));
        function.addParameter(new Variable("height", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_fillRectangle", null, true);
        function.addParameter(new Variable("x1", PrimitiveType.UINT16));
        function.addParameter(new Variable("y1", PrimitiveType.UINT16));
        function.addParameter(new Variable("width", PrimitiveType.UINT16));
        function.addParameter(new Variable("height", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_scrollUp", null, true);
        function.addParameter(new Variable("amount", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_drawSprite", null, true);
        arrayType = new ArrayType(PrimitiveType.UINT8, 0);
        pointerType = new PointerType(arrayType);
        function.addParameter(new Variable("sprite", pointerType));
        function.addParameter(new Variable("buffer", pointerType));
        function.addParameter(new Variable("x", PrimitiveType.UINT16));
        function.addParameter(new Variable("y", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
        function = new Function("graphics_restoreSprite", null, true);
        function.addParameter(new Variable("buffer", pointerType));
        function.addParameter(new Variable("x", PrimitiveType.UINT16));
        function.addParameter(new Variable("y", PrimitiveType.UINT16));
        resourceManager.addFunction(function);

        function = new Function("fat_init", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("fat_findAndCheckPartition", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("fat_findAndCheckFAT", PrimitiveType.UINT8, true);
        resourceManager.addFunction(function);
        function = new Function("fat_openFile", PrimitiveType.UINT8, true);
        arrayType = new ArrayType(PrimitiveType.UINT8, 0);
        pointerType = new PointerType(arrayType);
        function.addParameter(new Variable("filename", pointerType));
        resourceManager.addFunction(function);
        function = new Function("fat_readNextFileSector", PrimitiveType.UINT8, true);
        arrayType = new ArrayType(PrimitiveType.UINT16, 0);
        pointerType = new PointerType(arrayType);
        function.addParameter(new Variable("buffer", pointerType));
        function.addParameter(new Variable("io", PrimitiveType.UINT8));
        resourceManager.addFunction(function);

        function = new Function("math_divideByTen16unsigned", PrimitiveType.UINT32, true);
        function.addParameter(new Variable("n", PrimitiveType.UINT16));
        resourceManager.addFunction(function);
    }
}
