package moncky3.monckycompiler.resources;

import moncky3.monckycompiler.parsing.Value;

import java.util.HashMap;
import java.util.Map;

public class ConstantManager {
    private final Map<String, Value> constants;

    public ConstantManager() {
        this.constants = new HashMap<>();
    }

    public void addConstant(String name, Value value) {
        constants.put(name, value);
    }

    public Value findConstant(String name) {
        return constants.get(name);
    }
}
