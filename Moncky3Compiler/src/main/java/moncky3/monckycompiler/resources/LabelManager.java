package moncky3.monckycompiler.resources;

import java.util.HashMap;
import java.util.Map;

public class LabelManager {
    // used to generate unique labels
    private final Map<String, Integer> nextLabelNumbers;

    public LabelManager() {
        this.nextLabelNumbers = new HashMap<>();
    }

    /**
     * generates a label with a new number for the specified label.
     * used to create unique labels
     *
     * @param label the label for which a new number needs to be created
     * @return the label plus a number, specific to the given label, that increases every time this method is called
     */
    public String getNextLabel(String label) {
        int next = 0;
        if (nextLabelNumbers.containsKey(label)) {
            next = nextLabelNumbers.get(label);
        }
        nextLabelNumbers.put(label, next + 1);
        return label + next;
    }
}
