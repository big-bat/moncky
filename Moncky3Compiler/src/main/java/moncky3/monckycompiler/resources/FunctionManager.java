package moncky3.monckycompiler.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FunctionManager {
    private final Map<String, Function> functions;

    public FunctionManager() {
        this.functions = new HashMap<>();
    }

    public Function getFunction(String name) {
        return functions.get(name);
    }

    public void addFunction(Function function) {
        functions.put(function.getName(), function);
    }

    public List<Function> getUncalledFunctions() {
        List<Function> result = new ArrayList<>();
        for(Function function : functions.values()) {
            if (!function.isCalled()) {
                result.add(function);
            }
        }
        return result;
    }
}
