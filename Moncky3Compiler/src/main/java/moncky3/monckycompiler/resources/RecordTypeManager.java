package moncky3.monckycompiler.resources;

import moncky3.monckycompiler.types.RecordType;

import java.util.HashMap;
import java.util.Map;

public class RecordTypeManager {
    private final Map<String, RecordType> recordTypes;

    public RecordTypeManager() {
        this.recordTypes = new HashMap<>();
    }

    public void addRecordType(RecordType recordType) {
        this.recordTypes.put(recordType.getName(), recordType);
    }

    public RecordType getRecordType(String name) {
        return recordTypes.get(name);
    }
}
