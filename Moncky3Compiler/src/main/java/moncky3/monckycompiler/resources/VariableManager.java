package moncky3.monckycompiler.resources;

import java.util.HashMap;
import java.util.Map;

public class VariableManager {
    private final Map<String, Variable> globalVariables;
    private final Map<String, Variable> localVariables;

    public VariableManager() {
        this.globalVariables = new HashMap<>();
        this.localVariables = new HashMap<>();
    }

    public void addVariable(Variable variable) {
        if (variable.isLocal()) {
            localVariables.put(variable.getName(), variable);
        } else {
            globalVariables.put(variable.getName(), variable);
        }
    }

    /**
     * finds the variable that is associated with the given varname
     * the search is first done in the local variables, then the parameters, then in the global vars
     *
     * @param name            the name of the variable
     * @param currentFunction the function in which the compiler is currently in
     * @return null if the variable was not found
     */
    public Variable findVariable(String name, Function currentFunction) {
        Variable variable = localVariables.get(name);
        if (variable != null) return variable;
        if (currentFunction != null) {
            for (Variable var : currentFunction.getParameters()) {
                if (var.getName().equals(name)) return var;
            }
        }
        return globalVariables.get(name);
    }

    void clearLocalVariables() {
        localVariables.clear();
    }

    public Variable findGlobalVariable(String name) {
        return globalVariables.get(name);
    }
}
