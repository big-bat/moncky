package moncky3.monckycompiler.resources;

import moncky3.monckycompiler.types.Type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Function {
    private final String name;
    private final List<Variable> parameters;
    private final Type returnType;
    private final List<Variable> localVariables;
    private final boolean isFirmware;
    private boolean called;

    public Function(String name, Type returnType) {
        this.name = name;
        this.parameters = new ArrayList<>();
        this.returnType = returnType;
        this.localVariables = new ArrayList<>();
        this.isFirmware = false;
        this.called = false;
    }

    public Function(String name, Type returnType, boolean isFirmware) {
        this.name = name;
        this.parameters = new ArrayList<>();
        this.returnType = returnType;
        this.localVariables = new ArrayList<>();
        this.isFirmware = isFirmware;
    }

    public void addParameter(Variable parameter) {
        this.parameters.add(parameter);
    }

    public void addLocalVariable(Variable variable) {
        this.localVariables.add(variable);
    }

    public String getName() {
        return name;
    }

    public Type getReturnType() {
        return returnType;
    }

    public List<Variable> getParameters() {
        return Collections.unmodifiableList(parameters);
    }

    public int getParameterSize() {
        int size = 0;
        for (Variable parameter : parameters) {
            size += parameter.getType().sizeof();
        }
        return size;
    }

    public int getLocalVariableSize() {
        int size = 0;
        for (Variable parameter : localVariables) {
            size += parameter.getType().sizeof();
        }
        return size;
    }

    public List<Variable> getLocalVariables() {
        return Collections.unmodifiableList(localVariables);
    }

    public String getLabel() {
        if (isFirmware) {
            return name;
        } else {
            return "function_" + name;
        }
    }

    public boolean isCalled() {
        return called;
    }

    public void setCalled(boolean called) {
        this.called = called;
    }

    public boolean isFirmware() {
        return isFirmware;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder(name + "( ");
        for (Variable variable : parameters) {
            builder.append(variable);
            builder.append(" ");
        }
        builder.append(") : ");
        builder.append(returnType);
        return builder.toString();
    }
}
