package moncky3.monckycompiler.resources;

import moncky3.monckycompiler.InternalCompilerError;

public class RegisterManager {
    // indicates which registers are in use
    private final boolean[] registerUsed;

    public RegisterManager() {
        this.registerUsed = new boolean[16];
        for (int i = 3; i < 11; i++) registerUsed[i] = false;
        registerUsed[0] = true; // return value and expression (LSB)
        registerUsed[1] = true; // return value and expression (MSB)
        registerUsed[2] = true; // for calculating addresses (destinations)
        registerUsed[12] = true; // base pointer
        registerUsed[13] = true; // output signals
        registerUsed[14] = true; // stack pointer
        registerUsed[15] = true; // program counter
    }

    public int getFreeRegister() {
        for (int register = 0; register < 16; register++) {
            if (!registerUsed[register]) {
                registerUsed[register] = true;
                return register;
            }
        }
        throw new InternalCompilerError("no more registers available!  Sorry, cannot compile :-(");
    }

    public void setRegisterFree(int register) {
        if (register == 16 || register == -1)
            return; // hack so that 16 indicates a register that was never assigned (used in CalculationGenerator)
        if (register < 0 || register > 15) {
            throw new InternalCompilerError("cannot free register r" + register + ": it does not exist");
        }
        registerUsed[register] = false;
    }
}
