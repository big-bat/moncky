package moncky3.monckycompiler.resources;

import moncky3.monckycompiler.InternalCompilerError;
import moncky3.monckycompiler.types.Type;

import java.util.Objects;

public class Variable {
    private final String name;
    private final Type type;
    private final boolean local; // for parameters and local variables
    private final Object initValue; // the initial value if defined, null otherwise
    private Object address; // labelname for global vars and integer relative to bp for parameters and local variables

    public Variable(String name, Type type, Object address, boolean local, Object initValue) {
        if (local && !(address instanceof Integer)) {
            throw new InternalCompilerError("local variables must have a valid offset (integer) in stead of '" + address + "'");
        }
        this.name = name;
        this.type = type;
        this.address = address;
        this.local = local;
        this.initValue = initValue;
    }

    public Variable(String name, Type type) {
        this.name = name;
        this.type = type;
        this.address = 0;
        this.local = true;
        this.initValue = 0;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public boolean isLocal() {
        return local;
    }

    public Object getInitValue() {
        return initValue;
    }

    @Override
    public String toString() {
        if (local) {
            return name + ": " + type + " (bp+" + address + ")";
        } else {
            return name + ": " + type + " (" + address + ")";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Variable variable = (Variable) o;
        return Objects.equals(name, variable.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
