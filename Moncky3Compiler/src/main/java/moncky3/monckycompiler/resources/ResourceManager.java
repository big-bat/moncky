package moncky3.monckycompiler.resources;

import moncky3.monckycompiler.parsing.Value;
import moncky3.monckycompiler.types.RecordType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ResourceManager {
    private final LabelManager labelManager;
    private final LiteralManager literalManager;
    private final RegisterManager registerManager;
    private final VariableManager variableManager;
    private final RecordTypeManager recordTypeManager;
    private final FunctionManager functionManager;
    private final ConstantManager constantManager;

    public ResourceManager() {
        this.labelManager = new LabelManager();
        this.literalManager = new LiteralManager(labelManager);
        this.registerManager = new RegisterManager();
        this.variableManager = new VariableManager();
        this.recordTypeManager = new RecordTypeManager();
        this.functionManager = new FunctionManager();
        this.constantManager = new ConstantManager();
    }

    public String getNextLabel(String label) {
        return labelManager.getNextLabel(label);
    }

    public Map<String, String> getStringConstants() {
        return literalManager.getStringConstants();
    }

    public Map<String, List<Value>> getArrayConstants() {
        return literalManager.getArrayConstants();
    }

    public String createStringConstant(String stringValue) {
        return literalManager.createStringConstant(stringValue);
    }

    public String createLiteralArray(List<Value> array) {
        return literalManager.createLiteralArray(array);
    }

    public int getFreeRegister() {
        return registerManager.getFreeRegister();
    }

    public void setRegisterFree(int register) {
        registerManager.setRegisterFree(register);
    }

    public void addVariable(Variable variable) {
        variableManager.addVariable(variable);
    }

    public Variable findVariable(String name, Function currentFunction) {
        return variableManager.findVariable(name, currentFunction);
    }

    public void clearLocalVariables() {
        variableManager.clearLocalVariables();
    }

    public void addRecordType(RecordType recordType) {
        recordTypeManager.addRecordType(recordType);
    }

    public RecordType getRecordType(String name) {
        return recordTypeManager.getRecordType(name);
    }

    public Function getFunction(String name) {
        return functionManager.getFunction(name);
    }

    public void addFunction(Function function) {
        functionManager.addFunction(function);
    }

    public Variable findGlobalVariable(String name) {
        return variableManager.findGlobalVariable(name);
    }

    public void addConstant(String name, Value value) {
        constantManager.addConstant(name, value);
    }

    public Value findConstant(String name) {
        return constantManager.findConstant(name);
    }

    public List<Function> getUncalledFunctions() {
        return functionManager.getUncalledFunctions();
    }
}
