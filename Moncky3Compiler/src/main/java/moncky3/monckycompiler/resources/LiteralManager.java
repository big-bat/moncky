package moncky3.monckycompiler.resources;

import moncky3.monckycompiler.parsing.Value;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LiteralManager {
    // maps all string constants generated so far to their label.  Enables reuse
    private final Map<String, String> stringConstants;
    // maps all defined array literals to their label.
    private final Map<String, List<Value>> arrayConstants;

    private final LabelManager labelManager;

    public LiteralManager(LabelManager labelManager) {
        this.stringConstants = new HashMap<>();
        this.arrayConstants = new HashMap<>();
        this.labelManager = labelManager;
    }

    public Map<String, String> getStringConstants() {
        return Collections.unmodifiableMap(stringConstants);
    }

    public Map<String, List<Value>> getArrayConstants() {
        return Collections.unmodifiableMap(arrayConstants);
    }

    public String createStringConstant(String stringValue) {
        if (stringConstants.containsKey(stringValue)) {
            return stringConstants.get(stringValue);
        }
        String result = labelManager.getNextLabel("string_constant");
        this.stringConstants.put(stringValue, result);
        return result;
    }

    public String createLiteralArray(List<Value> array) {
        String result = labelManager.getNextLabel("array_constant");
        this.arrayConstants.put(result, array);
        return result;
    }
}
