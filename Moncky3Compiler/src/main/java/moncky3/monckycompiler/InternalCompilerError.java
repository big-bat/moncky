package moncky3.monckycompiler;

public class InternalCompilerError extends RuntimeException {
    public InternalCompilerError(String message) {
        super("Internal Compiler error: " + message);
    }
}
