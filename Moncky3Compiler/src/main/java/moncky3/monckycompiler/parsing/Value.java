package moncky3.monckycompiler.parsing;

import moncky3.monckycompiler.types.Type;

public class Value extends DestinationValue {
    private final int registerMSB; // which register holds the MSB of the value (-1 if not stored yet)
    private final Object value; // the value if not stored in a register, can be a label for string constants, global vars, arrays, pointers, ...

    public Value(int registerLSB, int registerMSB, Type type) {
        super(registerLSB, type);
        this.registerMSB = registerMSB;
        this.value = "invalid";
    }

    public Value(Type type, Object value) {
        super(-1, type);
        this.registerMSB = -1;
        this.value = value;
    }

    public int getRegisterMSB() {
        return registerMSB;
    }

    public Object getValue() {
        return value;
    }

    public long getLongValue() {
        return (Long) (value);
    }

    public String toString() {
        if (inRegister()) {
            return "Value (" + type + ") in r" + registerLSB + ((registerMSB >= 0) ? (" and r" + registerMSB) : "");
        } else {
            return "Value (" + type + "): " + value;
        }
    }

    public boolean isLabel() {
        return (value instanceof String); // && ((String)value).charAt(0) == ':';
    }
}
