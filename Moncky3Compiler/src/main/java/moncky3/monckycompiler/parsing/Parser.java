package moncky3.monckycompiler.parsing;

import moncky3.monckycompiler.CodeGenerator;
import moncky3.monckycompiler.InternalCompilerError;
import moncky3.monckycompiler.lex.LexicalAnalyser;
import moncky3.monckycompiler.lex.Token;
import moncky3.monckycompiler.resources.Function;
import moncky3.monckycompiler.resources.ResourceManager;
import moncky3.monckycompiler.resources.Variable;
import moncky3.monckycompiler.types.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Parser {
    private final LexicalAnalyser lexicalAnalyser;
    private final CodeGenerator codeGenerator;
    private final FirstChecker firstChecker;
    private final ResourceManager resourceManager;

    private int currentLine;
    private Function currentFunction;
    private Token lastStatement;

    public Parser(LexicalAnalyser lexicalAnalyser, ResourceManager resourceManager, CodeGenerator codeGenerator) {
        this.lexicalAnalyser = lexicalAnalyser;
        this.codeGenerator = codeGenerator;
        this.firstChecker = new FirstChecker();
        this.resourceManager = resourceManager;
        this.currentFunction = new Function("foo", PrimitiveType.INT32);
        this.currentLine = -1;
    }

    public void error(String message, Token token, String construct) {
        System.err.println("Error in line " + lexicalAnalyser.lineNumber() + "!");
        System.err.println(message + " while parsing " + construct);
        System.err.println("Encountered: " + token + " (" + lexicalAnalyser.stringValue + ")");
        throw new RuntimeException();
    }

    public void checkLineChange() {
        int line = lexicalAnalyser.lineNumber();
        if (currentLine != line) {
            currentLine = line;
            codeGenerator.comment("***** line " + line);
        }
    }

    public PrimitiveType parsePrimitiveType(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "primitiveType")) {
            error("Primitive type expected", token, "primitiveType");
        }
        switch (token) {
            case INT8:
                return PrimitiveType.INT8;
            case UINT8:
                return PrimitiveType.UINT8;
            case INT16:
                return PrimitiveType.INT16;
            case UINT16:
                return PrimitiveType.UINT16;
            case INT32:
                return PrimitiveType.INT32;
            case UINT32:
                return PrimitiveType.UINT32;
            default:
                return null;
        }
    }

    public ArrayType parseArrayType(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "arrayType")) {
            error("Array type expected", token, "arrayType");
        }
        int length = 0;
        token = lexicalAnalyser.nextToken();
        if (token == Token.LEFT_SQUARE) {
            token = lexicalAnalyser.nextToken();
            checkLineChange();
            if (token != Token.INTEGER_LITERAL) {
                error("Integer literal expected for size of array", token, "arrayType");
            }
            if (lexicalAnalyser.intValue < 0 || lexicalAnalyser.intValue > 65535) {
                error("Length of array cannot be negative or greater than 65535", token, "arrayType");
            }
            length = (int) lexicalAnalyser.intValue;
            token = lexicalAnalyser.nextToken();
            checkLineChange();
            if (token != Token.RIGHT_SQUARE) {
                error("']' expected", token, "arrayType");
            }
            token = lexicalAnalyser.nextToken();
        }
        checkLineChange();
        if (token != Token.OF) {
            error("'of' expected", token, "arrayType");
        }
        token = lexicalAnalyser.nextToken();
        Type type = parseType(token);
        return new ArrayType(type, length);
    }

    public PointerType parsePointerType(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "pointerType")) {
            error("Pointer type expected", token, "pointerType");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.TO) {
            error("'to' expected", token, "pointerType");
        }
        token = lexicalAnalyser.nextToken();
        Type type = parseType(token);
        return new PointerType(type);
    }

    public RecordType parseRecordType(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "recordType")) {
            error("Record type expected", token, "recordType");
        }
        RecordType type = resourceManager.getRecordType(lexicalAnalyser.stringValue);
        if (type == null) {
            error("Cannot find record with name '" + lexicalAnalyser.stringValue + "'", token, "recordType");
        }
        return type;
    }

    public Type parseType(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "type")) {
            error("Type expected", token, "type");
        }
        Type type;
        if (firstChecker.check(token, "primitiveType")) {
            type = parsePrimitiveType(token);
        } else if (firstChecker.check(token, "arrayType")) {
            type = parseArrayType(token);
        } else if (firstChecker.check(token, "pointerType")) {
            type = parsePointerType(token);
        } else if (firstChecker.check(token, "recordType")) {
            type = parseRecordType(token);
        } else {
            throw new InternalCompilerError("Unimplemented type while parsing type");
        }
        return type;
    }

    public Token parseUnaryOperation(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "unaryOperation")) {
            error("UnaryOperation expected", token, "unaryOperation");
        }
        return token;
    }

    public Token parseBinaryOperation(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "binaryOperation")) {
            error("BinaryOperation expected", token, "binaryOperation");
        }
        return token;
    }

    public DestinationValue parseExpression2(Token token, DestinationValue destval) {
        checkLineChange();
        if (!firstChecker.check(token, "expression2")) {
            error("Expression2 expected", token, "expression2");
        }
        if (destval instanceof Value) {
            error("cannot perform [] or . on a value", token, "expression2");
        }
        Destination destination = (Destination) destval;
        if (token == Token.POINT) {
            if (!(destination.getType() instanceof RecordType)) {
                error("Can only use '.' operator on records", token, "expression2");
            }
            RecordType recordType = (RecordType) destination.getType();
            token = lexicalAnalyser.nextToken();
            checkLineChange();
            if (token != Token.IDENTIFIER) {
                error("identifier expected after '.'", token, "expression2");
            }
            String fieldName = lexicalAnalyser.stringValue;
            RecordField field = recordType.getField(fieldName);
            if (field == null) {
                error("cannot find field " + fieldName + " in " + recordType.getName(), token, "expression2");
            }
            if (!destination.isLocal() && !destination.inRegister()) {
                destination = codeGenerator.loadDestinationInRegister(destination, 2);
            }
            if (!destination.inRegister()) {
                destination = new Destination(field.getType(), destination.getAddressAsInt() + field.getOffset(), destination.isLocal());
            } else {
                codeGenerator.addConstantToRegister(field.getOffset(), destination.getRegisterLSB());
                destination = new Destination(field.getType(), destination.isLocal(), destination.getRegisterLSB());
            }
        } else if (token == Token.LEFT_SQUARE) {
            if (!(destination.getType() instanceof ArrayType)) {
                error("Can only use '[]' operator on arrays (found: " + destination.getType() + ")", token, "expression2");
            }
            ArrayType arrayType = (ArrayType) destination.getType();
            token = lexicalAnalyser.nextToken();
            destval = parseExpression(token);
            Value value = getValue(destval);
            codeGenerator.castTo(value, PrimitiveType.UINT16);
            if (!value.inRegister()) {
                if (value.getLongValue() < 0) {
                    error("Array index must be positive (" + value.getLongValue() + " encountered)", token, "expression2");
                }
                if (arrayType.getLength() != 0 && value.getLongValue() >= arrayType.getLength()) {
                    error("Array index (" + value.getLongValue() + ") out of bounds (" + arrayType.getLength() + ")", token, "expression2");
                }
            }
            token = lexicalAnalyser.nextToken();
            checkLineChange();
            if (token != Token.RIGHT_SQUARE) {
                error("']' expected", token, "expression2");
            }
            int elementSize = arrayType.getType().sizeof();
            if (value.inRegister()) {
                codeGenerator.multiplyRegisterByConstant(value.getRegisterLSB(), elementSize);
                if (!destination.inRegister()) {
                    destination = codeGenerator.loadDestinationInRegister(destination, 2);
                }
                codeGenerator.addRegisters(destination.getRegisterLSB(), value.getRegisterLSB());
            } else {
                value = new Value(PrimitiveType.UINT16, ((value.getLongValue() * elementSize) & 0xFFFF));
                if (!destination.isLocal() && !destination.inRegister()) {
                    destination = codeGenerator.loadDestinationInRegister(destination, 2);
                }
                if (!destination.inRegister()) {
                    destination = new Destination(arrayType.getType(), (int)(destination.getAddressAsInt() + value.getLongValue()), destination.isLocal());
                } else {
                    //if (value.getLongValue() != 0) {
                        //codeGenerator.loadConstantInRegister(0, (int) value.getLongValue());
                        //codeGenerator.addRegisters(destination.getRegisterLSB(), 0);
                        codeGenerator.addConstantToRegister((int)(value.getLongValue()), destination.getRegisterLSB());
                    //}
                }
            }
            destination = new Destination(arrayType.getType(), destination.getAddress(), destination.isLocal(), destination.getRegisterLSB());
        } else {
            error("Unexpected token", token, "expression2");
        }
        token = lexicalAnalyser.nextToken();
        if (firstChecker.check(token, "expression2")) {
            return parseExpression2(token, destination);
        } else {
            lexicalAnalyser.putBack();
        }
        return destination;
    }

    public void parseArrayValues(Token token, PrimitiveType type, List<Value> values) {
        checkLineChange();
        if (!firstChecker.check(token, "arrayValues")) {
            error("Array values expected", token, "arrayValues");
        }
        Value value;
        if (token == Token.IDENTIFIER) {
            value = resourceManager.findConstant(lexicalAnalyser.stringValue);
            if (value == null) {
                error("Cannot find constant named '" + lexicalAnalyser.stringValue + "'", token, "arrayValues");
            }
        } else {
            value = parseLiteral(token);
        }
        if (value.getType().sizeof() > type.sizeof()) {
            error("Number " + value.getValue() + " out of range (expected a " + type + ")", token, "arrayValues");
        }
        long longValue = value.getLongValue();
        if (longValue < type.getMinValue() || longValue > type.getMaxValue()) {
            error("Number " + value.getValue() + " out of range (expected a " + type + ")", token, "arrayValues");
        }
        values.add(new Value(type, longValue));
        token = lexicalAnalyser.nextToken();
        if (firstChecker.check(token, "arrayValues")) {
            parseArrayValues(token, type, values);
        } else {
            lexicalAnalyser.putBack();
        }
    }

    public List<Value> compactArray(List<Value> array) {
        List<Value> result = new ArrayList<>(array.size()/2);
        for(int i = 0; i < (array.size()/2); i++) {
            Value value1 = array.get(2*i);
            Value value2 = array.get(2*i+1);
            result.add(new Value(PrimitiveType.UINT16, value1.getLongValue() + value2.getLongValue() * 256));
        }
        return result;
    }

    public Value parseArrayLiteral(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "arrayLiteral")) {
            error("Array literal expected", token, "arrayLiteral");
        }
        token = lexicalAnalyser.nextToken();
        PrimitiveType type = parsePrimitiveType(token);
        token = lexicalAnalyser.nextToken();
        List<Value> values = new ArrayList<>();
        parseArrayValues(token, type, values);
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.RIGHT_CURLY) {
            error("'}' expected at end of array literal", token, "arrayLiteral");
        }
        ArrayType arrayType = new ArrayType(type, values.size());
        if (type == PrimitiveType.UINT8 || type == PrimitiveType.INT8) {
            values = compactArray(values);
            arrayType = new ArrayType(PrimitiveType.UINT16, values.size());
        }
        return new Value(arrayType, values);
    }

    public Value parseLiteral(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "literal")) {
            error("Literal expected", token, "literal");
        }
        if (token == Token.INTEGER_LITERAL) {
            long val = lexicalAnalyser.intValue;
            if (val < 0) {
                if (val >= -128) {
                    return new Value(PrimitiveType.INT8, val);
                } else if (val >= -32767) {
                    return new Value(PrimitiveType.INT16, val);
                } else {
                    return new Value(PrimitiveType.INT32, val);
                }
            } else {
                if (val <= 127) {
                    return new Value(PrimitiveType.UINT8, val);
                } else if (val <= 65535) {
                    return new Value(PrimitiveType.UINT16, val);
                } else {
                    return new Value(PrimitiveType.UINT32, val);
                }
            }
        } else if (token == Token.CHAR_LITERAL) {
            int v = lexicalAnalyser.stringValue.charAt(0);
            if (v > 127) {
                return new Value(PrimitiveType.UINT16, (long) v);
            } else {
                return new Value(PrimitiveType.UINT8, (long) v);
            }
        } else if (token == Token.FLOAT_LITERAL) {
            // FLOAT32 is converted to UINT32 type
            long bits = Float.floatToRawIntBits((float) lexicalAnalyser.floatValue);
            if (bits < 0) bits += 4294967296L;
            return new Value(PrimitiveType.UINT32, bits);
        } else if (token == Token.TRUE) {
            return new Value(PrimitiveType.UINT16, -1L);
        } else if (token == Token.FALSE) {
            return new Value(PrimitiveType.UINT16, 0L);
        } else if (token == Token.STRING_LITERAL) {
            String label = resourceManager.createStringConstant(lexicalAnalyser.stringValue);
            PointerType pointerType = new PointerType(PrimitiveType.UINT8);
            return new Value(pointerType, label);
        } else if (firstChecker.check(token, "arrayLiteral")) {
            return parseArrayLiteral(token);
        } else {
            throw new InternalCompilerError("Unknown token encountered: " + token + " while parsing literal");
        }
    }

    public Value parseBinaryExpression(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "binaryExpression")) {
            error("Binary expression expected", token, "binaryExpression");
        }
        DestinationValue destval1 = parseExpression(token);
        Value value1 = getValue(destval1);
        if (value1.inRegister()) {
            codeGenerator.pushValue(value1);
        }
        token = lexicalAnalyser.nextToken();
        Token operation = parseBinaryOperation(token);
        token = lexicalAnalyser.nextToken();
        DestinationValue destval2 = parseExpression(token);
        Value value2 = getValue(destval2);

        if (!value1.inRegister() && !value2.inRegister()) {
            return codeGenerator.performOperationOnConstants(operation, value1, value2);
        } else if (value1.inRegister() && !value2.inRegister()) {
            value1 = codeGenerator.popValue(value1, 0, 1);
            return codeGenerator.performOperationOnRegister(operation, value1, value2);
        } else {
            int lsb = resourceManager.getFreeRegister();
            int msb = resourceManager.getFreeRegister();
            Value result;
            if (isCommutative(operation)) {
                if (value1.inRegister()) {
                    value1 = codeGenerator.popValue(value1, lsb, msb);
                }
                result = codeGenerator.performOperationOnRegister(operation, value2, value1);
            } else {
                value2 = codeGenerator.loadValueInRegisters(value2, lsb, msb);
                if (value1.inRegister()) {
                    value1 = codeGenerator.popValue(value1, 0, 1);
                } else {
                    value1 = codeGenerator.loadValueInRegisters(value1, 0, 1);
                }
                result = codeGenerator.performOperationOnRegister(operation, value1, value2);
            }
            resourceManager.setRegisterFree(msb);
            resourceManager.setRegisterFree(lsb);
            return result;
        }
    }

    private boolean isCommutative(Token operation) {
        return (operation == Token.PLUS
                || operation == Token.ASTERISK
                || operation == Token.PIPE
                || operation == Token.AMPERSAND
                || operation == Token.CIRCUMFLEX
                || operation == Token.BOOL_EQUALS
                || operation == Token.BOOL_NOT_EQUALS);
    }

    public DestinationValue parseExpression(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "expression")) {
            error("Expression expected", token, "expression");
        }
        DestinationValue result;
        if (firstChecker.check(token, "literal")) {
            return parseLiteral(token);
        } else if (firstChecker.check(token, "functionCall")) {
            return parseFunctionCall(token);
        } else if (token == Token.IDENTIFIER) {
            // check for constant
            String name = lexicalAnalyser.stringValue;
            Value value = resourceManager.findConstant(name);
            if (value != null) {
                return value;
            }
            Variable variable = resourceManager.findVariable(name, currentFunction);
            if (variable == null) {
                error("Cannot find variable '" + name + "'", token, "expression");
            }
            result = new Destination(variable.getType(), variable.getAddress(), variable.isLocal(), -1);
        } else if (token == Token.LEFT_BRACKET) {
            token = lexicalAnalyser.nextToken();
            if (firstChecker.check(token, "ternaryExpression")) {
                result = parseTernaryExpression(token);
            } else if (firstChecker.check(token, "unaryExpression")) {
                result = parseUnaryExpression(token);
            } else if (firstChecker.check(token, "binaryExpression")) {
                result = parseBinaryExpression(token);
            } else {
                throw new InternalCompilerError("unexpected token found: " + token);
            }
            token = lexicalAnalyser.nextToken();
            checkLineChange();
            if (token != Token.RIGHT_BRACKET) {
                error("')' expected", token, "expression");
            }
        } else {
            throw new InternalCompilerError("expression starts with " + token + "...");
        }
        if (result instanceof Value) {
            return result;
        }
        token = lexicalAnalyser.nextToken();
        if (firstChecker.check(token, "expression2")) {
            return parseExpression2(token, result);
        } else {
            lexicalAnalyser.putBack();
        }
        return result;
    }

    public DestinationValue parseUnaryExpression(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "unaryExpression")) {
            error("Unary expression expected", token, "unaryExpression");
        }
        DestinationValue result;
        if (firstChecker.check(token, "unaryOperation")) {
            Token operation = parseUnaryOperation(token);
            token = lexicalAnalyser.nextToken();
            result = parseExpression(token);
            if (result.inRegister() || (result instanceof Destination)) {
                if (!result.inRegister()) {
                    result = codeGenerator.loadValueInRegisters(result, 0, 1);
                }
                result = codeGenerator.performUnaryOperation(operation, result);
            } else {
                result = codeGenerator.performUnaryOperationOnConstant(operation, (Value) result);
            }
        } else if (token == Token.AMPERSAND) {
            token = lexicalAnalyser.nextToken();
            DestinationValue destval = parseExpression(token);
            if (destval instanceof Value) {
                error("Cannot use '&' on a value", token, "unaryExpression");
            }
            Destination destination = (Destination) destval;
            codeGenerator.saveDestinationToRegister(destination, 0);
            PointerType pointerType = new PointerType(destination.getType());
            result = new Value(0, 1, pointerType);
        } else if (token == Token.CIRCUMFLEX) {
            token = lexicalAnalyser.nextToken();
            result = parseExpression(token);
            if (result instanceof Destination) {
                Destination destination = (Destination) result;
                codeGenerator.loadDestinationInRegister(destination, 2);
                codeGenerator.loadPointer(2, destination.getType(), 2, -1);
                if (!(destination.getType() instanceof PointerType)) {
                    error("Can only perform ^ on pointer types", token, "unaryExpression");
                }
                Type type = ((PointerType) destination.getType()).getType();
                result = new Destination(type, destination.isLocal(), 2);
                return result;
            } else {
                Value value = (Value) result;
                if (!(value.getType() instanceof PointerType)) {
                    error("Can only perform ^ on pointer types", token, "unaryExpression");
                }
                PointerType pointerType = (PointerType) value.getType();
                codeGenerator.loadValueInRegisters(value, 0, 1);
                codeGenerator.loadPointer(0, pointerType.getType(), 0, 1);
                return new Value(0, 1, pointerType.getType());
            }
        } else {
            throw new InternalCompilerError("unexpected unary operation detected: " + token);
        }
        return result;
    }

    public Value parseTernaryExpression(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "ternaryExpression")) {
            error("Ternary expression expected", token, "ternaryExpression");
        }
        token = lexicalAnalyser.nextToken();
        DestinationValue destvalBool = parseExpression(token); // it is assumed that this represents a boolean value
        if (!(destvalBool.type instanceof PrimitiveType)) {
            error("Boolean should be primary type", token, "ternaryExpression");
        }
        int boolLsb = resourceManager.getFreeRegister();
        int boolMsb = resourceManager.getFreeRegister();
        Value valBool = codeGenerator.loadValueInRegisters(destvalBool, boolLsb, boolMsb);
        token = lexicalAnalyser.nextToken();
        DestinationValue destvalTrue = parseExpression(token);
        if (!(destvalTrue.type instanceof PrimitiveType) && !(destvalTrue.type instanceof PointerType)) {
            error("True value should be primitive type or pointer", token, "ternaryExpression");
        }
        int trueLsb = resourceManager.getFreeRegister();
        int trueMsb = resourceManager.getFreeRegister();
        Value valTrue = codeGenerator.loadValueInRegisters(destvalTrue, trueLsb, trueMsb);
        codeGenerator.performOperationOnRegister(Token.AMPERSAND, valTrue, valBool);
        token = lexicalAnalyser.nextToken();
        DestinationValue destvalFalse = parseExpression(token);
        if (!(destvalFalse.type instanceof PrimitiveType) && !(destvalFalse.type instanceof PointerType)) {
            error("False value should be primitive type or pointer", token, "ternaryExpression");
        }
        Value valFalse = codeGenerator.loadValueInRegisters(destvalFalse, 0, 1);
        valBool = (Value) codeGenerator.performUnaryOperation(Token.TILDE, valBool);
        codeGenerator.performOperationOnRegister(Token.AMPERSAND, valFalse, valBool);
        Value result = codeGenerator.performOperationOnRegister(Token.PIPE, valFalse, valTrue);
        resourceManager.setRegisterFree(trueMsb);
        resourceManager.setRegisterFree(trueLsb);
        resourceManager.setRegisterFree(boolMsb);
        resourceManager.setRegisterFree(boolLsb);
        return result;
    }

    public void parseParameterList(Token token, Function function, int parameterIndex) {
        checkLineChange();
        List<Variable> parameters = function.getParameters();
        if (parameters.size() <= parameterIndex) {
            error("Too many parameters given for function " + function.getName() + " (" + (parameterIndex + 1) + " in stead of " + parameters.size(), token, "parameterList");
        }
        if (!firstChecker.check(token, "parameterList")) {
            error("Parameter list expected", token, "parameterList");
        }
        DestinationValue destval = parseExpression(token);
        Value value = getValue(destval);
        Variable parameter = parameters.get(parameterIndex);
        value = codeGenerator.castTo(value, parameter.getType());
        codeGenerator.pushValue(value);
        token = lexicalAnalyser.nextToken();
        if (firstChecker.check(token, "parameterList")) {
            parseParameterList(token, function, parameterIndex + 1);
        } else {
            lexicalAnalyser.putBack();
            if (parameterIndex < (parameters.size() - 1)) {
                error("Too few parameters given for function " + function.getName() + " (" + (parameterIndex + 1) + " in stead of " + parameters.size(), token, "parameterList");
            }
        }
    }

    public Value parseFunctionCall(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "functionCall")) {
            error("Function call expected", token, "functionCall");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.IDENTIFIER) {
            error("Function name expected", token, "functionCall");
        }
        String functionName = lexicalAnalyser.stringValue;
        Function function = resourceManager.getFunction(functionName);
        if (function == null) {
            error("Function " + functionName + " does not exist", token, "functionCall");
        }
        function.setCalled(true);
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.LEFT_BRACKET) {
            error("'(' expected before function parameters", token, "functionCall");
        }
        token = lexicalAnalyser.nextToken();
        if (firstChecker.check(token, "parameterList")) {
            parseParameterList(token, function, 0);
            token = lexicalAnalyser.nextToken();
        } else if (function.getParameters().size() > 0) {
            error("parameters expected for function " + functionName, token, "functionCall");
        }
        checkLineChange();
        if (token != Token.RIGHT_BRACKET) {
            error("')' expected after function parameters", token, "functionCall");
        }
        Value result;
        if (function.getReturnType() == null) {
            result = new Value(PrimitiveType.UINT16, 0L);
        } else {
            result = new Value(0, 1, function.getReturnType());
        }
        codeGenerator.callFunction(function);
        codeGenerator.removeParametersFromStack(function);
        return result;
    }

    public void parseWhileStatement(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "whileStatement")) {
            error("While statement expected", token, "whileStatement");
        }
        token = lexicalAnalyser.nextToken();
        String startWhile = resourceManager.getNextLabel("whileStart");
        String endWhile = resourceManager.getNextLabel("whileEnd");
        codeGenerator.writeLabel(startWhile);
        DestinationValue destval = parseExpression(token);
        Value condition = getValue(destval);
        codeGenerator.jumpIfFalse(condition, endWhile);
        token = lexicalAnalyser.nextToken();
        parseCompoundStatement(token);
        codeGenerator.jumpToLabel(startWhile);
        codeGenerator.writeLabel(endWhile);
    }

    public void parseIfStatement(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "ifStatement")) {
            error("If statement expected", token, "ifStatement");
        }
        token = lexicalAnalyser.nextToken();
        DestinationValue destval = parseExpression(token);
        Value bool = getValue(destval);
        String elseLabel = resourceManager.getNextLabel("else");
        codeGenerator.jumpIfFalse(bool, elseLabel);
        token = lexicalAnalyser.nextToken();
        parseCompoundStatement(token);
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.ELSE) {
            codeGenerator.writeLabel(elseLabel);
            lexicalAnalyser.putBack();
            return;
        }
        String continueLabel = resourceManager.getNextLabel("continue");
        codeGenerator.jumpToLabel(continueLabel);
        codeGenerator.writeLabel(elseLabel);
        token = lexicalAnalyser.nextToken();
        parseCompoundStatement(token);
        codeGenerator.writeLabel(continueLabel);
    }

    private Value getValue(DestinationValue destval) {
        Value bool;
        if (destval instanceof Destination) {
            Destination destination = (Destination) destval;
            bool = codeGenerator.loadValueInRegisters(destination, 0, 1);
        } else {
            bool = (Value) destval;
        }
        return bool;
    }

    public void parseReturnStatement(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "returnStatement")) {
            error("Return statement expected", token, "returnStatement");
        }
        token = lexicalAnalyser.nextToken();
        if (firstChecker.check(token, "expression")) {
            if (currentFunction.getReturnType() == null) {
                error("Function '" + currentFunction.getName() + "' does not have a return value, yet you specify one", token, "returnStatement");
            }
            DestinationValue destval = parseExpression(token);
            Value value = getValue(destval);
            value = codeGenerator.castTo(value, currentFunction.getReturnType());
            codeGenerator.loadValueInRegisters(value, 0, 1);
        } else {
            lexicalAnalyser.putBack();
        }
        codeGenerator.returnFromFunction(currentFunction);
    }

    public void parseAssignmentStatement(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "assignmentStatement")) {
            error("Assignment statement expected", token, "assignmentStatement");
        }
        token = lexicalAnalyser.nextToken();
        DestinationValue destval = parseExpression(token);
        if (destval instanceof Value) {
            error("Destination expected at left side of '='", token, "assignmentStatement");
        }
        Destination destination = (Destination) destval;
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.EQUALS) {
            error("'=' expected", token, "assignmentStatement");
        }
        token = lexicalAnalyser.nextToken();
        if (destination.inRegister()) {
            codeGenerator.pushRegister(destination.getRegisterLSB());
        }
        destval = parseExpression(token);
        Value value = getValue(destval);
        if (destination.inRegister()) {
            codeGenerator.popRegister(destination.getRegisterLSB());
        }
        codeGenerator.storeValueInDestination(value, destination);
    }

    public void parseCompoundStatement(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "compoundStatement")) {
            error("CompoundStatement expected", token, "compoundStatement");
        }
        token = lexicalAnalyser.nextToken();
        while (firstChecker.check(token, "statement")) {
            parseStatement(token);
            token = lexicalAnalyser.nextToken();
        }
        checkLineChange();
        if (token != Token.RIGHT_CURLY) {
            error("Statement or '}' expected", token, "compoundStatement");
        }
    }

    public void parseStatement(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "statement")) {
            error("Statement expected", token, "statement");
        }
        if (firstChecker.check(token, "functionCall")) {
            parseFunctionCall(token);
        } else if (firstChecker.check(token, "whileStatement")) {
            parseWhileStatement(token);
        } else if (firstChecker.check(token, "ifStatement")) {
            parseIfStatement(token);
        } else if (firstChecker.check(token, "returnStatement")) {
            parseReturnStatement(token);
        } else if (firstChecker.check(token, "assignmentStatement")) {
            parseAssignmentStatement(token);
        } else {
            throw new InternalCompilerError("Other statements not implemented yet");
        }
        lastStatement = token;
    }

    private Variable parseParameterDeclaration(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "parameterDeclaration")) {
            error("Parameter declaration expected", token, "parameterDeclaration");
        }
        String name = lexicalAnalyser.stringValue;
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.COLON) {
            error("':' expected", token, "parameterDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        if (!firstChecker.check(token, "type")) {
            error("type expected for parameter", token, "parameterDeclaration");
        }
        Type type = parseType(token);
        return new Variable(name, type, 0, true, 0);
    }

    private Variable parseLocalVarDeclaration(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "localVarDeclaration")) {
            error("Local variable declaration expected", token, "localVarDeclaration");
        }
        String name = lexicalAnalyser.stringValue;
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.COLON) {
            error("':' expected", token, "localVarDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        if (!firstChecker.check(token, "type")) {
            error("type expected for local variable", token, "localVarDeclaration");
        }
        Type type = parseType(token);
        return new Variable(name, type, 0, true, 0);
    }

    public void parseFunctionDeclaration(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "functionDeclaration")) {
            error("Function declaration expected", token, "functionDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.IDENTIFIER) {
            error("Function name expected", token, "functionDeclaration");
        }
        String functionName = lexicalAnalyser.stringValue;
        if (resourceManager.getFunction(functionName) != null) {
            error("Function '" + functionName + "' already defined before", token, "functionDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.LEFT_BRACKET) {
            error("'(' expected", token, "functionDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        List<Variable> parameters = new ArrayList<>();
        while (firstChecker.check(token, "parameterDeclaration")) {
            Variable variable = parseParameterDeclaration(token);
            parameters.add(variable);
            token = lexicalAnalyser.nextToken();
        }
        checkLineChange();
        if (token != Token.RIGHT_BRACKET) {
            error("')' expected", token, "functionDeclaration");
        }
        Type returnType = null;
        token = lexicalAnalyser.nextToken();
        if (token == Token.COLON) {
            token = lexicalAnalyser.nextToken();
            returnType = parseType(token);
            token = lexicalAnalyser.nextToken();
        }
        token = parseLocalVariables(token, functionName, parameters, returnType);
        codeGenerator.comment("*****");
        codeGenerator.comment("* " + currentFunction);
        for (Variable variable : currentFunction.getLocalVariables()) {
            codeGenerator.comment("* " + variable);
        }
        codeGenerator.comment("*****");
        codeGenerator.startOfFunction(currentFunction);
        lastStatement = Token.LET;
        parseCompoundStatement(token);
        if (lastStatement != Token.RETURN) {
            if (currentFunction.getReturnType() != null) {
                error("function should return a value", token, "functionDeclaration");
            }
            codeGenerator.returnFromFunction(currentFunction);
        }
        resourceManager.clearLocalVariables();
    }

    private Token parseLocalVariables(Token token, String functionName, List<Variable> parameters, Type returnType) {
        List<Variable> localVars = new ArrayList<>();
        while (firstChecker.check(token, "localVarDeclaration")) {
            Variable variable = parseLocalVarDeclaration(token);
            localVars.add(variable);
            token = lexicalAnalyser.nextToken();
        }
        int address = 1;
        for (int i = localVars.size() - 1; i >= 0; i--) {
            Variable variable = localVars.get(i);
            variable.setAddress(address);
            address += variable.getType().sizeof();
        }
        address += 2; // skip old bp and return address
        for (int i = parameters.size() - 1; i >= 0; i--) {
            Variable variable = parameters.get(i);
            variable.setAddress(address);
            address += variable.getType().sizeof();
        }
        currentFunction = new Function(functionName, returnType);
        for (Variable variable : parameters) {
            if (currentFunction.getParameters().contains(variable)) {
                error("Parameter '" + variable.getName() + "' already defined in this scope", token, "functionDeclaration");
            }
            currentFunction.addParameter(variable);
            resourceManager.addVariable(variable);
        }
        for (Variable variable : localVars) {
            if (currentFunction.getLocalVariables().contains(variable) || currentFunction.getParameters().contains(variable)) {
                error("Local var '" + variable.getName() + "' already defined in this scope", token, "functionDeclaration");
            }
            currentFunction.addLocalVariable(variable);
            resourceManager.addVariable(variable);
        }
        resourceManager.addFunction(currentFunction);
        return token;
    }

    private void parseGlobalVarDeclaration(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "globalVarDeclaration")) {
            error("Global var declaration expected", token, "globalVarDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.IDENTIFIER) {
            error("Identifier expected", token, "globalVarDeclaration");
        }
        String name = lexicalAnalyser.stringValue;
        Variable variable = resourceManager.findGlobalVariable(name);
        if (variable != null) {
            error("Global var '" + name + "' already defined before", token, "globalVarDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.COLON) {
            error("':' expected", token, "globalVarDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        if (!firstChecker.check(token, "type")) {
            error("type expected for global variable", token, "globalVarDeclaration");
        }
        Type type = parseType(token);
        token = lexicalAnalyser.nextToken();
        Object initialValue = 0L;
        if (token == Token.EQUALS) {
            token = lexicalAnalyser.nextToken();
            DestinationValue destval = parseExpression(token);
            if ((destval instanceof Destination) || destval.inRegister()) {
                error("Initial value for global variable should be a constant", token, "globalVarDeclaration");
            }
            Value value = (Value) destval;
            if (value.getType().sizeof() > type.sizeof()) {
                if (!(type instanceof ArrayType) || !(type.sizeof() == 0)) {
                    error("Number " + value.getValue() + "(" + value.getType() + ") does not fit in a " + type, token, "globalVarDeclaration");
                }
            }
            initialValue = value.getValue();
        } else {
            lexicalAnalyser.putBack();
        }
        variable = new Variable(name, type, "globalVar_" + name, false, initialValue);
        codeGenerator.defineGlobalVariable(variable);
        resourceManager.addVariable(variable);
    }

    public void parseConstDeclaration(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "constDeclaration")) {
            error("Constant declaration expected", token, "constDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.IDENTIFIER) {
            error("Identifier expected", token, "constDeclaration");
        }
        String name = lexicalAnalyser.stringValue;
        Variable variable = resourceManager.findGlobalVariable(name);
        if (variable != null) {
            error("'" + name + "' is already defined as a global variable", token, "constDeclaration");
        }
        Value value = resourceManager.findConstant(name);
        if (value != null) {
            error("Constant '" + name + "' already defined", token, "constDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.EQUALS) {
            error("'=' expected", token, "constDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        value = parseLiteral(token);
        if (value.inRegister()) {
            error("Constant cannot be derived", token, "constDeclaration");
        }
        resourceManager.addConstant(name, value);
        codeGenerator.comment("defined constant " + name + " = " + value);
    }

    public void parseDeclaration(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "declaration")) {
            error("Declaration expected", token, "declaration");
        }
        if (firstChecker.check(token, "functionDeclaration")) {
            parseFunctionDeclaration(token);
        } else if (firstChecker.check(token, "globalVarDeclaration")) {
            parseGlobalVarDeclaration(token);
        } else if (firstChecker.check(token, "constDeclaration")) {
            parseConstDeclaration(token);
        } else if (firstChecker.check(token, "recordDeclaration")) {
            parseRecordDeclaration(token);
        } else {
            throw new InternalCompilerError("declaration " + token + " not implemented yet");
        }
    }

    private void parseRecordDeclaration(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "recordDeclaration")) {
            error("Record declaration expected", token, "recordDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.IDENTIFIER) {
            error("Record name expected", token, "recordDeclaration");
        }
        String recordName = lexicalAnalyser.stringValue;
        RecordType recordType = new RecordType(recordName);
        resourceManager.addRecordType(recordType);
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.LEFT_CURLY) {
            error("'{' name expected", token, "recordDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        while (firstChecker.check(token, "recordDeclarationLine")) {
            Variable variable = parseRecordDeclarationLine(token);
            recordType.addField(variable.getName(), variable.getType());
            token = lexicalAnalyser.nextToken();
        }
        checkLineChange();
        if (token != Token.RIGHT_CURLY) {
            error("} expected", token, "recordDeclaration");
        }
    }

    private Variable parseRecordDeclarationLine(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "recordDeclarationLine")) {
            error("Record declaration line expected", token, "recordDeclarationLine");
        }
        String name = lexicalAnalyser.stringValue;
        token = lexicalAnalyser.nextToken();
        checkLineChange();
        if (token != Token.COLON) {
            error("':' expected", token, "recordDeclarationLine");
        }
        token = lexicalAnalyser.nextToken();
        if (!firstChecker.check(token, "type")) {
            error("type expected for record entry", token, "recordDeclarationLine");
        }
        Type type = parseType(token);
        return new Variable(name, type, 0, true, 0);
    }

    public void parseMainDeclaration(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "mainDeclaration")) {
            error("Main declaration expected", token, "mainDeclaration");
        }
        token = lexicalAnalyser.nextToken();
        token = parseLocalVariables(token, "main", Collections.emptyList(), null);
        codeGenerator.comment("*****");
        codeGenerator.comment("* " + currentFunction);
        for (Variable variable : currentFunction.getLocalVariables()) {
            codeGenerator.comment("* " + variable);
        }
        codeGenerator.comment("*****");
        codeGenerator.startOfFunction(currentFunction);
        parseCompoundStatement(token);
        codeGenerator.returnFromFunction(currentFunction);
    }

    public void parseMonckySourceFile(Token token) {
        checkLineChange();
        if (!firstChecker.check(token, "monckySourceFile")) {
            error("Moncky source file expected", token, "monckySourceFile");
        }
        while (firstChecker.check(token, "declaration")) {
            parseDeclaration(token);
            token = lexicalAnalyser.nextToken();
        }
        parseMainDeclaration(token);
    }
}
