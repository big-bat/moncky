package moncky3.monckycompiler.parsing;

import moncky3.monckycompiler.types.Type;

/**
 * This class can represent a pointer to a variable or an actual value.
 * The pointer or the value can be stored in this class or in registers.
 * The compiler uses this to parse destinations and expressions.
 */
public class DestinationValue {
    protected final int registerLSB; // which register holds the LSB of the value (-1 if not stored yet)
    protected final Type type;

    public DestinationValue(int registerLSB, Type type) {
        this.registerLSB = registerLSB;
        this.type = type;
    }

    public int getRegisterLSB() {
        return registerLSB;
    }

    public Type getType() {
        return type;
    }

    public boolean inRegister() {
        return registerLSB >= 0;
    }
}
