package moncky3.monckycompiler.parsing;

import moncky3.monckycompiler.lex.Token;

import java.util.*;

import static moncky3.monckycompiler.lex.Token.*;

public class FirstChecker {
    private final Map<String, Set<Token>> firstMap;

    public FirstChecker() {
        this.firstMap = new HashMap<>();
        Set<Token> monckySourceFile = new HashSet<>();
        Set<Token> mainDeclaration = new HashSet<>();
        Set<Token> declaration = new HashSet<>();
        Set<Token> constDeclaration = new HashSet<>();
        Set<Token> recordDeclaration = new HashSet<>();
        Set<Token> globalVarDeclaration = new HashSet<>();
        Set<Token> functionDeclaration = new HashSet<>();
        Set<Token> recordDeclarationLine = new HashSet<>();
        Set<Token> parameterDeclaration = new HashSet<>();
        Set<Token> localVarDeclaration = new HashSet<>();
        Set<Token> compoundStatement = new HashSet<>();
        Set<Token> statement = new HashSet<>();
        Set<Token> assignmentStatement = new HashSet<>();
        Set<Token> expression2 = new HashSet<>();
        Set<Token> returnStatement = new HashSet<>();
        Set<Token> ifStatement = new HashSet<>();
        Set<Token> whileStatement = new HashSet<>();
        Set<Token> functionCall = new HashSet<>();
        Set<Token> parametersList = new HashSet<>();
        Set<Token> expression = new HashSet<>();
        Set<Token> ternaryExpression = new HashSet<>();
        Set<Token> unaryExpression = new HashSet<>();
        Set<Token> binaryExpression = new HashSet<>();
        Set<Token> literal = new HashSet<>();
        Set<Token> arrayLiteral = new HashSet<>();
        Set<Token> arrayValues = new HashSet<>();
        Set<Token> binaryOperation = new HashSet<>();
        Set<Token> unaryOperation = new HashSet<>();
        Set<Token> type = new HashSet<>();
        Set<Token> pointerType = new HashSet<>();
        Set<Token> recordType = new HashSet<>();
        Set<Token> arrayType = new HashSet<>();
        Set<Token> primitiveType = new HashSet<>();

        Token[] dummy = new Token[0];

        Collections.addAll(primitiveType, INT8, INT16, INT32, UINT8, UINT16, UINT32);
        Collections.addAll(arrayType, ARRAY);
        Collections.addAll(recordType, IDENTIFIER);
        Collections.addAll(pointerType, POINTER);
        Collections.addAll(type, primitiveType.toArray(dummy));
        Collections.addAll(type, pointerType.toArray(dummy));
        Collections.addAll(type, recordType.toArray(dummy));
        Collections.addAll(type, arrayType.toArray(dummy));
        Collections.addAll(unaryOperation, EXCLAMATION, TILDE);
        Collections.addAll(binaryOperation, PLUS, MINUS, ASTERISK, SLASH, PERCENT, PIPE, AMPERSAND, CIRCUMFLEX, SHL, SHR, ASHR, BOOL_EQUALS, BOOL_NOT_EQUALS, SMALLER_THAN, GREATER_THAN, SMALLER_THAN_OR_EQUAL, GREATER_THAN_OR_EQUAL);
        Collections.addAll(expression2, POINT, LEFT_SQUARE);
        Collections.addAll(arrayValues, INTEGER_LITERAL, FLOAT_LITERAL, CHAR_LITERAL, TRUE, FALSE, IDENTIFIER);
        Collections.addAll(arrayLiteral, LEFT_CURLY);
        Collections.addAll(literal, INTEGER_LITERAL, FLOAT_LITERAL, CHAR_LITERAL, TRUE, FALSE, STRING_LITERAL, MINUS);
        Collections.addAll(functionCall, CALL); // Deze regel niet verzetten want andere dingen hangen hiervan af
        Collections.addAll(literal, arrayLiteral.toArray(dummy));
        Collections.addAll(expression, literal.toArray(dummy)); // Deze regels niet verzetten want andere dingen hangen hiervan af
        Collections.addAll(expression, IDENTIFIER);
        Collections.addAll(expression, functionCall.toArray(dummy));
        Collections.addAll(expression, LEFT_BRACKET);
        Collections.addAll(binaryExpression, expression.toArray(dummy));
        Collections.addAll(unaryExpression, unaryOperation.toArray(dummy));
        Collections.addAll(unaryExpression, AMPERSAND);
        Collections.addAll(unaryExpression, CIRCUMFLEX);
        Collections.addAll(ternaryExpression, QUESTION_MARK);
        Collections.addAll(parametersList, expression.toArray(dummy));
        Collections.addAll(functionCall, CALL);
        Collections.addAll(whileStatement, WHILE);
        Collections.addAll(ifStatement, IF);
        Collections.addAll(returnStatement, RETURN);
        Collections.addAll(assignmentStatement, LET);
        Collections.addAll(compoundStatement, LEFT_CURLY);
        Collections.addAll(statement, functionCall.toArray(dummy));
        Collections.addAll(statement, whileStatement.toArray(dummy));
        Collections.addAll(statement, ifStatement.toArray(dummy));
        Collections.addAll(statement, returnStatement.toArray(dummy));
        Collections.addAll(statement, assignmentStatement.toArray(dummy));
        Collections.addAll(parameterDeclaration, IDENTIFIER);
        Collections.addAll(localVarDeclaration, IDENTIFIER);
        Collections.addAll(functionDeclaration, FUN);
        Collections.addAll(globalVarDeclaration, VAR);
        Collections.addAll(constDeclaration, CONST);
        Collections.addAll(recordDeclaration, RECORD);
        Collections.addAll(recordDeclarationLine, IDENTIFIER);
        Collections.addAll(declaration, functionDeclaration.toArray(dummy));
        Collections.addAll(declaration, globalVarDeclaration.toArray(dummy));
        Collections.addAll(declaration, constDeclaration.toArray(dummy));
        Collections.addAll(declaration, recordDeclaration.toArray(dummy));
        Collections.addAll(mainDeclaration, MAIN);
        Collections.addAll(monckySourceFile, declaration.toArray(dummy));
        Collections.addAll(monckySourceFile, mainDeclaration.toArray(dummy));

        this.firstMap.put("primitiveType", primitiveType);
        this.firstMap.put("arrayType", arrayType);
        this.firstMap.put("recordType", recordType);
        this.firstMap.put("pointerType", pointerType);
        this.firstMap.put("type", type);
        this.firstMap.put("unaryOperation", unaryOperation);
        this.firstMap.put("binaryOperation", binaryOperation);
        this.firstMap.put("expression2", expression2);
        this.firstMap.put("arrayValues", arrayValues);
        this.firstMap.put("arrayLiteral", arrayLiteral);
        this.firstMap.put("literal", literal);
        this.firstMap.put("binaryExpression", binaryExpression);
        this.firstMap.put("unaryExpression", unaryExpression);
        this.firstMap.put("ternaryExpression", ternaryExpression);
        this.firstMap.put("expression", expression);
        this.firstMap.put("parameterList", parametersList);
        this.firstMap.put("functionCall", functionCall);
        this.firstMap.put("whileStatement", whileStatement);
        this.firstMap.put("ifStatement", ifStatement);
        this.firstMap.put("returnStatement", returnStatement);
        this.firstMap.put("assignmentStatement", assignmentStatement);
        this.firstMap.put("compoundStatement", compoundStatement);
        this.firstMap.put("statement", statement);
        this.firstMap.put("parameterDeclaration", parameterDeclaration);
        this.firstMap.put("localVarDeclaration", localVarDeclaration);
        this.firstMap.put("functionDeclaration", functionDeclaration);
        this.firstMap.put("globalVarDeclaration", globalVarDeclaration);
        this.firstMap.put("constDeclaration", constDeclaration);
        this.firstMap.put("recordDeclaration", recordDeclaration);
        this.firstMap.put("recordDeclarationLine", recordDeclarationLine);
        this.firstMap.put("declaration", declaration);
        this.firstMap.put("mainDeclaration", mainDeclaration);
        this.firstMap.put("monckySourceFile", monckySourceFile);
    }

    public boolean check(Token token, String declaration) {
        return this.firstMap.get(declaration).contains(token);
    }
}
