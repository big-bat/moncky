package moncky3.monckycompiler.parsing;

import moncky3.monckycompiler.InternalCompilerError;
import moncky3.monckycompiler.types.Type;

/**
 * This class represents a destination in memory.
 * It is a kind of pointer used by the compiler.
 * The address of the destination is given by a label or an offset relative to the base pointer
 * A destination can be saved in a register (using the CodeGenerator) or not (register < 0)
 * When the destination is saved to a register, the value of address is set to null
 */
public class Destination extends DestinationValue {
    private final Object address; // labelname for global vars and integer relative to bp for parameters and local variables
    private final boolean isLocal;

    public Destination(Type type, boolean isLocal, int register) {
        super(register, type);
        this.address = null;
        this.isLocal = isLocal;
    }

    public Destination(Type type, int address, boolean isLocal) {
        super(-1, type);
        this.address = address;
        this.isLocal = isLocal;
    }

    public Destination(Type type, Object address, boolean isLocal, int register) {
        super(register, type);
        this.address = address;
        this.isLocal = isLocal;
    }

    public boolean isLocal() {
        return isLocal;
    }

    @Override
    public String toString() {
        if (inRegister()) {
            return "Destination: " + type + " @(r" + registerLSB + ")";
        } else if (isLocal) {
            return "Destination: " + type + " @bp+" + address;
        } else {
            return "Destination: " + type + " @" + address;
        }
    }

    public int getAddressAsInt() {
        if (!(address instanceof Integer)) {
            throw new InternalCompilerError("address should be an integer");
        }
        return (Integer) address;
    }

    public String getAddressAsString() {
        return (String) address;
    }

    public Object getAddress() {
        return address;
    }
}
