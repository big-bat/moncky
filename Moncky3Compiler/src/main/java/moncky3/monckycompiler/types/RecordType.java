package moncky3.monckycompiler.types;

import java.util.HashMap;
import java.util.Map;

public class RecordType extends Type {
    private final String name;
    private final Map<String, RecordField> fields;
    private int nextOffset;

    public RecordType(String name) {
        this.name = name;
        this.fields = new HashMap<>();
        this.nextOffset = 0;
    }

    public String getName() {
        return name;
    }

    public void addField(String name, Type type) {
        RecordField field = new RecordField(name, type, nextOffset);
        nextOffset += type.sizeof();
        this.fields.put(name, field);
    }

    @Override
    public int sizeof() {
        return nextOffset;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("record " + name + "{ ");
        for (RecordField field : fields.values()) {
            builder.append(field);
            builder.append(", ");
        }
        builder.delete(builder.length() - 2, builder.length());
        builder.append(" }");
        return builder.toString();
    }

    public RecordField getField(String name) {
        return fields.get(name);
    }
}
