package moncky3.monckycompiler.types;

public class PrimitiveType extends Type {
    public static final PrimitiveType INT8 = new PrimitiveType(1, "int8", -128, 127);
    public static final PrimitiveType UINT8 = new PrimitiveType(1, "uint8", 0, 255);
    public static final PrimitiveType INT16 = new PrimitiveType(1, "int16", -32768, 32767);
    public static final PrimitiveType UINT16 = new PrimitiveType(1, "uint16", 0, 65535);
    public static final PrimitiveType INT32 = new PrimitiveType(2, "int32", Integer.MIN_VALUE, Integer.MAX_VALUE);
    public static final PrimitiveType UINT32 = new PrimitiveType(2, "uint32", 0, ((long) Integer.MAX_VALUE) * 2 + 1);
    //public static final PrimitiveType FLOAT32 = new PrimitiveType(2, "float32", 0, 1);

    private final int size; // in 16 bit words
    private final String name;
    private final long minValue; // for integer types only
    private final long maxValue; // also used for the value "true"

    private PrimitiveType(int size, String name, long minValue, long maxValue) {
        this.size = size;
        this.name = name;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public int sizeof() {
        return size;
    }

    @Override
    public String toString() {
        return name;
    }

    public long getMinValue() {
        return minValue;
    }

    public long getMaxValue() {
        return maxValue;
    }
}
