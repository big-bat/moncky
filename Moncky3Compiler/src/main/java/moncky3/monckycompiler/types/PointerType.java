package moncky3.monckycompiler.types;

public class PointerType extends Type {
    private final Type type;

    public PointerType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    @Override
    public int sizeof() {
        return 1;
    }

    @Override
    public String toString() {
        return "pointer to " + type.toString();
    }
}
