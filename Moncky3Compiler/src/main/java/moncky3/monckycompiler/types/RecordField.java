package moncky3.monckycompiler.types;

public class RecordField {
    private final String name;
    private final Type type;
    private final int offset;

    public RecordField(String name, Type type, int offset) {
        this.name = name;
        this.type = type;
        this.offset = offset;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public int getOffset() {
        return offset;
    }

    public String toString() {
        return "(" + offset + ")" + name;// + ": " + type;
    }
}
