package moncky3.monckycompiler.types;

import moncky3.monckycompiler.parsing.Value;

import java.util.ArrayList;
import java.util.List;

public class ArrayType extends Type {
    private final Type type;
    private final int length;  // length = 0 for dynamic allocation

    public ArrayType(Type type, int length) {
        this.type = type;
        this.length = length;
    }

    public Type getType() {
        return type;
    }

    public long getLength() {
        return length;
    }

    @Override
    public int sizeof() {
        return type.sizeof() * length;
    }

    @Override
    public String toString() {
        if (length != 0) {
            return "array[" + length + "] of " + type.toString();
        } else {
            return "array of " + type.toString();
        }
    }
}
